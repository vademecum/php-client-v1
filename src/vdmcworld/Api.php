<?php

namespace vdmcworld;


use Monolog\Handler\NullHandler;
use Monolog\Logger;
use vdmcworld\api\HttpResponse;
use vdmcworld\api\model\AuthToken;
use vdmcworld\api\Request;
use vdmcworld\api\request\AuthRequest;
use vdmcworld\api\request\BaseRequest;
use vdmcworld\api\response\HtmlResponse;
use vdmcworld\api\response\XmlResponse;

class Api
{
    const FORMAT_JSON = "json";
    const FORMAT_HTML = "html";
    const FORMAT_XML = "xml";

    /**
     * Token that is received from the API to make calls
     * @var AuthToken
     */
    private $token = null;
    /**
     * Username that is used to get the token
     * @var string
     */
    public $username = null;

    /**
     * Password that is used to get the token
     * @var string
     */
    public $password = null;

    /**
     * Application Id that can be set to be sent on the headers
     * @var string
     */
    private $applicationId = null;

    /**
     * Language of response
     * @var string
     */
    public $locale = null;

    /**
     * Channel of request
     * @var string
     */
    public $channel = null;

    /**
     * Sets either to use the test or production environtment
     * @var bool
     */
    private $isSandbox = false;

    /**
     * Default return type. You can change this for each request explicitely as well
     * If you do not sey any value to the request, this value will be used.
     * @var string
     */
    private $defaultReturnType = self::FORMAT_JSON;

    /**
     * The key we use to set the return type parameter
     * @var string
     */
    private $_returnTypeKey = "_format";

    /**
     * Production URL where the requests will be sent
     * @var string
     */
    private $productionBaseUrl = "https://vapi.vademecumonline.com.tr/v1";

    /**
     * Sandbox URL where the requests will be sent if $isSandbox is set to true
     * @var string
     */
    private $sandboxBaseUrl = "https://vapi.vdmc.co/v1";

    /**
     * Custom URL allowing you to make request to special deployments
     * @var string
     */
    public $customUrl = null;

    /**
     * A logger that can be used to log things happening in the system.
     * You can attach your own Monolog logger here as well...
     * @var Logger
     */
    private $logger;

    /**
     * A reference to the last http response we received from the API.
     * @var HttpResponse
     */
    private $lastResponse;

    /**
     * Api constructor. You can create it by setting a username and password
     *   or you can leave them blank and set the token manually by using setToken
     *
     * @param string $username
     * @param string $password
     * @param string $applicationId
     * @param string $locale
     * @param string $channel
     */
    public function __construct($username = null, $password = null, $applicationId = null, $locale = 'tr', $channel = "web")
    {
        $this->username = $username;
        $this->password = $password;
        $this->applicationId = $applicationId;
        $this->locale = $locale;
        $this->channel = $channel;

        $this->logger = new Logger("VdmcWorld_Api_Logger", [new NullHandler()]);
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param Logger $logger
     * @return Api
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return String
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param String $locale
     * @return Api
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return String
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param String $channel
     * @return Api
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
        return $this;
    }

    /**
     * @return Api
     */
    public function setSandbox()
    {
        $this->isSandbox = true;
        $this->logger->info("Vdmcworld API will use sandbox environment. Log level will be set as debug");
        $this->customUrl = null;
        return $this;
    }

    /**
     * @return Api
     */
    public function setProduction()
    {
        $this->isSandbox = false;
        $this->logger->info("Vdmcworld API will use production environment");
        $this->customUrl = null;
        return $this;
    }

    /**
     * @param string $url
     * @return Api
     */
    public function setCustomUrl($url)
    {
        $this->customUrl = $url;
        $this->logger->info("Vdmcworld API will use custom environment: " . $url);
        return $this;
    }

    /**
     * @param AuthToken $token
     * @return Api
     */
    public function setToken(AuthToken $token)
    {
        $this->token = $token;
        $this->logger->debug("Auth Token is changed");
        return $this;
    }

    /**
     * @param string $defaultReturnType
     * @return $this
     * @throws \Exception
     */
    public function setDefaultReturnType($defaultReturnType)
    {
        if (in_array($defaultReturnType, self::getAvailableReturnTypes())) {
            $this->logger->debug("Return type is changed", [
                "oldReturnType" => $this->defaultReturnType,
                "newReturnType" => $defaultReturnType,
            ]);
            $this->defaultReturnType = $defaultReturnType;
            return $this;
        }
        throw new \Exception("Invalid Return Type provided");
    }

    /**
     * @return string
     */
    public function getDefaultReturnType()
    {
        return $this->defaultReturnType;
    }

    /**
     * @return array
     */
    public static function getAvailableReturnTypes()
    {
        return [self::FORMAT_HTML, self::FORMAT_JSON, self::FORMAT_XML];
    }

    /**
     * @return AuthToken
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getProductionBaseUrl()
    {
        return $this->productionBaseUrl;
    }

    /**
     * @return string
     */
    public function getSandboxBaseUrl()
    {
        return $this->sandboxBaseUrl;
    }

    /**
     * @return null|string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return null|string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getApplicationId()
    {
        return $this->applicationId;
    }

    /**
     * @param string $applicationId
     * @return Api
     */
    public function setApplicationId($applicationId)
    {
        $this->applicationId = $applicationId;
        return $this;
    }

    /**
     * @return HttpResponse
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * @return AuthToken $token
     * @throws \Exception
     */
    public function auth()
    {
        // Reset token as auth called...
        $this->token = null;
        $request = new AuthRequest([
            "username" => $this->username,
            "password" => $this->password
        ]);
        $this->token = new AuthToken("{$this->username}:{$this->password}", false);
        $this->logger->info("Authenticating user with credentials", [
            "username" => $this->username,
        ]);
        $response = $this->_send($request);
        if ($response->isSuccessful()) {
            $data = $response->getResponse();
            $this->token = $data->token;
        } else {
            throw new \Exception("Auth was not successful. Please check your credentials");
        }
        $this->logger->info("User is authenticated");
        return $this->token;
    }

    /**
     * @param Request $request
     * @return api\HttpResponse
     * @throws \Exception
     */
    public function send(Request $request)
    {
        if (null === $this->token) {
            throw new \Exception(
                "Token should be set to make requests. Either call auth() before or set the token manually"
            );
        }
        if ($this->token->isExpired()) {
            throw new \Exception("Token is expired please get a new token before making API calls");
        }
        $this->lastResponse = $this->_send($request);
        return $this->lastResponse;
    }

    /**
     * @param Request $request
     * @return HttpResponse
     * @throws \Exception
     */
    protected function _send(Request $request)
    {
        $url = $this->productionBaseUrl;
        if ($this->isSandbox) {
            $url = $this->sandboxBaseUrl;
        }
        if ($this->customUrl !== null) {
            $url = $this->customUrl;
        }

        $returnType = $request->getReturnType();
        if (null === $returnType) {
            $returnType = $this->defaultReturnType;
        }

        $this->logger->debug("Validating request and it's parameters");
        $request->isValid();

        $this->logger->info("Sending request to API");

        $headers = $request->getAdditionalHeaders();

        if (!empty($this->applicationId)) {
            $headers["ApplicationId"] = $this->applicationId;
        }

        if (!empty($this->locale)) {
            $headers["X-VDMC-LOCALE"] = $this->getLocale();
        }

        if (!empty($this->channel)) {
            $headers["X-VDMC-CHANNEL"] = $this->getChannel();
        }

        list($response, $responseHeaders) = $this->_doSend(
            $url . $request->getEndpoint(),
            $request->toParams(),
            $headers,
            $request->getMethod(),
            $returnType
        );

        $this->logger->info("API response is received");

        $responseObj = null;
        $success = false;
        $error = null;
        if (self::FORMAT_JSON === $returnType) {
            $jsonResponse = json_decode($response);
            $responseObj = null;
            if ($jsonResponse->success && $jsonResponse->data) {
                $responseObj = $request->parseResponseObject($jsonResponse);
            } else if (isset($jsonResponse->error) && $jsonResponse->error->code == 9001) {
                throw new \Exception($jsonResponse->error->message, $jsonResponse->error->code);
            }
            $success = $jsonResponse->success;
            $error = isset($jsonResponse->error) ? $jsonResponse->error : null;
        } else if (self::FORMAT_HTML === $returnType) {
            $success = true;
            $responseObj = new HtmlResponse($response);
        } else if (self::FORMAT_XML === $returnType) {
            $success = true;
            $responseObj = new XmlResponse($response);
        }

        return new HttpResponse([
            "success" => $success,
            "error" => $error,
        ], $responseObj, $responseHeaders);
    }

    /**
     * @param string $url
     * @param array $requestArray
     * @param array $additionalHeaders
     * @return array
     * @throws \Exception
     */
    protected function _doSend($url, $requestArray, $additionalHeaders, $method, $format)
    {
        $curl = curl_init();
        $queryString = parse_url($url, PHP_URL_QUERY);
        if (null !== $queryString) {
            $url .= "&" . $this->_returnTypeKey . "=" . $format;
        } else {
            $url .= "?" . $this->_returnTypeKey . "=" . $format;
        }
        $curlOptions = [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_HEADER => 0,
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_USERPWD => $this->token->getToken(),
        ];

        if (BaseRequest::HTTP_METHOD_POST === $method) {
            $curlOptions[CURLOPT_POST] = 1;
            $curlOptions[CURLOPT_POSTFIELDS] = http_build_query($requestArray);
        }

        if (!empty($additionalHeaders)) {
            $headers = [];
            foreach ($additionalHeaders as $k => $v) {
                $headers[] = sprintf("%s: %s", $k, $v);
            }
            $curlOptions[CURLOPT_HTTPHEADER] = $headers;
        }

        $start_time = microtime(true);
        $this->logger->debug("CURL request is prepared", [
            "url" => $url,
            "requestParams" => $requestArray,
            "method" => $method,
            "format" => $format
        ]);

        curl_setopt_array($curl, $curlOptions);

        $headers = [];
        curl_setopt($curl, CURLOPT_HEADERFUNCTION,
            function ($curl, $header) use (&$headers) {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) // ignore invalid headers
                    return $len;

                $name = strtolower(trim($header[0]));
                if (!array_key_exists($name, $headers))
                    $headers[$name] = [trim($header[1])];
                else
                    $headers[$name][] = trim($header[1]);

                return $len;
            }
        );

        $result = curl_exec($curl);

        $end_time = microtime(true);

        if ($result === false) {
            $this->logger->debug("CURL request encountered an error", [
                "error" => curl_error($curl),
                "errno" => curl_errno($curl),
                "duration" => $end_time - $start_time,
            ]);
            throw new \Exception("Invalid HTTP response is received");
        }

        $this->logger->debug("CURL response is received successfully", [
            "rawResponse" => $result,
            "duration" => $end_time - $start_time,
        ]);

        curl_close($curl);
        return [$result, $headers];
    }
}
