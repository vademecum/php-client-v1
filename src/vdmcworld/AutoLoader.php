<?php

// TODO: Make sure this works on Linux as well because of case sensitive file system

require __DIR__ . '/../../vendor/autoload.php';

set_include_path(__DIR__ . "/.." . PATH_SEPARATOR . get_include_path());
spl_autoload_register();
