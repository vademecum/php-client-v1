<?php

namespace vdmcworld\api;

interface Model
{
    /**
     * @param $json
     * @return Model
     */
    public static function fromJson($json);
}