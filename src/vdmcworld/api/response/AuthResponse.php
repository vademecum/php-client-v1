<?php

namespace vdmcworld\api\response;


use vdmcworld\api\model\AuthToken;
use vdmcworld\api\response\BaseResponse;

class AuthResponse extends BaseResponse
{
    /**
     * @var AuthToken
     */
    public $token;
}