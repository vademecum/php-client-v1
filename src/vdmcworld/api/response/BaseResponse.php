<?php

namespace vdmcworld\api\response;

use vdmcworld\api\Model;

class BaseResponse
{
    /**
     * @var mixed
     */
    protected $data;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $data
     * @return BaseResponse
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
}