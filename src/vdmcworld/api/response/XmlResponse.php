<?php

namespace vdmcworld\api\response;


use vdmcworld\api\Response;

class XmlResponse implements Response
{
    /**
     * @var string
     */
    private $resultXml;

    /**
     * @return string
     */
    public function getResultXml()
    {
        return $this->resultXml;
    }

    /**
     * HtmlResponse constructor.
     * @param string $xml
     */
    public function __construct($xml)
    {
        $this->resultXml = $xml;
    }
}