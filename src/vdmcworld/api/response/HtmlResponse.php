<?php

namespace vdmcworld\api\response;


use vdmcworld\api\Model;
use vdmcworld\api\Response;

class HtmlResponse implements Response
{
    /**
     * @var string
     */
    private $resultHtml;

    /**
     * @return string
     */
    public function getResultHtml()
    {
        return $this->resultHtml;
    }

    /**
     * HtmlResponse constructor.
     * @param string $html
     */
    public function __construct($html)
    {
        $this->resultHtml = $html;
    }
}