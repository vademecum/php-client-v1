<?php


namespace vdmcworld\api\response;


class BasePaginatableResponse extends BaseResponse
{
    /**
     * @var int
     */
    protected $totalItems;
    /**
     * @var int
     */
    protected $totalPages;

    /**
     * @return int
     */
    public function getTotalItems()
    {
        return $this->totalItems;
    }

    /**
     * @return int
     */
    public function getTotalPages()
    {
        return $this->totalPages;
    }

    /**
     * @param $json
     */
    public function parseJsonMeta($json)
    {
        if (isset($json->_meta)) {
            $this->totalItems = $json->_meta->totalCount;
            $this->totalPages = $json->_meta->pageCount;
        } else {
            $this->totalItems = count($json->data);
            $this->totalPages = 1;
        }
    }
}