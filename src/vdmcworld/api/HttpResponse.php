<?php

namespace vdmcworld\api;


class HttpResponse
{
    /**
     * @var bool
     */
    private $result;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var array
     */
    private $responseHeaders;

    /**
     * HttpResponse constructor.
     * @param array $result
     * @param Response $response
     */
    public function __construct($result, $response, $responseHeaders)
    {
        $this->result = $result;
        $this->response = $response;
        $this->responseHeaders = $responseHeaders;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->result["success"] === true;
    }

    /**
     * @return bool
     */
    public function hasData()
    {
        return $this->isSuccessful() && null !== $this->response;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return array
     */
    public function getResponseHeaders()
    {
        return $this->responseHeaders;
    }

    /**
     * @return array
     */
    public function getError()
    {
        return (array)$this->result["error"];
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        $e = (array)$this->result["error"];
        return $e["message"];
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
        $e = (array)$this->result["error"];
        return (int)$e["code"];
    }

    /**
     * @return string
     */
    public function getErrorName()
    {
        $e = (array)$this->result["error"];
        return $e["name"];
    }

    /**
     * @return int
     */
    public function getErrorStatus()
    {
        $e = (array)$this->result["error"];
        return (int)$e["status"];
    }
}
