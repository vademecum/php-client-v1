<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductOverdoseInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductOverdoseInfoRequest
 * @package vdmcworld\api\request
 * @method ProductOverdoseInfo fetchData(Api $api)
 */
class ProductOverdoseInfoRequest extends ProductBasedBaseRequest
{

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-overdose-info";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductOverdoseInfo::fromJson($json->data);
    }
}