<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\MainSearchResult;
use vdmcworld\api\response\BaseResponse;

/**
 * Class MainSearchRequest
 * @package vdmcworld\api\request
 * @method MainSearchResult fetchData(Api $api)
 */
class MainSearchRequest extends BaseRequest
{
    /**
     * @var string
     */
    protected $term = null;

    /**
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param string $term
     * @return MainSearchRequest
     */
    public function setTerm($term)
    {
        $this->term = $term;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/main-search?term=" . urlencode($this->term);
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null === $this->term) {
            throw new \Exception("Term should be set to make search requests");
        }
        return true;
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return MainSearchResult
     */
    protected function _parsResponseData($response, $json)
    {
        return MainSearchResult::fromJson($json->data);
    }
}