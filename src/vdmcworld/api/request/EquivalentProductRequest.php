<?php

namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\EquivalentProduct;
use vdmcworld\api\response\BaseResponse;

/**
 * Class EquivalentProductRequest
 * @package vdmcworld\api\request
 * @method EquivalentProduct fetchData(Api $api)
 */
class EquivalentProductRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/equivalent-products";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return EquivalentProduct
     */
    protected function _parsResponseData($response, $json)
    {
        return EquivalentProduct::fromJson($json->data);
    }
}