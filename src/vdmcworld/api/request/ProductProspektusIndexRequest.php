<?php


namespace vdmcworld\api\request;


use vdmcworld\api\model\ProductKubIndex;
use vdmcworld\api\model\ProductProspektusIndex;

class ProductProspektusIndexRequest extends ProductBasedBaseRequest
{

    protected function _parsResponseData($response, $json)
    {
        return ProductProspektusIndex::fromJson($json->data);
    }

    protected function baseEndpoint()
    {
        return "/product-prospektus-index";
    }
}