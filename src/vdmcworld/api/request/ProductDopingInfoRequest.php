<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductDopingData;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductDopingInfoRequest
 * @package vdmcworld\api\request
 * @method ProductDopingInfoRequest fetchData(Api $api)
 */
class ProductDopingInfoRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-doping-info";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductDopingData::fromJson($json->data);
    }
}