<?php

namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\ProductReimbursement;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductReimbursementRequest
 * @package vdmcworld\api\request
 * @method ProductReimbursement fetchData(Api $api)
 */
class ProductReimbursementRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-reimbursement";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductReimbursement::fromJson($json->data);
    }
}