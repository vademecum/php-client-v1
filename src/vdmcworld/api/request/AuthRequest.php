<?php

namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\AuthToken;
use vdmcworld\api\Request;
use vdmcworld\api\response\AuthResponse;
use vdmcworld\api\response\BaseResponse;

class AuthRequest extends BaseRequest
{
    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @param string $username
     * @return AuthRequest
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @param string $password
     * @return AuthRequest
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/auth-token";
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        return true;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

    /**
     * @return AuthResponse
     */
    public function getResponseClass()
    {
        return new AuthResponse();
    }

    /**
     * @return string
     */
    public function getReturnType()
    {
        return Api::FORMAT_JSON;
    }

    /**
     * @return BaseResponse
     * @var array $json
     */
    public function parseResponseObject($json)
    {
        $authResponse = new AuthResponse();
        $authResponse->token = AuthToken::fromJson($json->data);
        return $authResponse;
    }

    protected function _parsResponseData($response, $json)
    {
        // DO nothing as I have already overwritten parseResponseObject
    }
}
