<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\SubstanceDopingData;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductDopingInfoRequest
 * @package vdmcworld\api\request
 * @method SubstanceDopingInfoRequest fetchData(Api $api)
 */
class SubstanceDopingInfoRequest extends SubstanceBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/substance-doping-info";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceDopingData::fromJson($json->data);
    }
}