<?php

namespace vdmcworld\api\request;

use vdmcworld\api\model\RapRec;
use vdmcworld\api\request\parameter\IcdCode;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * Class RapRecRequest
 * @method RapRec[] fetchData(\vdmcworld\Api $api)
 */
class RapRecRequest extends BaseRequest
{
    /**
     * @var Product[]
     */
    private $products;

    /**
     * @var IcdCode[]
     */
    private $icdCodes;

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return IcdCode[]
     */
    public function getIcdCodes()
    {
        return $this->icdCodes;
    }

    /**
     * @param IcdCode[] $icdCodes
     * @return RapRecRequest
     */
    public function setIcdCodes($icdCodes)
    {
        $this->icdCodes = $icdCodes;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return \vdmcworld\api\model\RapRec[]
     */
    protected function _parsResponseData($response, $json)
    {
        $data = [];
        foreach ($json->data as $p) {
            $data[] = RapRec::fromJson($p);
        }
        return $data;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/rapico-rap-rec";
    }

    /**
     * @return array
     */
    public function toParams()
    {
        $ret = [];
        $products = [];
        foreach ($this->products as $p) {
            $products[] = $p->toParams();
        }

        $icdCodes = [];
        foreach ($this->icdCodes as $i) {
            $icdCodes[] = $i->toParams();
        }

        if ($products) {
            $ret["products"] = $this->jsonEncode($products);
        }
        if ($icdCodes) {
            $ret["icdcodes"] = $this->jsonEncode($icdCodes);
        }

        return $ret;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        // TODO: Implement isValid() method.
    }
}