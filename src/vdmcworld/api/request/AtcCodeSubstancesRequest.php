<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\AtcCodeSubstances;
use vdmcworld\api\request\parameter\Atc;
use vdmcworld\api\response\BaseResponse;

/**
 * Class AtcCodeSubstancesRequest
 * @package vdmcworld\api\request
 * @method AtcCodeSubstances fetchData(Api $api)
 */
class AtcCodeSubstancesRequest extends BaseRequest
{
    /**
     * @var Atc
     */
    protected $atc;

    /**
     * @return Atc
     */
    public function getAtc()
    {
        return $this->atc;
    }

    /**
     * @param Atc $atc
     */
    public function setAtc($atc)
    {
        $this->atc = $atc;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return AtcCodeSubstances::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/atc-code-substances/" . $this->atc->getCode();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->atc && null !== $this->atc->getCode()) {
            return true;
        }
        throw new \Exception("Atc and it's code cannot be null");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}