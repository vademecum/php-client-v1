<?php

namespace vdmcworld\api\request;


use vdmcworld\api\model\LandingCampaign;
use vdmcworld\api\response\BaseResponse;

/**
 * Class LandingBannersRequest
 * @package vdmcworld\api\request
 */
class LandingCampaignsRequest extends BaseRequest
{
    public function getEndpoint()
    {
        return "/mobile-landing-banners";
    }

    public function toParams()
    {
        return [];
    }

    public function isValid()
    {

    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        $data = [];
        foreach ($json->data as $item) {
            $data[] = LandingCampaign::fromJson($item);
        }

        return $data;
    }
}