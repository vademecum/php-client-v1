<?php

namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\request\parameter\PrescriptionCalculationProduct;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * Class PrescriptionCalculationRequest
 * @package vdmcworld\api\request
 * @method PrescriptionCalculationRequest fetchData(Api $api)
 */
class PrescriptionCalculationRequest extends BaseRequest
{
    /**
     * @var integer
     */
    protected $workStatus;

    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     * @return self
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @param boolean $workStatus
     * @return self
     */
    public function setWorkStatus($workStatus)
    {
        $this->workStatus = $workStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/prescription-calculation";
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (empty($this->products)) {
            throw new \Exception("Products cannot be empty");
        }

        if (null === $this->workStatus) {
            throw new \Exception("WorkStatus cannot be empty");
        }

        return true;
    }


    /**
     * @return array
     */
    public function toParams()
    {
        $ret = [];

        $products = [];
        foreach ($this->products as $p) {
            $products[] = $p->toParams();
        }
        if ($products) {
            $ret['products'] = $this->jsonEncode($products);
        }
        $ret['prescriptionCalculation'] = $this->jsonEncode(["workStatus" => $this->workStatus]);
        return $ret;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     */
    protected function _parsResponseData($response, $json)
    {
        return $json->data;
    }
}
