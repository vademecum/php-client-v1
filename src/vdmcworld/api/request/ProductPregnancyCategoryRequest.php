<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductPregnancyCategory;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductPregnancyCategoryRequest
 * @package vdmcworld\api\request
 * @method ProductPregnancyCategory fetchData(Api $api)
 */
class ProductPregnancyCategoryRequest extends ProductBasedBaseRequest
{
    protected function baseEndpoint()
    {
        return "/product-pregnancy-categories";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductPregnancyCategory::fromJson($json->data);
    }
}