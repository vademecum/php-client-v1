<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\SgkSutDetail;

/**
 * Class SgkSutHistoryRequest
 * @package vdmcworld\api\request
 * @method SgkSutDetail[] fetchData(Api $api)
 */
class SgkSutHistoryRequest extends SgkSutDetailRequest
{
    protected function _parsResponseData($response, $json)
    {
        $res = [];
        foreach ($json->data as $datum) {
            $res[] = SgkSutDetail::fromJson($datum);
        }
        return $res;
    }

    public function getEndpoint()
    {
        return "/sgk-sut-history/" . $this->getSutId()->getId();
    }
}