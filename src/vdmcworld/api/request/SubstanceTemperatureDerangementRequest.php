<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\SubstancePhDerangement;
use vdmcworld\api\model\SubstanceSolventDerangement;
use vdmcworld\api\model\SubstanceTemperatureDerangement;
use vdmcworld\api\response\BaseResponse;

/**
 * @package vdmcworld\api\request
 * @method SubstanceTemperatureDerangement fetchData(Api $api)
 */
class SubstanceTemperatureDerangementRequest extends SubstanceBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SubstanceTemperatureDerangement
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceTemperatureDerangement::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/substance-temperature-derangement";
    }
}
