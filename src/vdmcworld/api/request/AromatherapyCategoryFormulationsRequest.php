<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\AromatherapyFormulations;
use vdmcworld\api\response\BaseResponse;

/**
 * Class AromatherapyCategoryFormulations
 * @package vdmcworld\api\request
 * @method AromatherapyFormulations fetchData(Api $api)
 */
class AromatherapyCategoryFormulationsRequest extends AromatherapyCategoryBasedBaseRequest
{
    protected function _parsResponseData($response, $json)
    {
        $result = [];
        foreach ($json->data as $item) {
            $result[] = AromatherapyFormulations::fromJson($item);
        }

        return $result;
    }

    protected function baseEndpoint()
    {
        return "/aromatherapy-category-formulations";
    }
}