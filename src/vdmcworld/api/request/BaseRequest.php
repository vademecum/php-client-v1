<?php

namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\Request;
use vdmcworld\api\response\BaseResponse;

abstract class BaseRequest implements Request
{
    const HTTP_METHOD_POST = "post";
    const HTTP_METHOD_GET = "get";

    /**
     * @var string
     */
    private $returnType = null;

    public function __construct($params = null)
    {
        if (null !== $params) {
            // Provide an easier and faster way to define params
            foreach ($params as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    /**
     * @param string $retType
     * @return BaseRequest
     * @throws \Exception
     */
    public function setReturnType($retType)
    {
        if (in_array($retType, Api::getAvailableReturnTypes())) {
            $this->returnType = $retType;
            return $this;
        }
        throw new \Exception("Invalid Return Type provided");
    }

    /**
     * @param array|object $value
     * @return string
     */
    protected function jsonEncode($value)
    {
        return json_encode($value);
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::HTTP_METHOD_POST;
    }

    /**
     * @return string
     */
    public function getReturnType()
    {
        return $this->returnType;
    }

    /**
     * @param Api $api
     * @return mixed
     * @throws \Exception
     */
    public function fetchData(Api $api)
    {
        $response = $api->send($this)->getResponse();
        if ($response) {
            return $response->getData();
        }
        return null;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    abstract protected function _parsResponseData($response, $json);

    /**
     * @param array $json
     * @return BaseResponse
     */
    public function parseResponseObject($json)
    {
        $instance = new BaseResponse();
        $instance->setData($this->_parsResponseData($instance, $json));
        return $instance;
    }

    /**
     * @return array
     */
    public function getAdditionalHeaders()
    {
        return [];
    }
}