<?php


namespace vdmcworld\api\request;


use vdmcworld\api\model\ProductKtUsageIndex;

class ProductKtUsageIndexRequest extends ProductBasedBaseRequest
{

    protected function _parsResponseData($response, $json)
    {
        return ProductKtUsageIndex::fromJson($json->data);
    }

    protected function baseEndpoint()
    {
        return "/product-kt-usage-index";
    }
}