<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductSpecialInformation;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductSpecialInformationRequest
 * @package vdmcworld\api\request
 * @method ProductSpecialInformation fetchData(Api $api)
 */
class ProductSpecialInformationRequest extends ProductBasedBaseRequest
{
    protected function baseEndpoint()
    {
        return "/product-special-information-card";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductSpecialInformation::fromJson($json->data);
    }
}