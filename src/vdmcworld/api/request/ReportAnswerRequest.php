<?php

namespace vdmcworld\api\request;

use vdmcworld\api\model\ReportAnswer;
use vdmcworld\api\response\BaseResponse;

class ReportAnswerRequest extends BaseRequest
{
    /**
     * @var integer
     */
    private $answerId;


    /**
     * @var string
     */
    private $sessionId;

    /**
     * @return int
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * @param int $answerId
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;
    }

    /**
     * @return string
     */
    public function getsessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setsessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }


    /**
     * @param BaseResponse $response
     * @param array $json
     * @return \vdmcworld\api\model\ReportAnswer[]
     */
    protected function _parsResponseData($response, $json)
    {
        return ReportAnswer::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/report-answer";
    }

    /**
     * @return array
     */
    public function toParams()
    {
        $params = [
            "answer" => $this->answerId,
            "session" => $this->sessionId,
        ];
        return $params;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        // TODO: Implement isValid() method.
    }
}