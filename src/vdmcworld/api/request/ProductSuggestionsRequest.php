<?php

namespace vdmcworld\api\request;


use vdmcworld\api\model\ProductSuggestionsResult;
use vdmcworld\api\request\parameter\BeginsWithQuery;
use vdmcworld\api\request\parameter\SearchQuery;
use vdmcworld\api\response\BaseResponse;

class ProductSuggestionsRequest extends BaseRequest
{
    /**
     * @var string
     */
    protected $term;

    /**
     * @var SearchQuery[]
     */
    protected $searchQueries = [];

    /**
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param string $term
     * @return
     */
    public function setTerm($term)
    {
        $this->term = $term;
        $this->searchQueries = [];
        return $this;
    }

    /**
     * @param string $q
     * @return ProductSuggestionsRequest
     */
    public function setBeginsWith($term)
    {
        $q = new BeginsWithQuery();
        $q->setTerm($term);
        $this->searchQueries = [$q];
        $this->term = null;
        return $this;
    }

    public function getBeginsWith()
    {
        return $this->searchQueries[0];
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductSuggestionsResult::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        $suffix = "";
        if (count($this->searchQueries) > 0) {
            $q = $this->searchQueries[0];
            $suffix = $q->getKey() . "=" . urlencode(implode(",", $q->getValue()));
        } else {
            $suffix = "term=" . urlencode($this->term);
        }
        return "/product-suggestions?" . $suffix;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (empty($this->term) && count($this->searchQueries) < 1) {
            throw new \Exception("Term or Beginswith should be set");
        }
        return true;
    }

    public function getMethod()
    {
        return parent::HTTP_METHOD_GET;
    }
}