<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\SubstanceProducts;
use vdmcworld\api\request\parameter\Substance;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SubstanceProductsRequest
 * @package vdmcworld\api\request
 * @method SubstanceProducts fetchData(Api $api)
 */
class SubstanceProductsRequest extends BaseRequest
{
    /**
     * @var Substance
     */
    protected $substance = null;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @param Substance $substance
     * @return SubstanceProductsRequest
     */
    public function setSubstance($substance)
    {
        $this->substance = $substance;
        return $this;
    }

    public function getEndpoint()
    {
        return "/substance-products/" . $this->substance->getId();
    }

    public function toParams()
    {
        return [];
    }

    public function isValid()
    {
        if (null !== $this->substance && null !== $this->substance->getId()) {
            return true;
        }
        throw new \Exception("Substance and it's ID should be set to make this request");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceProducts::fromJson($json->data);
    }
}