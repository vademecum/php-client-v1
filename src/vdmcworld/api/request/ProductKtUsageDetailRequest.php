<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductKtUsageDetail;
use vdmcworld\api\model\ProductKubDetail;
use vdmcworld\api\request\parameter\HeaderId;

/**
 * Class ProductKubDetailRequest
 * @package vdmcworld\api\request
 * @method ProductKtUsageDetail fetchData(Api $api)
 */
class ProductKtUsageDetailRequest extends ProductBasedBaseRequest
{
    /**
     * @var HeaderId
     */
    protected $headerId;

    /**
     * @return HeaderId
     */
    public function getHeaderId()
    {
        return $this->headerId;
    }

    /**
     * @param HeaderId $headerId
     */
    public function setHeaderId($headerId)
    {
        $this->headerId = $headerId;
    }

    protected function _parsResponseData($response, $json)
    {
        return ProductKtUsageDetail::fromJson($json->data);
    }

    protected function baseEndpoint()
    {
        return "/product-kt-usage-details";
    }

    public function isValid()
    {
        if (null !== $this->headerId && !empty($this->headerId->getId())) {
            return true;
        }
        return parent::isValid();
    }

    public function getEndpoint()
    {
        return parent::getEndpoint() . "/" . $this->headerId->getId();
    }
}