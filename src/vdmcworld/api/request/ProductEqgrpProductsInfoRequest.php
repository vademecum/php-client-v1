<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductEqgrpProductsInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductEqgrpProductsInfoRequest
 * @package vdmcworld\api\request
 * @method ProductEqgrpProductsInfo fetchData(Api $api)
 */
class ProductEqgrpProductsInfoRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-eqgrp-products-info";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductEqgrpProductsInfo::fromJson($json->data);
    }
}