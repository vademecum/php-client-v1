<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\RecallAnnouncementInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class RecallAnnouncementDetailRequest
 * @package vdmcworld\api\request
 * @method RecallAnnouncementDetailRequest fetchData(Api $api)
 */
class RecallAnnouncementDetailRequest extends BaseRequest
{
    /**
     * @var int
     */
    private $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return RecallAnnouncementInfo
     */
    protected function _parsResponseData($response, $json)
    {
        return RecallAnnouncementInfo::fromJson($json->data);
    }

    public function getEndpoint()
    {
        return "/timeline/" . TimelineRequest::TYPE_RECALL . "/" . $this->id;
    }

    public function toParams()
    {
        return [];
    }

    public function isValid()
    {
        if (null == $this->id) {
            throw new \Exception("Id should be set to make this request");
        }
        return true;
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

}