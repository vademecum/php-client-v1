<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductNutritionInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductNutritionInfoRequest
 * @package vdmcworld\api\request
 * @method ProductNutritionInfo fetchData(Api $api)
 */
class ProductNutritionInfoRequest extends ProductBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductNutritionInfo
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductNutritionInfo::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return '/product-nutrition-info';
    }
}