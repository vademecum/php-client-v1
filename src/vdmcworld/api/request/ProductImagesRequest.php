<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductImages;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductImagesRequest
 * @package vdmcworld\api\request
 * @method ProductImages fetchData(Api $api)
 */
class ProductImagesRequest extends ProductBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductImages
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductImages::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-images";
    }
}