<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\SubstanceCard;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SubstanceCardRequest
 * @package vdmcworld\api\request
 * @method SubstanceCard fetchData(Api $api)
 */
class SubstanceCardRequest extends SubstanceBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceCard::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/substance-card";
    }
}