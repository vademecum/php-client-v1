<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductDosageCaution;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductDosageCautionRequest
 * @package vdmcworld\api\request
 * @method ProductDosageCaution fetchData(Api $api)
 */
class ProductDosageCautionRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-dosage-caution-info";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductDosageCaution
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductDosageCaution::fromJson($json->data);
    }
}