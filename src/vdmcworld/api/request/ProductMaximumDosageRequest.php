<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductMaximumDosage;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductMaximumDosageRequest
 * @package vdmcworld\api\request
 * @method ProductMaximumDosage fetchData(Api $api)
 */
class ProductMaximumDosageRequest extends ProductBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductMaximumDosage
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductMaximumDosage::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return '/product-maximum-dosage';
    }
}