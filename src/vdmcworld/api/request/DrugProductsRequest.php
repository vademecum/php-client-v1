<?php


namespace vdmcworld\api\request;


use vdmcworld\api\model\Drug;
use vdmcworld\api\model\DrugProducts;
use vdmcworld\api\response\BaseResponse;

class DrugProductsRequest extends BaseRequest
{
    /**
     * @var Drug
     */
    protected $drug;

    /**
     * @return Drug
     */
    public function getDrug()
    {
        return $this->drug;
    }

    /**
     * @param Drug $drug
     */
    public function setDrug($drug)
    {
        $this->drug = $drug;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return DrugProducts
     */
    protected function _parsResponseData($response, $json)
    {
        return DrugProducts::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/drug-products/" . $this->drug->getId();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->drug && null !== $this->drug->getId()) {
            return true;
        }
        throw new \Exception("Drug and it's id cannot be null");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}