<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\SgkSutProductDetail;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SgkSutIndexRequest
 * @package vdmcworld\api\request
 * @method SgkSutProductDetail fetchData(Api $api)
 */
class SgkSutProductDetailRequest extends BaseRequest
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SgkSutProductDetail
     */
    protected function _parsResponseData($response, $json)
    {
        return SgkSutProductDetail::fromJson($json->data);
    }

    public function getEndpoint()
    {
        return "/product-sgk-sut-details/" . $this->product->getId();
    }

    public function toParams()
    {
        return [];
    }

    public function isValid()
    {
        if (null == $this->product || null == $this->product->getId()) {
            throw new \Exception("ProductId should be set to make this request");
        }
        return true;
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}