<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductDocument;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductDocumentRequest
 * @package vdmcworld\api\request
 * @method ProductDocument fetchData(Api $api)
 */
class ProductDocumentRequest extends ProductBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductDocument
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductDocument::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-documents";
    }
}