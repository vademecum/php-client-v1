<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\DerangementMixtureStability;
use vdmcworld\api\request\parameter\Mixture;
use vdmcworld\api\response\BaseResponse;

/**
 * @package vdmcworld\api\request
 * @method DerangementMixtureStability fetchData(Api $api)
 */
class MixtureStabilityRequest extends BaseRequest
{
    /**
     * @var Mixture
     */
    protected $mixture = null;

    /**
     * @return Mixture
     */
    public function getMixture()
    {
        return $this->mixture;
    }

    /**
     * @param Mixture $mixture
     * @return MixtureStabilityRequest
     */
    public function setMixture($mixture)
    {
        $this->mixture = $mixture;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return DerangementMixtureStability
     */
    protected function _parsResponseData($response, $json)
    {
        return DerangementMixtureStability::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/mixture-stability";
    }

    public function getEndpoint()
    {
        return $this->baseEndpoint() . "/" . $this->mixture->getId();
    }

    public function toParams()
    {
        return [];
    }

    public function isValid()
    {
        if (null !== $this->mixture && null !== $this->mixture->getId()) {
            return true;
        }
        throw new \Exception("Mixture and it'is ID should be set to make this request");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}
