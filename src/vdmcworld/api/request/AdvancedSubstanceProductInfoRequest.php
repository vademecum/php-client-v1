<?php


namespace vdmcworld\api\request;


use vdmcworld\api\model\SubstanceProductsInfo;
use vdmcworld\api\request\parameter\Substance;
use vdmcworld\api\response\BaseResponse;

/**
 * Class AdvancedSubstanceProductInfoRequest
 * @package vdmcworld\api\request
 */
class AdvancedSubstanceProductInfoRequest extends SubstanceBasedBaseRequest
{
    /**
     * @var Substance
     */
    protected $substance = null;

    /**
     * @var string
     */
    protected $isReimbursement = null;

    /**
     * @var string
     */
    protected $onMarket = null;

    /**
     * @var string
     */
    protected $type = null;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return string
     */
    public function getIsReimbursement()
    {
        return $this->isReimbursement;
    }

    /**
     * @return string
     */
    public function getOnMarket()
    {
        return $this->onMarket;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Substance $substance
     * @return AdvancedSubstanceProductInfoRequest
     */
    public function setSubstance($substance)
    {
        $this->substance = $substance;
        return $this;
    }

    /**
     * @param string $isReimbursement
     * @return AdvancedSubstanceProductInfoRequest
     */
    public function setIsReimbursement($isReimbursement)
    {
        $this->isReimbursement = $isReimbursement;
        return $this;
    }

    /**
     * @param string $type
     * @return AdvancedSubstanceProductInfoRequest
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $onMarket
     * @return AdvancedSubstanceProductInfoRequest
     */
    public function setOnMarket($onMarket)
    {
        $this->onMarket = $onMarket;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceProductsInfo::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->baseEndpoint() . "/" . $this->substance->getId() . '?is-reimbursement=' . $this->isReimbursement . '&on-market=' . $this->onMarket . '&type=' . $this->type;
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/advanced-substance-products-info";
    }
}