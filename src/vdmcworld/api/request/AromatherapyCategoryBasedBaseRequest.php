<?php

namespace vdmcworld\api\request;

use vdmcworld\api\request\parameter\AromatherapyCategory;

abstract class AromatherapyCategoryBasedBaseRequest extends BaseRequest
{
    /**
     * @var AromatherapyCategory
     */
    protected $category = null;

    /**
     * @return AromatherapyCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param AromatherapyCategory $category
     * @return AromatherapyCategoryBasedBaseRequest
     */
    public function setCategory(AromatherapyCategory $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    protected abstract function baseEndpoint();

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->baseEndpoint() . "/" . $this->category->getId();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->category && null !== $this->category->getId()) {
            return true;
        }
        throw new \Exception("Category and it's ID should be set to make this request");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}