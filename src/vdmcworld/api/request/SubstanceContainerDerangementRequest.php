<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\ProductContainerDerangement;
use vdmcworld\api\model\SgkSutProductDetail;
use vdmcworld\api\model\SubstanceContainerDerangement;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * @package vdmcworld\api\request
 * @method SubstanceContainerDerangement fetchData(Api $api)
 */
class SubstanceContainerDerangementRequest extends SubstanceBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SubstanceContainerDerangement
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceContainerDerangement::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/substance-container-derangement";
    }
}
