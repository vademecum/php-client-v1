<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductReimbursementPrice;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductReimbursementPriceRequest
 * @package vdmcworld\api\request
 * @method ProductReimbursementPrice fetchData(Api $api)
 */
class ProductReimbursementPriceRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-reimbursement-price";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductReimbursementPrice::fromJson($json->data);
    }
}