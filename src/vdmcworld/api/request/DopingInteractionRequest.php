<?php

namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\DopingInteraction;
use vdmcworld\api\model\Interaction;
use vdmcworld\api\request\lists\BaseListRequest;
use vdmcworld\api\response\BaseResponse;
use vdmcworld\api\response\lists\BaseListResponse;

/**
 * Class DopingInteractionRequest
 * @package vdmcworld\api\request
 * @method DopingInteraction fetchData(Api $api)
 */
class DopingInteractionRequest extends PrescriptionClinicControlRequest
{
    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (empty($this->products)) {
            throw new \Exception("Products cannot be empty");
        }
        return true;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/doping-interaction";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        $data = [];
        foreach ($json->data as $p) {
            $data[] = DopingInteraction::fromJson($p);
        }

        return $data;
    }

}