<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductReimbursementDetail;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductReimbursementDetailRequest
 * @package vdmcworld\api\request
 * @method ProductReimbursementDetail fetchData(Api $api)
 */
class ProductReimbursementDetailRequest extends ProductBasedBaseRequest
{

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-reimbursement-detail";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductReimbursementDetail::fromJson($json->data);
    }
}