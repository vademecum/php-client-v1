<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\ProductContainerDerangement;
use vdmcworld\api\model\SgkSutProductDetail;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SgkSutIndexRequest
 * @package vdmcworld\api\request
 * @method ProductContainerDerangement fetchData(Api $api)
 */
class ProductContainerDerangementRequest extends ProductBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductContainerDerangement
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductContainerDerangement::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/product-container-derangement";
    }
}
