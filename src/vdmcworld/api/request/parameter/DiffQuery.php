<?php


namespace vdmcworld\api\request\parameter;


class DiffQuery extends SearchQuery
{
    /**
     * @var int
     */
    protected $diff;

    /**
     * @return int
     */
    public function getDiff()
    {
        return $this->diff;
    }

    /**
     * @param int $diff
     */
    public function setDiff($diff)
    {
        $this->diff = $diff;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "diff";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->diff];
    }
}