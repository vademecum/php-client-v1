<?php

namespace vdmcworld\api\request\parameter;


class IcdCode implements Parameter
{
    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return IcdCode
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * IcdCode constructor.
     * @param string $name
     */
    public function __construct($name = null)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "name" => $this->name,
        ];
    }
}