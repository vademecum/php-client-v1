<?php

namespace vdmcworld\api\request\parameter;


class Mixture implements Parameter
{
    /**
     * @var int
     */
    private $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Substance constructor.
     * @param int $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "id" => $this->id
        ];
    }
}