<?php

namespace vdmcworld\api\request\parameter;


class BarcodeQuery extends SearchQuery
{
    /**
     * @var string[]
     */
    protected $barcodes;

    /**
     * @return string[]
     */
    public function getBarcodes()
    {
        return $this->barcodes;
    }

    /**
     * @param string[] $barcodes
     * @return BarcodeQuery
     */
    public function setBarcodes($barcodes)
    {
        $this->barcodes = $barcodes;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "barcodes";
    }

    /**
     * @return string[]
     */
    public function getValue()
    {
        return $this->barcodes;
    }
}