<?php


namespace vdmcworld\api\request\parameter;


class SgkPriceRefCode extends EqGrpCode
{
    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "sgkPriceRefCode" => $this->code,
        ];
    }
}