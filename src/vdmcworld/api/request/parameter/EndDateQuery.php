<?php


namespace vdmcworld\api\request\parameter;


class EndDateQuery extends DateQuery
{
    /**
     * @return string
     */
    public function getKey()
    {
        return "end-date";
    }
}