<?php

namespace vdmcworld\api\request\parameter;
class PrescriptionCalculationProduct implements Parameter
{
    /**
     * @var integer
     */
    protected $boxAmount;

    /**
     * @var boolean
     */
    protected $isExemptFromParticipationFee = null;

    /**
     * @return int
     */
    public function getBoxAmount()
    {
        return $this->boxAmount;
    }

    /**
     * @return bool
     */
    public function isExemptFromParticipationFee()
    {
        return $this->isExemptFromParticipationFee;
    }

    /**
     * @param int $boxAmount
     * @return self
     */
    public function setBoxAmount($boxAmount)
    {
        $this->boxAmount = $boxAmount;
        return $this;
    }

    /**
     * @param boolean $isExemptFromParticipationFee
     * @return self
     */
    public function setIsExemptFromParticipationFee($isExemptFromParticipationFee)
    {
        $this->isExemptFromParticipationFee = $isExemptFromParticipationFee;
        return $this;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            'boxAmount' => $this->boxAmount,
            'isExemptFromParticipationFee' => $this->isExemptFromParticipationFee,
        ];
    }
}
