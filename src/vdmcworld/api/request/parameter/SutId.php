<?php


namespace vdmcworld\api\request\parameter;


class SutId implements Parameter
{
    /**
     * @var integer
     */
    private $id;

    /**
     * SutId constructor.
     * @param integer $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function toParams()
    {
        return ["sutId" => $this->id];
    }
}