<?php

namespace vdmcworld\api\request\parameter;


class DoctorInfo implements Parameter
{
    /**
     * @var int
     */
    private $specialization;

    /**
     * @var string
     */
    private $uniqueId;

    /**
     * @return int
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * @param int $specialization
     * @return DoctorInfo
     */
    public function setSpecialization($specialization)
    {
        $this->specialization = $specialization;
        return $this;
    }

    /**
     * @return string
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * @param string $uniqueId
     * @return DoctorInfo
     */
    public function setUniqueId($uniqueId)
    {
        $this->uniqueId = $uniqueId;
        return $this;
    }

    /**
     * DoctorInfo constructor.
     * @param int $specialization
     * @param string $uniqueId
     */
    public function __construct($specialization = 1, $uniqueId = null)
    {
        $this->specialization = $specialization;
        $this->uniqueId = $uniqueId;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "specialization" => $this->specialization,
            "uniqueId" => $this->uniqueId,
        ];
    }
}
