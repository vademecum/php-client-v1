<?php


namespace vdmcworld\api\request\parameter;


class StartTimestampQuery extends TimestampQuery
{
    /**
     * @return string
     */
    public function getKey()
    {
        return "start-date";
    }
}