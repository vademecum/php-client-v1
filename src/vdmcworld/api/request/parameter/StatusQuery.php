<?php


namespace vdmcworld\api\request\parameter;


class StatusQuery extends SearchQuery
{
    /**
     * @var int
     */
    protected $status;

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "status";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->status];
    }
}