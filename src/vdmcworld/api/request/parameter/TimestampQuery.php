<?php


namespace vdmcworld\api\request\parameter;


abstract class TimestampQuery extends SearchQuery
{
    /**
     * @var integer
     */
    protected $timestamp;

    /**
     * @return integer
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param integer $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->timestamp];
    }
}