<?php


namespace vdmcworld\api\request\parameter;


class EndTimestampQuery extends TimestampQuery
{
    /**
     * @return string
     */
    public function getKey()
    {
        return "end-date";
    }
}