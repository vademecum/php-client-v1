<?php

namespace vdmcworld\api\request\parameter;


class TermQuery extends SearchQuery
{
    /*
     * @var string
     */
    protected $term;

    /**
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param string $term
     * @return TermQuery
     */
    public function setTerm($term)
    {
        $this->term = $term;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "term";
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->term;
    }
}
