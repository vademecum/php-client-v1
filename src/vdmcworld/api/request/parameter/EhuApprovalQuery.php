<?php

namespace vdmcworld\api\request\parameter;


class EhuApprovalQuery extends SearchQuery
{
    /*
     * @var int[]
     */
    protected $ids;

    /**
     * @return int[]
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param int[] $ids
     * @return EhuApprovalQuery
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "ehu-approvements";
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return implode(',', $this->ids);
    }
}