<?php


namespace vdmcworld\api\request\parameter;


class Drug implements Parameter
{
    /**
     * @var int
     */
    private $id;

    /**
     * Product constructor.
     * @param int $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "id" => $this->id,
        ];
    }
}