<?php


namespace vdmcworld\api\request\parameter;


abstract class DateQuery extends SearchQuery
{
    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->date->format("Y-m-d")];
    }
}