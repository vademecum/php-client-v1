<?php

namespace vdmcworld\api\request\parameter;


abstract class SearchQuery
{
    /**
     * @return string
     */
    abstract public function getKey();

    /**
     * @return mixed
     */
    abstract public function getValue();
}