<?php

namespace vdmcworld\api\request\parameter;


class IdQuery extends SearchQuery
{
    /**
     * @var int[]
     */
    protected $ids;

    /**
     * @return string
     */
    public function getKey()
    {
        return "ids";
    }

    /**
     * @return int[]
     */
    public function getValue()
    {
        return $this->ids;
    }

    /**
     * @return int[]
     */
    public function getIds()
    {
        return $this->ids;
    }

    /**
     * @param int[] $ids
     * @return IdQuery
     */
    public function setIds($ids)
    {
        $this->ids = $ids;
        return $this;
    }
}