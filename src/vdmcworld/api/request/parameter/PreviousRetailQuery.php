<?php


namespace vdmcworld\api\request\parameter;


class PreviousRetailQuery extends SearchQuery
{
    /**
     * @var int
     */
    protected $previousRetail;

    /**
     * @return int
     */
    public function getPreviousRetail()
    {
        return $this->previousRetail;
    }

    /**
     * @param int $diff
     */
    public function setPreviousRetail($previousRetail)
    {
        $this->previousRetail = $previousRetail;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "previous-retail";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->previousRetail];
    }
}