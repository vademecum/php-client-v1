<?php

namespace vdmcworld\api\request\parameter;


class CompanyQuery extends SearchQuery
{
    /**
     * @var string[]
     */
    protected $company;

    /**
     * @return string[]
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string[] $company
     * @return CompanyQuery
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "company-name";
    }

    /**
     * @return string[]
     */
    public function getValue()
    {
        return $this->company;
    }
}