<?php

namespace vdmcworld\api\request\parameter;


class AreaOfSpecializationIdQuery extends SearchQuery
{
    /**
     * @var int
     */
    protected $areaOfSpecializationId;

    /**
     * @return string
     */
    public function getKey()
    {
        return "areaOfSpecializationId";
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->areaOfSpecializationId;
    }

    /**
     * @return int
     */
    public function getAreaOfSpecializationId()
    {
        return $this->areaOfSpecializationId;
    }

    /**
     * @param int $areaOfSpecializationId
     * @return AreaOfSpecializationIdQuery
     */
    public function setAreaOfSpecializationId($areaOfSpecializationId)
    {
        $this->areaOfSpecializationId = $areaOfSpecializationId;
        return $this;
    }
}