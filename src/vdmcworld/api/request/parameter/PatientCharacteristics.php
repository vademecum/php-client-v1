<?php

namespace vdmcworld\api\request\parameter;


class PatientCharacteristics implements Parameter
{
    const GENDER_FEMALE = "female";
    const GENDER_MALE = "male";

    /**
     * @var bool
     */
    private $drugHabit;

    /**
     * @var int
     */
    private $weight;

    /**
     * @var int
     */
    private $height;

    /**
     * @var bool
     */
    private $pregnant;

    /**
     * @var \DateTime
     */
    private $birthDate;

    /**
     * @var \DateTime
     */
    private $pregnancyDate;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var string
     */
    private $bloodType;

    /**
     * @var bool
     */
    private $smokingHabit;

    /**
     * @var bool
     */
    private $pregnancyProbability;

    /**
     * @var bool
     */
    private $breastFeeding;

    /**
     * @var bool
     */
    private $contactLensUsage;

    /**
     * @var bool
     */
    private $saltFreeDiet;

    /**
     * @var bool
     */
    private $electroshockTherapy;

    /**
     * @var bool
     */
    private $postpartumPeriod;

    /**
     * @var bool
     */
    private $bloodDonation;

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     * @return PatientCharacteristics
     */
    public function setBirthDate(\DateTime $birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPregnancyDate()
    {
        return $this->pregnancyDate;
    }

    /**
     * @param \DateTime $pregnancyDate
     * @return PatientCharacteristics
     */
    public function setPregnancyDate(\DateTime $pregnancyDate)
    {
        $this->pregnancyDate = $pregnancyDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return PatientCharacteristics
     * @throws \Exception
     */
    public function setGender($gender)
    {
        if (in_array($gender, [self::GENDER_FEMALE, self::GENDER_MALE])) {
            $this->gender = $gender;
            return $this;
        }
        throw new \Exception("Invalid Gender");
    }

    /**
     * @return string
     */
    public function getBloodType()
    {
        return $this->bloodType;
    }

    /**
     * @param string $bloodType
     * @return PatientCharacteristics
     */
    public function setBloodType($bloodType)
    {
        $this->bloodType = $bloodType;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSmokingHabit()
    {
        return $this->smokingHabit;
    }

    /**
     * @param boolean $smokingHabit
     * @return PatientCharacteristics
     */
    public function setSmokingHabit($smokingHabit)
    {
        $this->smokingHabit = $smokingHabit;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getPregnancyProbability()
    {
        return $this->pregnancyProbability;
    }

    /**
     * @param boolean $pregnancyProbability
     * @return PatientCharacteristics
     */
    public function setPregnancyProbability($pregnancyProbability)
    {
        $this->pregnancyProbability = $pregnancyProbability;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBreastFeeding()
    {
        return $this->breastFeeding;
    }

    /**
     * @param boolean $breastFeeding
     * @return PatientCharacteristics
     */
    public function setBreastFeeding($breastFeeding)
    {
        $this->breastFeeding = $breastFeeding;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getContactLensUsage()
    {
        return $this->contactLensUsage;
    }

    /**
     * @param boolean $contactLensUsage
     * @return PatientCharacteristics
     */
    public function setContactLensUsage($contactLensUsage)
    {
        $this->contactLensUsage = $contactLensUsage;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getSaltFreeDiet()
    {
        return $this->saltFreeDiet;
    }

    /**
     * @param boolean $saltFreeDiet
     * @return PatientCharacteristics
     */
    public function setSaltFreeDiet($saltFreeDiet)
    {
        $this->saltFreeDiet = $saltFreeDiet;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getElectroshockTherapy()
    {
        return $this->electroshockTherapy;
    }

    /**
     * @param boolean $electroshockTherapy
     * @return PatientCharacteristics
     */
    public function setElectroshockTherapy($electroshockTherapy)
    {
        $this->electroshockTherapy = $electroshockTherapy;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getPostpartumPeriod()
    {
        return $this->postpartumPeriod;
    }

    /**
     * @param boolean $postpartumPeriod
     * @return PatientCharacteristics
     */
    public function setPostpartumPeriod($postpartumPeriod)
    {
        $this->postpartumPeriod = $postpartumPeriod;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBloodDonation()
    {
        return $this->bloodDonation;
    }

    /**
     * @param boolean $bloodDonation
     * @return PatientCharacteristics
     */
    public function setBloodDonation($bloodDonation)
    {
        $this->bloodDonation = $bloodDonation;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return PatientCharacteristics
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDrugHabit()
    {
        return $this->drugHabit;
    }

    /**
     * @param bool $drugHabit
     * @return PatientCharacteristics
     */
    public function setDrugHabit($drugHabit)
    {
        $this->drugHabit = $drugHabit;
        return $this;
    }

    /**
     * @return bool
     */
    public function getPregnant()
    {
        return $this->pregnant;
    }

    /**
     * @param bool $pregnant
     * @return PatientCharacteristics
     */
    public function setPregnant($pregnant)
    {
        $this->pregnant = $pregnant;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return  PatientCharacteristics
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * PatientCharacteristics constructor.
     * @param bool $dHabit
     * @param int $weight
     * @param bool $pregnant
     */
    public function __construct($dHabit = null, $weight = null, $pregnant = null)
    {
        $this->drugHabit = $dHabit;
        $this->weight = $weight;
        $this->pregnant = $pregnant;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        $map = [
            "weight" => $this->weight,
            "height" => $this->height,
            "birthdate" => $this->birthDate ? $this->birthDate->format("Y-m-d") : null,
            "pregnancyDate" => $this->pregnancyDate ? $this->pregnancyDate->format("Y-m-d") : null,
            "gender" => $this->gender,
            "bloodType" => $this->bloodType,
            "smokingHabit" => $this->smokingHabit,
            "drugHabit" => $this->drugHabit,
            "pregnancyProbability" => $this->pregnancyProbability,
            "breastFeeding" => $this->breastFeeding,
            "contactLensUsage" => $this->contactLensUsage,
            "saltFreeDiet" => $this->saltFreeDiet,
            "electroshockTherapy" => $this->electroshockTherapy,
            "postpartumPeriod" => $this->postpartumPeriod,
            "bloodDonation" => $this->bloodDonation,
            "pregnant" => $this->pregnant,
        ];

        $res = [];
        foreach ($map as $key => $val) {
            if ($val) {
                $res[$key] = $val;
            }
        }

        return $res;
    }
}