<?php

namespace vdmcworld\api\request\parameter;


class CityQuery extends SearchQuery
{
    /**
     * @var string[]
     */
    protected $city;

    /**
     * @return string[]
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string[] $city
     * @return CityQuery
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "city";
    }

    /**
     * @return string[]
     */
    public function getValue()
    {
        return $this->city;
    }
}