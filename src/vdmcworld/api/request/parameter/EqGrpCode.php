<?php


namespace vdmcworld\api\request\parameter;


class EqGrpCode implements Parameter
{
    /**
     * @var string
     */
    protected $code;

    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return EqGrpCode
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "eqGrpCode" => $this->code,
        ];
    }
}