<?php


namespace vdmcworld\api\request\parameter;


class SortQuery extends SearchQuery
{
    /**
     * @var string
     */
    protected $sort;

    /**
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "sort";
    }

    /**
     * @return string[]
     */
    public function getValue()
    {
        return [$this->sort];
    }
}