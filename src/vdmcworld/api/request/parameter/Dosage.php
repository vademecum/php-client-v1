<?php

namespace vdmcworld\api\request\parameter;


class Dosage implements Parameter
{
    const DAILY_PERIOD = 3;
    const WEEKLY_PERIOD = 4;
    const MONTHLY_PERIOD = 5;
    const YEARLY_PERIOD = 6;

    /**
     * @var int
     */
    private $dosage;

    /**
     * @var int
     */
    private $dosagePeriod;

    /**
     * @var int
     */
    private $period;

    /**
     * @var int
     */
    private $periodAmount;

    /**
     * @return int
     */
    public function getDosage()
    {
        return $this->dosage;
    }

    /**
     * @param int $dosage
     * @return Dosage
     */
    public function setDosage($dosage)
    {
        $this->dosage = $dosage;
        return $this;
    }

    /**
     * @return int
     */
    public function getDosagePeriod()
    {
        return $this->dosagePeriod;
    }

    /**
     * @param int $dosagePeriod
     * @return Dosage
     */
    public function setDosagePeriod($dosagePeriod)
    {
        $this->dosagePeriod = $dosagePeriod;
        return $this;
    }

    /**
     * @return int
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param int $period
     * @return Dosage
     * @throws \Exception
     */
    public function setPeriod($period)
    {
        if (!in_array($period, $this->getAvailablePeriods())) {
            throw new \Exception("Invalid Period value");
        }
        $this->period = $period;
        return $this;
    }

    /**
     * @return int
     */
    public function getPeriodAmount()
    {
        return $this->periodAmount;
    }

    /**
     * @param int $periodAmount
     * @return Dosage
     */
    public function setPeriodAmount($periodAmount)
    {
        $this->periodAmount = $periodAmount;
        return $this;
    }

    /**
     * Dosage constructor.
     * @param int $dosage
     * @param int $dosagePeriod
     * @param int $period
     * @param int $periodAmount
     * @throws \Exception
     */
    public function __construct($dosage = null, $dosagePeriod = null, $period = null, $periodAmount = null)
    {
        $this->setDosage($dosage);
        $this->setDosagePeriod($dosagePeriod);
        $this->setPeriod($period);
        $this->setPeriodAmount($periodAmount);
    }

    public function getAvailablePeriods()
    {
        return [
            self::DAILY_PERIOD,
            self::WEEKLY_PERIOD,
            self::MONTHLY_PERIOD,
            self::YEARLY_PERIOD,
        ];
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "dosage" => $this->dosage,
            "dosagePeriod" => $this->dosagePeriod,
            "period" => $this->period,
            "periodAmount" => $this->periodAmount,
        ];
    }
}