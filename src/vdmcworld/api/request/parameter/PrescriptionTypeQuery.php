<?php


namespace vdmcworld\api\request\parameter;


class PrescriptionTypeQuery extends SearchQuery
{
    /**
     * @var string
     */
    protected $prescriptionType;

    /**
     * @return string
     */
    public function getPrescriptionType()
    {
        return $this->prescriptionType;
    }

    /**
     * @param int $prescriptionType
     */
    public function setPrescriptionType($prescriptionType)
    {
        $this->prescriptionType = $prescriptionType;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "prescription-type";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->prescriptionType];
    }
}