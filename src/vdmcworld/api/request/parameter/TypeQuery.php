<?php


namespace vdmcworld\api\request\parameter;


class TypeQuery extends SearchQuery
{
    /**
     * @var int
     */
    protected $type;

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "type";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->type];
    }
}