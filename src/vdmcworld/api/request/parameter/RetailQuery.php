<?php


namespace vdmcworld\api\request\parameter;


class RetailQuery extends SearchQuery
{
    /**
     * @var int
     */
    protected $retail;

    /**
     * @return int
     */
    public function getRetail()
    {
        return $this->retail;
    }

    /**
     * @param int $diff
     */
    public function setRetail($retail)
    {
        $this->retail = $retail;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "retail";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->retail];
    }
}