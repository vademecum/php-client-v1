<?php


namespace vdmcworld\api\request\parameter;


class DatetimeQuery extends DateQuery
{

    /**
     * @return string
     */
    public function getKey()
    {
        return "date";
    }
}