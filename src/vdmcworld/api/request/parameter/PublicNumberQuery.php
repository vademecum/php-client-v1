<?php


namespace vdmcworld\api\request\parameter;


class PublicNumberQuery extends SearchQuery
{
    /**
     * @var string[]
     */
    protected $publicNumbers;

    /**
     * @return \string[]
     */
    public function getPublicNumbers()
    {
        return $this->publicNumbers;
    }

    /**
     * @param \string[] $publicNumbers
     */
    public function setPublicNumbers($publicNumbers)
    {
        $this->publicNumbers = $publicNumbers;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "public-numbers";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->publicNumbers;
    }
}