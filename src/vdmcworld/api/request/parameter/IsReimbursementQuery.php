<?php


namespace vdmcworld\api\request\parameter;


class IsReimbursementQuery extends SearchQuery
{
    /**
     * @var boolean
     */
    protected $isReimbursement;

    /**
     * @return boolean
     */
    public function getIsReimbursement()
    {
        return $this->isReimbursement;
    }

    /**
     * @param boolean $isReimbursement
     */
    public function setIsReimbursement($isReimbursement)
    {
        $this->isReimbursement = $isReimbursement;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "is-reimbursement";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->isReimbursement];
    }
}