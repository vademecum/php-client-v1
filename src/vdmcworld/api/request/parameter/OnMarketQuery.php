<?php


namespace vdmcworld\api\request\parameter;


class OnMarketQuery extends SearchQuery
{
    /**
     * @var boolean
     */
    protected $onMarket;

    /**
     * @return boolean
     */
    public function getOnMarket()
    {
        return $this->onMarket;
    }

    /**
     * @param boolean $onMarket
     */
    public function setOnMarket($onMarket)
    {
        $this->onMarket = $onMarket;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "on-market";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->onMarket];
    }
}