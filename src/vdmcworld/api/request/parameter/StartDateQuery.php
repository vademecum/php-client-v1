<?php


namespace vdmcworld\api\request\parameter;


class StartDateQuery extends DateQuery
{

    /**
     * @return string
     */
    public function getKey()
    {
        return "start-date";
    }
}