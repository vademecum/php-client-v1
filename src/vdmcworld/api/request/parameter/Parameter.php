<?php

namespace vdmcworld\api\request\parameter;


interface Parameter
{
    /**
     * @return array
     */
    public function toParams();
}