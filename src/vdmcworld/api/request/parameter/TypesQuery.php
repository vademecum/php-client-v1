<?php


namespace vdmcworld\api\request\parameter;


class TypesQuery extends SearchQuery
{
    /**
     * @var int[]
     */
    protected $types;

    /**
     * @return int[]
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param int[] $types
     */
    public function setTypes($types)
    {
        $this->types = $types;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "types";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->types];
    }
}