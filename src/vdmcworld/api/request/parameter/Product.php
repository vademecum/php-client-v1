<?php

namespace vdmcworld\api\request\parameter;


class Product implements Parameter
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Dosage
     */
    private $dosage;

    /**
     * @var PrescriptionCalculationProduct
     */
    private $prescriptionCalculationProduct;

    /**
     * @var array
     */
    private $attrs;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Product
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Dosage
     */
    public function getDosage()
    {
        return $this->dosage;
    }

    /**
     * @param Dosage $dosage
     * @return Product
     */
    public function setDosage(Dosage $dosage)
    {
        $this->dosage = $dosage;
        return $this;
    }

    /**
     * @return PrescriptionCalculationProduct
     */
    public function getPrescriptionCalculationProduct()
    {
        return $this->prescriptionCalculationProduct;
    }

    /**
     * @param PrescriptionCalculationProduct $prescriptionCalculationProduct
     * @return Product
     */
    public function setPrescriptionCalculationProduct($prescriptionCalculationProduct)
    {
        $this->prescriptionCalculationProduct = $prescriptionCalculationProduct;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttrs()
    {
        return $this->attrs;
    }

    /**
     * @param array $attrs
     */
    public function setAttrs($attrs)
    {
        $this->attrs = $attrs;
    }

    /**
     * Product constructor.
     * @param int $id
     * @param Dosage $dosage
     * @param array $attrs
     */
    public function __construct($id = null, $dosage = null, $attrs = null)
    {
        $this->id = $id;
        $this->dosage = $dosage;
        if (null === $attrs) {
            $this->attrs = [];
        } else {
            $this->attrs = $attrs;
        }
    }

    /**
     * @return array
     */
    public function toParams()
    {
        $ret = [
            "id" => $this->id,
        ];
        if (null !== $this->attrs) {
            $ret["attrs"] = join(",", $this->attrs);
        }
        if (null !== $this->dosage) {
            $ret = array_merge($ret, $this->dosage->toParams());
        }
        if (null !== $this->prescriptionCalculationProduct) {
            $ret = array_merge($ret, $this->prescriptionCalculationProduct->toParams());
        }
        return $ret;
    }
}