<?php

namespace vdmcworld\api\request\parameter;


class Disease implements Parameter
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Disease
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Disease constructor.
     * @param int $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "id" => $this->id,
        ];
    }
}