<?php


namespace vdmcworld\api\request\parameter;


class BeginsWithQuery extends SearchQuery
{
    /**
     * @var string
     */
    protected $term;

    /**
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param string $term
     */
    public function setTerm($term)
    {
        $this->term = $term;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "begins-with";
    }

    /**
     * @return string[]
     */
    public function getValue()
    {
        return [$this->term];
    }
}