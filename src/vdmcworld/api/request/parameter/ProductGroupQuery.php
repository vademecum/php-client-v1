<?php

namespace vdmcworld\api\request\parameter;


class ProductGroupQuery extends SearchQuery
{
    /*
      * @var string
      */
    protected $code;

    /**
     * @return string
     */
    public function getGroup()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return ProductGroupQuery
     */
    public function setGroup($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "product-group";
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->code;
    }
}