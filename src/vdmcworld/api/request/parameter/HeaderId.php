<?php


namespace vdmcworld\api\request\parameter;


class HeaderId implements Parameter
{
    /**
     * @var int
     */
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function toParams()
    {
        return [
            "id" => $id,
        ];
    }
}