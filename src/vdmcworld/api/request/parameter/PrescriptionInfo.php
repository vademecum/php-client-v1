<?php

namespace vdmcworld\api\request\parameter;


class PrescriptionInfo implements Parameter
{

    const REPORT_TYPE_WITH_REPORT = "raporlu";
    const REPORT_TYPE_WITHOUT_REPORT = "raporsuz";

    const PATIENT_TYPE_OUT_PATIENT = "ayakta";
    const PATIENT_TYPE_IN_PATIENT = "yatan";

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $reportType;

    /**
     * @var string
     */
    private $patientType;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PrescriptionInfo
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * @param string $reportType
     * @return PrescriptionInfo
     * @throws \Exception
     */
    public function setReportType($reportType)
    {
        if (in_array($reportType, [self::REPORT_TYPE_WITH_REPORT, self::REPORT_TYPE_WITHOUT_REPORT])) {
            $this->reportType = $reportType;
            return $this;
        }
        throw new \Exception("Invalid Report Type");
    }

    /**
     * @return string
     */
    public function getPatientType()
    {
        return $this->patientType;
    }

    /**
     * @param string $patientType
     * @return PrescriptionInfo
     * @throws \Exception
     */
    public function setPatientType($patientType)
    {
        if (in_array($patientType, [self::PATIENT_TYPE_OUT_PATIENT, self::PATIENT_TYPE_IN_PATIENT])) {
            $this->patientType = $patientType;
            return $this;
        }
        throw new \Exception("Invalid Patient Type");
    }

    /**
     * PrescriptionInfo constructor.
     * @param $id
     * @param string $reportType
     * @param string $patientType
     */
    public function __construct($id = null, $reportType = self::REPORT_TYPE_WITH_REPORT, $patientType = self::PATIENT_TYPE_OUT_PATIENT)
    {
        $this->id = $id;
        $this->reportType = $reportType;
        $this->patientType = $patientType;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "id" => $this->id,
            "reportType" => $this->reportType,
            "patientType" => $this->patientType
        ];
    }
}