<?php

namespace vdmcworld\api\request\parameter;


class ApplicationMethodQuery extends SearchQuery
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "application-method";
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->name;
    }
}