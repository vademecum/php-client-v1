<?php


namespace vdmcworld\api\request\parameter;


class SgkEqCode extends EqGrpCode
{
    public function toParams()
    {
        return [
            "sqkEqCode" => $this->code
        ];
    }
}