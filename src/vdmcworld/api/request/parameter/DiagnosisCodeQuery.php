<?php

namespace vdmcworld\api\request\parameter;


class DiagnosisCodeQuery extends SearchQuery
{
    /*
      * @var string
      */
    protected $code;

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "diagnosis-code";
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->code;
    }
}