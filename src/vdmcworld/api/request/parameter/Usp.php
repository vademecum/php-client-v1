<?php


namespace vdmcworld\api\request\parameter;


class Usp implements Parameter
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Usp constructor.
     * @param int $id
     * @param string $code
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "id" => $this->id,
        ];
    }
}