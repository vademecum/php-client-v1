<?php

namespace vdmcworld\api\request\parameter;


class Symptom implements Parameter
{
    /**
     * @var int
     */
    private $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Symptom
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "id" => $this->id,
        ];
    }
}