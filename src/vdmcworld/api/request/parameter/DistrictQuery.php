<?php

namespace vdmcworld\api\request\parameter;


class DistrictQuery extends SearchQuery
{
    /**
     * @var string[]
     */
    protected $district;

    /**
     * @return string[]
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param string[] $district
     * @return DistrictQuery
     */
    public function setDistrict($district)
    {
        $this->district = $district;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "district";
    }

    /**
     * @return string[]
     */
    public function getValue()
    {
        return $this->district;
    }
}