<?php


namespace vdmcworld\api\request\parameter;


class RapRecRule implements Parameter
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $data;

    /**
     * @var string
     */
    protected $type = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Company constructor.
     * @param int $id
     * @param string $data
     */
    public function __construct($id = null, $data = null, $type = null)
    {
        $this->id = $id;
        $this->data = $data;
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        $params = [
            "ruleId" => $this->id,
            "data" => $this->data
        ];

        if (isset($this->type)) {
            $params["type"] = $this->type;
        }

        return $params;
    }
}