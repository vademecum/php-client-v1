<?php


namespace vdmcworld\api\request\parameter;


class NfcCode extends EqGrpCode
{
    public function toParams()
    {
        return [
            "nfcCode" => $this->code
        ];
    }
}