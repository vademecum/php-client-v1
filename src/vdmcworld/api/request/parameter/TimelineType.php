<?php

namespace vdmcworld\api\request\parameter;


class TimelineType implements Parameter
{
    /**
     * @var int
     */
    private $type;

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Timeline constructor.
     * @param int $type
     */
    public function __construct($type = null)
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "type" => $this->type
        ];
    }
}