<?php


namespace vdmcworld\api\request\parameter;


class Company implements Parameter
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Company constructor.
     * @param int $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "id" => $this->id,
        ];
    }
}