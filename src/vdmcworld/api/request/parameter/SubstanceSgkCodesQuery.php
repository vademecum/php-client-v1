<?php

namespace vdmcworld\api\request\parameter;


class SubstanceSgkCodesQuery extends SearchQuery
{
    /**
     * @var string
     */
    protected $sgk_code;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->sgk_code;
    }

    /**
     * @param string $sgk_code
     */
    public function setCode($sgk_code)
    {
        $this->sgk_code = $sgk_code;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "sgk-code";
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->sgk_code;
    }
}