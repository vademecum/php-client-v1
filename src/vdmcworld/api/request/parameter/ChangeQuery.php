<?php


namespace vdmcworld\api\request\parameter;


class ChangeQuery extends SearchQuery
{
    /**
     * @var int
     */
    protected $change;

    /**
     * @return int
     */
    public function getChange()
    {
        return $this->change;
    }

    /**
     * @param int $change
     */
    public function setChange($change)
    {
        $this->change = $change;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "change";
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return [$this->change];
    }
}