<?php

namespace vdmcworld\api\request\parameter;


class NameQuery extends SearchQuery
{
    /*
     * @var string[]
     */
    protected $names;

    /**
     * @return string[]
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param string[] $names
     * @return NameQuery
     */
    public function setNames($names)
    {
        $this->names = $names;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "names";
    }

    /**
     * @return string[]
     */
    public function getValue()
    {
        return $this->names;
    }
}