<?php


namespace vdmcworld\api\request\parameter;


class Atc implements Parameter
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Atc constructor.
     * @param int $id
     * @param string $code
     */
    public function __construct($id = null, $code = null)
    {
        $this->id = $id;
        $this->code = $code;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [
            "id" => $this->id,
            "code" => $this->code,
        ];
    }
}