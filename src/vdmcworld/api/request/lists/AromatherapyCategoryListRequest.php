<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\AromatherapyCategory;

/**
 * Class AromatherapyCategoryListRequest
 * @package vdmcworld\api\request\lists
 * @method AromatherapyCategory[] fetchData(Api $api)
 * @method AromatherapyCategory[] fetchAllData(Api $api)
 */
class AromatherapyCategoryListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/aromatherapy-categories";
    }

    protected function getItemModel()
    {
        return "AromatherapyCategory";
    }
}