<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\PrescriptionType;

/**
 * Class PrescriptionTypeListRequest
 * @package vdmcworld\api\request\lists
 * @method PrescriptionType[] fetchData(Api $api)
 * @method PrescriptionType[] fetchAllData(Api $api)
 */
class PrescriptionTypeListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/prescription-types";
    }

    protected function getItemModel()
    {
        return "PrescriptionType";
    }
}