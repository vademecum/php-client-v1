<?php


namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Product;

/**
 * Class ColdChainProductsRequest
 * @package vdmcworld\api\request
 * @method Product[] fetchData(Api $api)
 * @method Product[] fetchAllData(Api $api)
 */
class ColdChainProductsListRequest extends BaseListRequest
{
    /**
     * This method provides the base endpoint for list requests.
     * getEndpoint is implemented in this class so that common
     *   functionality is not duplicated.
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/cold-chain-products";
    }

    protected function getItemModel()
    {
        return "Product";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\NameQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\PrescriptionTypeQuery',
            'vdmcworld\api\request\parameter\OnMarketQuery',
            'vdmcworld\api\request\parameter\IsReimbursementQuery',
            'vdmcworld\api\request\parameter\CompanyQuery',
            'vdmcworld\api\request\parameter\TermQuery',
            'vdmcworld\api\request\parameter\TypeQuery',

        ]);
    }

}