<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Unit;

/**
 * Class UnitListRequest
 * @package vdmcworld\api\request\lists
 * @method Unit[] fetchData(Api $api)
 * @method Unit[] fetchAllData(Api $api)
 */
class UnitListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/units";
    }

    protected function getItemModel()
    {
        return "Unit";
    }
}