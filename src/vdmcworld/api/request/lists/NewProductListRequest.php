<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\NewProduct;
use vdmcworld\api\request\parameter\EndTimestampQuery;
use vdmcworld\api\request\parameter\StartTimestampQuery;

/**
 * Class NewProductListRequest
 * @package vdmcworld\api\request\lists
 * @method NewProduct[] fetchData(Api $api)
 * @method NewProduct[] fetchAllData(Api $api)
 */
class NewProductListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/new-products";
    }

    protected function getItemModel()
    {
        return "NewProduct";
    }

    protected function getValidSearchQueries()
    {
        return [
            'vdmcworld\api\request\parameter\StartTimestampQuery',
            'vdmcworld\api\request\parameter\EndTimestampQuery',
            'vdmcworld\api\request\parameter\NameQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\TypeQuery',
            'vdmcworld\api\request\parameter\CompanyQuery',
            'vdmcworld\api\request\parameter\TermQuery',
            'vdmcworld\api\request\parameter\IsReimbursementQuery',
            'vdmcworld\api\request\parameter\OnMarketQuery',
            'vdmcworld\api\request\parameter\BarcodeQuery',
        ];
    }

    public function isValid()
    {
        parent::isValid();
        if (!$this->queries[0] instanceof StartTimestampQuery || !$this->queries[1] instanceof EndTimestampQuery) {
            throw new \Exception("You have to provide start date and end date");
        }
        return true;
    }
}