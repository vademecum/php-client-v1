<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Drug;

/**
 * Class DrugListRequest
 * @package vdmcworld\api\request\lists
 * @method Drug[] fetchData(Api $api)
 * @method Drug[] fetchAllData(Api $api)
 */
class ProhibitedDopingProductsListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/prohibited-doping-products";
    }

    protected function getValidSearchQueries()
    {
        return [
            'vdmcworld\api\request\parameter\IdQuery',
            'vdmcworld\api\request\parameter\NameQuery',
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\BeginsWithQuery',
            'vdmcworld\api\request\parameter\SortQuery',
        ];
    }

    protected function getItemModel()
    {
        return "Product";
    }
}
