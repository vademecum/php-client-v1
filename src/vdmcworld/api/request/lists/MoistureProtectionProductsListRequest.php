<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\Product;

/**
 * Class MoistureProtectionProductsListRequest
 * @package vdmcworld\api\request\lists
 * @method Product[] fetchData(Api $api)
 * @method Product[] fetchAllData(Api $api)
 */
class MoistureProtectionProductsListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/moisture-protection-products";
    }

    protected function getItemModel()
    {
        return "Product";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\NameQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\PrescriptionTypeQuery',
            'vdmcworld\api\request\parameter\OnMarketQuery',
            'vdmcworld\api\request\parameter\IsReimbursementQuery',
            'vdmcworld\api\request\parameter\CompanyQuery',
            'vdmcworld\api\request\parameter\TermQuery',
            'vdmcworld\api\request\parameter\TypeQuery',
        ]);
    }
}