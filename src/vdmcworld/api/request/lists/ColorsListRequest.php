<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Color;

/**
 * Class ColorsListRequest
 * @package vdmcworld\api\request\lists
 * @method Color[] fetchData(Api $api)
 * @method Color[] fetchAllData(Api $api)
 */
class ColorsListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/colors";
    }

    protected function getItemModel()
    {
        return "Color";
    }
}