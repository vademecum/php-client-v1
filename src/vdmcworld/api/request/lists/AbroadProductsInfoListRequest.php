<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\DetailedProductInfo;

/**
 * Class AllProductCardsListRequest
 * @package vdmcworld\api\request\lists
 * @method DetailedProductInfo[] fetchData(Api $api)
 * @method DetailedProductInfo[] fetchAllData(Api $api)
 */
class AbroadProductsInfoListRequest extends BaseListRequest
{

    protected function getItemModel()
    {
        return "DetailedProductInfo";
    }

    protected function getBaseEndpoint()
    {
        return "/abroad-products";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\TermQuery',
            'vdmcworld\api\request\parameter\TypeQuery',
            'vdmcworld\api\request\parameter\IsReimbursementQuery',
            'vdmcworld\api\request\parameter\OnMarketQuery',
            'vdmcworld\api\request\parameter\NameQuery',
        ]);
    }
}