<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Company;

/**
 * Class CompanyListRequest
 * @package vdmcworld\api\request\lists
 * @method Company[] fetchData(Api $api)
 * @method Company[] fetchAllData(Api $api)
 */
abstract class BaseDerangementListRequest extends BaseListRequest
{
    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\IdQuery',
            'vdmcworld\api\request\parameter\NameQuery',
            'vdmcworld\api\request\parameter\BeginsWithQuery',
        ]);
    }
}