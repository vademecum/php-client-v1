<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Reimbursement;
use vdmcworld\api\model\ReimbursementCondition;

/**
 * Class ReimbursementConditionListRequest
 * @package vdmcworld\api\request\lists
 * @method Reimbursement[] fetchData(Api $api)
 * @method Reimbursement[] fetchAllData(Api $api)
 */
class ReimbursementConditionListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/reimbursement-conditions";
    }

    protected function getItemModel()
    {
        return "ReimbursementCondition";
    }
}