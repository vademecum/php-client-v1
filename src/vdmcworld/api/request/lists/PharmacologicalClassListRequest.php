<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\PharmacologicalClass;

/**
 * Class PharmacologicalClassListRequest
 * @package vdmcworld\api\request\lists
 * @method PharmacologicalClass[] fetchData(Api $api)
 * @method PharmacologicalClass[] fetchAllData(Api $api)
 */
class PharmacologicalClassListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/pharmalogical-classes";
    }

    protected function getItemModel()
    {
        return "PharmacologicalClass";
    }
}