<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\Substance;

/**
 * Class SubstanceListRequest
 * @package vdmcworld\api\request\lists
 * @method Substance[] fetchData(Api $api)
 * @method Substance[] fetchAllData(Api $api)
 */
class SubstanceListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/substances";
    }

    protected function getItemModel()
    {
        return "Substance";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BeginsWithQuery',
        ]);
    }
}