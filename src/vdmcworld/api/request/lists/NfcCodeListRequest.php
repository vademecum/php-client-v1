<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\NfcCode;

/**
 * Class NfcCodeListRequest
 * @package vdmcworld\api\request\lists
 * @method NfcCode[] fetchData(Api $api)
 * @method NfcCode[] fetchAllData(Api $api)
 */
class NfcCodeListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/nfc-codes";
    }

    protected function getItemModel()
    {
        return "NfcCode";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BarcodeQuery',
        ]);
    }
}