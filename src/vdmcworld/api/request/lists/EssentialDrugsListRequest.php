<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\EssentialDrugs;

/**
 * Class EssentialDrugsListRequest
 * @package vdmcworld\api\request\lists
 * @method EssentialDrugs[] fetchData(Api $api)
 * @method EssentialDrugs[] fetchAllData(Api $api)
 */
class EssentialDrugsListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/essential-drugs";
    }

    protected function getItemModel()
    {
        return "EssentialDrugs";
    }

    protected function getValidSearchQueries()
    {
        return [
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\NameQuery',
            'vdmcworld\api\request\parameter\SortQuery',
        ];
    }
}