<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\AllProduct;

/**
 * Class AllProductsListRequest
 * @package vdmcworld\api\request\lists
 * @method AllProduct[] fetchData(Api $api)
 * @method AllProduct[] fetchAllData(Api $api)
 */
class AllProductsListRequest extends ProductListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/all-products";
    }

    protected function getItemModel()
    {
        return "AllProduct";
    }
}