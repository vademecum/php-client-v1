<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Microorganism;

/**
 * Class MicroorganismListRequest
 * @package vdmcworld\api\request\lists
 * @method Microorganism[] fetchData(Api $api)
 * @method Microorganism[] fetchAllData(Api $api)
 */
class MicroorganismListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/microorganisms";
    }

    protected function getItemModel()
    {
        return "Microorganism";
    }
}