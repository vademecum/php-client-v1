<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\EquivalentGroup;
use vdmcworld\api\model\EquivalentProduct;

/**
 * Class EquivalentGroupListRequest
 * @package vdmcworld\api\request\lists
 * @method EquivalentGroup[] fetchData(Api $api)
 * @method EquivalentGroup[] fetchAllData(Api $api)
 */
class EquivalentGroupListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/equivalent-groups";
    }

    protected function getItemModel()
    {
        return "EquivalentGroup";
    }
}