<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Nutrition;

/**
 * Class NutritionListRequest
 * @package vdmcworld\api\request\lists
 * @method Nutrition[] fetchData(Api $api)
 * @method Nutrition[] fetchAllData(Api $api)
 */
class NutritionListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/nutritions";
    }

    protected function getItemModel()
    {
        return "Nutrition";
    }
}