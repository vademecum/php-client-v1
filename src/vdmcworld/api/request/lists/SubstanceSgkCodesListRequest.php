<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\SubstanceSgkCodes;

/**
 * Class SubstanceSgkCodesListRequest
 * @package vdmcworld\api\request\lists
 * @method SubstanceSgkCodes[] fetchData(Api $api)
 * @method SubstanceSgkCodes[] fetchAllData(Api $api)
 */
class SubstanceSgkCodesListRequest extends BaseListRequest
{
    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\SubstanceSgkCodesQuery',
            'vdmcworld\api\request\parameter\ApplicationMethodQuery',
            'vdmcworld\api\request\parameter\SubstanceNameQuery',
        ]);
    }

    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/substance-sgk-codes";
    }

    protected function getItemModel()
    {
        return "SubstanceSgkCodes";
    }
}