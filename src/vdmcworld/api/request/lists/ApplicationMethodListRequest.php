<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\ApplicationMethod;

/**
 * Class ApplicationMethodListRequest
 * @package vdmcworld\api\request\lists
 * @method ApplicationMethod[] fetchData(Api $api)
 * @method ApplicationMethod[] fetchAllData(Api $api)
 */
class ApplicationMethodListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/application-methods";
    }

    protected function getItemModel()
    {
        return "ApplicationMethod";
    }
}