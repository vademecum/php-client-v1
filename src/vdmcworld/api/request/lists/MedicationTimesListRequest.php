<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\MedicationTime;
use vdmcworld\api\model\MedicationTimeInfo;

/**
 * Class MedicationTimesListRequest
 * @package vdmcworld\api\request\lists
 * @method MedicationTime[] fetchData(Api $api)
 * @method MedicationTime[] fetchAllData(Api $api)
 */
class MedicationTimesListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/medication-times";
    }

    protected function getItemModel()
    {
        return "MedicationTime";
    }
}