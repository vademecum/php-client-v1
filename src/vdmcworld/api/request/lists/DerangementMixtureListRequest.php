<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\DerangementExcipient;

/**
 * Class DerangementMixtureListRequest
 * @package vdmcworld\api\request\lists
 * @method DerangementExcipient[] fetchData(Api $api)
 * @method DerangementExcipient[] fetchAllData(Api $api)
 */
class DerangementMixtureListRequest extends BaseDerangementListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/derangement-mixtures";
    }

    protected function getItemModel()
    {
        return "DerangementMixture";
    }
}
