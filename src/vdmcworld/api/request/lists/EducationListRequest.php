<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Education;

/**
 * Class EducationListRequest
 * @package vdmcworld\api\request\lists
 * @method Education[] fetchData(Api $api)
 * @method Education[] fetchAllData(Api $api)
 */
class EducationListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/educations";
    }

    protected function getItemModel()
    {
        return "Education";
    }
}