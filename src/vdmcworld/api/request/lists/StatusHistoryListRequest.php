<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\StatusHistory;

/**
 * Class StatusHistoryListRequest
 * @package vdmcworld\api\request\lists
 * @method StatusHistory[] fetchData(Api $api)
 * @method StatusHistory[] fetchAllData(Api $api)
 */
class StatusHistoryListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/status-history";
    }

    protected function getItemModel()
    {
        return "StatusHistory";
    }

    protected function getValidSearchQueries()
    {
        return [
            'vdmcworld\api\request\parameter\StartDateQuery',
            'vdmcworld\api\request\parameter\EndDateQuery',
            'vdmcworld\api\request\parameter\TypeQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\TermQuery',
            'vdmcworld\api\request\parameter\IsReimbursementQuery',
            'vdmcworld\api\request\parameter\OnMarketQuery',
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\NameQuery',
        ];
    }

    public function isValid()
    {
        parent::isValid();
        return true;
    }
}