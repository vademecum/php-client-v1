<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\DetailedProductInfo;

/**
 * Class StorageBasedProductsListRequest
 * @package vdmcworld\api\request\lists
 * @method DetailedProductInfo[] fetchData(Api $api)
 * @method DetailedProductInfo[] fetchAllData(Api $api)
 */
class StorageBasedProductsListRequest extends BaseListRequest
{

    protected function getItemModel()
    {
        return "DetailedProductInfo";
    }

    protected function getBaseEndpoint()
    {
        return "/storage-based-products";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\CompanyQuery',
        ]);
    }
}