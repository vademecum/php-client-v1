<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\LactationWarning;

/**
 * Class LactationWarningListRequest
 * @package vdmcworld\api\request\lists
 * @method LactationWarning[] fetchData(Api $api)
 * @method LactationWarning[] fetchAllData(Api $api)
 */
class LactationWarningListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/lactation-warnings";
    }

    protected function getItemModel()
    {
        return "LactationWarning";
    }
}