<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Specialist;

/**
 * Class SpecialistListRequest
 * @package vdmcworld\api\request\lists
 * @method Specialist[] fetchData(Api $api)
 * @method Specialist[] fetchAllData(Api $api)
 */
class SpecialistListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/specialists";
    }

    protected function getItemModel()
    {
        return "Specialist";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\AreaOfSpecializationIdQuery',
        ]);
    }
}
