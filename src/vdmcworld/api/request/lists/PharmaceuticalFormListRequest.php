<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\PharmaceuticalForm;

/**
 * Class PharmaceuticalFormListRequest
 * @package vdmcworld\api\request\lists
 * @method PharmaceuticalForm[] fetchData(Api $api)
 * @method PharmaceuticalForm[] fetchAllData(Api $api)
 */
class PharmaceuticalFormListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/pharmaceutical-forms";
    }

    protected function getItemModel()
    {
        return "PharmaceuticalForm";
    }
}