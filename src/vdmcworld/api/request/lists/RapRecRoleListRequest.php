<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\Specialist;

/**
 * Class SgkRapRoleListRequest
 * @package vdmcworld\api\request\lists
 * @method Specialist[] fetchData(Api $api)
 * @method Specialist[] fetchAllData(Api $api)
 */
class RapRecRoleListRequest extends BaseListRequest
{
    protected function getItemModel()
    {
        return "SgkSpecialist";
    }

    protected function getBaseEndpoint()
    {
        return "/rapico-rap-rec-roles";
    }
}
