<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\api\model\SgkSutIndex;

/**
 * Class SgkSutIndexRequest
 * @method SgkSutIndex[] fetchData(\vdmcworld\Api $api)
 */
class SgkSutIndexRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/sgk-sut-index";
    }

    protected function getItemModel()
    {
        return "SgkSutIndex";
    }

    protected function getValidSearchQueries()
    {
        return [
            'vdmcworld\api\request\parameter\TermQuery',
        ];
    }
}
