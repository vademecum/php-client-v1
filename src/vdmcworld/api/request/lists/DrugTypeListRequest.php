<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\DrugType;

/**
 * Class DrugTypeListRequest
 * @package vdmcworld\api\request\lists
 * @method DrugType[] fetchData(Api $api)
 * @method DrugType[] fetchAllData(Api $api)
 */
class DrugTypeListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/drug-types";
    }

    protected function getItemModel()
    {
        return "DrugType";
    }
}