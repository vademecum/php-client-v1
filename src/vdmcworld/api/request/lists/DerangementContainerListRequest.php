<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\DerangementContainer;

/**
 * Class DerangementContainerListRequest
 * @package vdmcworld\api\request\lists
 * @method DerangementContainer[] fetchData(Api $api)
 * @method DerangementContainer[] fetchAllData(Api $api)
 */
class DerangementContainerListRequest extends BaseDerangementListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/derangement-containers";
    }

    protected function getItemModel()
    {
        return "DerangementContainer";
    }
}
