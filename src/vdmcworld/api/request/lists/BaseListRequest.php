<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\request\BasePaginatableRequest;
use vdmcworld\api\request\BaseRequest;
use vdmcworld\api\request\parameter\SearchQuery;
use vdmcworld\api\response\lists\BaseListResponse;

abstract class BaseListRequest extends BasePaginatableRequest
{
    /**
     * @var SearchQuery[]
     */
    protected $queries = [];

    /**
     * @var string
     */
    protected $sortBy = null;

    /**
     * @var string
     */
    protected $method = BaseRequest::HTTP_METHOD_GET;

    /**
     * @var array
     */
    protected $allowedMethods = [
        BaseRequest::HTTP_METHOD_GET,
        BaseRequest::HTTP_METHOD_POST,
    ];

    /**
     * @param string $sortBy
     * @return BaseListRequest
     */
    public function setSortBy($sortBy)
    {
        $this->sortBy = $sortBy;
        return $this;
    }

    /**
     * @return SearchQuery[]
     */
    public function getQueries()
    {
        return $this->queries;
    }

    /**
     * @return string
     */
    public function getSortBy()
    {
        return $this->sortBy;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        if ($this->method == BaseRequest::HTTP_METHOD_POST && !empty($this->queries)) {
            $ret = [];
            foreach ($this->queries as $q) {
                $ret[$q->getKey()] = implode(',',$q->getValue());
            }

            return $ret;
        }

        return [];
    }

    /**
     * @param SearchQuery $q
     * @return BaseListRequest
     * @throws \Exception
     */
    public function addQuery(SearchQuery $q)
    {
        $validSearchQueries = $this->getValidSearchQueries();
        if (!in_array(get_class($q), $validSearchQueries)) {
            throw new \Exception("Invalid Search Query Provided");
        }
        $this->queries[] = $q;
        return $this;
    }

    protected function _buildUrl()
    {
        list($base, $urlParams) = parent::_buildUrl();

        if ($this->method == BaseRequest::HTTP_METHOD_POST) {
            $base .= "/search";

            return [$base, $urlParams];
        }

        if (!empty($this->queries)) {
            $base .= "/search";
            foreach ($this->queries as $q) {
                $value = $q->getValue();
                if (is_array($value)) {
                    $value = implode(",", $value);
                } else {
                    $value = $q->getValue();
                }
                $urlParams[] = $q->getKey() . "=" . urlencode($value);
            }
        }

        if (null !== $this->sortBy) {
            $urlParams[] = "sort=" . urlencode($this->sortBy);
        }

        return [$base, $urlParams];
    }

    /**
     * Returns a list of class names that are valid SearchQuery classes
     * @return string[]
     */
    protected function getValidSearchQueries()
    {
        return [
            'vdmcworld\api\request\parameter\IdQuery',
            'vdmcworld\api\request\parameter\NameQuery',
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        $validSearchQueries = $this->getValidSearchQueries();
        foreach ($this->queries as $q) {
            $qClass = get_class($q);
            if (!in_array($qClass, $validSearchQueries)) {
                throw new \Exception("Invalid Search Query Provided");
            }
        }
        return true;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return void
     */
    public function setMethod($method)
    {
        if (in_array($method, $this->allowedMethods)) {
            $this->method = $method;
        }
    }

    /**
     * @param Api $api
     * @return mixed
     */
    public function fetchData(Api $api)
    {
        $response = parent::fetchData($api);
        if (null !== $response) {
            return $response;
        }
        return [];
    }

    abstract protected function getItemModel();

    public function parseResponseObject($json)
    {
        $instance = new BaseListResponse();
        $instance->setData($this->_parsResponseData($instance, $json));
        $instance->parseJsonMeta($json);
        return $instance;
    }

    protected function _buildFromJson($json)
    {
        $className = '\vdmcworld\api\model\\' . $this->getItemModel();
        return $className::fromJson($json);
    }
}
