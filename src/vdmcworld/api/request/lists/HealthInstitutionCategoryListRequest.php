<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\HealthInstitutionCategory;

/**
 * Class HealthInstitutionCategoryListRequest
 * @package vdmcworld\api\request\lists
 * @method HealthInstitutionCategory[] fetchData(Api $api)
 * @method HealthInstitutionCategory[] fetchAllData(Api $api)
 */
class HealthInstitutionCategoryListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/health-institution-categories";
    }

    protected function getItemModel()
    {
        return "HealthInstitutionCategory";
    }
}
