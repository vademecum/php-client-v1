<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\ReportType;

/**
 * Class ReportTypeListRequest
 * @package vdmcworld\api\request\lists
 * @method ReportType[] fetchData(Api $api)
 * @method ReportType[] fetchAllData(Api $api)
 */
class ReportTypeListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/report-types";
    }

    protected function getItemModel()
    {
        return "ReportType";
    }
}