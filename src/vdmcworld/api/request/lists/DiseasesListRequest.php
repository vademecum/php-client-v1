<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Disease;

/**
 * Class InteractionRequest
 * @package vdmcworld\api\request
 * @method Disease[] fetchData(Api $api)
 * @method Disease[] fetchAllData(Api $api)
 */
class DiseasesListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/diseases";
    }

    protected function getItemModel()
    {
        return "Disease";
    }
}