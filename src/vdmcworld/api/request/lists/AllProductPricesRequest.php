<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\AllProductPrice;

/**
 * Class AllProductPricesRequest
 * @package vdmcworld\api\request
 * @method AllProductPrice[] fetchData(Api $api)
 * @method AllProductPrice[] fetchAllData(Api $api)
 */
class AllProductPricesRequest extends ProductListRequest
{
    protected function getItemModel()
    {
        return "AllProductPrice";
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    protected function getBaseEndpoint()
    {
        return "/all-product-prices";
    }
}