<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\LactationProductWarning;

/**
 * Class LactationProductWarningListRequest
 * @package vdmcworld\api\request\lists
 * @method LactationProductWarning[] fetchData(Api $api)
 * @method LactationProductWarning[] fetchAllData(Api $api)
 */
class LactationProductWarningListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/lactation-product-warnings";
    }

    protected function getItemModel()
    {
        return "LactationProductWarning";
    }

    protected function getValidSearchQueries()
    {
        return [
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\NameQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\PrescriptionTypeQuery',
            'vdmcworld\api\request\parameter\OnMarketQuery',
            'vdmcworld\api\request\parameter\IsReimbursementQuery',
            'vdmcworld\api\request\parameter\CompanyQuery',
            'vdmcworld\api\request\parameter\TermQuery',
        ];
    }
}