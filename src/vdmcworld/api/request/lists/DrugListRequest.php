<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Drug;

/**
 * Class DrugListRequest
 * @package vdmcworld\api\request\lists
 * @method Drug[] fetchData(Api $api)
 * @method Drug[] fetchAllData(Api $api)
 */
class DrugListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/drugs";
    }

    protected function getItemModel()
    {
        return "Drug";
    }
}