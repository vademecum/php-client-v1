<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\FuturePrice;

/**
 * Class AllProductFuturePricesListRequest
 * @package vdmcworld\api\request\lists
 * @method FuturePrice[] fetchData(Api $api)
 * @method FuturePrice[] fetchAllData(Api $api)
 */
class AllProductFuturePricesListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/all-product-future-prices";
    }

    protected function getItemModel()
    {
        return "FuturePrice";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\TypeQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\CompanyQuery',
            'vdmcworld\api\request\parameter\StatusQuery',
            'vdmcworld\api\request\parameter\ChangeQuery',
            'vdmcworld\api\request\parameter\DiffQuery',
            'vdmcworld\api\request\parameter\RetailQuery',
            'vdmcworld\api\request\parameter\PreviousRetailQuery',
            'vdmcworld\api\request\parameter\PrescriptionTypeQuery',
            'vdmcworld\api\request\parameter\DatetimeQuery',
            'vdmcworld\api\request\parameter\OnMarketQuery',
            'vdmcworld\api\request\parameter\IsReimbursementQuery',
        ]);
    }

}