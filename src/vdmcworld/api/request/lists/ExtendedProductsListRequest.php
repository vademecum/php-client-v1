<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\Product;

/**
 * Class ExtendedProductsListRequest
 * @package vdmcworld\api\request\lists
 * @method Product[] fetchData(Api $api)
 * @method Product[] fetchAllData(Api $api)
 */
class ExtendedProductsListRequest extends ProductListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/extended-products";
    }

    protected function getItemModel()
    {
        return "Product";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\ProductGroupQuery',
            'vdmcworld\api\request\parameter\SortQuery',
        ]);
    }
}