<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\AromatherapyPurposeCategory;

/**
 * Class AromatherapyPurposeListRequest
 * @package vdmcworld\api\request\lists
 * @method AromatherapyPurposeCategory[] fetchData(Api $api)
 * @method AromatherapyPurposeCategory[] fetchAllData(Api $api)
 */
class AromatherapyPurposeListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/aromatherapy-purposes";
    }

    protected function getItemModel()
    {
        return "AromatherapyPurposeCategory";
    }
}