<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\GenericStatusType;

/**
 * Class GenericStatusTypeListRequest
 * @package vdmcworld\api\request\lists
 * @method GenericStatusType[] fetchData(Api $api)
 * @method GenericStatusType[] fetchAllData(Api $api)
 */
class GenericStatusTypeListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/generic-status-types";
    }

    protected function getItemModel()
    {
        return "GenericStatusType";
    }
}