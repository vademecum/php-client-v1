<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Symptom;

/**
 * Class SymptomListRequest
 * @package vdmcworld\api\request\lists
 * @method Symptom[] fetchData(Api $api)
 * @method Symptom[] fetchAllData(Api $api)
 */
class SymptomListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/symptoms";
    }

    protected function getItemModel()
    {
        return "Symptom";
    }
}