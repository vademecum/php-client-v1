<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\ProductStatus;

/**
 * Class ProductStatusListRequest
 * @package vdmcworld\api\request\lists
 * @method ProductStatus[] fetchData(Api $api)
 * @method ProductStatus[] fetchAllData(Api $api)
 */
class ProductStatusListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/product-statuses";
    }

    protected function getItemModel()
    {
        return "ProductStatus";
    }
}