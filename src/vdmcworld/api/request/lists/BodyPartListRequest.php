<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\BodyPart;

/**
 * Class BodyPartListRequest
 * @package vdmcworld\api\request\lists
 * @method BodyPart[] fetchData(Api $api)
 */
class BodyPartListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/body-parts";
    }

    protected function getItemModel()
    {
        return "BodyPart";
    }
}