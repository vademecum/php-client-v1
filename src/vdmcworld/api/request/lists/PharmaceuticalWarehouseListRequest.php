<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\PharmaceuticalWarehouse;

/**
 * Class PharmaceuticalWarehouseRequest
 * @package vdmcworld\api\request\lists
 * @method PharmaceuticalWarehouse[] fetchData(Api $api)
 * @method PharmaceuticalWarehouse[] fetchAllData(Api $api)
 */
class PharmaceuticalWarehouseListRequest extends BaseListRequest
{
    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BeginsWithQuery',
            'vdmcworld\api\request\parameter\CityQuery',
            'vdmcworld\api\request\parameter\DistrictQuery'
        ]);
    }

    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/pharmaceutical-warehouses";
    }

    protected function getItemModel()
    {
        return "PharmaceuticalWarehouse";
    }
}