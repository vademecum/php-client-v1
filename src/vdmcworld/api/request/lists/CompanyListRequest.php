<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Company;

/**
 * Class CompanyListRequest
 * @package vdmcworld\api\request\lists
 * @method Company[] fetchData(Api $api)
 * @method Company[] fetchAllData(Api $api)
 */
class CompanyListRequest extends BaseListRequest
{
    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BeginsWithQuery',
            'vdmcworld\api\request\parameter\TypesQuery',
            'vdmcworld\api\request\parameter\CityQuery',
        ]);
    }

    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/companies";
    }

    protected function getItemModel()
    {
        return "Company";
    }
}