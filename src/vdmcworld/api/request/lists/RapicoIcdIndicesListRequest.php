<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\RapicoIcd;

/**
 * Class RapicoIcdIndicesListRequest
 * @package vdmcworld\api\request\lists
 * @method RapicoIcd[] fetchData(Api $api)
 */
class RapicoIcdIndicesListRequest extends BaseListRequest
{
    /**
     * @var int
     */
    private $group;

    /**
     * @var int
     */
    private $diagnosisCode;

    /**
     * @return int
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param int $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return int
     */
    public function getDiagnosisCode()
    {
        return $this->diagnosisCode;
    }

    /**
     * @param int $diagnosisCode
     */
    public function setDiagnosisCode($diagnosisCode)
    {
        $this->diagnosisCode = $diagnosisCode;
    }

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/rapico-icd-indices";
    }

    /**
     * @return string
     */
    protected function getItemModel()
    {
        return "RapicoIcd";
    }

    /**
     * @return array|string[]
     */
    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\TermQuery',
            'vdmcworld\api\request\parameter\ProductGroupQuery',
            'vdmcworld\api\request\parameter\DiagnosisCodeQuery',
            'vdmcworld\api\request\parameter\SgkEqCodeQuery',

        ]);
    }
}
