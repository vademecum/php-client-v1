<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\Usp;

/**
 * Class UspIndexListRequest
 * @package vdmcworld\api\request\lists
 * @method Usp[] fetchData(Api $api)
 * @method Usp[] fetchAllData(Api $api)
 */
class UspListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/usp-classes";
    }

    protected function getItemModel()
    {
        return "Usp";
    }
}