<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Product;

/**
 * Class ProductListRequest
 * @package vdmcworld\api\request\lists
 * @method Product[] fetchData(Api $api)
 * @method Product[] fetchAllData(Api $api)
 */
class ProductListRequest extends BaseListRequest
{
    /**
     * @var \DateTime
     */
    protected $updatedAt = null;

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/products";
    }

    protected function getItemModel()
    {
        return "Product";
    }

    protected function _buildUrl()
    {
        list($base, $urlParams) = parent::_buildUrl();

        if ($this->updatedAt !== null) {
            $urlParams[] = "updated-at=" . $this->updatedAt->getTimestamp();
        }

        return [$base, $urlParams];
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\PublicNumberQuery',
            'vdmcworld\api\request\parameter\BeginsWithQuery',
            'vdmcworld\api\request\parameter\SortQuery',
        ]);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if ($this->updatedAt !== null && !$this->updatedAt instanceof \DateTime) {
            return false;
        }
        return true;
    }
}