<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Sport;

/**
 * Class SportListRequest
 * @package vdmcworld\api\request\lists
 * @method Sport[] fetchData(Api $api)
 * @method Sport[] fetchAllData(Api $api)
 */
class SportListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/sports";
    }

    protected function getItemModel()
    {
        return "Sport";
    }
}