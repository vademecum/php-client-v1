<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\DerangementExcipient;

/**
 * Class DerangementExcipientListRequest
 * @package vdmcworld\api\request\lists
 * @method DerangementExcipient[] fetchData(Api $api)
 * @method DerangementExcipient[] fetchAllData(Api $api)
 */
class DerangementExcipientListRequest extends BaseDerangementListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/derangement-excipients";
    }

    protected function getItemModel()
    {
        return "DerangementExcipient";
    }
}
