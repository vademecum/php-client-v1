<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\DetailedProductInfo;

/**
 * Class IndicationCompatibilityProductListRequest
 * @package vdmcworld\api\request\lists
 * @method DetailedProductInfo[] fetchData(Api $api)
 * @method DetailedProductInfo[] fetchAllData(Api $api)
 */
class IndicationCompatibilityProductListRequest extends BaseListRequest
{

    protected function getItemModel()
    {
        return "DetailedProductInfo";
    }

    protected function getBaseEndpoint()
    {
        return "/indication-compatibility-products";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\NameQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\PrescriptionTypeQuery',
            'vdmcworld\api\request\parameter\OnMarketQuery',
            'vdmcworld\api\request\parameter\IsReimbursementQuery',
            'vdmcworld\api\request\parameter\CompanyQuery',
            'vdmcworld\api\request\parameter\TermQuery',
        ]);
    }
}