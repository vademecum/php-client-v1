<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\Product;

/**
 * Class DailyTreatmentProductListRequest
 * @package vdmcworld\api\request\lists
 * @method Product[] fetchData(Api $api)
 * @method Product[] fetchAllData(Api $api)
 */
class EhuApprovalProductListRequest extends BaseListRequest
{

    protected function getItemModel()
    {
        return "Product";
    }

    protected function getBaseEndpoint()
    {
        return "/ehu-approval-products";
    }

    protected function getValidSearchQueries()
    {
        return array_merge(parent::getValidSearchQueries(), [
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\NameQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\OnMarketQuery',
            'vdmcworld\api\request\parameter\IsReimbursementQuery',
            'vdmcworld\api\request\parameter\CompanyQuery',
            'vdmcworld\api\request\parameter\EhuApprovalQuery',
            'vdmcworld\api\request\parameter\TermQuery',
        ]);
    }
}