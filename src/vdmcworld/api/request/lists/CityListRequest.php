<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\City;

/**
 * Class CityListRequest
 * @package vdmcworld\api\request\lists
 * @method City[] fetchData(Api $api)
 * @method City[] fetchAllData(Api $api)
 */
class CityListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/cities";
    }

    protected function getItemModel()
    {
        return "City";
    }
}