<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\DerangementSolvent;

/**
 * Class DerangementSolventListRequest
 * @package vdmcworld\api\request\lists
 * @method DerangementSolvent[] fetchData(Api $api)
 * @method DerangementSolvent[] fetchAllData(Api $api)
 */
class DerangementSolventListRequest extends BaseDerangementListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/derangement-solvents";
    }

    protected function getItemModel()
    {
        return "DerangementSolvent";
    }
}
