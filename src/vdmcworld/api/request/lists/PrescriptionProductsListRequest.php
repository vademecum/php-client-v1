<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\Product;

/**
 * Class PrescriptionProductsListRequest
 * @package vdmcworld\api\request\lists
 * @method Product[] fetchData(Api $api)
 * @method Product[] fetchAllData(Api $api)
 */
class PrescriptionProductsListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/prescription-products";
    }

    protected function getItemModel()
    {
        return "Product";
    }

    public function isValid()
    {
        parent::isValid();
        if (count($this->queries) < 1) {
            throw new \Exception("You have to provide a search query to make this request");
        }
        $validValues = [3, 4, 5, 6, 7, 9, 10];

        $typeQuery = null;
        foreach ($this->queries as $query) {
            if ($query instanceof \vdmcworld\api\request\parameter\TypeQuery) {
                $typeQuery = $query;
                break;
            }
        }

        if ($typeQuery) {
            $qVal = $typeQuery->getValue();
            if (!in_array($qVal[0], $validValues)) {
                throw new \Exception("You can only set one for these values: " . implode(",", $validValues));
            }
        }
    }

    protected function getValidSearchQueries()
    {
        return [
            'vdmcworld\api\request\parameter\TypeQuery',
            'vdmcworld\api\request\parameter\SortQuery',
            'vdmcworld\api\request\parameter\BarcodeQuery',
            'vdmcworld\api\request\parameter\NameQuery',
            'vdmcworld\api\request\parameter\PrescriptionTypeQuery',
            'vdmcworld\api\request\parameter\OnMarketQuery',
            'vdmcworld\api\request\parameter\IsReimbursementQuery',
            'vdmcworld\api\request\parameter\CompanyQuery',
            'vdmcworld\api\request\parameter\TermQuery',
        ];
    }
}