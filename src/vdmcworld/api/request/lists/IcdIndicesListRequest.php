<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\Icd;

/**
 * Class IcdIndicesListRequest
 * @package vdmcworld\api\request\lists
 * @method Icd[] fetchData(Api $api)
 * @method Icd[] fetchAllData(Api $api)
 */
class IcdIndicesListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/icd-indices";
    }

    protected function getItemModel()
    {
        return "Icd";
    }
}