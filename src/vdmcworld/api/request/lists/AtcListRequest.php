<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\Atc;

/**
 * Class AtcIndexListRequest
 * @package vdmcworld\api\request\lists
 * @method Atc[] fetchData(Api $api)
 * @method Atc[] fetchAllData(Api $api)
 */
class AtcListRequest extends BaseListRequest
{

    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/atc-indices";
    }

    protected function getItemModel()
    {
        return "Atc";
    }
}