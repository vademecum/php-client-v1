<?php

namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\AllProductCard;

/**
 * Class AllProductCardsListRequest
 * @package vdmcworld\api\request\lists
 * @method AllProductCard[] fetchData(Api $api)
 * @method AllProductCard[] fetchAllData(Api $api)
 */
class AllProductCardsListRequest extends ProductListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/all-product-cards";
    }

    protected function getItemModel()
    {
        return "AllProductCard";
    }
}