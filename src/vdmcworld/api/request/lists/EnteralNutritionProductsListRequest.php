<?php


namespace vdmcworld\api\request\lists;

use vdmcworld\Api;
use vdmcworld\api\model\Product;

/**
 * Class EnteralNutritionProductsListRequest
 * @package vdmcworld\api\request\lists
 * @method Product[] fetchData(Api $api)
 * @method Product[] fetchAllData(Api $api)
 */
class EnteralNutritionProductsListRequest extends ProductListRequest
{
    /**
     * @return string
     */
    protected function getBaseEndpoint()
    {
        return "/enteral-nutrition-products";
    }

    protected function getItemModel()
    {
        return "Product";
    }
}