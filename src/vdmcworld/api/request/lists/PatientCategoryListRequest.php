<?php

namespace vdmcworld\api\request\lists;


use vdmcworld\Api;
use vdmcworld\api\model\PatientCategory;

/**
 * Class PatientCategoryListRequest
 * @package vdmcworld\api\request\lists
 * @method PatientCategory[] fetchData(Api $api)
 * @method PatientCategory[] fetchAllData(Api $api)
 */
class PatientCategoryListRequest extends BaseListRequest
{
    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/patient-categories";
    }

    protected function getItemModel()
    {
        return "PatientCategory";
    }
}