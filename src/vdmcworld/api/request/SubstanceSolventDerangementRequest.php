<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\SubstancePhDerangement;
use vdmcworld\api\model\SubstanceSolventDerangement;
use vdmcworld\api\response\BaseResponse;

/**
 * @package vdmcworld\api\request
 * @method SubstanceSolventDerangement fetchData(Api $api)
 */
class SubstanceSolventDerangementRequest extends SubstanceBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SubstanceSolventDerangement
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceSolventDerangement::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/substance-solvent-derangement";
    }
}
