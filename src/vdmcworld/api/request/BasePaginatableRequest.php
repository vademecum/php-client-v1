<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\Model;
use vdmcworld\api\response\BasePaginatableResponse;

abstract class BasePaginatableRequest extends BaseRequest
{
    /**
     * @var int
     */
    protected $page = null;

    /**
     * @var int
     */
    protected $perPage = null;

    /**
     * @param int $page
     * @return BasePaginatableRequest
     */
    public function setPage($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @param int $perPage
     * @return BasePaginatableRequest
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    abstract protected function getBaseEndpoint();

    /**
     * @return string
     */
    public function getEndpoint()
    {
        list($base, $urlParams) = $this->_buildUrl();

        if (!empty($urlParams)) {
            $base .= "?" . implode("&", $urlParams);
        }

        return $base;
    }

    protected function _buildUrl()
    {
        $base = $this->getBaseEndpoint();
        $urlParams = [];

        if (null !== $this->page) {
            $urlParams[] = "page=" . urlencode($this->page);
        }

        if (null !== $this->perPage) {
            $urlParams[] = "per-page=" . urlencode($this->perPage);
        }
        return [$base, $urlParams];
    }

    /**
     * @param array $json
     * @return Model
     */
    abstract protected function _buildFromJson($json);

    public function parseResponseObject($json)
    {
        $instance = new BasePaginatableResponse();
        $instance->setData($this->_parsResponseData($instance, $json));
        $instance->parseJsonMeta($json);
        return $instance;
    }

    protected function _parsResponseData($response, $json)
    {
        $data = [];
        foreach ($json->data as $p) {
            $data[] = $this->_buildFromJson($p);
        }

        return $data;
    }

    /**
     * @param Api $api
     * @return array
     * @throws \Exception
     */
    public function fetchAllData(Api $api)
    {
        $currentPage = 1;
        $totalPages = 1;
        $items = [];
        do {
            $this->setPage($currentPage);
            $response = $api->send($this)->getResponse();
            if (!$response) {
                throw new \Exception("Cannot Fetch response Data");
            }
            $totalPages = $response->getTotalPages();
            $currentPage += 1;
            foreach ($response->getData() as $r) {
                $items[] = $r;
            }
        } while ($totalPages >= $currentPage);

        return $items;
    }
}