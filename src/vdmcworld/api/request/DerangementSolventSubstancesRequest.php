<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\DerangementContainer;
use vdmcworld\api\model\DerangementContainerSubstances;
use vdmcworld\api\model\DerangementSolventSubstances;
use vdmcworld\api\model\SubstanceContainerDerangement;
use vdmcworld\api\response\BaseResponse;

/**
 * @package vdmcworld\api\request
 * @method DerangementSolventSubstances fetchData(Api $api)
 */
class DerangementSolventSubstancesRequest extends BaseRequest
{
    /**
     * @var \vdmcworld\api\request\parameter\DerangementSolvent
     */
    protected $solvent = null;

    /**
     * @return \vdmcworld\api\request\parameter\DerangementSolvent
     */
    public function getSolvent()
    {
        return $this->solvent;
    }

    /**
     * @param \vdmcworld\api\request\parameter\DerangementSolvent $solvent
     * @return $this
     */
    public function setSolvent($solvent)
    {
        $this->solvent = $solvent;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return DerangementSolventSubstances
     */
    protected function _parsResponseData($response, $json)
    {
        return DerangementSolventSubstances::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/derangement-solvent-substances";
    }

    public function getEndpoint()
    {
        return $this->baseEndpoint() . "/" . $this->solvent->getId();
    }

    public function toParams()
    {
        return [];
    }

    public function isValid()
    {
        if (null !== $this->solvent && null !== $this->solvent->getId()) {
            return true;
        }
        throw new \Exception("Solvent and it'is ID should be set to make this request");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}
