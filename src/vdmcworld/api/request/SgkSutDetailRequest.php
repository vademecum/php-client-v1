<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\SgkSutDetail;
use vdmcworld\api\model\SgkSutIndex;
use vdmcworld\api\request\parameter\SutId;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SgkSutIndexRequest
 * @package vdmcworld\api\request
 * @method SgkSutDetail fetchData(Api $api)
 */
class SgkSutDetailRequest extends BaseRequest
{
    /**
     * @var SutId
     */
    private $sutId;

    /**
     * @return SutId
     */
    public function getSutId()
    {
        return $this->sutId;
    }

    /**
     * @param SutId $sutId
     */
    public function setSutId($sutId)
    {
        $this->sutId = $sutId;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SgkSutDetail
     */
    protected function _parsResponseData($response, $json)
    {
        return SgkSutDetail::fromJson($json->data);
    }

    public function getEndpoint()
    {
        return "/sgk-sut-details/" . $this->sutId->getId();
    }

    public function toParams()
    {
        return [];
    }

    public function isValid()
    {
        if (null == $this->sutId || null == $this->sutId->getId()) {
            throw new \Exception("SutId should be set to make this request");
        }
        return true;
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}