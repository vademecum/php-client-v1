<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductCautionCard;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductCautionCardRequest
 * @package vdmcworld\api\request
 * @method ProductCautionCard fetchData(Api $api)
 */
class ProductCautionCardRequest extends ProductBasedBaseRequest
{

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-caution-card";
    }

    /**
     * @return BaseResponse
     * @var array $json
     */
    public function parseResponseObject($json)
    {
        $instance = new BaseResponse();
        $instance->setData(ProductCautionCard::fromJson($json->data));
        return $instance;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductCautionCard::fromJson($json);
    }
}