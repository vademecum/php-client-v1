<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\KatarProductPriceHistory;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductTitckPriceHistoryRequest
 * @package vdmcworld\api\request
 * @method ProductTitckPriceHistory fetchData(Api $api)
 */
class KatarProductPriceHistoryRequest extends ProductBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return KatarProductPriceHistory
     */
    protected function _parsResponseData($response, $json)
    {
        return KatarProductPriceHistory::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/katar/product-price-history";
    }
}