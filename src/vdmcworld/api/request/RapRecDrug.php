<?php


namespace vdmcworld\api\request;


use vdmcworld\api\Model;

class RapRecDrug implements Model
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param $json
     * @return RapRecDrug
     */
    public static function fromJson($json)
    {
        $instance = new RapRecDrug();
        if ($json->code) {
            $instance->code = $json->code;
        }
        if ($json->name) {
            $instance->name = $json->name;
        }
        return $instance;
    }
}