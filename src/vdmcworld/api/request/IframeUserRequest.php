<?php


namespace vdmcworld\api\request;


use vdmcworld\api\model\IframeUser;
use vdmcworld\api\response\BaseResponse;

class IframeUserRequest extends BaseRequest
{
    /**
     * @var string
     */
    protected $iframeToken;

    /**
     * @var string
     */
    protected $iframeIp;

    /**
     * @return string
     */
    public function getIframeToken()
    {
        return $this->iframeToken;
    }

    /**
     * @param string $iframeToken
     * @return IframeUserRequest
     */
    public function setIframeToken($iframeToken)
    {
        $this->iframeToken = $iframeToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getIframeIp()
    {
        return $this->iframeIp;
    }

    /**
     * @param string $iframeIp
     * @return IframeUserRequest
     */
    public function setIframeIp($iframeIp)
    {
        $this->iframeIp = $iframeIp;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return IframeUser::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return '/iframe-user';
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if ($this->iframeToken && $this->iframeIp) {
            return true;
        }
        throw new \Exception('Iframe Token & Ip should be set');
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

    public function getAdditionalHeaders()
    {
        return [
            'Vdmc-Iframe-Token' => $this->iframeToken,
            'Vdmc-Iframe-Ip' => $this->iframeIp,
        ];
    }
}