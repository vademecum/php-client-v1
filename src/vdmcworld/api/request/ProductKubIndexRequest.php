<?php


namespace vdmcworld\api\request;


use vdmcworld\api\model\ProductKubIndex;

class ProductKubIndexRequest extends ProductBasedBaseRequest
{

    protected function _parsResponseData($response, $json)
    {
        return ProductKubIndex::fromJson($json->data);
    }

    protected function baseEndpoint()
    {
        return "/product-kub-index";
    }
}