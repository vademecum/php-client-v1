<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductTitckPriceHistory;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductTitckPriceHistoryRequest
 * @package vdmcworld\api\request
 * @method ProductTitckPriceHistory fetchData(Api $api)
 */
class ProductTitckPriceHistoryRequest extends ProductBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductTitckPriceHistory
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductTitckPriceHistory::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-titck-price-history";
    }
}