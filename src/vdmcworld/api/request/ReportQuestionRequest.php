<?php

namespace vdmcworld\api\request;

use vdmcworld\api\model\ReportQuestion;
use vdmcworld\api\request\parameter\IcdCode;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\request\parameter\SgkEqCode;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ReportQuestionRequest
 * @method ReportQuestion[] fetchData(\vdmcworld\Api $api)
 */
class ReportQuestionRequest extends BaseRequest
{
    /**
     * @var Product[]
     */
    private $products;

    /**
     * @var IcdCode[]
     */
    private $icdCodes;


    /**
     * @var SgkEqCode
     */
    private $sgkCode;

    /**
     * @var string
     */
    private $sessionId;


    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return IcdCode[]
     */
    public function getIcdCodes()
    {
        return $this->icdCodes;
    }

    /**
     * @param IcdCode[] $icdCodes
     * @return ReportQuestion
     */
    public function setIcdCodes($icdCodes)
    {
        $this->icdCodes = $icdCodes;
        return $this;
    }

    /**
     * @return SgkEqCode
     */
    public function getSgkCode()
    {
        return $this->sgkCode;
    }

    /**
     * @param SgkEqCode $sgkCode
     */
    public function setSgkCode($sgkCode)
    {
        $this->sgkCode = $sgkCode;
    }

    /**
     * @return string
     */
    public function getsessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     */
    public function setsessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }


    /**
     * @param BaseResponse $response
     * @param array $json
     * @return \vdmcworld\api\model\ReportQuestion
     */
    protected function _parsResponseData($response, $json)
    {
        return ReportQuestion::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/report-question";
    }

    /**
     * @return array
     */
    public function toParams()
    {
        $ret = [];
        $products = [];
        if ($this->products != null) {
            foreach ($this->products as $p) {
                $products[] = $p->toParams();
            }
        }

        $icdCodes = [];
        if ($this->icdCodes != null) {
            foreach ($this->icdCodes as $i) {
                $icdCodes[] = $i->toParams();
            }
        }

        if ($products) {
            $ret["products"] = $this->jsonEncode($products);
        }
        if ($icdCodes) {
            $ret["icdcodes"] = $this->jsonEncode($icdCodes);
        }

        if ($this->sgkCode) {
            $ret["sgkEqCodes"] = $this->jsonEncode([$this->sgkCode->getCode()]);
        }
        $ret["session"] = $this->sessionId;

        return $ret;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        // TODO: Implement isValid() method.
    }
}