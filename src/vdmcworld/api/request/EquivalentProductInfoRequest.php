<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\EquivalentProductInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class EquivalentProductInfoRequest
 * @package vdmcworld\api\request
 * @method EquivalentProductInfo fetchData(Api $api)
 */
class EquivalentProductInfoRequest extends ProductBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return EquivalentProductInfo
     */
    protected function _parsResponseData($response, $json)
    {
        return EquivalentProductInfo::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/equivalent-products-info";
    }
}