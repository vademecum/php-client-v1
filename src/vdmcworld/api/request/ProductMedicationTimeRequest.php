<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductMedicationTime;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductMedicationTimeRequest
 * @package vdmcworld\api\request
 * @method ProductMedicationTime fetchData(Api $api)
 */
class ProductMedicationTimeRequest extends ProductBasedBaseRequest
{
    protected function baseEndpoint()
    {
        return "/product-medication-time";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductMedicationTime::fromJson($json->data);
    }
}