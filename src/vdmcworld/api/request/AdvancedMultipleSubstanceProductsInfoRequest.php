<?php

namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\MultipleSubstanceProductsInfo;
use vdmcworld\api\request\parameter\Substance;

/**
 * Class CityListRequest
 * @package vdmcworld\api\request\lists
 * @method MultipleSubstanceProductsInfo[] fetchData(Api $api)
 * @method MultipleSubstanceProductsInfo[] fetchAllData(Api $api)
 */
class AdvancedMultipleSubstanceProductsInfoRequest extends BaseRequest
{
    /**
     * @var Substance[]
     */
    protected $substances = null;

    /**
     * @return Substance
     */
    public function getSubstances()
    {
        return $this->substances;
    }

    /**
     * @param Substance[] $substances
     * @return \vdmcworld\api\request\AdvancedMultipleSubstanceProductsInfoRequest
     */
    public function setSubstances($substances)
    {
        $this->substances = json_encode($substances);
        return $this;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/advanced-multiple-substance-products-info";
    }

    protected function _parsResponseData($response, $json)
    {
        return MultipleSubstanceProductsInfo::fromJson($json->data);
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_POST;
    }

    public function toParams()
    {
        return ["substances" => $this->substances];
    }

    public function isValid()
    {
        if (null !== $this->substances) {
            return true;
        }
        throw new \Exception("Substances must be set");
    }

}