<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductCustomCard;
use vdmcworld\api\response\BaseResponse;

/**
 * Class CustomCardRequest
 * @package vdmcworld\api\request
 * @method ProductCustomCard fetchData(Api $api)
 */
class CustomCardRequest extends ProductBasedBaseRequest
{
    /**
     * @var string
     */
    protected $domain = null;

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     * @return CustomCardRequest
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        if ($this->domain === null) {
            return "/custom-product-card";
        }
        return "/" . $this->domain . "/product-card";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductCustomCard
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductCustomCard::fromJson($json->data);
    }
}