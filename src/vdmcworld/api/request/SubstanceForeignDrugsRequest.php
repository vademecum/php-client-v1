<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\SubstanceForeignDrugs;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SubstanceForeignDrugsRequest
 * @package vdmcworld\api\request
 * @method SubstanceForeignDrugs fetchData(Api $api)
 */
class SubstanceForeignDrugsRequest extends SubstanceBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SubstanceForeignDrugs
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceForeignDrugs::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/substance-foreign-drugs";
    }
}