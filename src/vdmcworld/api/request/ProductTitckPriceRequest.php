<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductTitckPrice;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductTitckPriceRequest
 * @package vdmcworld\api\request
 * @method ProductTitckPrice fetchData(Api $api)
 */
class ProductTitckPriceRequest extends ProductBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductTitckPrice
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductTitckPrice::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-titck-price";
    }
}