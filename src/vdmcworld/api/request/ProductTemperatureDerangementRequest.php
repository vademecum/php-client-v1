<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\ProductContainerDerangement;
use vdmcworld\api\model\ProductTemperatureDerangement;
use vdmcworld\api\model\SgkSutProductDetail;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SgkSutIndexRequest
 * @package vdmcworld\api\request
 * @method ProductTemperatureDerangement fetchData(Api $api)
 */
class ProductTemperatureDerangementRequest extends ProductBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductTemperatureDerangement
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductTemperatureDerangement::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/product-temperature-derangement";
    }
}
