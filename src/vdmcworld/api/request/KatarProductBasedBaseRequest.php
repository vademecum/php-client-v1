<?php

namespace vdmcworld\api\request;


use vdmcworld\api\request\parameter\Product;

abstract class KatarProductBasedBaseRequest extends BaseRequest
{
    /**
     * @var Product
     */
    protected $serialNo = null;

    /**
     * @return Product
     */
    public function getSerialNo()
    {
        return $this->serialNo;
    }

    /**
     * @param Product $serialNo
     */
    public function setSerialNo($serialNo)
    {
        $this->serialNo = $serialNo;
    }


    /**
     * @return string
     */
    protected abstract function baseEndpoint();

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->baseEndpoint() . "/" . $this->serialNo;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->serialNo) {
            return true;
        }
        throw new \Exception("Product and it's ID should be set to make this request");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}