<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\CompanyCard;
use vdmcworld\api\request\parameter\Company;
use vdmcworld\api\response\BaseResponse;

/**
 * Class CompanyCardRequest
 * @package vdmcworld\api\request
 * @method CompanyCard fetchData(Api $api)
 */
class AdvancedCompanyCardRequest extends BaseRequest
{
    /**
     * @var Company
     */
    protected $company;

    /**
     * @var string
     */
    protected $isReimbursement;

    /**
     * @var string
     */
    protected $onMarket;

    /**
     * @var $type
     */
    protected $type;

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return string
     */
    public function getIsReimbursement()
    {
        return $this->isReimbursement;
    }

    /**
     * @return string
     */
    public function getOnMarket()
    {
        return $this->onMarket;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @param string $isReimbursement
     */
    public function setIsReimbursement($isReimbursement)
    {
        $this->isReimbursement = $isReimbursement;
    }

    /**
     * @param string $onMarket
     */
    public function setOnMarket($onMarket)
    {
        $this->onMarket = $onMarket;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return CompanyCard
     */
    protected function _parsResponseData($response, $json)
    {
        return CompanyCard::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/advanced-company-card/" . $this->company->getId() . '?is-reimbursement=' . $this->isReimbursement . '&on-market=' . $this->onMarket . '&type=' . $this->type;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->company && null !== $this->company->getId()) {
            return true;
        }
        throw new \Exception("Company and it's id cannot be null");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}