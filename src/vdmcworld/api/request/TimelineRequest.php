<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\News;
use vdmcworld\api\model\RecallAnnouncement;
use vdmcworld\api\request\parameter\SearchQuery;
use vdmcworld\api\request\parameter\TimelineType;
use vdmcworld\api\response\BaseResponse;

/**
 * Class TimelineRequest
 * @package vdmcworld\api\request
 * @method News fetchData(Api $api)
 */
class TimelineRequest extends BasePaginatableRequest
{
    const PAGE_SIZE = 25;
    const TYPE_RECALL = 3;

    /**
     * @var TimelineType
     */
    protected $timeline = null;

    /**
     * @var SearchQuery[]
     */
    protected $queries = [];

    /**
     * @var integer
     */
    protected $id;


    /**
     * @return TimelineType
     */
    public function getTimeline()
    {
        return $this->timeline;
    }

    /**
     * @param TimelineType $timeline
     * @return TimelineRequest
     */
    public function setTimelineType($timeline)
    {
        $this->timeline = $timeline;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        $requestedId = $this->id ? "/".$this->id : null;
        return "/timeline/" . $this->timeline->getType() . $requestedId;
    }

    protected function _buildUrl()
    {
        list($base, $urlParams) = parent::_buildUrl();

        if (!empty($this->queries)) {
            foreach ($this->queries as $q) {
                $value = $q->getValue();
                if (is_array($value)) {
                    $value = implode(",", $value);
                } else {
                    $value = $q->getValue();
                }
                $urlParams[] = $q->getKey() . "=" . urlencode($value);
            }
        }

        return [$base, $urlParams];
    }

    /**
     * @param SearchQuery $q
     * @return BasePaginatableRequest
     * @throws \Exception
     */
    public function addQuery(SearchQuery $q)
    {
        $validSearchQueries = $this->getValidSearchQueries();
        if (!in_array(get_class($q), $validSearchQueries)) {
            throw new \Exception("Invalid Search Query Provided");
        }
        $this->queries[] = $q;
        return $this;
    }

    public function toParams()
    {
        return [];
    }

    public function isValid()
    {
        if (null !== $this->timeline && null !== $this->timeline->getType()) {
            return true;
        }
        throw new \Exception("Type should be set to make this request");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        if ($this->timeline->getType() == '3') {
            $data = RecallAnnouncement::fromJson($json->data);
        } else {
            $data = [];
            foreach ($json->data as $p) {
                $data[] = News::fromJson($p);
            }
        }
        return $data;
    }

    protected function _buildFromJson($json)
    {
        return;
    }

    protected function getValidSearchQueries()
    {
        return [
            'vdmcworld\api\request\parameter\StartDateQuery',
            'vdmcworld\api\request\parameter\EndDateQuery',
        ];
    }

    /**
     * @param $json
     */
    public function _parseResponseObject($json)
    {
        if (isset($json->_meta)) {
            $this->totalItems = $json->_meta->totalCount;
            $this->totalPages = $json->_meta->pageCount;
        } else {
            $this->totalItems = count($json->data);
            $this->totalPages = 1;
        }
    }
}