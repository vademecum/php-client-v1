<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductDosageInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductDosageInfoRequest
 * @package vdmcworld\api\request
 * @method ProductDosageInfo fetchData(Api $api)
 */
class ProductDosageInfoRequest extends ProductBasedBaseRequest
{

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-dosage-info";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductDosageInfo::fromJson($json->data);
    }
}