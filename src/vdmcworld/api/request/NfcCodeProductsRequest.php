<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\NfcCodeProducts;
use vdmcworld\api\model\SgkEqCodeProducts;
use vdmcworld\api\request\parameter\NfcCode;
use vdmcworld\api\request\parameter\SgkEqCode;
use vdmcworld\api\response\BaseResponse;

/**
 * Class NfcCodeProductsRequest
 * @package vdmcworld\api\request
 * @method NfcCodeProducts fetchData(Api $api)
 */
class NfcCodeProductsRequest extends BaseRequest
{
    /**
     * @var NfcCode
     */
    protected $nfcCode;

    /**
     * @return NfcCode
     */
    public function getNfcCode()
    {
        return $this->nfcCode;
    }

    /**
     * @param NfcCode $nfcCode
     * @return NfcCodeProductsRequest
     */
    public function setNfcCode($nfcCode)
    {
        $this->nfcCode = $nfcCode;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return NfcCodeProducts
     */
    protected function _parsResponseData($response, $json)
    {
        return NfcCodeProducts::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/nfc-code-products/" . $this->getNfcCode()->getCode();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->nfcCode && !empty($this->nfcCode->getCode())) {
            return true;
        }
        throw new \Exception("NFC Code cannot be empty");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}