<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\ProductContainerDerangement;
use vdmcworld\api\model\ProductPhDerangement;
use vdmcworld\api\model\SgkSutProductDetail;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SgkSutIndexRequest
 * @package vdmcworld\api\request
 * @method ProductPhDerangement fetchData(Api $api)
 */
class ProductPhDerangementRequest extends ProductBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductPhDerangement
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductPhDerangement::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/product-ph-derangement";
    }
}
