<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\SubstanceMixtures;
use vdmcworld\api\model\SubstancePhDerangement;
use vdmcworld\api\model\SubstanceSolventDerangement;
use vdmcworld\api\model\SubstanceTemperatureDerangement;
use vdmcworld\api\response\BaseResponse;

/**
 * @package vdmcworld\api\request
 * @method SubstanceMixtures fetchData(Api $api)
 */
class SubstanceMixturesRequest extends SubstanceBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SubstanceMixtures
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceMixtures::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/substance-mixtures";
    }
}
