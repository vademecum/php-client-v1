<?php

namespace vdmcworld\api\request;


use vdmcworld\api\Request;
use vdmcworld\api\Response;

class CustomRequest extends BaseRequest
{
    /**
     * @var string
     */
    private $endPointUrl;

    /**
     * @var array
     */
    private $params;

    /**
     * @var Response
     */
    private $responseInstance;

    public function __construct($endPointUrl, $params, $responseInstance)
    {
        $this->endPointUrl = $endPointUrl;
        $this->params = $params;
        $this->responseInstance = $responseInstance;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endPointUrl;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return $this->params;
    }

    /**
     * @return Response
     */
    public function getResponseClass()
    {
        return $this->responseInstance;
    }
}