<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\SubstancePharmacologicalClass;
use vdmcworld\api\response\BaseResponse;

/**
 * @package vdmcworld\api\request
 * @method SubstancePharmacologicalClassRequest fetchData(Api $api)
 */
class SubstancePharmacologicalClassRequest extends SubstanceBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SubstancePharmacologicalClass
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstancePharmacologicalClass::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/substance-pharmacological-class";
    }
}
