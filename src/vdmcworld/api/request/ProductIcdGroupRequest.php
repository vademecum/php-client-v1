<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductIcdGroup;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductIcdGroupRequest
 * @package vdmcworld\api\request
 * @method ProductIcdGroup fetchData(Api $api)
 */
class ProductIcdGroupRequest extends ProductBasedBaseRequest
{
    protected function baseEndpoint()
    {
        return "/product-icd-group";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductIcdGroup::fromJson($json->data);
    }
}