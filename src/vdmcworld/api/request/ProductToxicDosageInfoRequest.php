<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductToxicDosageInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductToxicDosageInfoRequest
 * @package vdmcworld\api\request
 * @method ProductToxicDosageInfo fetchData(Api $api)
 */
class ProductToxicDosageInfoRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-toxic-dosage-info";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductToxicDosageInfo::fromJson($json->data);
    }
}