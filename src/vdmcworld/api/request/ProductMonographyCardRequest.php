<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductMonographyCard;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductMonographyCardRequest
 * @package vdmcworld\api\request
 * @method ProductMonographyCard fetchData(Api $api)
 */
class ProductMonographyCardRequest extends ProductBasedBaseRequest
{
    protected function baseEndpoint()
    {
        return "/product-monography-card";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductMonographyCard::fromJson($json->data);
    }
}