<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\ProductContainerDerangement;
use vdmcworld\api\model\ProductSolventDerangement;
use vdmcworld\api\model\SgkSutProductDetail;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SgkSutIndexRequest
 * @package vdmcworld\api\request
 * @method ProductSolventDerangement fetchData(Api $api)
 */
class ProductSolventDerangementRequest extends ProductBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductSolventDerangement
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductSolventDerangement::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/product-solvent-derangement";
    }
}
