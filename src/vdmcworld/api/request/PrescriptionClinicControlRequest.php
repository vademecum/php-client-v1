<?php

namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\PrescriptionClinicControl;
use vdmcworld\api\Request;
use vdmcworld\api\request\parameter\Disease;
use vdmcworld\api\request\parameter\DoctorInfo;
use vdmcworld\api\request\parameter\IcdCode;
use vdmcworld\api\request\parameter\Nutrition;
use vdmcworld\api\request\parameter\PatientCharacteristics;
use vdmcworld\api\request\parameter\PrescriptionInfo;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\request\parameter\Symptom;
use vdmcworld\api\response\BaseResponse;

/**
 * Class PrescriptionClinicControlRequest
 * @package vdmcworld\api\request
 * @method PrescriptionClinicControl fetchData(Api $api)
 */
class PrescriptionClinicControlRequest extends BaseRequest
{
    /**
     * @var DoctorInfo
     */
    protected $doctorInfo = null;
    /**
     * @var PrescriptionInfo
     */
    protected $prescriptionInfo = null;
    /**
     * @var Product[]
     */
    protected $products = [];
    /**
     * @var Nutrition[]
     */
    protected $nutritions = [];
    /**
     * @var Symptom[]
     */
    protected $symptoms = [];
    /**
     * @var PatientCharacteristics
     */
    protected $patientCharacteristics;

    /**
     * @var IcdCode[]
     */
    protected $icdCodes = [];

    /**
     * @var Disease[]
     */
    protected $diseases = [];

    /**
     * @var string
     */
    protected $facilityCode = null;

    /**
     * @var integer
     */
    protected $interactionColor = null;

    /**
     * @return DoctorInfo
     */
    public function getDoctorInfo()
    {
        return $this->doctorInfo;
    }

    /**
     * @param DoctorInfo $doctorInfo
     * @return PrescriptionClinicControlRequest
     */
    public function setDoctorInfo(DoctorInfo $doctorInfo)
    {
        $this->doctorInfo = $doctorInfo;
        return $this;
    }

    /**
     * @return PrescriptionInfo
     */
    public function getPrescriptionInfo()
    {
        return $this->prescriptionInfo;
    }

    /**
     * @param PrescriptionInfo $prescriptionInfo
     * @return PrescriptionClinicControlRequest
     */
    public function setPrescriptionInfo(PrescriptionInfo $prescriptionInfo)
    {
        $this->prescriptionInfo = $prescriptionInfo;
        return $this;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     * @return PrescriptionClinicControlRequest
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return Nutrition[]
     */
    public function getNutritions()
    {
        return $this->nutritions;
    }

    /**
     * @param Nutrition[] $nutritions
     * @return PrescriptionClinicControlRequest
     */
    public function setNutritions($nutritions)
    {
        $this->nutritions = $nutritions;
        return $this;
    }

    /**
     * @return Symptom[]
     */
    public function getSymptoms()
    {
        return $this->symptoms;
    }

    /**
     * @param Symptom[] $symptoms
     * @return PrescriptionClinicControlRequest
     */
    public function setSymptoms($symptoms)
    {
        $this->symptoms = $symptoms;
        return $this;
    }

    /**
     * @return PatientCharacteristics
     */
    public function getPatientCharacteristics()
    {
        return $this->patientCharacteristics;
    }

    /**
     * @param PatientCharacteristics $patientCharacteristics
     * @return PrescriptionClinicControlRequest
     */
    public function setPatientCharacteristics(PatientCharacteristics $patientCharacteristics)
    {
        $this->patientCharacteristics = $patientCharacteristics;
        return $this;
    }

    /**
     * @return IcdCode[]
     */
    public function getIcdCodes()
    {
        return $this->icdCodes;
    }

    /**
     * @param array $icdCodes []
     * @return PrescriptionClinicControlRequest
     */
    public function setIcdCodes($icdCodes)
    {
        $this->icdCodes = $icdCodes;
        return $this;
    }

    /**
     * @return Disease[]
     */
    public function getDiseases()
    {
        return $this->diseases;
    }

    /**
     * @param Disease[] $diseases
     * @return PrescriptionClinicControlRequest
     */
    public function setDiseases($diseases)
    {
        $this->diseases = $diseases;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacilityCode()
    {
        return $this->facilityCode;
    }

    /**
     * @param string $facilityCode
     * @return PrescriptionClinicControlRequest
     */
    public function setFacilityCode($facilityCode)
    {
        $this->facilityCode = $facilityCode;
        return $this;
    }

    /**
     * @return integer $interactionColor
     */
    public function getInteractionColor()
    {
        return $this->interactionColor;
    }

    /**
     * @param integer $interactionColor
     * @return PrescriptionClinicControlRequest
     */
    public function setInteractionColor($interactionColor)
    {
        $this->interactionColor = $interactionColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/prescription-clinic-control";
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (empty($this->products)) {
            throw new \Exception("Products cannot be empty");
        }

        if (null === $this->doctorInfo) {
            throw new \Exception("DoctorInfo cannot be empty");
        }

        if (null === $this->doctorInfo->getUniqueId()) {
            throw new \Exception("DoctorInfo uniqueId cannot be empty");
        }

        if (null === $this->facilityCode) {
            throw new \Exception("Facility Code cannot be empty");
        }

        return true;
    }


    /**
     * @return array
     */
    public function toParams()
    {
        // TODO: I did not like this structure, let's find a better way later...
        $symptoms = [];
        foreach ($this->symptoms as $s) {
            $symptoms[] = $s->toParams();
        }
        $products = [];
        foreach ($this->products as $p) {
            $products[] = $p->toParams();
        }
        $nutritions = [];
        foreach ($this->nutritions as $n) {
            $nutritions[] = $n->toParams();
        }
        $icdCodes = [];
        foreach ($this->icdCodes as $i) {
            $icdCodes[] = $i->toParams();
        }
        $diseases = [];
        foreach ($this->diseases as $d) {
            $diseases[] = $d->toParams();
        }

        $ret = [];
        if ($this->doctorInfo) {
            $ret["doctor-info"] = $this->jsonEncode($this->doctorInfo->toParams());
        }
        if ($this->prescriptionInfo) {
            $ret["prescription-info"] = $this->jsonEncode($this->prescriptionInfo->toParams());
        }
        if ($this->patientCharacteristics) {
            $ret["patient-characteristics"] = $this->jsonEncode($this->patientCharacteristics->toParams());
        }
        if ($symptoms) {
            $ret["symptoms"] = $this->jsonEncode($symptoms);
        }
        if ($products) {
            $ret["products"] = $this->jsonEncode($products);
        }
        if ($nutritions) {
            $ret["nutritions"] = $this->jsonEncode($nutritions);
        }
        if ($icdCodes) {
            $ret["icdcodes"] = $this->jsonEncode($icdCodes);
        }
        if ($diseases) {
            $ret["diseases"] = $this->jsonEncode($diseases);
        }

        $ret["interaction-color"] = $this->interactionColor;

        $ret["facility-code"] = $this->facilityCode;

        return $ret;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return PrescriptionClinicControl
     */
    protected function _parsResponseData($response, $json)
    {
        return PrescriptionClinicControl::fromJson($json->data);
    }
}
