<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductIndicationInfo;
use vdmcworld\api\model\ProductTermsOfUseInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductTermsOfUseInfoRequest
 * @package vdmcworld\api\request
 * @method ProductTermsOfUseInfo fetchData(Api $api)
 */
class ProductTermOfUseInfoRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-terms-of-use-info";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductTermsOfUseInfo::fromJson($json->data);
    }
}