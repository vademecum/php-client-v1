<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductCustomCard;
use vdmcworld\api\model\SubstanceCustomCard;
use vdmcworld\api\response\BaseResponse;

/**
 * Class CustomSubstanceCardRequest
 * @package vdmcworld\api\request
 * @method ProductCustomCard fetchData(Api $api)
 */
class CustomSubstanceCardRequest extends SubstanceBasedBaseRequest
{
    /**
     * @var string
     */
    protected $domain = null;

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     * @return CustomCardRequest
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        if ($this->domain === null) {
            return "/custom-substance-card";
        }
        return "/" . $this->domain . "/substance-card";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SubstanceCustomCard
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceCustomCard::fromJson($json->data);
    }
}