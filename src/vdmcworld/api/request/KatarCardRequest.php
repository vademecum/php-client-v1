<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductKatarCard;
use vdmcworld\api\response\BaseResponse;

/**
 * Class CustomCardRequest
 * @package vdmcworld\api\request
 * @method ProductKatarCard fetchData(Api $api)
 */
class KatarCardRequest extends KatarProductBasedBaseRequest
{
    /**
     * @var string
     */
    protected $serialNo = null;

    /**
     * @return string
     */
    public function getSerialNo()
    {
        return $this->serialNo;
    }

    /**
     * @param string $serialNo
     */
    public function setSerialNo($serialNo)
    {
        $this->serialNo = $serialNo;
    }


    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/katar/product-card";
    }


    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductKatarCard
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductKatarCard::fromJson($json->data);
    }
}