<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductDosageCard;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductDosageCardRequest
 * @package vdmcworld\api\request
 * @method ProductDosageCard fetchData(Api $api)
 */
class ProductDosageCardRequest extends ProductBasedBaseRequest
{

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-dosage-card";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductDosageCard
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductDosageCard::fromJson($json->data);
    }
}