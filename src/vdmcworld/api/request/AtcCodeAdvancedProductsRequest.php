<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\AtcCodeProducts;
use vdmcworld\api\request\parameter\Atc;
use vdmcworld\api\response\BaseResponse;

/**
 * Class AtcCodeProductsRequest
 * @package vdmcworld\api\request
 * @method AtcCodeProducts fetchData(Api $api)
 */
class AtcCodeAdvancedProductsRequest extends BaseRequest
{
    /**
     * @var Atc
     */
    protected $atc;

    /**
     * @var string
     */
    protected $isReimbursement;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $onMarket;

    /**
     * @return Atc
     */
    public function getAtc()
    {
        return $this->atc;
    }

    /**
     * @return string
     */
    public function getIsReimbursement()
    {
        return $this->isReimbursement;
    }

    /**
     * @return string
     */
    public function getOnMarket()
    {
        return $this->onMarket;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Atc $atc
     */
    public function setAtc($atc)
    {
        $this->atc = $atc;
    }

    /**
     * @param string $isReimbursement
     */
    public function setIsReimbursement($isReimbursement)
    {
        $this->isReimbursement = $isReimbursement;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param string $onMarket
     */
    public function setOnMarket($onMarket)
    {
        $this->onMarket = $onMarket;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return AtcCodeProducts::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/atc-code-advanced-products/" . $this->atc->getCode() . "?is-reimbursement=" . $this->isReimbursement . "&on-market=" . $this->onMarket . "&type=" . $this->type;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->atc && null !== $this->atc->getCode()) {
            return true;
        }
        throw new \Exception("Atc and it's code cannot be null");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}