<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductAllergy;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductAllergyRequest
 * @package vdmcworld\api\request
 * @method ProductAllergy fetchData(Api $api)
 */
class ProductAllergyRequest extends ProductBasedBaseRequest
{

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-allergy";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductAllergy
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductAllergy::fromJson($json->data);
    }

}