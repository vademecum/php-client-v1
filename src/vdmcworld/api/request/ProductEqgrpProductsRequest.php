<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductEqgrpProducts;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductEqgrpProductsRequest
 * @package vdmcworld\api\request
 * @method ProductEqgrpProducts fetchData(Api $api)
 */
class ProductEqgrpProductsRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-eqgrp-products";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductEqgrpProducts
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductEqgrpProducts::fromJson($json->data);
    }
}