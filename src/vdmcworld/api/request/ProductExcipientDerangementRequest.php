<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\ProductContainerDerangement;
use vdmcworld\api\model\ProductExcipientDerangement;
use vdmcworld\api\model\SgkSutProductDetail;
use vdmcworld\api\request\parameter\Product;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SgkSutIndexRequest
 * @package vdmcworld\api\request
 * @method ProductExcipientDerangement fetchData(Api $api)
 */
class ProductExcipientDerangementRequest extends ProductBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductExcipientDerangement
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductExcipientDerangement::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/product-excipient-derangement";
    }
}
