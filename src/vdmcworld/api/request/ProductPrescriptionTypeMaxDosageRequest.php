<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductPrescriptionTypeMaxDosage;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductPrescriptionTypeMaxDosageRequest
 * @package vdmcworld\api\request
 * @method ProductPrescriptionTypeMaxDosage fetchData(Api $api)
 */
class ProductPrescriptionTypeMaxDosageRequest extends ProductBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductPrescriptionTypeMaxDosage
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductPrescriptionTypeMaxDosage::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return '/product-prescription-type-max-dosage';
    }
}