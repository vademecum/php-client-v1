<?php


namespace vdmcworld\api\request;


use vdmcworld\api\model\RapicoRapRecRule;
use vdmcworld\api\request\parameter\RapRecRule;
use vdmcworld\api\response\BaseResponse;

class RapRecRuleRequest extends BaseRequest
{
    /**
     * @var RapRecRule
     */
    protected $rapRecRule;

    /**
     * @return RapRecRule
     */
    public function getRapRecRule()
    {
        return $this->rapRecRule;
    }

    /**
     * @param RapRecRule $rapRecRule
     */
    public function setRapRecRule($rapRecRule)
    {
        $this->rapRecRule = $rapRecRule;
    }

    protected function _parsResponseData($response, $json)
    {
        return RapicoRapRecRule::fromJson($json->data);
    }

    public function getEndpoint()
    {
        return "/rapico-rap-rec-rule";
    }

    public function toParams()
    {
        return $this->rapRecRule->toParams();
    }

    public function isValid()
    {
        return true;
    }
}
