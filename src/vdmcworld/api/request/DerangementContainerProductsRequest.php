<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\DerangementContainer;
use vdmcworld\api\model\DerangementContainerProducts;
use vdmcworld\api\model\SubstanceContainerDerangement;
use vdmcworld\api\response\BaseResponse;

/**
 * @package vdmcworld\api\request
 * @method DerangementContainerProducts fetchData(Api $api)
 */
class DerangementContainerProductsRequest extends BaseRequest
{
    /**
     * @var \vdmcworld\api\request\parameter\DerangementContainer
     */
    protected $container = null;

    /**
     * @return \vdmcworld\api\request\parameter\DerangementContainer
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param \vdmcworld\api\request\parameter\DerangementContainer $substance
     * @return $this
     */
    public function setContainer($container)
    {
        $this->container = $container;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return DerangementContainerProducts
     */
    protected function _parsResponseData($response, $json)
    {
        return DerangementContainerProducts::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/derangement-container-products";
    }

    public function getEndpoint()
    {
        return $this->baseEndpoint() . "/" . $this->container->getId();
    }

    public function toParams()
    {
        return [];
    }

    public function isValid()
    {
        if (null !== $this->container && null !== $this->container->getId()) {
            return true;
        }
        throw new \Exception("Container and it'is ID should be set to make this request");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}
