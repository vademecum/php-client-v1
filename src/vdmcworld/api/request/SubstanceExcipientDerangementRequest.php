<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\SubstanceExcipientDerangement;
use vdmcworld\api\response\BaseResponse;

/**
 * @package vdmcworld\api\request
 * @method SubstanceExcipientDerangement fetchData(Api $api)
 */
class SubstanceExcipientDerangementRequest extends SubstanceBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SubstanceExcipientDerangement
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceExcipientDerangement::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/substance-excipient-derangement";
    }
}
