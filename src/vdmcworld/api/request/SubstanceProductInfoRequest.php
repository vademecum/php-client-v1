<?php


namespace vdmcworld\api\request;


use vdmcworld\api\model\SubstanceProductsInfo;
use vdmcworld\api\request\parameter\Substance;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SubstanceProductInfoRequest
 * @package vdmcworld\api\request
 */
class SubstanceProductInfoRequest extends SubstanceBasedBaseRequest
{
    /**
     * @var Substance
     */
    protected $substance = null;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @param Substance $substance
     * @return SubstanceProductInfoRequest
     */
    public function setSubstance($substance)
    {
        $this->substance = $substance;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceProductsInfo::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/substance-products-info";
    }
}