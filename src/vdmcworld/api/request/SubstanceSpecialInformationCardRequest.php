<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\SubstanceSpecialInformation;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SubstanceSpecialInformationCardRequest
 * @package vdmcworld\api\request
 * @method SubstanceSpecialInformation fetchData(Api $api)
 */
class SubstanceSpecialInformationCardRequest extends SubstanceBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SubstanceSpecialInformation
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstanceSpecialInformation::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/substance-special-information-card";
    }
}