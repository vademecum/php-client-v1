<?php


namespace vdmcworld\api\request;


use vdmcworld\api\request\parameter\Substance;

abstract class SubstanceBasedBaseRequest extends BaseRequest
{
    /**
     * @var Substance
     */
    protected $substance = null;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @param Substance $substance
     * @return SubstanceBasedBaseRequest
     */
    public function setSubstance($substance)
    {
        $this->substance = $substance;
        return $this;
    }

    /**
     * @return string
     */
    protected abstract function baseEndpoint();

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->baseEndpoint() . "/" . $this->substance->getId();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->substance && null !== $this->substance->getId()) {
            return true;
        }
        throw new \Exception("Substance and it'is ID should be set to make this request");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}