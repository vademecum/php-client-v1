<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductIndicationInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductIndicationInfoRequest
 * @package vdmcworld\api\request
 * @method ProductIndicationInfo fetchData(Api $api)
 */
class ProductIndicationInfoRequest extends ProductBasedBaseRequest
{

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-indication-info";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductIndicationInfo::fromJson($json->data);
    }
}