<?php

namespace vdmcworld\api\request;

use vdmcworld\api\request\parameter\AromatherapyPurpose;

abstract class AromatherapyPurposeBasedBaseRequest extends BaseRequest
{
    /**
     * @var AromatherapyPurpose
     */
    protected $purpose = null;

    /**
     * @return AromatherapyPurpose
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @param AromatherapyPurpose $purpose
     * @return AromatherapyPurposeBasedBaseRequest
     */
    public function setPurpose(AromatherapyPurpose $purpose)
    {
        $this->purpose = $purpose;
        return $this;
    }

    /**
     * @return string
     */
    protected abstract function baseEndpoint();

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->baseEndpoint() . "/" . $this->purpose->getId();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->purpose && null !== $this->purpose->getId()) {
            return true;
        }
        throw new \Exception("Purpose and it's ID should be set to make this request");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}