<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\MainSearchResult;
use vdmcworld\api\response\BaseResponse;

/**
 * Class AdvancedSearchRequest
 * @package vdmcworld\api\request
 * @method MainSearchResult fetchData(Api $api)
 */
class AdvancedSearchRequest extends BaseRequest
{
    protected $term = null;
    protected $isReimbursement = null;
    protected $onMarket = null;
    protected $type = null;

    /**
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @return string
     */
    public function getIsReimbursement()
    {
        return $this->isReimbursement;
    }

    /**
     * @return string
     */
    public function getOnMarket()
    {
        return $this->onMarket;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $term
     * @return AdvancedSearchRequest
     */
    public function setTerm($term)
    {
        $this->term = $term;
        return $this;
    }

    /**
     * @param string $isReimbursement
     * @return AdvancedSearchRequest
     */
    public function setIsReimbursement($term)
    {
        $this->isReimbursement = $term;
        return $this;
    }

    /**
     * @param string $onMarket
     * @return AdvancedSearchRequest
     */
    public function setOnMarket($term)
    {
        $this->onMarket = $term;
        return $this;
    }

    /**
     * @param string $type
     * @return AdvancedSearchRequest
     */
    public function setType($term)
    {
        $this->type = $term;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/advanced-search?term=" . urlencode($this->term) . "&on-market=" . $this->onMarket . "&is-reimbursement=" . $this->isReimbursement . "&type=" . $this->type;
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null === $this->term) {
            throw new \Exception("Term should be set to make search requests");
        }
        return true;
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return MainSearchResult
     */
    protected function _parsResponseData($response, $json)
    {
        return MainSearchResult::fromJson($json->data);
    }
}