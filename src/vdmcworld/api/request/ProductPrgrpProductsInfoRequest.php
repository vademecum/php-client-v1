<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductPrgrpProductsInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductEqgrpProductsInfoRequest
 * @package vdmcworld\api\request
 * @method ProductPrgrpProductsInfo fetchData(Api $api)
 */
class ProductPrgrpProductsInfoRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-prgrp-products-info";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductPrgrpProductsInfo::fromJson($json->data);
    }
}