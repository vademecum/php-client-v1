<?php


namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\SubstancePhDerangement;
use vdmcworld\api\response\BaseResponse;

/**
 * @package vdmcworld\api\request
 * @method SubstancePhDerangement fetchData(Api $api)
 */
class SubstancePhDerangementRequest extends SubstanceBasedBaseRequest
{
    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SubstancePhDerangement
     */
    protected function _parsResponseData($response, $json)
    {
        return SubstancePhDerangement::fromJson($json->data);
    }

    public function baseEndpoint()
    {
        return "/substance-ph-derangement";
    }
}
