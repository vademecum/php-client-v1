<?php

namespace vdmcworld\api\request;


use vdmcworld\api\model\SplashScreen;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SplashScreenRequest
 * @package vdmcworld\api\request
 */
class SplashScreenRequest extends BaseRequest
{
    public function getEndpoint()
    {
        return "/mobile-splash-screen";
    }

    public function toParams()
    {
        return [];
    }

    public function isValid()
    {

    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        $data = SplashScreen::fromJson($json->data[0]);

        return $data;
    }
}