<?php


namespace vdmcworld\api\request;


use vdmcworld\api\model\ProductSticker;
use vdmcworld\api\response\BaseResponse;

class ProductStickerRequest extends ProductBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductSticker::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-sticker";
    }

    public function getEndpoint()
    {
        $endpoint = parent::getEndpoint();

        if (null !== $this->product->getAttrs()) {
            $endpoint .= "?attr=" . join(",", $this->product->getAttrs());
        }

        return $endpoint;
    }
}