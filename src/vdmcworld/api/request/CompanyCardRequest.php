<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\CompanyCard;
use vdmcworld\api\request\parameter\Company;
use vdmcworld\api\response\BaseResponse;

/**
 * Class CompanyCardRequest
 * @package vdmcworld\api\request
 * @method CompanyCard fetchData(Api $api)
 */
class CompanyCardRequest extends BaseRequest
{
    /**
     * @var Company
     */
    protected $company;

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return CompanyCard
     */
    protected function _parsResponseData($response, $json)
    {
        return CompanyCard::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/company-card/" . $this->company->getId();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->company && null !== $this->company->getId()) {
            return true;
        }
        throw new \Exception("Company and it's id cannot be null");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}