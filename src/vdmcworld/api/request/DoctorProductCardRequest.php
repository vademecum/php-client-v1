<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\DoctorProductCard;
use vdmcworld\api\response\BaseResponse;

/**
 * Class DoctorProductCardRequest
 * @package vdmcworld\api\request
 * @method DoctorProductCard fetchData(Api $api)
 */
class DoctorProductCardRequest extends ProductBasedBaseRequest
{

    protected function baseEndpoint()
    {
        return "/doctor-product-card";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return DoctorProductCard
     */
    protected function _parsResponseData($response, $json)
    {
        return DoctorProductCard::fromJson($json->data);
    }
}