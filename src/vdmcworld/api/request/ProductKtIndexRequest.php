<?php


namespace vdmcworld\api\request;


use vdmcworld\api\model\ProductKtIndex;

class ProductKtIndexRequest extends ProductBasedBaseRequest
{

    protected function _parsResponseData($response, $json)
    {
        return ProductKtIndex::fromJson($json->data);
    }

    protected function baseEndpoint()
    {
        return "/product-kt-index";
    }
}