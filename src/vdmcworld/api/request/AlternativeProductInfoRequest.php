<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\AlternativeProductInfo;
use vdmcworld\api\response\BaseResponse;

/**
 * Class AlternativeProductInfoRequest
 * @package vdmcworld\api\request
 * @method AlternativeProductInfo fetchData(Api $api)
 */
class AlternativeProductInfoRequest extends ProductBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return AlternativeProductInfo
     */
    protected function _parsResponseData($response, $json)
    {
        return AlternativeProductInfo::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/alternative-products-info";
    }
}