<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductCard;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductCardRequest
 * @package vdmcworld\api\request
 * @method ProductCard fetchData(Api $api)
 */
class ProductCardRequest extends ProductBasedBaseRequest
{

    protected function baseEndpoint()
    {
        return "/product-card";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductCard
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductCard::fromJson($json->data);
    }
}