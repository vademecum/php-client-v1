<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\Model;
use vdmcworld\api\model\Drug;

/**
 * Class IndicationSearchRequest
 * @package vdmcworld\api\request
 * @method Drug[] fetchData(Api $api)
 */
class IndicationSearchRequest extends BasePaginatableRequest
{
    /**
     * @var string
     */
    protected $term = null;

    /**
     * @param string $term
     * @return IndicationSearchRequest
     */
    public function setTerm($term)
    {
        $this->term = $term;
        return $this;
    }

    /**
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @return string
     */
    public function getBaseEndpoint()
    {
        return "/indication-search";
    }

    protected function _buildUrl()
    {
        list($base, $urlParams) = parent::_buildUrl();

        $urlParams[] = "term=" . urlencode($this->term);

        return [$base, $urlParams];
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null === $this->term) {
            throw new \Exception("You have to provide a search term to make this request");
        }
        return true;
    }

    /**
     * @param array $json
     * @return Model
     */
    protected function _buildFromJson($json)
    {
        return Drug::fromJson($json);
    }
}