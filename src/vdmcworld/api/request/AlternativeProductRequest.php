<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\AlternativeProduct;
use vdmcworld\api\response\BaseResponse;

/**
 * Class AlternativeProductRequest
 * @package vdmcworld\api\request
 * @method AlternativeProduct fetchData(Api $api)
 */
class AlternativeProductRequest extends ProductBasedBaseRequest
{

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/alternative-products";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return AlternativeProduct
     */
    protected function _parsResponseData($response, $json)
    {
        return AlternativeProduct::fromJson($json->data);
    }
}