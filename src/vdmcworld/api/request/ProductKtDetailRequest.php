<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductKtDetail;
use vdmcworld\api\model\ProductKubDetail;
use vdmcworld\api\request\parameter\HeaderId;

/**
 * Class ProductKtDetailRequest
 * @package vdmcworld\api\request
 * @method ProductKtDetail fetchData(Api $api)
 */
class ProductKtDetailRequest extends ProductBasedBaseRequest
{
    /**
     * @var HeaderId
     */
    protected $headerId;

    /**
     * @return HeaderId
     */
    public function getHeaderId()
    {
        return $this->headerId;
    }

    /**
     * @param HeaderId $headerId
     */
    public function setHeaderId($headerId)
    {
        $this->headerId = $headerId;
    }

    protected function _parsResponseData($response, $json)
    {
        return ProductKtDetail::fromJson($json->data);
    }

    public function getEndpoint()
    {
        return parent::getEndpoint() . "/" . $this->headerId->getId();
    }

    public function isValid()
    {
        if (null !== $this->headerId && !empty($this->headerId->getId())) {
            return true;
        }
        return parent::isValid();
    }

    protected function baseEndpoint()
    {
        return "/product-kt-details";
    }
}