<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\EqGrpCodeProducts;
use vdmcworld\api\request\parameter\EqGrpCode;
use vdmcworld\api\response\BaseResponse;

/**
 * Class EqGrpCodeProductsRequest
 * @package vdmcworld\api\request
 * @method EqGrpCodeProducts fetchData(Api $api)
 */
class EqGrpCodeProductsRequest extends BaseRequest
{
    /**
     * @var EqGrpCode
     */
    protected $eqGrpCode;

    /**
     * @return EqGrpCode
     */
    public function getEqGrpCode()
    {
        return $this->eqGrpCode;
    }

    /**
     * @param EqGrpCode $eqGrpCode
     * @return EqGrpCodeProductsRequest
     */
    public function setEqGrpCode($eqGrpCode)
    {
        $this->eqGrpCode = $eqGrpCode;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return EqGrpCodeProducts
     */
    protected function _parsResponseData($response, $json)
    {
        return EqGrpCodeProducts::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/eqgrp-code-products/" . $this->getEqGrpCode()->getCode();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->eqGrpCode && !empty($this->eqGrpCode->getCode())) {
            return true;
        }
        throw new \Exception("Equivalent Group Code cannot be empty");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}