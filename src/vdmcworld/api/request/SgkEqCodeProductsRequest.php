<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\SgkEqCodeProducts;
use vdmcworld\api\request\parameter\SgkEqCode;
use vdmcworld\api\response\BaseResponse;

/**
 * Class SqkEqCodeProductsRequest
 * @package vdmcworld\api\request
 * @method SgkEqCodeProducts fetchData(Api $api)
 */
class SgkEqCodeProductsRequest extends BaseRequest
{
    /**
     * @var SgkEqCode
     */
    protected $sgkEqCode;

    /**
     * @return SgkEqCode
     */
    public function getSgkEqCode()
    {
        return $this->sgkEqCode;
    }

    /**
     * @param SgkEqCode $sgkEqCode
     * @return SgkEqCodeProductsRequest
     */
    public function setSgkEqCode($sgkEqCode)
    {
        $this->sgkEqCode = $sgkEqCode;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return SgkEqCodeProducts
     */
    protected function _parsResponseData($response, $json)
    {
        return SgkEqCodeProducts::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/sgk-eqcode-products/" . $this->getSgkEqCode()->getCode();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->sgkEqCode && !empty($this->sgkEqCode->getCode())) {
            return true;
        }
        throw new \Exception("SGK Equivalent Group Code cannot be empty");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}