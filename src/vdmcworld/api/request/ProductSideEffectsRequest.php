<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductSideEffects;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductSideEffects
 * @package vdmcworld\api\request
 * @method ProductSideEffects fetchData(Api $api)
 */
class ProductSideEffectsRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-side-effects";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductSideEffects
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductSideEffects::fromJson($json->data);
    }
}