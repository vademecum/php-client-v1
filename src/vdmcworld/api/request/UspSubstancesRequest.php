<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\UspSubstances;
use vdmcworld\api\request\parameter\Usp;
use vdmcworld\api\response\BaseResponse;

/**
 * Class UspSubstancesRequest
 * @package vdmcworld\api\request
 * @method UspSubstances fetchData(Api $api)
 */
class UspSubstancesRequest extends BaseRequest
{
    /**
     * @var Usp
     */
    protected $usp;

    /**
     * @return Usp
     */
    public function getUsp()
    {
        return $this->usp;
    }

    /**
     * @param Usp $usp
     */
    public function setUsp($usp)
    {
        $this->usp = $usp;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return UspSubstances::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/usp-substances/" . $this->usp->getId();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->usp && null !== $this->usp->getId()) {
            return true;
        }
        throw new \Exception("Usp and it's code cannot be null");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}