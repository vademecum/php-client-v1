<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\SgkPriceRefCodeProducts;
use vdmcworld\api\request\parameter\SgkPriceRefCode;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ExtendedSgkPriceRefCodeProductsRequest
 * @package vdmcworld\api\request
 * @method SgkPriceRefCodeProducts fetchData(Api $api)
 */
class ExtendedSgkPriceRefCodeProductsRequest extends BaseRequest
{

    /**
     * @var SgkPriceRefCode
     */
    protected $priceRefCode;

    /**
     * @return SgkPriceRefCode
     */
    public function getPriceRefCode()
    {
        return $this->priceRefCode;
    }

    /**
     * @param SgkPriceRefCode $priceRefCode
     * @return ExtendedSgkPriceRefCodeProductsRequest
     */
    public function setPriceRefCode($priceRefCode)
    {
        $this->priceRefCode = $priceRefCode;
        return $this;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return SgkPriceRefCodeProducts::fromJson($json->data);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/extended-sgk-price-ref-code-products/" . $this->priceRefCode->getCode();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->priceRefCode && !empty($this->priceRefCode->getCode())) {
            return true;
        }
        throw new \Exception("SGK Price Ref Code cannot be empty");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}