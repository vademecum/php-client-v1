<?php

namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\ProductPrescriptionRule;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductPrescriptionRuleRequest
 * @package vdmcworld\api\request
 * @method ProductPrescriptionRule fetchData(Api $api)
 */
class ProductPrescriptionRuleRequest extends ProductBasedBaseRequest
{
    protected function baseEndpoint()
    {
        return "/product-prescription-rules";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return mixed
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductPrescriptionRule::fromJson($json->data);
    }
}