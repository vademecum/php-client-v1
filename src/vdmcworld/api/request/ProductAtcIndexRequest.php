<?php

namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductAtcIndex;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductAtcIndexRequest
 * @package vdmcworld\api\request
 * @method ProductAtcIndex fetchData(Api $api)
 */
class ProductAtcIndexRequest extends ProductBasedBaseRequest
{
    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-atc-index";
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductAtcIndex
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductAtcIndex::fromJson($json->data);
    }
}