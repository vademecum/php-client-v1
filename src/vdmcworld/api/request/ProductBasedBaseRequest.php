<?php

namespace vdmcworld\api\request;


use vdmcworld\api\request\parameter\Product;

abstract class ProductBasedBaseRequest extends BaseRequest
{
    /**
     * @var Product
     */
    protected $product = null;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return ProductBasedBaseRequest
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return string
     */
    protected abstract function baseEndpoint();

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->baseEndpoint() . "/" . $this->product->getId();
    }

    /**
     * @return array
     */
    public function toParams()
    {
        return [];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (null !== $this->product && null !== $this->product->getId()) {
            return true;
        }
        throw new \Exception("Product and it's ID should be set to make this request");
    }

    public function getMethod()
    {
        return self::HTTP_METHOD_GET;
    }
}