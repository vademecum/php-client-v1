<?php

namespace vdmcworld\api\request;

use vdmcworld\Api;
use vdmcworld\api\model\Interaction;
use vdmcworld\api\response\BaseResponse;

/**
 * Class InteractionRequest
 * @package vdmcworld\api\request
 * @method Interaction fetchData(Api $api)
 */
class InteractionRequest extends PrescriptionClinicControlRequest
{
    /**
     * @return string
     */
    public function getEndpoint()
    {
        return "/interaction";
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid()
    {
        if (empty($this->products)) {
            throw new \Exception("Products cannot be empty");
        }
        return true;
    }

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return Interaction
     */
    protected function _parsResponseData($response, $json)
    {
        return Interaction::fromJson($json->data);
    }
}