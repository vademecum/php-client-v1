<?php


namespace vdmcworld\api\request;


use vdmcworld\Api;
use vdmcworld\api\model\ProductPriceHistory;
use vdmcworld\api\response\BaseResponse;

/**
 * Class ProductPriceHistoryRequest
 * @package vdmcworld\api\request
 * @method ProductPriceHistory fetchData(Api $api)
 */
class ProductPriceHistoryRequest extends ProductBasedBaseRequest
{

    /**
     * @param BaseResponse $response
     * @param array $json
     * @return ProductPriceHistory
     */
    protected function _parsResponseData($response, $json)
    {
        return ProductPriceHistory::fromJson($json->data);
    }

    /**
     * @return string
     */
    protected function baseEndpoint()
    {
        return "/product-price-history";
    }
}