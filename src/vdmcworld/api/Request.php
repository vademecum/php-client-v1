<?php

namespace vdmcworld\api;


use vdmcworld\api\response\BaseResponse;

interface Request
{
    /**
     * @return string
     */
    public function getEndpoint();

    /**
     * @return array
     */
    public function toParams();

    /**
     * @return bool
     * @throws \Exception
     */
    public function isValid();

    /**
     * @return string either post or get
     */
    public function getMethod();

    /**
     * @return BaseResponse
     * @var array $json
     */
    public function parseResponseObject($json);

    /**
     * @return string
     */
    public function getReturnType();

    /**
     * @return array
     */
    public function getAdditionalHeaders();
}