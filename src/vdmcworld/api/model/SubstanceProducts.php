<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceProducts implements Model
{
    /**
     * @var Substance
     */
    protected $substance;

    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var array
     */
    protected $sgkCodes;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return array
     */
    public function getSgkCodes()
    {
        return $this->sgkCodes;
    }

    /**
     * @param $json
     * @return SubstanceProducts
     */
    public static function fromJson($json)
    {
        $instance = new SubstanceProducts();
        if (isset($instance->substance)) {
            $instance->substance = Substance::fromJson($json->substance);
        }

        if (isset($json->products)) {
            foreach ($json->products as $p) {
                $instance->products[] = Product::fromJson($p);
            }
        }

        if (isset($json->product)) {
            $instance->product = Product::fromJson($json->product);
        }

        if (isset($json->sgkCodes)) {
            foreach ($json->sgkCodes as $sgkCode) {
                $instance->sgkCodes[] = $sgkCode;
            }
        }

        return $instance;
    }
}