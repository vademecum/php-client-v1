<?php


namespace vdmcworld\api\model;


class RecallProductIngredient extends Substance
{
    /**
     * @var int
     */
    protected $amount;

    /**
     * @var Unit
     */
    protected $unit;

    /**
     * @var ChemicalForm
     */
    protected $chemicalForm;

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @return ChemicalForm
     */
    public function getChemicalForm()
    {
        return $this->chemicalForm;
    }

    /**
     * @param $json
     * @return RecallProductIngredient
     */
    public static function fromJson($json)
    {
        $instance = new RecallProductIngredient();
        self::parseJson($json, $instance);
        if (isset($json->amount) && !empty($json->amount)) {
            $instance->amount = $json->amount;
        }
        if (isset($json->unit) && !empty($json->unit)) {
            $instance->unit = Unit::fromJson($json->unit);
        }
        if (isset($json->chemicalForm) && !empty($json->chemicalForm)) {
            $instance->chemicalForm = ChemicalForm::fromJson($json->chemicalForm);
        }
        return $instance;
    }
}