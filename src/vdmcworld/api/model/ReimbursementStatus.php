<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ReimbursementStatus implements Model
{
    /**
     * @var bool
     */
    private $status;
    /**
     * @var string
     */
    private $description;
    /**
     * @var bool
     */
    private $reportStatus;
    /**
     * @var string
     */
    private $reportStatusDescription;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getReportStatusDescription()
    {
        return $this->reportStatusDescription;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function getReportStatus()
    {
        return $this->reportStatus;
    }

    /**
     * @param $json
     * @return ReimbursementStatus
     */
    public static function fromJson($json)
    {
        $s = new ReimbursementStatus();
        $s->status = $json->status;
        $s->description = $json->description;
        if (!empty($json->reportStatus)) {
            $s->reportStatus = $json->reportStatus;
        }
        if (!empty($json->reportStatusDescription)) {
            $s->reportStatusDescription = $json->reportStatusDescription;
        }
        return $s;
    }
}