<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SgkReimbursementConditionSut implements Model
{
    /**
     * @var ReimbursementCondition
     */
    private $reimbursementCondition;

    /**
     * @var SgkSutDetail[]
     */
    private $sut = [];

    /**
     * @var SgkSutDetail[]
     */
    private $generalSut = [];

    /**
     * @var SgkSpecialConditionSut[]
     */
    private $specialSut = [];

    /**
     * @return ReimbursementCondition
     */
    public function getReimbursementCondition()
    {
        return $this->reimbursementCondition;
    }

    /**
     * @return SgkSutDetail[]
     */
    public function getSut()
    {
        return $this->sut;
    }

    /**
     * @return SgkSutDetail[]
     */
    public function getGeneralSut()
    {
        return $this->generalSut;
    }

    /**
     * @return SgkSpecialConditionSut[]
     */
    public function getSpecialSut()
    {
        return $this->specialSut;
    }

    /**
     * @param \StdClass $json
     * @return SgkReimbursementConditionSut
     */
    public static function fromJson($json)
    {
        $instance = new SgkReimbursementConditionSut();

        if (isset($json->reimbursementCondition)) {
            $instance->reimbursementCondition = ReimbursementCondition::fromJson($json->reimbursementCondition);
        }


        if (isset($json->sut)) {
            foreach ($json->sut as $sut) {
                $instance->sut [] = SgkSutDetail::fromJson($sut);
            }
        }


        if (isset($json->generalSut)) {
            foreach ($json->generalSut as $sut) {
                $instance->generalSut [] = SgkSutDetail::fromJson($sut);
            }
        }


        if (isset($json->specialSut)) {
            foreach ($json->specialSut as $sut) {
                $instance->specialSut [] = SgkSpecialConditionSut::fromJson($sut);
            }
        }


        return $instance;
    }
}
