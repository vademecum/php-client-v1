<?php


namespace vdmcworld\api\model;


class Ingredient extends Substance
{
    /**
     * @var int
     */
    protected $amount;

    /**
     * @var Unit
     */
    protected $unit;

    /**
     * @var ChemicalForm
     */
    protected $chemicalForm;

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @return ChemicalForm
     */
    public function getChemicalForm()
    {
        return $this->chemicalForm;
    }

    /**
     * @param $json
     * @return Ingredient
     */
    public static function fromJson($json)
    {
        $instance = new Ingredient();
        self::parseJson($json, $instance);
        $instance->amount = $json->amount;
        $instance->unit = Unit::fromJson($json->unit);
        if ($json->chemicalForm) {
            $instance->chemicalForm = ChemicalForm::fromJson($json->chemicalForm);
        }
        return $instance;
    }
}