<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class News implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $type_id;

    /**
     * @var int
     */
    protected $color_id;

    /**
     * @var string
     */
    protected $content_min;

    /**
     * @var string
     */
    protected $content_full;

    /**
     * @var string
     */
    protected $image_url;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var int
     */
    protected $date;

    /**
     * @var string
     */
    protected $dateFormat;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getColorId()
    {
        return $this->color_id;
    }

    /**
     * @return int
     */
    public function getTypeId()
    {
        return $this->type_id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getContentFull()
    {
        return $this->content_full;
    }

    /**
     * @return string
     */
    public function getContentMin()
    {
        return $this->content_min;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * @param $json
     * @return News
     */
    public static function fromJson($json)
    {
        $instance = new News();

        $instance->id = $json->id;
        $instance->color_id = $json->color_id;
        $instance->status = $json->status;
        $instance->type_id = $json->type_id;
        $instance->content_min = $json->content_min;
        $instance->content_full = $json->content_full;
        $instance->image_url = $json->image_url;
        $instance->date = $json->date;
        $instance->dateFormat = $json->dateFormat;

        return $instance;
    }
}