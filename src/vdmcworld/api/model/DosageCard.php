<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class DosageCard implements Model
{
    /**
     * @var string
     */
    protected $dosage;

    /**
     * @var string
     */
    protected $dosageCaution;

    /**
     * @var string
     */
    protected $dailyMaximumDosage;

    /**
     * @var string
     */
    protected $toxicDosage;

    /**
     * @var string
     */
    protected $overdose;

    /**
     * @var string
     */
    protected $overdoseTreatment;

    /**
     * @var MedicationTimeInfo
     */
    protected $medicationTimeInfo;

    /** @var MedulaMaximumDosage[] */
    protected $medulaMaximumDosageInfo;

    /**
     * @return string
     */
    public function getDosage()
    {
        return $this->dosage;
    }

    /**
     * @return string
     */
    public function getDailyMaximumDosage()
    {
        return $this->dailyMaximumDosage;
    }

    /**
     * @return string
     */
    public function getDosageCaution()
    {
        return $this->dosageCaution;
    }

    /**
     * @return string
     */
    public function getToxicDosage()
    {
        return $this->toxicDosage;
    }

    /**
     * @return string
     */
    public function getOverdose()
    {
        return $this->overdose;
    }

    /**
     * @return string
     */
    public function getOverdoseTreatment()
    {
        return $this->overdoseTreatment;
    }

    /**
     * @return MedicationTimeInfo
     */
    public function getMedicationTimeInfo()
    {
        return $this->medicationTimeInfo;
    }

    /**
     * @return MedulaMaximumDosage
     */
    public function getMedulaMaximumDosage()
    {
        return $this->medulaMaximumDosageInfo;
    }

    /**
     * @param $json
     * @return DosageCard
     */
    public static function fromJson($json)
    {
        $instance = new DosageCard();
        if ($json->medicationTimeInfo) {
            $instance->medicationTimeInfo = MedicationTimeInfo::fromJson($json->medicationTimeInfo);
        }
        if ($json->medulaMaximumDosageInfo) {
            foreach($json->medulaMaximumDosageInfo as $item) {
                $instance->medulaMaximumDosageInfo[] = MedulaMaximumDosage::fromJson($item);
            }
        }

        foreach ([
                     "dosage", "dosageCaution", "dailyMaximumDosage", "toxicDosage", "overdose", "overdoseTreatment",
                 ] as $key) {
            $instance->{$key} = $json->{$key};
        }
        return $instance;
    }
}