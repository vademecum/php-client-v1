<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementMixtureIngredientSolvent implements Model
{
    /**
     * @var DerangementSolvent
     */
    protected $solvent = null;

    /**
     * @var DerangementMixtureIngredientConcentration
     */
    protected $concentration = null;

    /**
     * @return DerangementSolvent
     */
    public function getSolvent()
    {
        return $this->solvent;
    }

    /**
     * @return DerangementMixtureIngredientConcentration
     */
    public function getConcentration()
    {
        return $this->concentration;
    }

    public static function fromJson($json)
    {
        $c = new self();
        if (isset($json->solvent)) {
            $c->solvent = DerangementSolvent::fromJson($json->solvent);
        }
        if (isset($json->concentration)) {
            $c->concentration = DerangementMixtureIngredientConcentration::fromJson($json->concentration);
        }
        return $c;
    }
}