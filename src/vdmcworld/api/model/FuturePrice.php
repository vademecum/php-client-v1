<?php


namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class FuturePrice implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var float
     */
    protected $price;

    /**
     * @var float
     */
    protected $factory;

    /**
     * @var float
     */
    protected $storage;

    /**
     * @var float
     */
    protected $pharmacy;

    /**
     * @var \DateTime
     */
    protected $effectiveDate;

    /**
     * @var string
     */
    protected $currencyCode;

    /**
     * @var float
     */
    protected $currentPrice;

    /**
     * @var float
     */
    protected $diff;

    /**
     * @var float
     */
    protected $status;

    /**
     * @var float
     */
    protected $change;

    /**
     * @var string
     */
    protected $companyName;

    /**
     * @var integer
     */
    protected $companyId;

    /**
     * @var PrescriptionType
     */
    protected $prescriptionType;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var Status
     */
    protected $reimbursementStatus;

    /**
     * @var Status
     */
    protected $onMarketStatus;

    /**
     * @var Substance[]
     */
    protected $substances;

    /**
     * @var float|null
     */
    protected $publicPrice;

    /**
     * @var array|null
     */
    protected $publicPaidPrice;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @return float
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @return float
     */
    public function getPharmacy()
    {
        return $this->pharmacy;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @return float
     */
    public function getCurrentPrice()
    {
        return $this->currentPrice;
    }    /**
     * @return float
     */
    public function getDiff()
    {
        return $this->diff;
    }    /**
     * @return float
     */
    public function getStatus()
    {
        return $this->status;
    }    /**
     * @return float
     */
    public function getChange()
    {
        return $this->change;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @return PrescriptionType
     */
    public function getPrescriptionType()
    {
        return $this->prescriptionType;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return Status
     */
    public function getReimbursementStatus()
    {
        return $this->reimbursementStatus;
    }

    /**
     * @return Status
     */
    public function getMarketStatus()
    {
        return $this->onMarketStatus;
    }

    public function getSubstances()
    {
        return $this->substances;
    }

    public function getPublicPrice()
    {
        return $this->publicPrice;
    }

    public function getPublicPaidPrice()
    {
        return $this->publicPaidPrice;
    }

    /**
     * @param $json
     * @return FuturePrice
     */
    public static function fromJson($json)
    {
        $instance = new FuturePrice();
        $instance->product = Product::fromJson($json->product);
        $instance->price = $json->price;
        $instance->factory = $json->factory;
        $instance->storage = $json->storage;
        $instance->pharmacy = $json->pharmacy;
        $instance->effectiveDate = \DateTime::createFromFormat(
            "Y-m-d H:i:s",
            $json->effectiveDate . " 00:00:00",
            new \DateTimeZone("UTC")
        );
        $instance->currencyCode = $json->currencyCode;
        $instance->currentPrice = $json->currentPrice;
        $instance->companyName = !empty($json->companyName) ? $json->companyName : null;
        $instance->companyId = !empty($json->companyId) ? $json->companyId : null;
        if (isset($json->diff)) {
            $instance->diff = !empty($json->diff)  ? $json->diff : null;
        }
        if (isset($json->change)) {
            $instance->change = !empty($json->change) ? $json->change : null;
        }
        if (isset($json->status)) {
            $instance->status = !empty($json->status) ? $json->status : null;
        }
        $instance->prescriptionType = PrescriptionType::fromJson($json->prescriptionType) ? PrescriptionType::fromJson($json->prescriptionType) : null;

        if (isset($json->company) && !empty($json->company)) {
            $instance->company = Company::fromJson($json->company);
        }
        if (isset($json->reimbursementStatus)) {
            $instance->reimbursementStatus = Status::fromJson($json->reimbursementStatus);
        }
        if (isset($json->onMarketStatus)) {
            $instance->onMarketStatus = Status::fromJson($json->onMarketStatus);
        }

        if (isset($json->substances)) {
            $instance->substances = $json->substances;
        }

        if (isset($json->publicPrice)) {
            $instance->publicPrice = $json->publicPrice;
        }

        if (isset($json->publicPaidPrice)) {
            $instance->publicPaidPrice = $json->publicPaidPrice;
        }
        return $instance;
    }
}