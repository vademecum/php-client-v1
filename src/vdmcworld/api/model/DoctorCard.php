<?php

namespace vdmcworld\api\model;


class DoctorCard extends BaseCard
{
    /**
     * @var PregnancyCategory[]
     */
    protected $pregnancyCategories = [];

    /**
     * @var string
     */
    protected $dosage;

    /**
     * @var Reimbursement
     */
    protected $reimbursement;

    /**
     * @var string
     */
    protected $caution;

    /**
     * @var string
     */
    protected $geriatrics;

    /**
     * @var string
     */
    protected $pediatrics;

    /**
     * @return PregnancyCategory[]
     */
    public function getPregnancyCategories()
    {
        return $this->pregnancyCategories;
    }

    /**
     * @return Reimbursement
     */
    public function getReimbursement()
    {
        return $this->reimbursement;
    }

    /**
     * @return string
     */
    public function getDosage()
    {
        return $this->dosage;
    }

    /**
     * @return string
     */
    public function getCaution()
    {
        return $this->caution;
    }

    /**
     * @return string
     */
    public function getGeriatrics()
    {
        return $this->geriatrics;
    }

    /**
     * @return string
     */
    public function getPediatrics()
    {
        return $this->pediatrics;
    }

    /**
     * @param $json
     * @return DoctorCard
     */
    public static function fromJson($json)
    {
        $instance = new DoctorCard();
        self::parseJson($json, $instance);

        foreach ($json->pregnancyCategories as $pc) {
            $instance->pregnancyCategories[] = PregnancyCategory::fromJson($pc);
        }

        $instance->reimbursement = Reimbursement::fromJson($json->reimbursement);

        foreach (["dosage", "caution", "geriatrics", "pediatrics"] as $val) {
            $instance->{$val} = $json->{$val};
        }

        return $instance;
    }
}