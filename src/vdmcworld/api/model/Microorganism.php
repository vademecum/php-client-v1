<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Microorganism implements Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;

    /**
     * @var object
     */
    private $oxygen;

    /**
     * @var string
     */
    private $gramStatus;

    /**
     * @var string
     */
    private $info;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getOxygen()
    {
        return $this->oxygen;
    }

    /**
     * @return string
     */
    public function getGramStatus()
    {
        return $this->gramStatus;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param $json
     * @return Microorganism
     */
    public static function fromJson($json)
    {
        $model = new Microorganism();
        $model->id = $json->id;
        $model->name = $json->name;
        $model->oxygen = $json->oxygen;
        $model->gramStatus = $json->gramStatus;
        $model->info = $json->info;
        return $model;
    }
}