<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class RapRec implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var string
     */
    private $substanceCode;

    /**
     * @var SubstanceGroup
     */
    private $substanceGroup;

    /**
     * @var Icd
     */
    private $icd;

    /**
     * @var Ek4d
     */
    private $ek4d;

    /**
     * @var RapRecRule
     */
    private $rule;

    /**
     * @var Sut[]
     */
    private $sut;

    /**
     * @var NutritionInfo
     */
    private $nutritionInfo;

    /**
     * @var PackageAmount
     */
    private $packageAmount;

    /**
     * @var bool
     */
    private $indicationComplianceWarning;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getSubstanceCode()
    {
        return $this->substanceCode;
    }

    /**
     * @return SubstanceGroup
     */
    public function getSubstanceGroup()
    {
        return $this->substanceGroup;
    }

    /**
     * @return Icd
     */
    public function getIcd()
    {
        return $this->icd;
    }

    /**
     * @return Ek4d
     */
    public function getEk4d()
    {
        return $this->ek4d;
    }

    /**
     * @return RapRecRule
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @return Sut[]
     */
    public function getSut()
    {
        return $this->sut;
    }

    /**
     * @return PackageAmount
     */
    public function getPackageAmount()
    {
        return $this->packageAmount;
    }

    /**
     * @return NutritionInfo
     */
    public function getNutritionInfo()
    {
        return $this->nutritionInfo;
    }

    /**
     * @return bool
     */
    public function getIndicationComplianceWarning()
    {
        return $this->indicationComplianceWarning;
    }

    /**
     * @param $json
     * @return RapRec
     */
    public static function fromJson($json)
    {
        $instance = new RapRec();

        if ($json->product) {
            $instance->product = Product::fromJson($json->product);
        }
        if ($json->substanceGroup) {
            $instance->substanceGroup = SubstanceGroup::fromJson($json->substanceGroup);
        }
        if ($json->icd) {
            $instance->icd = Icd::fromJson($json->icd);
        }
        if ($json->ek4d) {
            $instance->ek4d = Ek4d::fromJson($json->ek4d);
        }
        if ($json->rule) {
            $instance->rule = RapRecRule::fromJson($json->rule);
        }

        if ($json->packageAmount) {
            $instance->packageAmount = PackageAmount::fromJson($json->packageAmount);
        }

        if ($json->nutritionInfo) {
            $instance->nutritionInfo = NutritionInfo::fromJson($json->nutritionInfo);
        }

        if (isset($json->indicationComplianceWarning)) {
            $instance->indicationComplianceWarning = $json->indicationComplianceWarning;
        }

        if ($json->sut) {
            $instance->sut = [];
            foreach ($json->sut as $s) {
                $instance->sut[] = Sut::fromJson($s);
            }
        }

        $simpleKeys = ["substanceCode"];
        foreach ($simpleKeys as $k) {
            if ($json->$k) {
                $instance->$k = $json->$k;
            }
        }

        return $instance;
    }
}
