<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductTitckPriceHistory implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var TitckPriceInfo[]
     */
    protected $priceHistory = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return TitckPriceInfo[]
     */
    public function getPriceHistory()
    {
        return $this->priceHistory;
    }

    /**
     * @param $json
     * @return ProductTitckPriceHistory
     */
    public static function fromJson($json)
    {
        $instance = new ProductTitckPriceHistory();
        $instance->product = Product::fromJson($json->product);
        if ($json->priceInfo) {
            foreach ($json->priceInfo as $p) {
                $instance->priceHistory[] = TitckPriceInfo::fromJson($p);
            }
        }
        return $instance;
    }
}