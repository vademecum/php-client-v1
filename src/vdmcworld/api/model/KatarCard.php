<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

/**
 * Class KatarCard
 * @package vdmcworld\api\model
 */
class KatarCard implements Model
{

    /**
     * @param $json
     * @return KatarCard
     */
    public static function fromJson($json)
    {
        $instance = new KatarCard();

        foreach ($json as $k => $v) {
            $instance->$k = $v;
        }
        return $instance;
    }
}