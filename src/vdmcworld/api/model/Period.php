<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class Period implements Model
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $id;


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @param $json
     * @return Period
     */
    public static function fromJson($json)
    {
        $p = new Period();
        $p->id = $json->id;
        $p->name = $json->name;

        return $p;
    }
}