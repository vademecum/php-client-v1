<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class AgeRange implements Model
{
    /**
     * @var int
     */
    protected $minAge;

    /**
     * @var int
     */
    protected $maxAge;


    /**
     * @return int
     */
    public function getMinAge()
    {
        return $this->minAge;
    }

    /**
     * @return int
     */
    public function getMaxAge()
    {
        return $this->maxAge;
    }


    /**
     * @param $json
     * @return AgeRange
     */
    public static function fromJson($json)
    {
        $p = new AgeRange();
        $p->minAge = $json->minAge;
        $p->maxAge = $json->maxAge;

        return $p;
    }
}