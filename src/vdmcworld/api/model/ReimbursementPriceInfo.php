<?php

namespace vdmcworld\api\model;


class ReimbursementPriceInfo extends PriceInfo
{
    /**
     * @var boolean
     */
    protected $passive;

    /**
     * @var boolean
     */
    protected $reimbursement;

    /**
     * @var boolean
     */
    protected $alternativeReimbursement;

    /**
     * @var FuturePrice
     */
    protected $futurePrice;

    /**
     * @var boolean
     */
    protected $skipPriceCalc;

    /**
     * @return bool
     */
    public function isSkipPriceCalc()
    {
        return $this->skipPriceCalc;
    }

    /**
     * @return bool
     */
    public function isPassive()
    {
        return $this->passive;
    }

    /**
     * @return bool
     */
    public function isReimbursement()
    {
        return $this->reimbursement;
    }

    /**
     * @return bool
     */
    public function isAlternativeReimbursement()
    {
        return $this->alternativeReimbursement;
    }

    /**
     * @return FuturePrice
     */
    public function getFuturePrice()
    {
        return $this->futurePrice;
    }

    /**
     * @param ReimbursementPriceInfo $instance
     * @param $json
     * @return ReimbursementPriceInfo
     */
    protected static function _parseJson($instance, $json)
    {
        parent::_parseJson($instance, $json);
        foreach (["skipPriceCalc", "passive", "reimbursement", "alternativeReimbursement"] as $key) {
            $instance->$key = $json->$key;
        }

        if ($json->futurePrice) {
            $instance->futurePrice = ReimbursementFuturePrice::fromJson($json->futurePrice);
        }

        return $instance;
    }

    /**
     * @param $json
     * @return ReimbursementPriceInfo
     */
    public static function fromJson($json)
    {
        return ReimbursementPriceInfo::_parseJson(new ReimbursementPriceInfo(), $json);
    }
}