<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class AromatherapyFormulations implements Model
{
    /**
     * @var AromatherapyCategory
     */
    protected $category;

    /**
     * @var AromatherapyPurpose
     */
    protected $purpose;

    /**
     * @var AromatherapyFormulations
     */
    protected $formulations;

    /**
     * @var string
     */
    protected $description;

    /**
     * @return AromatherapyCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return AromatherapyPurpose
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @return AromatherapyFormulations
     */
    public function getFormulations()
    {
        return $this->formulations;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return AromatherapyFormulations
     */
    public static function fromJson($json)
    {
        $aromatherapyPurpose = new AromatherapyFormulations();

        $aromatherapyPurpose->category = AromatherapyCategory::fromJson($json->category);
        $aromatherapyPurpose->purpose = AromatherapyPurpose::fromJson($json->purpose);
        $aromatherapyPurpose->description = $json->description;
        if ($json->formulations) {
            $aromatherapyPurpose->formulations = AromatherapyFormulationInfo::fromJson($json->formulations);
        }

        return $aromatherapyPurpose;
    }
}