<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class AromatherapyCategory implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return AromatherapyCategory
     */
    public static function fromJson($json)
    {
        $aromatherapyCategory = new AromatherapyCategory();
        $aromatherapyCategory->id = $json->id;
        $aromatherapyCategory->name = $json->name;

        return $aromatherapyCategory;
    }
}