<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Question implements Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $question;

    /**
     * @var string
     */
    public $parameter;

    /**
     * @var array
     */
    public $answers;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @return string
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * @return array
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param array $json
     * @return Question
     */
    public static function fromJson($json)
    {
        $instance = new Question();
        $instance->id = $json->id;
        $instance->question = $json->question;
        $instance->parameter = $json->parameter;
        $instance->answers = Answer::fromJson($json->answers);
        return $instance;
    }
}