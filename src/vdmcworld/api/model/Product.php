<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class Product implements Model
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $drugName;
    /**
     * @var string
     */
    protected $barcode;
    /**
     * @var string
     */
    protected $titckName;
    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string[]
     */
    protected $barcodes;

    /**
     * @var Status
     */
    protected $reimbursementStatus;

    /**
     * @var boolean
     */
    protected $onMarket;

    /**
     * @var Status
     */
    protected $onMarketStatus;

    /**
     * @var string[]
     */
    protected $company;

    /**
     * @var Substance[]
     */
    protected $substances;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDrugName()
    {
        return $this->drugName;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @return string
     */
    public function getTitckName()
    {
        return $this->titckName;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string[]
     */
    public function getBarcodes()
    {
        return $this->barcodes;
    }

    /**
     * @return Status
     */
    public function getReimbursementStatus()
    {
        return $this->reimbursementStatus;
    }

    /**
     * @return boolean
     */
    public function getMarketStatus()
    {
        return $this->onMarket;
    }

    /**
     * @return Status
     */
    public function getOnMarketStatus()
    {
        return $this->onMarketStatus;
    }

    public function getCompany()
    {
        return $this->company;
    }


    public function getSubstances()
    {
        return $this->substances;
    }

    /**
     * @param $json
     * @return Product
     */
    public static function fromJson($json)
    {
        $p = new Product();
        $p->id = $json->id;
        $p->name = $json->name;
        $p->drugName = isset($json->drugName) ? $json->drugName : null;
        $p->barcode = $json->barcode;
        if (isset($json->titckName)) {
            $p->titckName = $json->titckName;
        }
        if (isset($json->slug)) {
            $p->slug = $json->slug;
        }
        $p->barcodes = [];
        if (isset($json->barcodes) && count($json->barcodes) > 0) {
            $p->barcodes = $json->barcodes;
        }
        if (isset($json->reimbursementStatus)) {
            $p->reimbursementStatus = Status::fromJson($json->reimbursementStatus);
        }
        if (isset($json->onMarket)) {
            $p->onMarket = $json->onMarket;
        }
        if (isset($json->onMarketStatus)) {
            $p->onMarketStatus = Status::fromJson($json->onMarketStatus);;
        }
        if (isset($json->company)) {
            $p->company = $json->company;
        }
        if (isset($json->substances)) {
            $p->substances = $json->substances;
        }

        return $p;
    }
}