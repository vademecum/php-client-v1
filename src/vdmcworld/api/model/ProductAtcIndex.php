<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductAtcIndex implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var AtcIndex[]
     */
    protected $atcIndices = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return AtcIndex[]
     */
    public function getAtcIndices()
    {
        return $this->atcIndices;
    }

    /**
     * @param $json
     * @return ProductAtcIndex
     */
    public static function fromJson($json)
    {
        $pai = new ProductAtcIndex();
        $pai->product = Product::fromJson($json->product);
        foreach ($json->productAtc as $item) {
            $pai->atcIndices[] = AtcIndex::fromJson($item);
        }
        return $pai;
    }
}