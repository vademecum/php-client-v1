<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ReimbursementMaxReportTime implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */

    protected $maximumReportTime;

    /**
     * @var string
     */

    protected $maximumReportTimeText;

    /**
     * @var string
     */
    protected $note;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @return string
     */
    public function getMaximumReportTimeText()
    {
        return $this->maximumReportTimeText;
    }

    /**
     * @return int
     */
    public function getMaximumReportTime()
    {
        return $this->maximumReportTime;
    }

    /**
     * @param $json
     * @return ReimbursementMaxReportTime
     */
    public static function fromJson($json)
    {
        $rt = new ReimbursementMaxReportTime();
        $rt->id = $json->id;
        $rt->note = $json->note;
        $rt->maximumReportTime = $json->maximumReportTime;
        $rt->maximumReportTimeText = $json->maximumReportTimeText;
        return $rt;
    }
}