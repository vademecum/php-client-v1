<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductSpecialInformation implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductProductSpecialInformation
     */
    protected $specialInformation;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ProductProductSpecialInformation
     */
    public function getSpecialInformation()
    {
        return $this->specialInformation;
    }

    /**
     * @param $json
     * @return ProductSpecialInformation
     */
    public static function fromJson($json)
    {
        $instance = new ProductSpecialInformation();
        $instance->product = Product::fromJson($json->product);
        $instance->specialInformation = ProductProductSpecialInformation::fromJson($json->specialInformation);
        return $instance;
    }
}