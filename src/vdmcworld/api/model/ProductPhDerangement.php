<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductPhDerangement implements Model
{
    /* @var Product */
    protected $product;

    /* @var PhDerangement[] */
    protected $phDerangements = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return PhDerangement[]
     */
    public function getPhDerangements()
    {
        return $this->phDerangements;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->product)) {
            $c->product = Product::fromJson($json->product);
        }

        if (isset($json->phDerangements)) {
            foreach ($json->phDerangements as $cd) {
                $c->phDerangements [] = PhDerangement::fromJson($cd);
            }
        }

        return $c;
    }
}
