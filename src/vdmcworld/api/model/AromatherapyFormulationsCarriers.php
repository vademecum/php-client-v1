<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class AromatherapyFormulationsCarriers implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $amount;

    /**
     * @var Unit
     */
    protected $unit;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param $json
     * @return AromatherapyFormulationsCarriers
     */
    public static function fromJson($json)
    {
        $aromatherapyFormulationsCarriers = new AromatherapyFormulationsCarriers();

        $aromatherapyFormulationsCarriers->id = $json->carrier->id;
        $aromatherapyFormulationsCarriers->name = $json->carrier->name;
        $aromatherapyFormulationsCarriers->amount = $json->carrierAmount->amount;
        $aromatherapyFormulationsCarriers->unit = Unit::fromJson($json->carrierAmount->unit);


        return $aromatherapyFormulationsCarriers;
    }
}