<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductContainerDerangement implements Model
{
    /* @var Product */
    protected $product;

    /* @var ContainerDerangement[] */
    protected $containerDerangements = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ContainerDerangement[]
     */
    public function getContainerDerangements()
    {
        return $this->containerDerangements;
    }

    public static function fromJson($json)
    {
        $c = new ProductContainerDerangement();

        if (isset($json->product)) {
            $c->product = Product::fromJson($json->product);
        }

        if (isset($json->containerDerangements)) {
            foreach ($json->containerDerangements as $cd) {
                $c->containerDerangements [] = ContainerDerangement::fromJson($cd);
            }
        }

        return $c;
    }
}
