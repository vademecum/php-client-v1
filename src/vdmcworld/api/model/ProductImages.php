<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductImages implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var File[]
     */
    protected $images;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return File[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param $json
     * @return ProductImages
     */
    public static function fromJson($json)
    {
        $instance = new ProductImages();
        $instance->product = Product::fromJson($json->product);
        $instance->images = [];
        if ($json->images) {
            foreach ($json->images as $i) {
                $instance->images[] = File::fromJson($i);
            }
        }
        return $instance;
    }
}