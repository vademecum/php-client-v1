<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductKatarCard implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var CustomCard
     */
    protected $card;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return CustomCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param $json
     * @return ProductCustomCard
     */
    public static function fromJson($json)
    {
        $instance = new ProductKatarCard();
        $instance->product = KatarProduct::fromJson($json->product);
        $instance->card = KatarCard::fromJson($json->card);
        return $instance;
    }
}