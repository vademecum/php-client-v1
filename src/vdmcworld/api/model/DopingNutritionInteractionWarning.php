<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DopingNutritionInteractionWarning implements Model
{
    /**
     * @var Substance
     */
    protected $substance;

    /**
     * @var string
     */
    protected $info;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }


    public static function fromJson($json)
    {
        $instance = new DopingNutritionInteractionWarning();
        $instance->substance = Substance::fromJson($json->substance);
        $instance->info = $json->info;

        return $instance;
    }
}