<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class TitckPriceInfo implements Model
{
    /**
     * @var float
     */
    protected $factory;

    /**
     * @var float
     */
    protected $storage;

    /**
     * @var float
     */
    protected $pharmacy;

    /**
     * @var float
     */
    protected $retail;

    /**
     * @var \DateTime
     */
    protected $effectiveDate;

    /**
     * @var float
     */
    protected $taxPercent;

    /**
     * @var string
     */
    protected $currencyCode;

    /**
     * @var bool
     */
    protected $abroadProduct;

    /**
     * @var float
     */
    protected $abroadRetail;

    /**
     * @var float
     */
    protected $storageSalesPrice;

    /**
     * @var bool
     */
    protected $storageBased;

    /**
     * @return float
     */
    public function getStorageSalesPrice()
    {
        return $this->storageSalesPrice;
    }

    /**
     * @return bool
     */
    public function isStorageBased()
    {
        return $this->storageBased;
    }

    /**
     * @return float
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @return float
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @return float
     */
    public function getPharmacy()
    {
        return $this->pharmacy;
    }

    /**
     * @return float
     */
    public function getRetail()
    {
        return $this->retail;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }

    /**
     * @return float
     */
    public function getTaxPercent()
    {
        return $this->taxPercent;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @return bool
     */
    public function isAbroadProduct()
    {
        return $this->abroadProduct;
    }

    /**
     * @return float
     */
    public function getAbroadRetail()
    {
        return $this->abroadRetail;
    }

    /**
     * @param $json
     * @return TitckPriceInfo
     */
    public static function fromJson($json)
    {
        $instance = new TitckPriceInfo();

        foreach ([
                     "factory", "storage", "pharmacy", "retail", "taxPercent", "currencyCode",
                     "abroadProduct", "abroadRetail", "storageSalesPrice", "storageBased"
                 ] as $key) {
            $instance->$key = $json->$key;
        }

        $instance->effectiveDate = \DateTime::createFromFormat(
            "Y-m-d H:i:s",
            $json->effectiveDate . " 00:00:00",
            new \DateTimeZone("UTC")
        );

        return $instance;
    }
}