<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SgkSutProductDetail implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var SgkReimbursementConditionSut[]
     */
    private $sutDetails = [];

    /**
     * @return SgkReimbursementConditionSut[]
     */
    public function getSutDetails()
    {
        return $this->sutDetails;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param \StdClass $json
     * @return SgkSutProductDetail
     */
    public static function fromJson($json)
    {
        $instance = new SgkSutProductDetail();

        $instance->product = Product::fromJson($json->product);

        if (isset($json->sutDetails)) {
            foreach ($json->sutDetails as $sutDetail) {
                $instance->sutDetails [] = SgkReimbursementConditionSut::fromJson($sutDetail);
            }
        }

        return $instance;
    }
}
