<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class SubstanceExcipientDerangement implements Model
{
    /* @var Substance */
    protected $substance;

    /* @var ExcipientDerangement[] */
    protected $excipientDerangements = [];

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return ExcipientDerangement[]
     */
    public function getExcipientDerangements()
    {
        return $this->excipientDerangements;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->substance)) {
            $c->substance = Substance::fromJson($json->substance);
        }

        if (isset($json->excipientDerangements)) {
            foreach ($json->excipientDerangements as $cd) {
                $c->excipientDerangements [] = ExcipientDerangement::fromJson($cd);
            }
        }

        return $c;
    }
}
