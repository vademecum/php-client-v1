<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductDosageCaution implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var string
     */
    protected $dosageCaution;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getDosageCaution()
    {
        return $this->dosageCaution;
    }

    /**
     * @param $json
     * @return ProductDosageCaution
     */
    public static function fromJson($json)
    {
        $instance = new ProductDosageCaution();
        $instance->product = Product::fromJson($json->product);
        $instance->dosageCaution = $json->dosageCaution;
        return $instance;
    }
}