<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SpecialInformation implements Model
{
    /**
     * @var Status
     */
    protected $highRiskStatus;

    /**
     * @return Status
     */
    public function getHighRiskStatus()
    {
        return $this->highRiskStatus;
    }


    /**
     * @param $json
     * @return SpecialInformation
     */
    public static function fromJson($json)
    {
        $instance = new SpecialInformation();
        $instance->highRiskStatus = Status::fromJson($json->highRiskStatus);
        return $instance;
    }
}