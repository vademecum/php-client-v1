<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductPregnancyCategory implements Model
{
    /**
     * @var PregnancyCategory[]
     */
    protected $pregnancyCategories = [];

    /**
     * @var Product
     */
    protected $product;

    /**
     * @return PregnancyCategory[]
     */
    public function getPregnancyCategories()
    {
        return $this->pregnancyCategories;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param $json
     * @return ProductPregnancyCategory
     */
    public static function fromJson($json)
    {
        $ppc = new ProductPregnancyCategory();
        $ppc->product = Product::fromJson($json->product);
        if ($json->pregnancyCategories) {
            foreach ($json->pregnancyCategories as $pc) {
                $ppc->pregnancyCategories[] = PregnancyCategory::fromJson($pc);
            }
        }
        return $ppc;
    }
}