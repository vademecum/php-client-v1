<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PharmaceuticalWarehouse implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $district;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param $json
     * @return PharmaceuticalWarehouse
     */
    public static function fromJson($json)
    {
        $c = new PharmaceuticalWarehouse();
        foreach (["id", "name", "phone", "address", "city", "district"] as $f) {
            $c->$f = $json->$f;
        }
        return $c;
    }
}