<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Diagnosis implements Model
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return Diagnosis
     */
    public static function fromJson($json)
    {
        $d = new Diagnosis();
        if ($json->code) {
            $d->code = $json->code;
        }
        if ($json->name) {
            $d->name = $json->name;
        }
        return $d;
    }
}