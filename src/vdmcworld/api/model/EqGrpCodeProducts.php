<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class EqGrpCodeProducts implements Model
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param $json
     * @return EqGrpCodeProducts
     */
    public static function fromJson($json)
    {
        $instance = new EqGrpCodeProducts();
        $instance->code = $json->eqGrpCode;
        if ($json->products) {
            foreach ($json->products as $p) {
                $instance->products[] = Product::fromJson($p);
            }
        }
        return $instance;
    }
}