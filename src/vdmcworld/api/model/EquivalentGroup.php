<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class EquivalentGroup implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return EquivalentGroup
     */
    public static function fromJson($json)
    {
        $instance = new EquivalentGroup();
        $instance->id = $json->id;
        $instance->name = $json->name;
        return $instance;
    }
}