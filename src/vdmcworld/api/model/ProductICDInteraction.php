<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductICDInteraction implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var IcdCode
     */
    private $affectingIcdCode;

    /**
     * @var Color
     */
    private $color;

    /**
     * @var string
     */
    private $description;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return IcdCode
     */
    public function getAffectingIcdCode()
    {
        return $this->affectingIcdCode;
    }

    /**
     * @return Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return ProductICDInteraction
     */
    public static function fromJson($json)
    {
        $r = new ProductICDInteraction();
        $r->product = Product::fromJson($json->product);
        $r->affectingIcdCode = IcdCode::fromJson($json->affectingICDCode);
        $r->color = Color::fromJson($json->color);
        $r->description = $json->description;
        return $r;
    }
}