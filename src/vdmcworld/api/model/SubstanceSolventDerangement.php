<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class SubstanceSolventDerangement implements Model
{
    /* @var Substance */
    protected $substance;

    /* @var SolventDerangement[] */
    protected $solventDerangements = [];

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return SolventDerangement[]
     */
    public function getSolventDerangements()
    {
        return $this->solventDerangements;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->substance)) {
            $c->substance = Substance::fromJson($json->substance);
        }

        if (isset($json->solventDerangements)) {
            foreach ($json->solventDerangements as $cd) {
                $c->solventDerangements [] = SolventDerangement::fromJson($cd);
            }
        }

        return $c;
    }
}
