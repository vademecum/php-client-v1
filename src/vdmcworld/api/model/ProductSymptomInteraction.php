<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductSymptomInteraction implements Model
{
    /**
     * @var Product
     */
    private $product;
    /**
     * @var Symptom
     */
    private $affectingSymptom;
    /**
     * @var Color
     */
    private $color;
    /**
     * @var string
     */
    private $description;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Symptom
     */
    public function getAffectingSymptom()
    {
        return $this->affectingSymptom;
    }

    /**
     * @return Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return ProductSymptomInteraction
     */
    public static function fromJson($json)
    {
        $p = new ProductSymptomInteraction();
        $p->product = Product::fromJson($json->product);
        $p->affectingSymptom = Symptom::fromJson($json->affectingSymptom);
        $p->color = Color::fromJson($json->color);
        $p->description = $json->description;
        return $p;
    }
}