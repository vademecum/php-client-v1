<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class LandingCampaign implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $start_date;

    /**
     * @var int
     */
    protected $end_date;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var LandingBanner[]
     */
    protected $banners;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStartDate(): int
    {
        return $this->start_date;
    }

    /**
     * @param int $start_date
     */
    public function setStartDate(int $start_date): void
    {
        $this->start_date = $start_date;
    }

    /**
     * @return int
     */
    public function getEndDate(): int
    {
        return $this->end_date;
    }

    /**
     * @param int $end_date
     */
    public function setEndDate(int $end_date): void
    {
        $this->end_date = $end_date;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return LandingBanner[]
     */
    public function getBanners(): array
    {
        return $this->banners;
    }

    /**
     * @param LandingBanner[] $banners
     */
    public function setBanners(array $banners): void
    {
        $this->banners = $banners;
    }


    /**
     * @param $json
     * @return LandingCampaign
     */
    public static function fromJson($json)
    {
        $instance = new LandingCampaign();

        $instance->id = $json->id;
        $instance->start_date = $json->start_date;
        $instance->end_date = $json->end_date;
        $instance->name = $json->name;
        $instance->banners = $json->banners;

        return $instance;
    }
}