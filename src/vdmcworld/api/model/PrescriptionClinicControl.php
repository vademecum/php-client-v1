<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PrescriptionClinicControl implements Model
{
    /**
     * @var Interaction
     */
    protected $interaction;

    /**
     * @var ProductAllergy[]
     */
    protected $productAllergies = [];

    /**
     * @var ProductMaximumDosage[]
     */
    protected $productMaximumDosages;

    /**
     * @var Status
     */
    protected $prescriptionMaximumDosage;

    /**
     * @var Antibiotic[]
     */
    protected $antibiotics = [];

    /**
     * @return Interaction
     */
    public function getInteraction()
    {
        return $this->interaction;
    }

    /**
     * @return ProductAllergy[]
     */
    public function getProductAllergies()
    {
        return $this->productAllergies;
    }

    /**
     * @return ProductMaximumDosage[]
     */
    public function getProductMaximumDosages()
    {
        return $this->productMaximumDosages;
    }

    /**
     * @return Status
     */
    public function getPrescriptionMaximumDosage()
    {
        return $this->prescriptionMaximumDosage;
    }

    /**
     * @return Antibiotic[]
     */
    public function getAntibiotics()
    {
        return $this->antibiotics;
    }

    /**
     * @param $data
     * @return PrescriptionClinicControl
     */
    public static function fromJson($data)
    {
        $r = new PrescriptionClinicControl();
        $r->interaction = Interaction::fromJson($data->interaction);
        if (isset($data->productAllergy)) {
            foreach ($data->productAllergy as $item) {
                $r->productAllergies[] = ProductAllergy::fromJson($item);
            }
        }

        foreach ($data->productMaximumDosage as $item) {
            $r->productMaximumDosages[] = ProductMaximumDosage::fromJson($item);
        }

        if ($data->prescriptionMaximumDosage) {
            $r->prescriptionMaximumDosage = Status::fromJson($data->prescriptionMaximumDosage);
        }

        if (isset($data->antibiotic)) {
            foreach ($data->antibiotic as $item) {
                $r->antibiotics[] = Antibiotic::fromJson($item);
            }
        }

        return $r;
    }
}