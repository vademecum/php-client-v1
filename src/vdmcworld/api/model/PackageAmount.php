<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PackageAmount implements Model
{
    /**
     * @var int
     */
    protected $amount;

    /**
     * @var Unit
     */
    protected $unit;

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param $json
     * @return PackageAmount
     */
    public static function fromJson($json)
    {
        $instance = new PackageAmount();
        $instance->amount = $json->amount;
        $instance->unit = Unit::fromJson($json->unit);
        return $instance;
    }
}