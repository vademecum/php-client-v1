<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Allergy implements Model
{
    /**
     * @var Substance
     */
    private $substance;
    /**
     * @var AllergyInfo
     */
    private $substanceAllergyInfo;
    /**
     * @var AllergicReaction[]
     */
    private $allergicReactions;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return AllergyInfo
     */
    public function getSubstanceAllergyInfo()
    {
        return $this->substanceAllergyInfo;
    }

    /**
     * @return AllergicReaction[]
     */
    public function getAllergicReactions()
    {
        return $this->allergicReactions;
    }

    /**
     * @param $json
     * @return Allergy
     */
    public static function fromJson($json)
    {
        $a = new Allergy();
        $a->substance = Substance::fromJson($json->substance);

        if ($json->substanceAllergyInfo) {
            $a->substanceAllergyInfo = AllergyInfo::fromJson($json->substanceAllergyInfo);
        } else {
            $a->substanceAllergyInfo = null;
        }

        if (is_array($json->substanceAllergicReactions)){
            foreach ($json->substanceAllergicReactions as $item) {
                $a->allergicReactions[] = SubstanceAllergicReaction::fromJson($item);
            }
        }

        return $a;
    }
}
