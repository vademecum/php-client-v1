<?php

namespace vdmcworld\api\model;

class RapicoIcd extends Icd
{
    /**
     * @var Ek4d
     */
    protected $ek4d;

    /**/
    protected $displayName;

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @param $json
     * @return Icd
     */
    public static function fromJson($json)
    {
        $icd = new RapicoIcd();
        if ($json->ek4d) {
            $icd->ek4d = Ek4d::fromJson($json->ek4d);
        }
        $icd->name = $json->name;
        $icd->displayName = $json->displayName;

        return $icd;
    }
}
