<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class MedulaMaximumDosage implements Model
{
    /**
     * @var int
     */
    protected $dosage;

    /**
     * @var int
     */
    protected $periodAmount;

    /**
     * @var boolean
     */
    protected $reimbursement;

    /** @var Period */
    protected $period;

    /** @var Unit */
    protected $dosageUnit;

    /** @var ReimbursementCondition */
    protected $reimbursementCondition;

    /** @var AgeRange */
    protected $ageRange;

    /** @var SingleDosage */
    protected $singleDosage;

    /**
     * @return int
     */
    public function getDosage()
    {
        return $this->dosage;
    }

    /**
     * @return int
     */
    public function getPeriodAmount()
    {
        return $this->periodAmount;
    }

    /**
     * @return boolean
     */
    public function getReimbursement()
    {
        return $this->reimbursement;
    }

    /**
     * @return Period
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @return Unit
     */
    public function getDosageUnit()
    {
        return $this->dosageUnit;
    }

    /**
     * @return ReimbursementCondition
     */
    public function getReimbursementCondition()
    {
        return $this->reimbursementCondition;
    }

    /**
     * @return AgeRange
     */
    public function getAgeRange()
    {
        return $this->ageRange;
    }

    /**
     * @return SingleDosage
     */
    public function getSingleDosage()
    {
        return $this->singleDosage;
    }



    /**
     * @param $json
     * @return MedulaMaximumDosage
     */
    public static function fromJson($json)
    {
        $instance = new MedulaMaximumDosage();
        $instance->period = $json->period;
        $instance->periodAmount = $json->periodAmount;
        $instance->dosage = $json->dosage;
        $instance->dosageUnit = $json->dosageUnit;
        $instance->reimbursementCondition = $json->reimbursementCondition;
        $instance->reimbursement = $json->reimbursement;
        $instance->ageRange = $json->ageRange;
        $instance->singleDosage = $json->singleDosage;

        return $instance;
    }
}