<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class DrugType implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return DrugType
     */
    public static function fromJson($json)
    {
        $d = new DrugType();
        $d->id = $json->id;
        $d->name = $json->name;
        return $d;
    }
}