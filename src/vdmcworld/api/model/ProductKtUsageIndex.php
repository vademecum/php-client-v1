<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductKtUsageIndex implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var KtUsageIndex[]
     */
    private $ktUsageIndex = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return KtUsageIndex[]
     */
    public function getKtUsageIndex()
    {
        return $this->ktUsageIndex;
    }

    /**
     * @param $json
     * @return ProductKtUsageIndex
     */
    public static function fromJson($json)
    {
        $instance = new self();
        $instance->product = Product::fromJson($json->product);
        $instance->ktUsageIndex = [];
        if ($json->ktUsageIndex && !empty($json->ktUsageIndex)) {
            foreach ($json->ktUsageIndex as $k) {
                $instance->ktUsageIndex[] = KtUsageIndex::fromJson($k);
            }
        }
        return $instance;
    }
}