<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SgkSpecialist extends Specialist implements Model
{
    protected $isSpecial;

    /**
     * @return int
     */
    public function isSpecial()
    {
        return $this->isSpecial;
    }

    /**
     * @param $json
     * @return SgkSpecialist
     */
    public static function fromJson($json)
    {
        $s = new SgkSpecialist();
        $s->id = $json->id;
        $s->name = $json->name;
        $s->isSpecial = $json->isSpecial;

        return $s;
    }
}
