<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class RapRecRuleDataItem implements Model
{
    /**
     * @var int[]
     */
    private $sut = [];

    /**
     * @return int[]
     */
    public function getSut()
    {
        return $this->sut;
    }

    /**
     * @param array $json
     * @return RapRecRuleDataItem
     */
    public static function fromJson($json)
    {
        $instance = new RapRecRuleDataItem();
        $instance->sut = $json->sut;
        return $instance;
    }
}