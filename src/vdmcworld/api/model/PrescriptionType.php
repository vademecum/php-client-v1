<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PrescriptionType implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Color
     */
    protected $color;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param $json
     * @return PrescriptionType
     */
    public static function fromJson($json)
    {
        $p = new PrescriptionType();
        $p->id = $json->id;
        $p->name = $json->name;
        if (isset($json->color)) {
            $p->color = Color::fromJson($json->color);
        }
        return $p;
    }
}