<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductProductSpecialInformation extends BaseSpecialInformation
{
    /**
     * @param $json
     * @return ProductProductSpecialInformation
     */
    public static function fromJson($json)
    {
        $instance = new ProductProductSpecialInformation();
        self::parseJson($json, $instance);
        return $instance;
    }
}
