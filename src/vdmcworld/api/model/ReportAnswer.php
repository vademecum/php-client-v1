<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ReportAnswer implements Model
{
    const TYPE_VALID = 1;
    const TYPE_INVALID = 2;
    const TYPE_NEXT_QUESTION = 3;

    /**
     * @var integer
     */
    public $result;

    /**
     * @var Question
     */
    public $question;

    /**
     * @return int
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param $json
     * @return ReportAnswer
     */
    public static function fromJson($json)
    {
        $instance = new ReportAnswer();

        if ($json->reportQuestion) {
            $instance->question = Question::fromJson($json->reportQuestion);
        }

        $instance->result = $json->result;
        return $instance;
    }
}
