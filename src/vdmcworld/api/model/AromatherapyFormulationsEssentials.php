<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class AromatherapyFormulationsEssentials implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $amount;

    /**
     * @var Unit
     */
    protected $unit;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param $json
     * @return AromatherapyFormulationsEssentials
     */
    public static function fromJson($json)
    {
        $aromatherapyFormulationsEssentials = new AromatherapyFormulationsEssentials();

        $aromatherapyFormulationsEssentials->id = $json->essential->id;
        $aromatherapyFormulationsEssentials->name = $json->essential->name;
        $aromatherapyFormulationsEssentials->amount = $json->essentialAmount->amount;
        $aromatherapyFormulationsEssentials->unit = Unit::fromJson($json->essentialAmount->unit);


        return $aromatherapyFormulationsEssentials;
    }
}