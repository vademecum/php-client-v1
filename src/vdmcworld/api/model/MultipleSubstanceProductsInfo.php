<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class MultipleSubstanceProductsInfo implements Model
{
    /**
     * @var array
     */
    protected $productsInfo;

    /**
     * @var Substance[]
     */
    protected $substances;

    public function getProductsInfo()
    {
        return $this->productsInfo;
    }

    public function getSubstances()
    {
        return $this->substances;
    }

    public static function fromJson($json)
    {
        $instance = new MultipleSubstanceProductsInfo();
        $productInfos = $substances = [];
        foreach ($json->substances as $substance) {
            $substances[] = Substance::fromJson($substance);
        }
        foreach ($json->productsInfo as $key => $info) {
            $productInfos[$key]["product"] = Product::fromJson($info->product);
            $productInfos[$key]["company"] = Company::fromJson($info->company);
            $productInfos[$key]["reimbursementStatus"] = ReimbursementStatus::fromJson($info->reimbursementStatus);
            $productInfos[$key]["genericStatusType"] = GenericStatusType::fromJson($info->genericStatusType);
            $productInfos[$key]["onMarket"] = Status::fromJson($info->onMarket);
        }
        $instance->substances = $substances;
        $instance->productsInfo = $productInfos;
        return $instance;
    }
}