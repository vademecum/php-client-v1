<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;
use vdmcworld\api\request\RapRecDrug;

class Ek4d implements Model
{
    /**
     * @var Diagnosis
     */
    private $diagnosis;
    /**
     * @var RapRecDrug
     */
    private $drug;
    /**
     * @var string
     */
    private $condition;
    /**
     * @var string
     */
    private $info;

    /**
     * @var bool
     */
    private $participationShare;

    /**
     * @var bool
     */
    private $offLabelUsage;

    /**
     * @var bool
     */
    private $indicationIcd;

    /**
     * @return Diagnosis
     */
    public function getDiagnosis()
    {
        return $this->diagnosis;
    }

    /**
     * @return RapRecDrug
     */
    public function getDrug()
    {
        return $this->drug;
    }

    /**
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @return bool
     */
    public function getParticipationShare()
    {
        return $this->participationShare;
    }

    /**
     * @return bool
     */
    public function getOffLabelUsage()
    {
        return $this->offLabelUsage;
    }

    /**
     * @return bool
     */
    public function getIndicationIcd()
    {
        return $this->indicationIcd;
    }

    /**
     * @param $json
     * @return Ek4d
     */
    public static function fromJson($json)
    {
        $instance = new Ek4d();

        $instance->diagnosis = Diagnosis::fromJson($json->diagnosis);

        if (isset($json->drug)) {
            $instance->drug = RapRecDrug::fromJson($json->drug);
        }
        if (isset($json->condition)) {
            $instance->condition = $json->condition;
        }
        if (isset($json->info)) {
            $instance->info = $json->info;
        }

        if (isset($json->participationShare)) {
            $instance->participationShare = $json->participationShare;
        }

        if (isset($json->offLabelUsage)) {
            $instance->offLabelUsage = $json->offLabelUsage;
        }

        if (isset($json->indicationIcd)) {
            $instance->indicationIcd = $json->indicationIcd;
        }

        return $instance;
    }
}
