<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class City implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return City
     */
    public static function fromJson($json)
    {
        $c = new City();
        $c->id = $json->id;
        $c->name = $json->name;
        return $c;
    }
}