<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class LactationProductWarning implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var Status
     */
    protected $reimbursementStatus;

    /**
     * @var Status
     */
    protected $onMarketStatus;

    /**
     * @var LactationWarning
     */
    protected $lactationWarning;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return LactationWarning
     */
    public function getLactationWarning()
    {
        return $this->lactationWarning;
    }

    /**
     * @return Status
     */
    public function getReimbursementStatus()
    {
        return $this->reimbursementStatus;
    }

    /**
     * @return Status
     */
    public function getMarketStatus()
    {
        return $this->onMarketStatus;
    }

    /**
     * @param $json
     * @return LactationProductWarning
     */
    public static function fromJson($json)
    {
        $instance = new LactationProductWarning();

        $instance->product = Product::fromJson($json->product);
        $instance->lactationWarning = LactationWarning::fromJson($json->lactationWarning);

        if (isset($json->company) && !empty($json->company)) {
            $instance->company = Company::fromJson($json->company);
        }

        if (isset($json->reimbursementStatus)) {
            $instance->reimbursementStatus = Status::fromJson($json->reimbursementStatus);
        }
        if (isset($json->onMarketStatus)) {
            $instance->onMarketStatus = Status::fromJson($json->onMarketStatus);
        }

        return $instance;
    }
}