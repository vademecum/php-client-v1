<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductAllergy implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Allergy[]
     */
    private $allergies;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Allergy[]
     */
    public function getAllergies()
    {
        return $this->allergies;
    }

    /**
     * @param $json
     * @return Model
     */
    public static function fromJson($json)
    {
        $pa = new ProductAllergy();
        $pa->product = Product::fromJson($json->product);
        foreach ($json->allergy as $item) {
            $pa->allergies[] = Allergy::fromJson($item);
        }
        return $pa;
    }
}