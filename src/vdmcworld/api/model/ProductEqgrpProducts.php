<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductEqgrpProducts implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var EqgrpProducts[]
     */
    protected $equivalentGroupProducts = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return EqgrpProducts[]
     */
    public function getEquivalentGroupProducts()
    {
        return $this->equivalentGroupProducts;
    }

    /**
     * @param $json
     * @return ProductEqgrpProducts
     */
    public static function fromJson($json)
    {
        $instance = new ProductEqgrpProducts();
        $instance->product = Product::fromJson($json->product);
        if (null !== $json->equivalentGroupProducts) {
            foreach ($json->equivalentGroupProducts as $egp) {
                $instance->equivalentGroupProducts[] = EqgrpProducts::fromJson($egp);
            }
        }
        return $instance;
    }
}