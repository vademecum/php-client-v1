<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class NfcCode implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return NfcCode
     */
    public static function fromJson($json)
    {
        $n = new NfcCode();
        $n->id = $json->id;
        $n->name = $json->name;
        $n->description = $json->description;
        return $n;
    }
}