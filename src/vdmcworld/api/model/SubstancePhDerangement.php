<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class SubstancePhDerangement implements Model
{
    /* @var Substance */
    protected $substance;

    /* @var PhDerangement[] */
    protected $phDerangements = [];

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return PhDerangement[]
     */
    public function getPhDerangements()
    {
        return $this->phDerangements;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->substance)) {
            $c->substance = Substance::fromJson($json->substance);
        }

        if (isset($json->phDerangements)) {
            foreach ($json->phDerangements as $cd) {
                $c->phDerangements [] = PhDerangement::fromJson($cd);
            }
        }

        return $c;
    }
}
