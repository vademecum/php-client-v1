<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class AromatherapyFormulationInfo implements Model
{
    /**
     * @var AromatherapyFormulation
     */
    protected $formulation;

    /**
     * @var AromatherapyFormulationsCarriers[]
     */
    protected $carriers;

    /**
     * @var AromatherapyFormulationsEssentials[]
     */
    protected $essentials;

    /**
     * @return AromatherapyFormulationsEssentials[]
     */
    public function getEssentials()
    {
        return $this->essentials;
    }

    /**
     * @return AromatherapyFormulationsCarriers[]
     */
    public function getCarriers()
    {
        return $this->carriers;
    }

    /**
     * @return AromatherapyFormulation
     */
    public function getFormulation()
    {
        return $this->formulation;
    }

    /**
     * @param $json
     * @return AromatherapyFormulationInfo
     */
    public static function fromJson($json)
    {
        $aromatherapyFormulationInfo = new AromatherapyFormulationInfo();

        $aromatherapyFormulationInfo->formulation = AromatherapyFormulation::fromJson($json->formulation);
        foreach ($json->essentials as $essential) {
            $aromatherapyFormulationInfo->essentials[] = AromatherapyFormulationsEssentials::fromJson($essential);
        }
        foreach ($json->carriers as $carrier) {
            $aromatherapyFormulationInfo->carriers[] = AromatherapyFormulationsCarriers::fromJson($carrier);
        }

        return $aromatherapyFormulationInfo;
    }
}