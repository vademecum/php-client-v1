<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Interaction implements Model
{

    /**
     * @var ProductWarning[]
     */
    protected $productWarnings = [];

    /**
     * @var ProductInteraction[]
     */
    protected $productInteractions = [];

    /**
     * @var ProductNutritionInteraction[]
     */
    protected $productNutritionInteractions = [];

    /**
     * @var ProductICDInteraction[]
     */
    protected $productICDInteractions = [];

    /**
     * @var ProductPatientCharacteristicInteraction[]
     */
    protected $productPatientCharacteristicInteractions = [];

    /**
     * @var ProductSymptomInteraction[]
     */
    protected $productSymptomInteractions = [];

    /**
     * @return ProductWarning[]
     */
    public function getProductWarnings()
    {
        return $this->productWarnings;
    }

    /**
     * @return ProductInteraction[]
     */
    public function getProductInteractions()
    {
        return $this->productInteractions;
    }

    /**
     * @return ProductNutritionInteraction[]
     */
    public function getProductNutritionInteractions()
    {
        return $this->productNutritionInteractions;
    }

    /**
     * @return ProductICDInteraction[]
     */
    public function getProductICDInteractions()
    {
        return $this->productICDInteractions;
    }

    /**
     * @return ProductPatientCharacteristicInteraction[]
     */
    public function getProductPatientCharacteristicInteractions()
    {
        return $this->productPatientCharacteristicInteractions;
    }

    /**
     * @return ProductSymptomInteraction[]
     */
    public function getProductSymptomInteractions()
    {
        return $this->productSymptomInteractions;
    }

    /**
     * @param $json
     * @return Interaction
     */
    public static function fromJson($json)
    {
        $instance = new Interaction();
        foreach ($json->productWarning as $pw) {
            $instance->productWarnings[] = ProductWarning::fromJson($pw);
        }
        foreach ($json->productInteraction as $item) {
            $instance->productInteractions[] = ProductInteraction::fromJson($item);
        }
        foreach ($json->productNutritionInteraction as $item) {
            $instance->productNutritionInteractions[] = ProductNutritionInteraction::fromJson($item);
        }
        foreach ($json->productICDInteraction as $item) {
            $instance->productICDInteractions[] = ProductICDInteraction::fromJson($item);
        }
        foreach ($json->productPatientCharacteristicInteraction as $item) {
            $instance->productPatientCharacteristicInteractions[] = ProductPatientCharacteristicInteraction::fromJson($item);
        }
        foreach ($json->productSymptomInteraction as $item) {
            $instance->productSymptomInteractions[] = ProductSymptomInteraction::fromJson($item);
        }
        return $instance;

    }
}