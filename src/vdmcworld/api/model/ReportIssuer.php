<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ReportIssuer implements Model
{
    /**
     * @var string
     */
    protected $name;
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $note;
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }


    /**
     * @param $json
     * @return ReportIssuer
     */
    public static function fromJson($json)
    {
        $ri = new ReportIssuer();
        $ri->name = $json->name;
        $ri->id = $json->id;
        if (!empty($json->note)) {
            $ri->note = $json->note;
        }

        return $ri;
    }
}