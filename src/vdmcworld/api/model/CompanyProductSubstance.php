<?php


namespace vdmcworld\api\model;


class CompanyProductSubstance extends Substance
{
    /**
     * @param $json
     * @return CompanyProductSubstance
     */
    public static function fromJson($json)
    {
        return self::parseJson($json, new CompanyProductSubstance());
    }
}