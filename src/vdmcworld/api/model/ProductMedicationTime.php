<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductMedicationTime implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var MedicationTimeInfo
     */
    protected $medicationTimeInfo;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return MedicationTimeInfo
     */
    public function getMedicationTimeInfo()
    {
        return $this->medicationTimeInfo;
    }

    /**
     * @param $json
     * @return ProductMedicationTime
     */
    public static function fromJson($json)
    {
        $instance = new ProductMedicationTime();
        $instance->product = Product::fromJson($json->product);
        if ($json->productMedicationTime) {
            $instance->medicationTimeInfo = MedicationTimeInfo::fromJson($json->productMedicationTime);
        }
        return $instance;
    }
}