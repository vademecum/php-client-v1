<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class PharmacologicalClass implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $branchOf;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getBranchOf()
    {
        return $this->branchOf;
    }

    /**
     * @param $json
     * @return PharmacologicalClass
     */
    public static function fromJson($json)
    {
        $pharmacologicalClass = new PharmacologicalClass();
        $pharmacologicalClass->id = $json->id;
        $pharmacologicalClass->name = $json->name;
        if (isset($json->branchOf)) {
            $pharmacologicalClass->branchOf = $json->branchOf;
        }

        return $pharmacologicalClass;
    }
}