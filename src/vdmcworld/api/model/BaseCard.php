<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class BaseCard implements Model
{
    /**
     * @var Ingredient[]
     */
    protected $ingredients = [];

    /**
     * @var Status
     */
    protected $inMarket;

    /**
     * @var Product[]
     */
    protected $otherForms = [];

    /**
     * @var Product[]
     */
    protected $extendedOtherForms = [];

    /**
     * @return Ingredient[]
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @return Status
     */
    public function getInMarket()
    {
        return $this->inMarket;
    }

    /**
     * @return Product[]
     */
    public function getOtherForms()
    {
        return $this->otherForms;
    }

   /**
     * @return Product[]
     */
    public function getExtendedOtherForms()
    {
        return $this->extendedOtherForms;
    }

    public static function parseJson($json, $instance)
    {
        if ($json->substances) {
            foreach ($json->substances as $s) {
                $instance->ingredients[] = Ingredient::fromJson($s);
            }
        }

        if ($json->otherForms) {
            foreach ($json->otherForms as $of) {
                $instance->otherForms[] = Product::fromJson($of);
            }
        }

        if ($json->extendedOtherForms) {
            foreach ($json->extendedOtherForms as $of) {
                $instance->extendedOtherForms[] = Product::fromJson($of);
            }
        }

        if ($json->inMarket) {
            $instance->inMarket = Status::fromJson($json->inMarket);
        }

        return $instance;
    }

    /**
     * @param $json
     * @return BaseCard
     */
    public static function fromJson($json)
    {
        return BaseCard::parseJson($json, new BaseCard());
    }
}