<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class MedicationTimeInfo implements Model
{
    /**
     * @var string
     */
    protected $description;

    /**
     * @var MedicationTime
     */
    protected $medicationTime;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return MedicationTime
     */
    public function getMedicationTime()
    {
        return $this->medicationTime;
    }

    /**
     * @param $json
     * @return MedicationTimeInfo
     */
    public static function fromJson($json)
    {
        $instance = new MedicationTimeInfo();
        $instance->description = $json->description;
        $instance->medicationTime = MedicationTime::fromJson($json->medicationTime);
        return $instance;
    }
}