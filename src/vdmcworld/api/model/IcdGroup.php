<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class IcdGroup implements Model
{
    /**
     * @var Ek4d
     */
    protected $ek4d;

    /**/
    protected $name;

    /**/
    protected $description;

    /**/
    protected $displayName;

    /**/
    protected $participationShare;

    /**/
    protected $offLabelUsage;

    /**/
    protected $indicationIcd;

    /**/
    protected $condition;

    protected $reportCodes;

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return boolean
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @return boolean
     */
    public function getOffLabelUsage()
    {
        return $this->offLabelUsage;
    }

    /**
     * @return boolean
     */
    public function getParticipationShare()
    {
        return $this->participationShare;
    }

    /**
     * @return boolean
     */
    public function getIndicationIcd()
    {
        return $this->indicationIcd;
    }

    /**
     * @return Ek4d
     */
    public function getEk4d()
    {
        return $this->ek4d;
    }

    /**
     * @return array
     */
    public function getReportCodes()
    {
        return $this->reportCodes;
    }

    /**
     * @param $json
     * @return IcdGroup
     */
    public static function fromJson($json)
    {
        $instance = new IcdGroup();

        $instance->name = $json->name;
        $instance->description = $json->description;
        if ($json->ek4d) {
            $instance->ek4d = Ek4d::fromJson($json->ek4d);
        }
        $instance->displayName = $json->displayName;
        $instance->condition = $json->condition;
        $instance->offLabelUsage = $json->offLabelUsage;
        $instance->participationShare = $json->participationShare;
        $instance->indicationIcd = isset($json->indicationIcd) ? $json->indicationIcd : null;
        $instance->reportCodes = isset($json->reportCodes) ? $json->reportCodes : null;

        return $instance;
    }
}
