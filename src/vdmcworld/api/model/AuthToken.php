<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class AuthToken implements Model
{
    /**
     * @var string
     */
    private $token = null;
    /**
     * @var string
     */
    private $iframeToken = null;
    /**
     * @var \DateTime
     */
    private $expiresAt = null;

    /**
     * @var bool
     */
    private $nonExpirable = null;

    /**
     * AuthToken constructor.
     * @param string $token
     * @param int|bool $expiresAt unix date time or false for an unlimited, non expirable token
     * @param string $iframeToken
     */
    public function __construct($token, $expiresAt, $iframeToken = null)
    {
        $this->token = $token;
        $this->iframeToken = $iframeToken;
        if (false === $expiresAt) {
            $this->nonExpirable = true;
        } else {
            $this->expiresAt = new \DateTime();
            $this->expiresAt->setTimestamp($expiresAt);
        }
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        if ($this->nonExpirable) {
            return false;
        }
        return new \DateTime() > $this->expiresAt;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @return string
     */
    public function getIframeToken()
    {
        return $this->iframeToken;
    }

    /**
     * @param $json
     * @return AuthToken
     */
    public static function fromJson($json)
    {
        return new AuthToken($json->token, $json->expires_at, $json->iframe_token);
    }
}
