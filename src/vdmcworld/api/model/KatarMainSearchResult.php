<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class KatarMainSearchResult implements Model
{
    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }


    /**
     * @param $json
     * @return MainSearchResult
     */
    public static function fromJson($json)
    {
        $s = new KatarMainSearchResult();
        foreach ($json->products as $p) {
            $s->products[] = KatarProduct::fromJson($p);
        }
        return $s;
    }
}