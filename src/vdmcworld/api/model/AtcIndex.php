<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class AtcIndex implements Model
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var Atc[]
     */
    protected $tree = [];

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return Atc[]
     */
    public function getTree()
    {
        return $this->tree;
    }

    /**
     * @param $json
     * @return AtcIndex
     */
    public static function fromJson($json)
    {
        $ai = new AtcIndex();
        $ai->code = $json->code;
        $ai->description = $json->description;
        foreach ($json->tree as $leaf) {
            $ai->tree[] = Atc::fromJson($leaf);
        }
        return $ai;
    }
}