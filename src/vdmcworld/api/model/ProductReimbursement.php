<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductReimbursement implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Reimbursement
     */
    protected $reimbursement;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Reimbursement
     */
    public function getReimbursement()
    {
        return $this->reimbursement;
    }

    /**
     * @param $json
     * @return ProductReimbursement
     */
    public static function fromJson($json)
    {
        $pr = new ProductReimbursement();
        $pr->product = Product::fromJson($json->product);
        if ($json->reimbursement) {
            $pr->reimbursement = Reimbursement::fromJson($json->reimbursement);
        }
        return $pr;
    }
}