<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class SingleDosage implements Model
{
    /**
     * @var int
     */
    protected $amount;

    /**
     * @var Unit
     */
    protected $unit;


    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }


    /**
     * @param $json
     * @return SingleDosage
     */
    public static function fromJson($json)
    {
        $p = new SingleDosage();
        $p->amount = $json->amount;
        $p->unit = $json->unit;

        return $p;
    }
}