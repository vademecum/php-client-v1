<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class EqgrpProducts implements Model
{

    /**
     * @var EquivalentGroup
     */
    protected $equivalentGroup;

    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @return EquivalentGroup
     */
    public function getEquivalentGroup()
    {
        return $this->equivalentGroup;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param $json
     * @return EqgrpProducts
     */
    public static function fromJson($json)
    {
        $instance = new EqgrpProducts();
        $instance->equivalentGroup = EquivalentGroup::fromJson($json->equivalentGroup);
        foreach ($json->products as $p) {
            $instance->products[] = Product::fromJson($p);
        }
        return $instance;
    }
}