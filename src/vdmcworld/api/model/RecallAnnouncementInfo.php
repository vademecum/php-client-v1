<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class RecallAnnouncementInfo implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $recall_class;

    /**
     * @var string
     */
    protected $recall_level;

    /**
     * @var string
     */
    protected $date;

    /**
     * @var string
     */
    protected $dateFormat;

    /**
     * @var string
     */
    protected $reason;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var RecallAnnouncementProduct[]
     */
    protected $products;

    /**
     * @var string
     */
    protected $serialNumbers;

    /**
     * @var string
     */
    protected $batchNumbers;

    /**
     * @var RecallEvent
     */
    protected $event;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getRecallClass()
    {
        return $this->recall_class;
    }

    /**
     * @return string
     */
    public function getRecallLevel()
    {
        return $this->recall_level;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return RecallAnnouncementProduct[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return string
     */
    public function getSerialNumbers()
    {
        return $this->serialNumbers;
    }

    /**
     * @return string
     */
    public function getBatchNumbers()
    {
        return $this->batchNumbers;
    }

    /**
     * @return RecallEvent
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * @param $json
     * @return RecallAnnouncementInfo
     */
    public static function fromJson($json)
    {
        $instance = new RecallAnnouncementInfo();

        $instance->id = $json->id;
        if (!empty($json->recall_level) && isset($json->recall_level)) {
            $instance->recall_level = $json->recall_level;
        }
        if (!empty($json->recall_class) && isset($json->recall_class)) {
            $instance->recall_class = $json->recall_class;
        }

        $instance->date = $json->date;
        $instance->dateFormat = $json->dateFormat;
        $instance->title = $json->title;
        $instance->reason = $json->reason;
        if (!empty($json->products) && isset($json->products)) {
            foreach ($json->products as $product) {
                $instance->products[] = RecallAnnouncementProduct::fromJson($product);
            }
        }
        if (!empty($json->event) && isset($json->event)) {
            $instance->event = RecallEvent::fromJson($json->event);
        }

        if (!empty($json->serialNumbers) && isset($json->serialNumbers)) {
            foreach ($json->serialNumbers as $serialNumber) {
                $instance->serialNumbers[] = $serialNumber;
            }
        }

        if (!empty($json->batchNumbers) && isset($json->batchNumbers)) {
            foreach ($json->batchNumbers as $batchNumber) {
                $instance->batchNumbers[] = $batchNumber;
            }
        }

        return $instance;
    }
}