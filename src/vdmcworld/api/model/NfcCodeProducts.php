<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class NfcCodeProducts implements Model
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var CodeProduct[]
     */
    protected $products = [];

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param $json
     * @return NfcCodeProducts
     */
    public static function fromJson($json)
    {
        $instance = new NfcCodeProducts();
        $instance->code = $json->nfcCode;
        if ($json->products) {
            foreach ($json->products as $p) {
                $instance->products[] = CodeProduct::fromJson($p);
            }
        }
        return $instance;
    }
}