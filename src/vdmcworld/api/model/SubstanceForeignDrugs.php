<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceForeignDrugs implements Model
{
    /**
     * @var Substance
     */
    protected $substance;

    /**
     * @var ForeignDrug[]
     */
    protected $foreignDrugs = [];

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return ForeignDrug[]
     */
    public function getForeignDrugs()
    {
        return $this->foreignDrugs;
    }

    /**
     * @param $json
     * @return SubstanceForeignDrugs
     */
    public static function fromJson($json)
    {
        $instance = new SubstanceForeignDrugs();

        $instance->substance = Substance::fromJson($json->substance);

        if ($json->foreignDrugs) {
            foreach ($json->foreignDrugs as $fd) {
                $instance->foreignDrugs[] = ForeignDrug::fromJson($fd);
            }
        }

        return $instance;
    }
}