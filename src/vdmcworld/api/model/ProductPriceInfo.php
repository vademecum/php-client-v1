<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductPriceInfo implements Model
{

    /**
     * @var float
     */
    protected $factory;

    /**
     * @var float
     */
    protected $storage;

    /**
     * @var float
     */
    protected $pharmacy;

    /**
     * @var float
     */
    protected $retail;

    /**
     * @var \DateTime
     */
    protected $effectiveDate;

    /**
     * @var \DateTime
     */
    protected $historyDate;


    /**
     * @var \DateTime
     */
    protected $historyDescription;

    /**
     * @var float
     */
    protected $taxPercent;

    /**
     * @var string
     */
    protected $currencyCode;

    /**
     * @var bool
     */
    protected $abroadProduct;

    /**
     * @var float
     */
    protected $abroadRetail;

    /**
     * @var float
     */
    protected $storageSalesPrice;

    /**
     * @var bool
     */
    protected $storageBased;

    /**
     * @var float
     */
    protected $discountRate;
    /**
     * @var float
     */
    protected $publicPriceVATInc;
    /**
     * @var float
     */
    protected $publicPrice;

    /**
     * @var PublicPaidPrice[]
     */
    protected $publicPaidPrice;

    /**
     * @var float
     */
    protected $abroadPublicPrice;

    /**
     * @var float
     */
    protected $publicFactory;

    /**
     * @var float
     */
    protected $publicStorage;

    /**
     * @var integer
     */
    protected $discountRateType;

    /**
     * @var PublicPaidPrice[]
     */
    protected $abroadPublicPaidPrice;


    /**
     * @return float
     */
    public function getStorageSalesPrice()
    {
        return $this->storageSalesPrice;
    }

    /**
     * @return bool
     */
    public function isStorageBased()
    {
        return $this->storageBased;
    }

    /**
     * @return float
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @return float
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @return float
     */
    public function getPharmacy()
    {
        return $this->pharmacy;
    }

    /**
     * @return float
     */
    public function getRetail()
    {
        return $this->retail;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }

    /**
     * @return \DateTime
     */
    public function getHistoryDate()
    {
        return $this->historyDate;
    }

    /**
     * @return \DateTime
     */
    public function getHistoryDescription()
    {
        return $this->historyDescription;
    }


    /**
     * @return float
     */
    public function getTaxPercent()
    {
        return $this->taxPercent;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @return bool
     */
    public function isAbroadProduct()
    {
        return $this->abroadProduct;
    }

    /**
     * @return float
     */
    public function getAbroadRetail()
    {
        return $this->abroadRetail;
    }

    /**
     * @return float
     */
    public function getDiscountRate()
    {
        return $this->discountRate;
    }

    /**
     * @param float $discountRate
     */
    public function setDiscountRate($discountRate)
    {
        $this->discountRate = $discountRate;
    }

    /**
     * @return float
     */
    public function getPublicPriceVATInc()
    {
        return $this->publicPriceVATInc;
    }

    /**
     * @param float $publicPriceVATInc
     */
    public function setPublicPriceVATInc($publicPriceVATInc)
    {
        $this->publicPriceVATInc = $publicPriceVATInc;
    }

    /**
     * @return float
     */
    public function getPublicPrice()
    {
        return $this->publicPrice;
    }

    /**
     * @param float $publicPrice
     */
    public function setPublicPrice($publicPrice)
    {
        $this->publicPrice = $publicPrice;
    }

    /**
     * @return PublicPaidPrice
     */
    public function getPublicPaidPrice()
    {
        return $this->publicPaidPrice;
    }

    /**
     * @param PublicPaidPrice $publicPaidPrice
     */
    public function setPublicPaidPrice($publicPaidPrice)
    {
        $this->publicPaidPrice = $publicPaidPrice;
    }

    /**
     * @return float
     */
    public function getAbroadPublicPrice()
    {
        return $this->abroadPublicPrice;
    }

    /**
     * @param PublicPaidPrice $abroadPublicPrice
     */
    public function setAbroadPublicPrice($abroadPublicPrice)
    {
        $this->abroadPublicPrice = $abroadPublicPrice;
    }

    /**
     * @return float
     */
    public function getPublicFactory()
    {
        return $this->publicFactory;
    }

    /**
     * @param float $publicFactory
     */
    public function setPublicFactory($publicFactory)
    {
        $this->publicFactory = $publicFactory;
    }

    /**
     * @return float
     */
    public function getPublicStorage()
    {
        return $this->publicStorage;
    }

    /**
     * @param float $publicStorage
     */
    public function setPublicStorage($publicStorage)
    {
        $this->publicStorage = $publicStorage;
    }

    /**
     * @return int
     */
    public function getDiscountRateType()
    {
        return $this->discountRateType;
    }

    /**
     * @param int $discountRateType
     */
    public function setDiscountRateType($discountRateType)
    {
        $this->discountRateType = $discountRateType;
    }

    /**
     * @return float
     */
    public function getAbroadPublicPaidPrice()
    {
        return $this->abroadPublicPaidPrice;
    }

    /**
     * @param float $abroadPublicPaidPrice
     */
    public function setAbroadPublicPaidPrice($abroadPublicPaidPrice)
    {
        $this->abroadPublicPaidPrice = $abroadPublicPaidPrice;
    }


    /**
     * @param $json
     * @return TitckPriceInfo
     */
    public static function fromJson($json)
    {
        $instance = new ProductPriceInfo();

        foreach ([
                     "factory", "storage", "pharmacy", "retail", "taxPercent", "currencyCode",
                     "abroadProduct", "abroadRetail", "storageSalesPrice", "storageBased", "discountRate",
                     "publicPriceVATInc", "publicPrice", "abroadPublicPrice", "abroadPublicPaidPrice", "publicFactory", "publicStorage", "discountRateType", "historyDescription"
                 ] as $key) {
            if (isset($json->$key)) {
                $instance->$key = $json->$key;
            }
        }
        $instance->publicPaidPrice = [];
        if ($json->publicPaidPrice) {
            foreach ($json->publicPaidPrice as $publicPaidPrice) {
                $instance->publicPaidPrice[] = PublicPaidPrice::fromJson($publicPaidPrice);
            }
        }

        $instance->abroadPublicPaidPrice = [];
        if ($json->abroadPublicPaidPrice) {
            foreach ($json->abroadPublicPaidPrice as $item) {
                $instance->abroadPublicPaidPrice[] = PublicPaidPrice::fromJson($item);
            }
        }

        $instance->effectiveDate = \DateTime::createFromFormat(
            "Y-m-d H:i:s",
            $json->effectiveDate . " 00:00:00",
            new \DateTimeZone("UTC")
        );

        if (isset($json->historyDate)) {
            $instance->historyDate = \DateTime::createFromFormat(
                "Y-m-d H:i:s",
                $json->historyDate . " 00:00:00",
                new \DateTimeZone("UTC")
            );
        }


        return $instance;
    }
}