<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductProspektusIndex implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var ProspektusIndex[]
     */
    private $prospektusIndex = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ProspektusIndex[]
     */
    public function getProspektusIndex()
    {
        return $this->prospektusIndex;
    }

    /**
     * @param $json
     * @return ProductProspektusIndex
     */
    public static function fromJson($json)
    {
        $instance = new self();
        $instance->product = Product::fromJson($json->product);
        $instance->prospektusIndex = [];
        if ($json->prospektusIndex && !empty($json->prospektusIndex)) {
            foreach ($json->prospektusIndex as $k) {
                $instance->prospektusIndex[] = ProspektusIndex::fromJson($k);
            }
        }
        return $instance;
    }
}