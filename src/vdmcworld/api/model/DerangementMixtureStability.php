<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementMixtureStability implements Model
{
    /* @var DerangementMixture */
    protected $mixture;

    /* @var DerangementStability[] */
    protected $stabilities = [];

    /**
     * @return DerangementMixture
     */
    public function getMixture()
    {
        return $this->mixture;
    }

    /**
     * @return DerangementStability[]
     */
    public function getStabilities()
    {
        return $this->stabilities;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->mixture)) {
            $c->mixture = DerangementMixture::fromJson($json->mixture);
        }

        if (!empty($json->stabilities)) {
            foreach ($json->stabilities as $st) {
                $c->stabilities [] = DerangementStability::fromJson($st);
            }
        }

        return $c;
    }
}
