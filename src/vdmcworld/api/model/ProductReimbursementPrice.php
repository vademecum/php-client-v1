<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductReimbursementPrice implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ReimbursementPriceInfo
     */
    protected $priceInfo;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ReimbursementPriceInfo
     */
    public function getPriceInfo()
    {
        return $this->priceInfo;
    }


    /**
     * @param $json
     * @return ProductReimbursementPrice
     */
    public static function fromJson($json)
    {
        $instance = new ProductReimbursementPrice();
        $instance->product = Product::fromJson($json->product);
        if ($json->priceInfo) {
            $instance->priceInfo = ReimbursementPriceInfo::fromJson($json->priceInfo);
        }
        return $instance;
    }
}