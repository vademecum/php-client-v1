<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceGroup implements Model
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return SubstanceGroup
     */
    public static function fromJson($json)
    {
        $g = new SubstanceGroup();
        if ($json->id) {
            $g->id = $json->id;
        }
        if ($json->name) {
            $g->name = $json->name;
        }
        return $g;
    }
}