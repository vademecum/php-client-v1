<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class CompanyCard implements Model
{
    /**
     * @var Company
     */
    protected $company;

    /**
     * @var CompanyCardCard
     */
    protected $card;

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return CompanyCardCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param $json
     * @return CompanyCard
     */
    public static function fromJson($json)
    {
        $instance = new CompanyCard();
        $instance->company = Company::fromJson($json->company);
        $instance->card = CompanyCardCard::fromJson($json->card);
        return $instance;
    }
}