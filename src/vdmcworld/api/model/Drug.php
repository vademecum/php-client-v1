<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Drug implements Model
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return Model
     */
    public static function fromJson($json)
    {
        $d = new Drug();
        $d->id = $json->id;
        $d->name = $json->name;
        return $d;
    }
}