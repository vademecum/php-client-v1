<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DopingData implements Model
{
    /**
     * @var string
     */
    protected $dopingInfo;

    /**
     * @var string
     */
    protected $nutritionInteractionWarnings;

    /**
     * @var string
     */
    protected $nutritionInteractions;

    /**
     * @return string
     */
    public function getDopingInfo()
    {
        return $this->dopingInfo;
    }

    /**
     * @return string
     */
    public function getNutritionInteractionWarnings()
    {
        return $this->nutritionInteractionWarnings;
    }

    /**
     * @return string
     */
    public function getNutritionInteractions()
    {
        return $this->nutritionInteractions;
    }

    /**
     * @param $json
     * @return DopingData
     */
    public static function fromJson($json)
    {
        $instance = new DopingData();
        $instance->dopingInfo = $json->dopingInfo ? DopingInfo::fromJson($json->dopingInfo) : null;
        $instance->nutritionInteractions = null;
        if ($json->nutritionInteractions) {
            foreach ($json->nutritionInteractions as $interaction) {
                $instance->nutritionInteractions[] = DopingNuritionInteraction::fromJson($interaction);
            }
        }
        $instance->nutritionInteractionWarnings = null;
        if ($json->nutritionInteractionWarnings) {
            foreach ($json->nutritionInteractionWarnings as $interaction) {
                $instance->nutritionInteractionWarnings[] = DopingNutritionInteractionWarning::fromJson($interaction);
            }
        }

        return $instance;
    }
}