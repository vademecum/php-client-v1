<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class LactationWarning implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return LactationWarning
     */
    public static function fromJson($json)
    {
        $lactationWarning = new LactationWarning();
        $lactationWarning->id = $json->id;
        $lactationWarning->name = $json->name;


        return $lactationWarning;
    }
}