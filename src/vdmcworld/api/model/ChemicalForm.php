<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ChemicalForm implements Model
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return ChemicalForm
     */
    public static function fromJson($json)
    {
        $instance = new ChemicalForm();
        $instance->id = $json->id;
        $instance->name = $json->name;
        return $instance;
    }
}