<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Unit implements Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return Unit
     */
    public static function fromJson($json)
    {
        $u = new Unit();
        $u->id = $json->id;
        $u->name = $json->name;
        return $u;
    }
}