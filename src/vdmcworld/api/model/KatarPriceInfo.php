<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class KatarPriceInfo implements Model
{

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var float
     */
    protected $retailPrice;

    /**
     * @var float
     */
    protected $factoryPrice;

    /**
     * @var float
     */
    protected $averageWholesalePrice;

    /**
     * @var float
     */
    protected $wholesaleAcquisitionCost;


    /**
     * @var float
     */
    protected $wholesaleSellingPrice;

    /**
     * @var string
     */
    protected $currency;

    /**
     * @var float
     */
    protected $manufacturersSuggestedRetailPrice;

    /**
     * @var float
     */
    protected $pharmacyBenefits;

    /**
     * @var float
     */
    protected $cifPrice;

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return float
     */
    public function getRetailPrice()
    {
        return $this->retailPrice;
    }

    /**
     * @return float
     */
    public function getFactoryPrice()
    {
        return $this->factoryPrice;
    }

    /**
     * @return float
     */
    public function getAverageWholesalePrice()
    {
        return $this->averageWholesalePrice;
    }

    /**
     * @return float
     */
    public function getWholesaleAcquisitionCost()
    {
        return $this->wholesaleAcquisitionCost;
    }

    /**
     * @return float
     */
    public function getWholesaleSellingPrice()
    {
        return $this->wholesaleSellingPrice;
    }


    /**
     * @return float
     */
    public function getManufacturersSuggestedRetailPrice()
    {
        return $this->manufacturersSuggestedRetailPrice;
    }

    /**
     * @return float
     */
    public function getPharmacyBenefits()
    {
        return $this->pharmacyBenefits;
    }

    /**
     * @return float
     */
    public function getCifPrice()
    {
        return $this->cifPrice;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }


    /**
     * @param $json
     * @return KatarPriceInfo
     */
    public static function fromJson($json)
    {
        $instance = new KatarPriceInfo();
        $keys = [
            "date",
            "retailPrice",
            "factoryPrice",
            "averageWholesalePrice",
            "wholesaleAcquisitionCost",
            "wholesaleSellingPrice",
            "manufacturersSuggestedRetailPrice",
            "pharmacyBenefits",
            "cifPrice",
            "currency"
        ];

        foreach ($keys as $key) {
            $instance->$key = $json->$key;
        }

        $instance->date = \DateTime::createFromFormat(
            "Y-m-d H:i:s",
            $json->date . " 00:00:00",
            new \DateTimeZone("UTC")
        );

        return $instance;
    }
}