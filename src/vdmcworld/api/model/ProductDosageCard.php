<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductDosageCard implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var DosageCard
     */
    protected $card;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return DosageCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param $json
     * @return ProductDosageCard
     */
    public static function fromJson($json)
    {
        $instance = new ProductDosageCard();
        $instance->product = Product::fromJson($json->product);
        $instance->card = DosageCard::fromJson($json->card);
        return $instance;
    }
}