<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ReimbursementDetail implements Model
{
    /**
     * @var ReimbursementCondition
     */
    protected $condition;

    /**
     * @var ReimbursementConditionDetail[]
     */
    protected $details = [];

    /**
     * @return ReimbursementCondition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @return ReimbursementConditionDetail[]
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param $json
     * @return ReimbursementDetail
     */
    public static function fromJson($json)
    {
        $instance = new ReimbursementDetail();
        if ($json->reimbursementCondition) {
            $instance->condition = ReimbursementCondition::fromJson($json->reimbursementCondition);
        }
        if ($json->detail) {
            foreach ($json->detail as $d) {
                $instance->details[] = ReimbursementConditionDetail::fromJson($d);
            }
        }
        return $instance;
    }
}