<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementMixtureIngredientSubstance implements Model
{
    /**
     * @var Substance
     */
    protected $substance = null;

    /**
     * @var DerangementMixtureIngredientConcentration
     */
    protected $concentration = null;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return DerangementMixtureIngredientConcentration
     */
    public function getConcentration()
    {
        return $this->concentration;
    }

    public static function fromJson($json)
    {
        $c = new self();
        if (isset($json->substance)) {
            $c->substance = Substance::fromJson($json->substance);
        }
        if (isset($json->concentration)) {
            $c->concentration = DerangementMixtureIngredientConcentration::fromJson($json->concentration);
        }
        return $c;
    }
}