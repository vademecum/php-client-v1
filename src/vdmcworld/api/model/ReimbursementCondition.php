<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ReimbursementCondition implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return ReimbursementCondition
     */
    public static function fromJson($json)
    {
        $rc = new ReimbursementCondition();
        $rc->id = $json->id;
        $rc->name = $json->name;
        return $rc;
    }
}