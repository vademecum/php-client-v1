<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ReimbursementConditionDetail implements Model
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $weight;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param $json
     * @return ReimbursementConditionDetail
     */
    public static function fromJson($json)
    {
        $instance = new ReimbursementConditionDetail();
        $instance->name = $json->name;
        $instance->description = $json->description;
        $instance->weight = $json->weight;
        return $instance;
    }
}