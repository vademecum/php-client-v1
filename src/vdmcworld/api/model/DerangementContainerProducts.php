<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementContainerProducts implements Model
{
    /* @var DerangementContainer */
    protected $container;

    /* @var Product[] */
    protected $products = [];

    /**
     * @return DerangementContainer
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->container)) {
            $c->container = DerangementContainer::fromJson($json->container);
        }

        if (isset($json->products)) {
            foreach ($json->products as $cd) {
                $c->products [] = Product::fromJson($cd);
            }
        }

        return $c;
    }
}
