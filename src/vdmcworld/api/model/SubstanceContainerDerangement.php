<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class SubstanceContainerDerangement implements Model
{
    /* @var Substance */
    protected $substance;

    /* @var ContainerDerangement[] */
    protected $containerDerangements = [];

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return ContainerDerangement[]
     */
    public function getContainerDerangements()
    {
        return $this->containerDerangements;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->substance)) {
            $c->substance = Substance::fromJson($json->substance);
        }

        if (isset($json->containerDerangements)) {
            foreach ($json->containerDerangements as $cd) {
                $c->containerDerangements [] = ContainerDerangement::fromJson($cd);
            }
        }

        return $c;
    }
}
