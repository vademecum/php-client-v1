<?php

namespace vdmcworld\api\model;


class ProductCardSpecialInformation extends BaseSpecialInformation
{
    /**
     * @param $json
     * @return ProductCardSpecialInformation
     */
    public static function fromJson($json)
    {
        $instance = new ProductCardSpecialInformation();

        self::parseJson($json, $instance);

        return $instance;
    }
}
