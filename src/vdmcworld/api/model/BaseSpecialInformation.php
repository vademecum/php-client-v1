<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

abstract class BaseSpecialInformation implements Model
{
    /**
     * @var Status
     */
    protected $pediatricStatus;

    /**
     * @var Status
     */
    protected $emergencyPediatricStatus;

    /**
     * @var Status
     */
    protected $lightProtectionStatus;

    /**
     * @var Status
     */
    protected $moistureProtectionStatus;

    /**
     * @var Status
     */
    protected $coldChainStatus;

    /**
     * @var Status
     */
    protected $cytotoxicStatus;

    /**
     * @var Status
     */
    protected $additionalMonitoringStatus;

    /**
     * @var Status
     */
    protected $orderedDistributionStatus;

    /**
     * @var Status
     */
    protected $controlledDistributionStatus;

    /**
     * @var Status
     */
    protected $mandatoryChemoDrugStatus;

    /**
     * @var Status
     */
    protected $dailyTreatmentStatus;

    /**
     * @var Status
     */
    protected $indicationCompatibilityStatus;

    /**
     * @var Status
     */
    protected $offLabelUsageStatus;

    /**
     * @var Status
     */
    protected $hospitalProductStatus;

    /**
     * @var Status
     */
    protected $dualPricedProductStatus;

    /**
     * @var Status
     */
    protected $currentExchangeRateProductStatus;

    /**
     * @var Status
     */
    protected $orphanDrugStatus;

    /**
     * @var Status
     */
    protected $patientApprovalFormStatus;

    /**
     * @var Status
     */
    protected $drugSafetyMonitoringFormStatus;

    /**
     * @var Status
     */
    protected $ceCertificationStatus;

    /**
     * @var Status
     */
    protected $highRiskStatus;

    /**
     * @var Status
     */
    protected $freeMarketPriceStatus;

    /**
     * @var Status
     */
    protected $lactationStatus;

    /**
     * @return Status
     */
    public function getPediatricStatus()
    {
        return $this->pediatricStatus;
    }

    /**
     * @return Status
     */
    public function getEmergencyPediatricStatus()
    {
        return $this->emergencyPediatricStatus;
    }

    /**
     * @return Status
     */
    public function getLightProtectionStatus()
    {
        return $this->lightProtectionStatus;
    }

    /**
     * @return Status
     */
    public function getMoistureProtectionStatus()
    {
        return $this->moistureProtectionStatus;
    }

    /**
     * @return Status
     */
    public function getColdChainStatus()
    {
        return $this->coldChainStatus;
    }

    /**
     * @return Status
     */
    public function getCytotoxicStatus()
    {
        return $this->cytotoxicStatus;
    }

    /**
     * @return Status
     */
    public function getAdditionalMonitoringStatus()
    {
        return $this->additionalMonitoringStatus;
    }

    /**
     * @return Status
     */
    public function getOrderedDistributionStatus()
    {
        return $this->orderedDistributionStatus;
    }

    /**
     * @return Status
     */
    public function getControlledDistributionStatus()
    {
        return $this->controlledDistributionStatus;
    }

    /**
     * @return Status
     */
    public function getMandatoryChemoDrugStatus()
    {
        return $this->mandatoryChemoDrugStatus;
    }

    /**
     * @return Status
     */
    public function getDailyTreatmentStatus()
    {
        return $this->dailyTreatmentStatus;
    }

    /**
     * @return Status
     */
    public function getIndicationCompatibilityStatus()
    {
        return $this->indicationCompatibilityStatus;
    }

    /**
     * @return Status
     */
    public function getOffLabelUsageStatus()
    {
        return $this->offLabelUsageStatus;
    }

    /**
     * @return Status
     */
    public function getHospitalProductStatus()
    {
        return $this->hospitalProductStatus;
    }

    /**
     * @return Status
     */
    public function getDualPricedProductStatus()
    {
        return $this->dualPricedProductStatus;
    }

    /**
     * @return Status
     */
    public function getCurrentExchangeRateProductStatus()
    {
        return $this->currentExchangeRateProductStatus;
    }

    /**
     * @return Status
     */
    public function getOrphanDrugStatus()
    {
        return $this->orphanDrugStatus;
    }

    /**
     * @return Status
     */
    public function getPatientApprovalFormStatus()
    {
        return $this->patientApprovalFormStatus;
    }

    /**
     * @return Status
     */
    public function getDrugSafetyMonitoringFormStatus()
    {
        return $this->drugSafetyMonitoringFormStatus;
    }

    /**
     * @return Status
     */
    public function getCeCertificationStatus()
    {
        return $this->ceCertificationStatus;
    }

    /**
     * @return Status
     */
    public function getHighRiskStatus()
    {
        return $this->highRiskStatus;
    }

    /**
     * @return Status
     */
    public function getFreeMarketPriceStatus()
    {
        return $this->freeMarketPriceStatus;
    }

    /**
     * @return Status
     */
    public function getLactation()
    {
        return $this->lactationStatus;
    }

    /**
     * @param $json
     * @param BaseSpecialInformation
     * @return BaseSpecialInformation
     */
    public static function parseJson($json, $instance)
    {
        foreach (["pediatricStatus", "emergencyPediatricStatus",
                     "lightProtectionStatus", "moistureProtectionStatus", "coldChainStatus",
                     "cytotoxicStatus", "additionalMonitoringStatus", "orderedDistributionStatus", "controlledDistributionStatus",
                     "mandatoryChemoDrugStatus", "dailyTreatmentStatus", "indicationCompatibilityStatus",
                     "offLabelUsageStatus", "hospitalProductStatus", "dualPricedProductStatus",
                     "currentExchangeRateProductStatus", "orphanDrugStatus", "patientApprovalFormStatus", "drugSafetyMonitoringFormStatus",
                     "ceCertificationStatus", "highRiskStatus", "freeMarketPriceStatus", "lactationStatus",
                 ]
                 as $k) {
            $instance->{$k} = Status::fromJson($json->{$k});
        }

        return $instance;
    }
}