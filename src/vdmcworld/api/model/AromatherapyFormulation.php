<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class AromatherapyFormulation implements Model
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return AromatherapyFormulation
     */
    public static function fromJson($json)
    {
        $aromatherapyFormulation = new AromatherapyFormulation();

        $aromatherapyFormulation->id = $json->id;
        $aromatherapyFormulation->name = $json->name;

        return $aromatherapyFormulation;
    }
}