<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SplashScreen implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $start_date;

    /**
     * @var int
     */
    protected $end_date;

    /**
     * @var int
     */
    protected $duration;

    /**
     * @var string
     */
    protected $image_url;

    /**
     * @var string
     */
    protected $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param int $start_date
     */
    public function setStartDate(int $start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return int
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param int $end_date
     */
    public function setEndDate(int $end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @param string $image_url
     */
    public function setImageUrl(string $image_url)
    {
        $this->image_url = $image_url;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }


    /**
     * @param $json
     * @return SplashScreen
     */
    public static function fromJson($json)
    {
        $instance = new SplashScreen();

        $instance->id = $json->id;
        $instance->start_date = $json->start_date;
        $instance->end_date = $json->end_date;
        $instance->duration = $json->duration;
        $instance->image_url = $json->image_url;
        $instance->description = $json->description;

        return $instance;
    }
}