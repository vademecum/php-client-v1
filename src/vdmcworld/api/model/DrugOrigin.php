<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class DrugOrigin implements Model
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return DrugOrigin
     */
    public static function fromJson($json)
    {
        $instance = new DrugOrigin();
        $instance->id = $json->id;
        $instance->description = $json->description;
        return $instance;
    }
}