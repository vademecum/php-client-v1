<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceSgkCodes implements Model
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $sgk_code;
    /**
     * @var ApplicationMethod
     */
    protected $application_method;

    /** @var SubstanceSgkCodesSubstance */
    protected $substances;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSgkCode()
    {
        return $this->sgk_code;
    }

    /**
     * @return ApplicationMethod
     */
    public function getApplicationMethod()
    {
        return $this->application_method;
    }

    /**
     * @return SubstanceSgkCodesSubstance
     */
    public function getSubstances()
    {
        return $this->substances;
    }


    protected static function parseJson($json, $instance)
    {
        $instance->id = $json->id;
        $instance->sgk_code = $json->sgk_code;
        if (isset($json->application_method) && !empty($json->application_method)) {
            $instance->application_method = $json->application_method;
        }
        if (isset($json->substances) && !empty($json->substances)) {
            $instance->substances = $json->substances;
        }
        return $instance;
    }

    /**
     * @param $json
     * @return SubstanceSgkCodes
     */
    public static function fromJson($json)
    {
        return self::parseJson($json, new SubstanceSgkCodes());
    }
}