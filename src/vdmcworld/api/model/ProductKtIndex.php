<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductKtIndex implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var KtIndex[]
     */
    private $ktIndex = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return KtIndex[]
     */
    public function getKtIndex()
    {
        return $this->ktIndex;
    }

    /**
     * @param $json
     * @return ProductKtIndex
     */
    public static function fromJson($json)
    {
        $instance = new ProductKtIndex();
        $instance->product = Product::fromJson($json->product);
        $instance->ktIndex = [];
        if ($json->ktIndex && !empty($json->ktIndex)) {
            foreach ($json->ktIndex as $k) {
                $instance->ktIndex[] = KtIndex::fromJson($k);
            }
        }
        return $instance;
    }
}