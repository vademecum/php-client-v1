<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductKubIndex implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var KubIndex[]
     */
    private $kubIndex = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return KubIndex[]
     */
    public function getKubIndex()
    {
        return $this->kubIndex;
    }

    /**
     * @param $json
     * @return ProductKubIndex
     */
    public static function fromJson($json)
    {
        $instance = new ProductKubIndex();
        $instance->product = Product::fromJson($json->product);
        $instance->kubIndex = [];
        if ($json->kubIndex && !empty($json->kubIndex)) {
            foreach ($json->kubIndex as $k) {
                $instance->kubIndex[] = KubIndex::fromJson($k);
            }
        }
        return $instance;
    }
}