<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductPrescriptionRule implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var SubstancePrescriptionRule[]
     */
    protected $substancePrescriptionRules;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return SubstancePrescriptionRule[]
     */
    public function getSubstancePrescriptionRules()
    {
        return $this->substancePrescriptionRules;
    }

    /**
     * @param $json
     * @return ProductPrescriptionRule
     */
    public static function fromJson($json)
    {
        $ppr = new ProductPrescriptionRule();
        $ppr->product = Product::fromJson($json->product);
        if ($json->substancePrescriptionRules) {
            foreach ($json->substancePrescriptionRules as $spr) {
                $ppr->substancePrescriptionRules[] = SubstancePrescriptionRule::fromJson($spr);
            }
        }
        return $ppr;
    }
}