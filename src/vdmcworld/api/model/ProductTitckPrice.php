<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductTitckPrice implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var TitckPriceInfo
     */
    protected $priceInfo;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return TitckPriceInfo
     */
    public function getPriceInfo()
    {
        return $this->priceInfo;
    }

    /**
     * @param $json
     * @return ProductTitckPrice
     */
    public static function fromJson($json)
    {
        $instance = new ProductTitckPrice();
        $instance->product = Product::fromJson($json->product);
        if ($json->priceInfo) {
            $instance->priceInfo = TitckPriceInfo::fromJson($json->priceInfo);
        }
        return $instance;
    }
}