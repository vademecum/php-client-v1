<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementContainerSubstances implements Model
{
    /* @var DerangementContainer */
    protected $container;

    /* @var Substance[] */
    protected $substances = [];

    /**
     * @return DerangementContainer
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return Substance[]
     */
    public function getSubstances()
    {
        return $this->substances;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->container)) {
            $c->container = DerangementContainer::fromJson($json->container);
        }

        if (isset($json->substances)) {
            foreach ($json->substances as $cd) {
                $c->substances [] = Substance::fromJson($cd);
            }
        }

        return $c;
    }
}
