<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class Prescriber implements Model
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $areaOfSpecializationId;

    /**
     * @var string
     */
    protected $note;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAreaOfSpecializationId()
    {
        return $this->areaOfSpecializationId;
    }

    /**
     * @return int
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param $json
     * @return Prescriber
     */
    public static function fromJson($json)
    {
        $p = new Prescriber();
        $p->id = $json->id;
        $p->name = $json->name;
        if (!empty($json->note)) {
            $p->note = $json->note;
        }
        if (!empty($json->areaOfSpecializationId)) {
            $p->areaOfSpecializationId = $json->areaOfSpecializationId;
        }

        return $p;
    }
}