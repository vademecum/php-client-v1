<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Status implements Model
{
    /**
     * @var bool
     */
    private $status;
    /**
     * @var string
     */
    private $description;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $json
     * @return Status
     */
    public static function fromJson($json)
    {
        $s = new Status();
        $s->status = $json->status;
        $s->description = $json->description;
        return $s;
    }
}