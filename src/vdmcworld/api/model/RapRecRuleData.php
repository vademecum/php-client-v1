<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class RapRecRuleData implements Model
{
    /**
     * @var RapRecRuleDataItem
     */
    private $reportWriter;

    /**
     * @var RapRecRuleDataItem
     */
    private $childPatientHeightGrowUnder2;

    /**
     * @var RapRecRuleDataItem
     */
    private $childPatientWeightGrowUnder2;

    /**
     * @var RapRecRuleDataItem
     */
    private $growdPatientWeightLess10Percent;

    /**
     * @var RapRecRuleDataItem
     */
    private $growdPatientSubjectiveCategoryCOrD;

    /**
     * @var RapRecRuleDataItem
     */
    private $productNameSpecified;

    /**
     * @var RapRecRuleDataItem
     */
    private $dailyCalorieNeedSpecified;

    /**
     * @var RapRecRuleDataItem
     */
    private $dailyUsageAmountSpecified;

    /**
     * @var RapRecRuleDataItem
     */
    private $maximumDosageAmountIs30;

    /**
     * @return RapRecRuleDataItem
     */
    public function getReportWriter()
    {
        return $this->reportWriter;
    }

    /**
     * @return RapRecRuleDataItem
     */
    public function getChildPatientHeightGrowUnder2()
    {
        return $this->childPatientHeightGrowUnder2;
    }

    /**
     * @return RapRecRuleDataItem
     */
    public function getChildPatientWeightGrowUnder2()
    {
        return $this->childPatientWeightGrowUnder2;
    }

    /**
     * @return RapRecRuleDataItem
     */
    public function getGrowdPatientWeightLess10Percent()
    {
        return $this->growdPatientWeightLess10Percent;
    }

    /**
     * @return RapRecRuleDataItem
     */
    public function getGrowdPatientSubjectiveCategoryCOrD()
    {
        return $this->growdPatientSubjectiveCategoryCOrD;
    }

    /**
     * @return RapRecRuleDataItem
     */
    public function getProductNameSpecified()
    {
        return $this->productNameSpecified;
    }

    /**
     * @return RapRecRuleDataItem
     */
    public function getDailyCalorieNeedSpecified()
    {
        return $this->dailyCalorieNeedSpecified;
    }

    /**
     * @return RapRecRuleDataItem
     */
    public function getDailyUsageAmountSpecified()
    {
        return $this->dailyUsageAmountSpecified;
    }

    /**
     * @return RapRecRuleDataItem
     */
    public function getMaximumDosageAmountIs30()
    {
        return $this->maximumDosageAmountIs30;
    }

    /**
     * @param $json
     * @return RapRecRuleData
     */
    public static function fromJson($json)
    {
        $instance = new RapRecRuleData();
        $fieldMap = [
            "report_writer" => "reportWriter",
            "child_patient_height_grow_under_2" => "childPatientHeightGrowUnder2",
            "child_patient_weight_grow_under_2" => "childPatientWeightGrowUnder2",
            "growd_patient_weight_loss_10_percent" => "growdPatientWeightLess10Percent",
            "growd_patient_subjective_category_c_or_d" => "growdPatientSubjectiveCategoryCOrD",
            "product_name_specified" => "productNameSpecified",
            "daily_calorie_need_specified" => "dailyCalorieNeedSpecified",
            "daily_usage_amount_specified" => "dailyUsageAmountSpecified",
            "maximum_dosage_amount_is_30" => "maximumDosageAmountIs30",
        ];
        foreach ($fieldMap as $f => $attr) {
            if ($json->$f) {
                $instance->$attr = RapRecRuleDataItem::fromJson($json->$f);
            }
        }

        return $instance;
    }
}