<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Disease implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return Disease
     */
    public static function fromJson($json)
    {
        $instance = new Disease();
        $instance->id = $json->id;
        $instance->name = $json->name;
        return $instance;
    }
}