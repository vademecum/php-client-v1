<?php


namespace vdmcworld\api\model;


use Cassandra\Date;
use vdmcworld\api\Model;

class Sut implements Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $sutId;
    /**
     * @var string
     */
    private $sutMadde;
    /**
     * @var string
     */
    private $sutMaddeNo;
    /**
     * @var string
     */
    private $sutMaddeNo2;
    /**
     * @var string
     */
    private $sutMaddeNo3;
    /**
     * @var string
     */
    private $sutMaddeNo4;
    /**
     * @var string
     */
    private $sutMaddeNo5;
    /**
     * @var string
     */
    private $sutMaddeNo6;
    /**
     * @var string
     */
    private $sutMaddeNo7;
    /**
     * @var string
     */
    private $sutMaddeNo8;
    /**
     * @var string
     */
    private $sutMaddeNo9;
    /**
     * @var string
     */
    private $sutMaddeNo10;
    /**
     * @var string
     */
    private $text;
    /**
     * @var string
     */
    private $summary;
    /**
     * @var string
     */
    private $changeType;
    /**
     * @var string
     */
    private $changeNo;
    /**
     * @var \DateTime
     */
    private $changeDate;
    /**
     * @var \DateTime
     */
    private $effectDate;
    /**
     * @var string
     */
    private $changeSummary;
    /**
     * @var \DateTime
     */
    private $createdAt;
    /**
     * @var \DateTime
     */
    private $updatedAt;
    /**
     * @var int
     */
    private $rapRecId;
    /**
     * @var string
     */
    private $warning;
    /**
     * @var boolean
     */
    private $isTitle;
    /**
     * @var boolean
     */
    private $isSubTitle;
    /**
     * @var boolean
     */
    private $isFikra;
    /**
     * @var boolean
     */
    private $isBent;
    /**
     * @var boolean
     */
    private $isAltBent;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSutId()
    {
        return $this->sutId;
    }

    /**
     * @return string
     */
    public function getSutMadde()
    {
        return $this->sutMadde;
    }

    /**
     * @return string
     */
    public function getSutMaddeNo()
    {
        return $this->sutMaddeNo;
    }

    /**
     * @return string
     */
    public function getSutMaddeNo2()
    {
        return $this->sutMaddeNo2;
    }

    /**
     * @return string
     */
    public function getSutMaddeNo3()
    {
        return $this->sutMaddeNo3;
    }

    /**
     * @return string
     */
    public function getSutMaddeNo4()
    {
        return $this->sutMaddeNo4;
    }

    /**
     * @return string
     */
    public function getSutMaddeNo5()
    {
        return $this->sutMaddeNo5;
    }

    /**
     * @return string
     */
    public function getSutMaddeNo6()
    {
        return $this->sutMaddeNo6;
    }

    /**
     * @return string
     */
    public function getSutMaddeNo7()
    {
        return $this->sutMaddeNo7;
    }

    /**
     * @return string
     */
    public function getSutMaddeNo8()
    {
        return $this->sutMaddeNo8;
    }

    /**
     * @return string
     */
    public function getSutMaddeNo9()
    {
        return $this->sutMaddeNo9;
    }

    /**
     * @return string
     */
    public function getSutMaddeNo10()
    {
        return $this->sutMaddeNo10;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @return string
     */
    public function getChangeType()
    {
        return $this->changeType;
    }

    /**
     * @return string
     */
    public function getChangeNo()
    {
        return $this->changeNo;
    }

    /**
     * @return \DateTime
     */
    public function getChangeDate()
    {
        return $this->changeDate;
    }

    /**
     * @return \DateTime
     */
    public function getEffectDate()
    {
        return $this->effectDate;
    }

    /**
     * @return string
     */
    public function getChangeSummary()
    {
        return $this->changeSummary;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getRapRecId()
    {
        return $this->rapRecId;
    }

    /**
     * @return string
     */
    public function getWarning()
    {
        return $this->warning;
    }

    /**
     * @return bool
     */
    public function isTitle()
    {
        return $this->isTitle;
    }

    /**
     * @return bool
     */
    public function isSubTitle()
    {
        return $this->isSubTitle;
    }

    /**
     * @return bool
     */
    public function isFikra()
    {
        return $this->isFikra;
    }

    /**
     * @return bool
     */
    public function isBent()
    {
        return $this->isBent;
    }

    /**
     * @return bool
     */
    public function isAltBent()
    {
        return $this->isAltBent;
    }

    /**
     * @param $json
     * @return Sut
     * @throws \Exception
     */
    public static function fromJson($json)
    {
        $instance = new Sut();

        $keys = [
            "id" => "id",
            "sut_id" => "sutId",
            "sut_madde" => "sutMadde",
            "sut_madde_no" => "sutMaddeNo",
            "sut_madde_no_2" => "sutMaddeNo2",
            "sut_madde_no_3" => "sutMaddeNo3",
            "sut_madde_no_4" => "sutMaddeNo4",
            "sut_madde_no_5" => "sutMaddeNo5",
            "sut_madde_no_6" => "sutMaddeNo6",
            "sut_madde_no_7" => "sutMaddeNo7",
            "sut_madde_no_8" => "sutMaddeNo8",
            "sut_madde_no_9" => "sutMaddeNo9",
            "sut_madde_no_10" => "sutMaddeNo10",
            "text" => "text",
            "summary" => "summary",
            "change_type" => "changeType",
            "change_no" => "changeNo",
            "raprec_id" => "rapRecId",
            "warning" => "warning",
        ];
        foreach ($keys as $k => $v) {
            if ($json->$k) {
                $instance->$v = $json->$k;
            }
        }

        $bools = [
            "is_title" => "isTitle",
            "is_sub_title" => "isSubTitle",
            "is_fikra" => "isFikra",
            "is_bent" => "isBent",
            "is_alt_bent" => "isAltBent",
        ];
        foreach ($bools as $k => $v) {
            if ($json->$k) {
                $instance->$v = true;
            } else {
                $instance->$v = false;
            }
        }

        $instance->changeDate = \DateTime::createFromFormat("Y-m-d", $json->change_date);
        $instance->effectDate = \DateTime::createFromFormat("Y-m-d", $json->effect_date);

        $instance->createdAt = new \DateTime();
        $instance->createdAt->setTimestamp($json->created_at);
        $instance->updatedAt = new \DateTime();
        $instance->updatedAt->setTimestamp($json->updated_at);

        return $instance;
    }
}