<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class SubstanceTemperatureDerangement implements Model
{
    /* @var Substance */
    protected $substance;

    /* @var TemperatureDerangement[] */
    protected $temperatureDerangements = [];

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return TemperatureDerangement[]
     */
    public function getTemperatureDerangements()
    {
        return $this->temperatureDerangements;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->substance)) {
            $c->substance = Substance::fromJson($json->substance);
        }

        if (isset($json->temperatureDerangements)) {
            foreach ($json->temperatureDerangements as $cd) {
                $c->temperatureDerangements [] = TemperatureDerangement::fromJson($cd);
            }
        }

        return $c;
    }
}
