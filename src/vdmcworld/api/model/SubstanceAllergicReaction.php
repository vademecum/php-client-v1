<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceAllergicReaction implements Model
{
    /**
     * @var ApplicationMethod
     */
    private $applicationMethod;

    /**
     * @var AllergicReaction
     */
    private $allergicReaction;

    /**
     * @return ApplicationMethod
     */
    public function getApplicationMethod()
    {
        return $this->applicationMethod;
    }

    /**
     * @return AllergicReaction
     */
    public function getAllergicReaction()
    {
        return $this->allergicReaction;
    }

    /**
     * @param $json
     * @return SubstanceAllergicReaction
     */
    public static function fromJson($json)
    {
        $sar = new SubstanceAllergicReaction();
        $sar->applicationMethod = ApplicationMethod::fromJson($json->applicationMethod);
        $sar->allergicReaction = AllergicReaction::fromJson($json->allergicReaction);
        return $sar;
    }
}