<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PregnancyCategoryDetail implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $priority;

    /**
     * @var string
     */
    protected $value;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $json
     * @return PregnancyCategoryDetail
     */
    public static function fromJson($json)
    {
        $d = new PregnancyCategoryDetail();
        foreach (["id", "name", "description", "priority", "value"] as $key) {
            $d->$key = $json->$key;
        }
        return $d;
    }
}