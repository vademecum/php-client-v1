<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class AlternativeProduct implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Product[]
     */
    protected $alternativeProducts = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Product[]
     */
    public function getAlternativeProducts()
    {
        return $this->alternativeProducts;
    }

    /**
     * @param $json
     * @return AlternativeProduct
     */
    public static function fromJson($json)
    {
        $instance = new AlternativeProduct();
        $instance->product = Product::fromJson($json->product);
        if ($json->alternativeProducts) {
            foreach ($json->alternativeProducts as $p) {
                $instance->alternativeProducts[] = Product::fromJson($p);
            }
        }
        return $instance;
    }
}