<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductReimbursementDetail implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ReimbursementDetail[]
     */
    protected $reimbursementDetail = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ReimbursementDetail[]
     */
    public function getReimbursementDetail()
    {
        return $this->reimbursementDetail;
    }

    /**
     * @param $json
     * @return Model
     */
    public static function fromJson($json)
    {
        $instance = new ProductReimbursementDetail();
        $instance->product = Product::fromJson($json->product);
        if ($json->reimbursementDetail) {
            foreach ($json->reimbursementDetail as $rd) {
                $instance->reimbursementDetail[] = ReimbursementDetail::fromJson($rd);
            }
        }
        return $instance;
    }
}