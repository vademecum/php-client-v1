<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Substance implements Model
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string[]
     */
    protected $alternativeNames;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAlternativeNames()
    {
        return $this->alternativeNames;
    }

    protected static function parseJson($json, $instance)
    {
        $instance->id = $json->id;
        $instance->name = $json->name;
        if (isset($json->alternativeNames) && !empty($json->alternativeNames)) {
            $instance->alternativeNames = $json->alternativeNames;
        }
        return $instance;
    }

    /**
     * @param $json
     * @return Substance
     */
    public static function fromJson($json)
    {
        return self::parseJson($json, new Substance());
    }
}