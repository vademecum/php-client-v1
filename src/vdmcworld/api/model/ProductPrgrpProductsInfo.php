<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductPrgrpProductsInfo implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var PrgrpProductsInfo[]
     */
    protected $priceReferenceGroupProductsInfo = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return PrgrpProductsInfo[]
     */
    public function getPriceReferenceGroupProductsInfo()
    {
        return $this->priceReferenceGroupProductsInfo;
    }

    /**
     * @param $json
     * @return ProductPrgrpProductsInfo
     */
    public static function fromJson($json)
    {
        $instance = new ProductPrgrpProductsInfo();
        $instance->product = Product::fromJson($json->product);
        if ($json->priceReferenceGroupProductsInfo) {
            foreach ($json->priceReferenceGroupProductsInfo as $p) {
                $instance->priceReferenceGroupProductsInfo[] = PrgrpProductsInfo::fromJson($p);
            }
        }

        return $instance;
    }
}