<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class NewProduct implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var DrugType
     */
    protected $drugType;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var Status
     */
    protected $reimbursementStatus;

    /**
     * @var Status
     */
    protected $onMarketStatus;

    /**
     * @var Substance[]
     */
    protected $substances;

    /**
     * @var integer
     */
    protected $createdAt;


    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return DrugType
     */
    public function getDrugType()
    {
        return $this->drugType;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }


    /**
     * @return Status
     */
    public function getReimbursementStatus()
    {
        return $this->reimbursementStatus;
    }

    /**
     * @return Status
     */
    public function getMarketStatus()
    {
        return $this->onMarketStatus;
    }

    public function getSubstances()
    {
        return $this->substances;
    }

    /**
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $json
     * @return NewProduct
     */
    public static function fromJson($json)
    {
        $instance = new NewProduct();

        $instance->product = Product::fromJson($json->product);
        $instance->drugType = DrugType::fromJson($json->drugType);
        if ($json->company) {
            $instance->company = Company::fromJson($json->company);
        }

        if (isset($json->reimbursementStatus)) {
            $instance->reimbursementStatus = Status::fromJson($json->reimbursementStatus);
        }
        if (isset($json->onMarketStatus)) {
            $instance->onMarketStatus = Status::fromJson($json->onMarketStatus);
        }

        if (isset($json->substances)) {
            $instance->substances = $json->substances;
        }

        $instance->createdAt = $json->createdAt;

        return $instance;
    }
}