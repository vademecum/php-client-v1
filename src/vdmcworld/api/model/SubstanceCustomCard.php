<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceCustomCard implements Model
{
    /**
     * @var Substance
     */
    protected $substance;

    /**
     * @var CustomCard
     */
    protected $card;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return CustomCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param $json
     * @return SubstanceCustomCard
     */
    public static function fromJson($json)
    {
        $instance = new SubstanceCustomCard();
        $instance->substance = Substance::fromJson($json->substance);
        $instance->card = CustomCard::fromJson($json->card);
        return $instance;
    }
}