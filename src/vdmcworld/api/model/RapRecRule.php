<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class RapRecRule implements Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $icdGroupId;

    /**
     * @var array
     */
    private $data;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIcdGroupId()
    {
        return $this->icdGroupId;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $json
     * @return RapRecRule
     */
    public static function fromJson($json)
    {
        $instance = new RapRecRule();
        $instance->id = $json->id;
        $instance->icdGroupId = $json->icdGroupId;
        $instance->data = $json->data;
        return $instance;
    }
}