<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class AllProductCard implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var CustomCard
     */
    protected $card;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @var boolean
     */
    protected $published;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return CustomCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param $json
     * @return AllProductCard
     */
    public static function fromJson($json)
    {
        $instance = new AllProductCard();
        $instance->product = Product::fromJson($json->product);
        $instance->card = CustomCard::fromJson($json->card);
        if (isset($json->published)) {
            $instance->published = $json->published;
        }
        if ($json->updatedAt) {
            $instance->updatedAt = new \DateTime();
            $instance->updatedAt->setTimezone(new \DateTimeZone("UTC"));
            $instance->updatedAt->setTimestamp($json->updatedAt);
        }
        return $instance;
    }
}