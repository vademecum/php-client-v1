<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Specialist implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return Specialist
     */
    public static function fromJson($json)
    {
        $s = new Specialist();
        $s->id = $json->id;
        $s->name = $json->name;
        return $s;
    }
}