<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class MonographyCard implements Model
{
    /**
     * @var string
     */
    protected $caution;

    /**
     * @var string
     */
    protected $dosage;

    /**
     * @var string
     */
    protected $indication;

    /**
     * @var MedicationTimeInfo
     */
    protected $medicationTimeInfo;

    /**
     * @var string
     */
    protected $geriatrics;

    /**
     * @var string
     */
    protected $pediatrics;

    /**
     * @var string
     */
    protected $dailyMaximumDosage;

    /**
     * @var string
     */
    protected $overdose;

    /**
     * @var string
     */
    protected $toxicDosage;

    /**
     * @var string
     */
    protected $dosageCaution;

    /**
     * @var string
     */
    protected $derangement;

    /**
     * @var string
     */
    protected $specialCaution;

    /**
     * @var string
     */
    protected $interactionWithLabTests;

    /**
     * @var string
     */
    protected $overdoseTreatment;

    /**
     * @var string
     */
    protected $effectOnDriver;

    /**
     * @return string
     */
    public function getCaution()
    {
        return $this->caution;
    }

    /**
     * @return string
     */
    public function getDosage()
    {
        return $this->dosage;
    }

    /**
     * @return string
     */
    public function getIndication()
    {
        return $this->indication;
    }

    /**
     * @return MedicationTimeInfo
     */
    public function getMedicationTimeInfo()
    {
        return $this->medicationTimeInfo;
    }

    /**
     * @return string
     */
    public function getGeriatrics()
    {
        return $this->geriatrics;
    }

    /**
     * @return string
     */
    public function getPediatrics()
    {
        return $this->pediatrics;
    }

    /**
     * @return string
     */
    public function getDailyMaximumDosage()
    {
        return $this->dailyMaximumDosage;
    }

    /**
     * @return string
     */
    public function getOverdose()
    {
        return $this->overdose;
    }

    /**
     * @return mixed
     */
    public function getToxicDosage()
    {
        return $this->toxicDosage;
    }

    /**
     * @return string
     */
    public function getDosageCaution()
    {
        return $this->dosageCaution;
    }

    /**
     * @return string
     */
    public function getDerangement()
    {
        return $this->derangement;
    }

    /**
     * @return string
     */
    public function getSpecialCaution()
    {
        return $this->specialCaution;
    }

    /**
     * @return string
     */
    public function getInteractionWithLabTests()
    {
        return $this->interactionWithLabTests;
    }

    /**
     * @return string
     */
    public function getEffectOnDriver()
    {
        return $this->effectOnDriver;
    }

    /**
     * @return string
     */
    public function getOverdoseTreatment()
    {
        return $this->overdoseTreatment;
    }

    /**
     * @param $json
     * @return MonographyCard
     */
    public static function fromJson($json)
    {
        $instance = new MonographyCard();
        if ($json->medicationTimeInfo) {
            $instance->medicationTimeInfo = MedicationTimeInfo::fromJson($json->medicationTimeInfo);
        }
        foreach (["caution", "dosage", "indication", "geriatrics", "pediatrics", "dailyMaximumDosage", "overdose",
                     "toxicDosage", "dosageCaution", "derangement", "specialCaution",
                     "interactionWithLabTests", "effectOnDriver", "overdoseTreatment"] as $key) {
            $instance->{$key} = $json->{$key};
        }
        return $instance;
    }
}