<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class RapicoRapRecRule implements Model
{
    /**
     * @var int
     */
    private $ruleId;

    /**
     * @var string
     */
    private $result;

    /**
     * @return int
     */
    public function getRuleId()
    {
        return $this->ruleId;
    }

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param $json
     * @return RapicoRapRecRule
     */
    public static function fromJson($json)
    {
        $instance = new RapicoRapRecRule();
        $instance->ruleId = $json->ruleId;
        $instance->result = $json->result;
        return $instance;
    }
}