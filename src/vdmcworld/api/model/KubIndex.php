<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class KubIndex implements Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $parentId;

    /**
     * @var bool
     */
    private $hasContent;

    public function hasContent()
    {
        return $this->hasContent;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param $json
     * @return KubIndex
     */
    public static function fromJson($json)
    {
        $instance = new KubIndex();
        $instance->id = $json->id;
        $instance->parentId = $json->parentId;
        $instance->title = $json->title;
        $instance->hasContent = $json->hasContent;
        return $instance;
    }
}