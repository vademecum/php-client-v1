<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class KatarProductPriceHistory implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var TitckPriceInfo[]
     */
    protected $priceHistory = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return TitckPriceInfo[]
     */
    public function getPriceHistory()
    {
        return $this->priceHistory;
    }

    /**
     * @param $json
     * @return KatarProductPriceHistory
     */
    public static function fromJson($json)
    {
        $instance = new KatarProductPriceHistory();
        $instance->product = Product::fromJson($json->product);
        if ($json->priceInfo) {
            foreach ($json->priceInfo as $p) {
                $instance->priceHistory[] = KatarPriceInfo::fromJson($p);
            }
        }
        return $instance;
    }
}