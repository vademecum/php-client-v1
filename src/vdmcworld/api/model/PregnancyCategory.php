<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PregnancyCategory implements Model
{
    /**
     * @var PregnancyCategoryDetail
     */
    protected $categoryDetail;

    /**
     * @var PregnancyPeriod
     */
    protected $period;

    /**
     * @return PregnancyCategoryDetail
     */
    public function getCategoryDetail()
    {
        return $this->categoryDetail;
    }

    /**
     * @return PregnancyPeriod
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param $json
     * @return PregnancyCategory
     */
    public static function fromJson($json)
    {
        $pc = new PregnancyCategory();
        $pc->categoryDetail = PregnancyCategoryDetail::fromJson($json->category);
        $pc->period = PregnancyPeriod::fromJson($json->period);
        return $pc;
    }
}