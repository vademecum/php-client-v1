<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class MaximumDosage implements Model
{
    /**
     * @var int
     */
    private $dosage = null;

    /**
     * @var Unit
     */
    private $unit = null;

    /**
     * @return int
     */
    public function getDosage()
    {
        return $this->dosage;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param $json
     * @return MaximumDosage
     */
    public static function fromJson($json)
    {
        $md = new MaximumDosage();
        $md->dosage = $json->dosage;
        $md->unit = Unit::fromJson($json->unit);
        return $md;
    }
}