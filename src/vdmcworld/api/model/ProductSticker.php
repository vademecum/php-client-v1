<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductSticker implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Sticker
     */
    protected $sticker;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Sticker
     */
    public function getSticker()
    {
        return $this->sticker;
    }

    /**
     * @param $json
     * @return ProductSticker
     */
    public static function fromJson($json)
    {
        $instance = new ProductSticker();

        $instance->product = Product::fromJson($json->product);
        $instance->sticker = Sticker::fromJson($json->sticker);

        return $instance;
    }
}