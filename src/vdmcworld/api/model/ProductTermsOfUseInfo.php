<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductTermsOfUseInfo implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var string
     */
    protected $termsOfUse;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getTermsOfUse()
    {
        return $this->termsOfUse;
    }

    /**
     * @param $json
     * @return ProductTermsOfUseInfo
     */
    public static function fromJson($json)
    {
        $instance = new ProductTermsOfUseInfo();
        $instance->product = Product::fromJson($json->product);
        $instance->termsOfUse = $json->termsOfUse;
        return $instance;
    }
}