<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class RecallEvent implements Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return RecallEvent
     */
    public static function fromJson($json)
    {
        $u = new RecallEvent();
        $u->id = $json->id;
        $u->name = $json->name;
        return $u;
    }
}