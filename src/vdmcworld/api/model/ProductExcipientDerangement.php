<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductExcipientDerangement implements Model
{
    /* @var Product */
    protected $product;

    /* @var ExcipientDerangement[] */
    protected $excipientDerangements = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ExcipientDerangement[]
     */
    public function getExcipientDerangements()
    {
        return $this->excipientDerangements;
    }

    public static function fromJson($json)
    {
        $c = new ProductExcipientDerangement();

        if (isset($json->product)) {
            $c->product = Product::fromJson($json->product);
        }

        if (isset($json->excipientDerangements)) {
            foreach ($json->excipientDerangements as $cd) {
                $c->excipientDerangements [] = ExcipientDerangement::fromJson($cd);
            }
        }

        return $c;
    }
}
