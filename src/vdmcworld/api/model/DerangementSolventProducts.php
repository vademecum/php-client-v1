<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementSolventProducts implements Model
{
    /* @var DerangementSolvent */
    protected $solvent;

    /* @var Product[] */
    protected $products = [];

    /**
     * @return DerangementSolvent
     */
    public function getSolvent()
    {
        return $this->solvent;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->solvent)) {
            $c->solvent = DerangementSolvent::fromJson($json->solvent);
        }

        if (isset($json->products)) {
            foreach ($json->products as $cd) {
                $c->products [] = Product::fromJson($cd);
            }
        }

        return $c;
    }
}
