<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductPriceHistory implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductPriceInfo[]
     */
    protected $priceHistory = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ProductPriceInfo[]
     */
    public function getPriceHistory()
    {
        return $this->priceHistory;
    }

    /**
     * @param $json
     * @return ProductPriceHistory
     */
    public static function fromJson($json)
    {
        $instance = new ProductPriceHistory();
        $instance->product = Product::fromJson($json->product);
        if ($json->priceHistory) {
            foreach ($json->priceHistory as $p) {
                $instance->priceHistory[] = ProductPriceInfo::fromJson($p);
            }
        }
        return $instance;
    }
}