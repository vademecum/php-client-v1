<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PharmaceuticalForm implements Model
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var ApplicationMethod
     */
    protected $applicationMethod;

    /**
     * @var BodyPart
     */
    protected $affectingBodyPart;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return ApplicationMethod
     */
    public function getApplicationMethod()
    {
        return $this->applicationMethod;
    }

    /**
     * @return BodyPart
     */
    public function getAffectingBodyPart()
    {
        return $this->affectingBodyPart;
    }

    /**
     * @param $json
     * @return PharmaceuticalForm
     */
    public static function fromJson($json)
    {
        $p = new PharmaceuticalForm();
        $p->id = $json->id;
        $p->name = $json->name;
        $p->applicationMethod = ApplicationMethod::fromJson($json->applicationMethod);
        $p->affectingBodyPart = BodyPart::fromJson($json->affectingBodyPart);
        return $p;
    }
}