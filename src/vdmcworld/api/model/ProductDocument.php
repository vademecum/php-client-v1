<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductDocument implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Document
     */
    private $document;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param $json
     * @return ProductDocument
     */
    public static function fromJson($json)
    {
        $instance = new ProductDocument();
        $instance->product = Product::fromJson($json->product);
        $instance->document = Document::fromJson($json->documents);
        return $instance;
    }
}