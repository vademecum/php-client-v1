<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DopingNuritionInteraction implements Model
{
    /**
     * @var Substance
     */
    protected $substance;

    /**
     * @var Nutrition
     */
    protected $affectingNutrition;

    /**
     * @var string
     */
    protected $info;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return Substance
     */
    public function getAffectingNutrition()
    {
        return $this->affectingNutrition;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }


    public static function fromJson($json)
    {
        $instance = new DopingNuritionInteraction();
        $instance->substance = Substance::fromJson($json->substance);
        $instance->affectingNutrition = Nutrition::fromJson($json->affectingNutrition);
        $instance->info = $json->info;

        return $instance;
    }
}