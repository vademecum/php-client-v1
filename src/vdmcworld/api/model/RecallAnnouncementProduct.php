<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class RecallAnnouncementProduct implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $barcode;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var RecallProductIngredient[]
     */
    protected $ingredientsInfo = [];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @return RecallProductIngredient[]
     */
    public function getIngredientsInfo()
    {
        return $this->ingredientsInfo;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param $json
     * @return RecallAnnouncementProduct
     */
    public static function fromJson($json)
    {
        $p = new RecallAnnouncementProduct();
        $p->id = $json->id;
        $p->name = $json->name;
        $p->barcode = $json->barcode;

        if (isset($json->ingredientsInfo) && $json->ingredientsInfo) {
            foreach ($json->ingredientsInfo as $i) {
                $p->ingredientsInfo[] = RecallProductIngredient::fromJson($i);
            }
        }
        if (isset($json->company) && $json->company) {
            $p->company = Company::fromJson($json->company);
        }
        return $p;
    }
}