<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class DrugProducts implements Model
{
    /**
     * @var Drug
     */
    protected $drug;

    /**
     * @var Product[]
     */
    protected $products;

    /**
     * @param $json
     * @return DrugProducts
     */
    public static function fromJson($json)
    {
        $instance = new DrugProducts();

        $instance->drug = Drug::fromJson($json->drug);

        $instance->products = [];
        if (isset($json->products) && $json->products) {
            foreach ($json->products as $p) {
                $instance->products[] = Product::fromJson($p);
            }
        }

        return $instance;
    }
}