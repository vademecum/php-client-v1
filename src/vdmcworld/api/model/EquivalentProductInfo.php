<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class EquivalentProductInfo implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductInfo[]
     */
    protected $equivalentProductInfos;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ProductInfo[]
     */
    public function getEquivalentProductInfos()
    {
        return $this->equivalentProductInfos;
    }

    /**
     * @param $json
     * @return EquivalentProductInfo
     */
    public static function fromJson($json)
    {
        $instance = new EquivalentProductInfo();
        $instance->product = Product::fromJson($json->product);
        $instance->equivalentProductInfos = [];
        if ($json->equivalentProductsInfo) {
            foreach ($json->equivalentProductsInfo as $eqp) {
                $instance->equivalentProductInfos[] = ProductInfo::fromJson($eqp);
            }
        }
        return $instance;
    }
}