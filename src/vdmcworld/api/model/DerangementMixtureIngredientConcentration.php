<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementMixtureIngredientConcentration implements Model
{
    /**
     * @var string
     */
    protected $amount = null;

    /**
     * @var Unit
     */
    protected $unit = null;

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param $json
     * @return DerangementMixtureIngredientConcentration
     */
    public static function fromJson($json)
    {
        $c = new self();
        if (isset($json->amount)) {
            $c->amount = $json->amount;
        }
        if (isset($json->unit)) {
            $c->unit = Unit::fromJson($json->unit);
        }
        return $c;
    }
}