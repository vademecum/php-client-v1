<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class AllergicReaction implements Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return AllergicReaction
     */
    public static function fromJson($json)
    {
        $allergic = new AllergicReaction();
        $allergic->id = $json->id;
        $allergic->name = $json->name;
        return $allergic;
    }
}