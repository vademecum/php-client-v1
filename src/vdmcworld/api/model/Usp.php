<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Usp implements Model
{

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var integer
     */
    protected $parent_id;


    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return integer
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param $json
     * @return Usp
     */
    public static function fromJson($json)
    {
        $a = new Usp();
        $a->id = $json->id;
        $a->name = $json->name;
        $a->parent_id = $json->parent_id;
        return $a;
    }
}