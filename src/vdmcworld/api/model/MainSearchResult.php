<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class MainSearchResult implements Model
{
    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @var Substance[]
     */
    protected $substances = [];

    /**
     * @var Company[]
     */
    protected $companies = [];


    /**
     * @var Product[]
     */
    protected $suggestedProducts = [];

    /**
     * @var Substance[]
     */
    protected $suggestedSubstances = [];

    /**
     * @var Company[]
     */
    protected $suggestedCompanies = [];


    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return Substance[]
     */
    public function getSubstances()
    {
        return $this->substances;
    }

    /**
     * @return Company[]
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * @return Product[]
     */
    public function getSuggestedProducts()
    {
        return $this->suggestedProducts;
    }

    /**
     * @return Substance[]
     */
    public function getSuggestedSubstances()
    {
        return $this->suggestedSubstances;
    }

    /**
     * @return Company[]
     */
    public function getSuggestedCompanies()
    {
        return $this->suggestedCompanies;
    }
    /**
     * @param $json
     * @return MainSearchResult
     */
    public static function fromJson($json)
    {
        $s = new MainSearchResult();
        foreach ($json->products as $p) {
            $s->products[] = Product::fromJson($p);
        }
        foreach ($json->substances as $sub) {
            $s->substances[] = Substance::fromJson($sub);
        }
        foreach ($json->companies as $c) {
            $s->companies[] = Company::fromJson($c);
        }
        foreach ($json->suggestedProducts as $sp) {
            $s->suggestedProducts[] = Product::fromJson($sp);
        }
        foreach ($json->suggestedSubstances as $ss) {
            $s->suggestedSubstances[] = Substance::fromJson($ss);
        }
        foreach ($json->suggestedCompanies as $sc) {
            $s->suggestedCompanies[] = Company::fromJson($sc);
        }
        return $s;
    }
}