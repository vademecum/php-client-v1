<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductPrescriptionTypeMaxDosage implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var PrescriptionTypeMaxDosage[]
     */
    protected $prescriptionTypeMaxDosages = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return PrescriptionTypeMaxDosage[]
     */
    public function getPrescriptionTypeMaxDosages()
    {
        return $this->prescriptionTypeMaxDosages;
    }

    /**
     * @param $json
     * @return ProductPrescriptionTypeMaxDosage
     */
    public static function fromJson($json)
    {
        $instance = new ProductPrescriptionTypeMaxDosage();
        $instance->product = Product::fromJson($json->product);
        if ($json->prescriptionTypeMaxDosage) {
            foreach ($json->prescriptionTypeMaxDosage as $d)
                $instance->prescriptionTypeMaxDosages[] = PrescriptionTypeMaxDosage::fromJson($d);
        }

        return $instance;
    }
}