<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PublicPaidPriceInfo implements Model
{

    /**
     * @var float
     */
    protected $price;

    /**
     * @var float
     */
    protected $priceVatInc;

    /**
     * @var string
     */
    protected $groupCode;

    /**
     * @var int
     */
    protected $groupType;

    /**
     * @var boolean
     */
    protected $groupLowestUnitPrice;

    /**
     * @var string
     */
    protected $equivalentGroupCode;

    /**
     * @return string
     */
    public function getGroupCode()
    {
        return $this->groupCode;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getGroupType()
    {
        return $this->groupType;
    }

    /**
     * @return string
     */
    public function getEquivalentGroupCode()
    {
        return $this->equivalentGroupCode;
    }

    /**
     * @return float
     */
    public function getPriceVatInc()
    {
        return $this->priceVatInc;
    }

    /**
     * @return boolean
     */
    public function isGroupLowestUnitPrice()
    {
        return $this->groupLowestUnitPrice;
    }

    /**
     * @param $json
     * @return PublicPaidPriceInfo
     */
    public static function fromJson($json)
    {
        $instance = new PublicPaidPriceInfo();
        foreach (["groupCode", "price", "groupType", "equivalentGroupCode", "groupLowestUnitPrice"] as $key) {
            $instance->$key = $json->$key;
        }
        if (isset($json->priceVatInc)) {
            $instance->priceVatInc = $json->priceVatInc;
        }
        return $instance;
    }
}