<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Education implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return Education
     */
    public static function fromJson($json)
    {
        $instance = new Education();
        $instance->name = $json->name;
        $instance->id = $json->id;
        return $instance;
    }
}