<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceSpecialInformation implements Model
{

    /**
     * @var Substance
     */
    protected $substance;

    /**
     * @var SpecialInformation
     */
    protected $specialInformation;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return SpecialInformation
     */
    public function getSpecialInformation()
    {
        return $this->specialInformation;
    }

    /**
     * @param $json
     * @return SubstanceSpecialInformation
     */
    public static function fromJson($json)
    {
        $instance = new SubstanceSpecialInformation();
        $instance->substance = Substance::fromJson($json->substance);
        $instance->specialInformation = SpecialInformation::fromJson($json->specialInformation);
        return $instance;
    }
}