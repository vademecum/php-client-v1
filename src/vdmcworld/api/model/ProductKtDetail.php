<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductKtDetail implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var KtDetail
     */
    private $ktDetail;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return KtDetail
     */
    public function getKtDetail()
    {
        return $this->ktDetail;
    }

    /**
     * @param $json
     * @return ProductKtDetail
     */
    public static function fromJson($json)
    {
        $instance = new ProductKtDetail();
        $instance->product = Product::fromJson($json->product);
        if ($json->ktDetails) {
            $instance->ktDetail = KtDetail::fromJson($json->ktDetails);
        }
        return $instance;
    }
}