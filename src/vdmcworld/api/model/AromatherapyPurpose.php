<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class AromatherapyPurpose implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return AromatherapyPurpose
     */
    public static function fromJson($json)
    {
        $aromatherapyPurpose = new AromatherapyPurpose();
        $aromatherapyPurpose->id = $json->id;
        $aromatherapyPurpose->name = $json->name;

        return $aromatherapyPurpose;
    }
}