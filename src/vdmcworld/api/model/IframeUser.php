<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class IframeUser implements Model
{
    /**
     * @var int
     */
    protected $userId;

    /**
     * @var int
     */
    protected $userType;

    /**
     * @var int
     */
    protected $userStatus;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var \DateTime
     */
    protected $expiresAt;

    /**
     * @var string
     */
    protected $clientName;

    /**
     * @var string
     */
    protected $clientPackageId;

    /**
     * @var string
     */
    protected $clientPackageKeyAmount;

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @return string
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @return string
     */
    public function getClientPackageId()
    {
        return $this->clientPackageId;
    }

    /**
     * @return string
     */
    public function getClientPackageKeyAmount()
    {
        return $this->clientPackageKeyAmount;
    }

    /**
     * @param $json
     * @return Model|IframeUser
     * @throws \Exception
     */
    public static function fromJson($json)
    {
        $instance = new IframeUser();
        $instance->userId = $json->user_id;
        $instance->userType = $json->user_type;
        $instance->clientName = isset($json->client_name) ? $json->client_name : null;
        $instance->clientPackageId = isset($json->client_package_id) ? $json->client_package_id : null;
        $instance->clientPackageKeyAmount = isset($json->client_package_key_amount) ? $json->client_package_key_amount : 0;
        $instance->expiresAt = new \DateTime();
        $instance->expiresAt->setTimestamp($json->expires_at);
        $instance->userStatus = $json->status;
        return $instance;
    }
}