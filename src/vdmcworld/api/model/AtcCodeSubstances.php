<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class AtcCodeSubstances implements Model
{
    /**
     * @var string
     */
    protected $atc;

    /**
     * @var Substance[]
     */
    protected $substances;

    /**
     * @return string
     */
    public function getAtc()
    {
        return $this->atc;
    }

    /**
     * @return Substance[]
     */
    public function getSubstances()
    {
        return $this->substances;
    }

    /**
     * @param $json
     * @return AtcCodeSubstances
     */
    public static function fromJson($json)
    {
        $instance = new AtcCodeSubstances();
        $instance->atc = $json->atc;
        $instance->substances = [];
        foreach ($json->substances as $s) {
            $instance->substances[] = Substance::fromJson($s);
        }
        return $instance;
    }
}
