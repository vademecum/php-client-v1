<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class NutritionInfo implements Model
{

    /**
     * @var Amount
     */
    protected $energyAmount;

    /**
     * @var Amount
     */
    protected $totalEnergyAmount;

    /**
     * @var Amount
     */
    protected $perUnitAmount;

    /**
     * @return Amount
     */
    public function getEnergyAmount()
    {
        return $this->energyAmount;
    }

    /**
     * @return Amount
     */
    public function getPerUnitAmount()
    {
        return $this->perUnitAmount;
    }

    /**
     * @return Amount
     */
    public function getTotalEnergyAmount()
    {
        return $this->totalEnergyAmount;
    }

    /**
     * @param $json
     * @return NutritionInfo
     */
    public static function fromJson($json)
    {
        $instance = new NutritionInfo();
        foreach (['energyAmount', 'perUnitAmount', 'totalEnergyAmount'] as $k) {
            if ($json->$k) {
                $instance->$k = Amount::fromJson($json->$k);
            }
        }
        return $instance;
    }
}
