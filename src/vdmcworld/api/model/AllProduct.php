<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class AllProduct implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $barcode;

    /**
     * @var string[]
     */
    protected $barcodes;

    /**
     * @var int
     */
    protected $drugId;

    /**
     * @var int
     */
    protected $drugFormId;

    /**
     * @var int
     */
    protected $publicNumber;

    /**
     * @var int
     */
    protected $pharmaceuticalFormId;

    /**
     * @var int
     */
    protected $nfcCodeId;

    /**
     * @var IngredientInfo[]
     */
    protected $ingredients = [];

    /**
     * @var int
     */
    protected $drugTypeId;

    /**
     * @var int
     */
    protected $genericStatusId;

    /**
     * @var int
     */
    protected $companyId;

    /**
     * @var int
     */
    protected $prescriptionTypeId;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @var boolean
     */
    protected $published;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @return string[]
     */
    public function getBarcodes()
    {
        return $this->barcodes;
    }

    /**
     * @return int
     */
    public function getDrugId()
    {
        return $this->drugId;
    }

    /**
     * @return int
     */
    public function getDrugFormId()
    {
        return $this->drugFormId;
    }

    /**
     * @return int
     */
    public function getPublicNumber()
    {
        return $this->publicNumber;
    }

    /**
     * @return int
     */
    public function getPharmaceuticalFormId()
    {
        return $this->pharmaceuticalFormId;
    }

    /**
     * @return int
     */
    public function getNfcCodeId()
    {
        return $this->nfcCodeId;
    }

    /**
     * @return IngredientInfo[]
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @return int
     */
    public function getDrugTypeId()
    {
        return $this->drugTypeId;
    }

    /**
     * @return int
     */
    public function getGenericStatusId()
    {
        return $this->genericStatusId;
    }

    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @return int
     */
    public function getPrescriptionTypeId()
    {
        return $this->prescriptionTypeId;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param $json
     * @return AllProduct
     */
    public static function fromJson($json)
    {
        $p = new AllProduct();
        $p->id = $json->productId;
        $p->name = $json->productName;
        $p->barcode = $json->barcode;
        $p->barcodes = $json->barcodes;
        $p->drugId = $json->drugId;
        $p->drugFormId = $json->drugFormId;
        $p->publicNumber = $json->publicNumber;
        $p->pharmaceuticalFormId = $json->pharmaceuticalFormId;
        $p->nfcCodeId = $json->nfcCodeId;
        $p->companyId = $json->companyId;
        $p->prescriptionTypeId = $json->prescriptionTypeId;
        $p->slug = $json->slug;
        if ($json->ingredients) {
            foreach ($json->ingredients as $i) {
                $p->ingredients[] = IngredientInfo::fromJson($i);
            }
        }
        $p->drugTypeId = $json->drugTypeId;
        $p->genericStatusId = $json->genericStatusId;
        if (isset($json->published)) {
            $p->published = $json->published;
        }
        if ($json->updatedAt) {
            $p->updatedAt = new \DateTime();
            $p->updatedAt->setTimezone(new \DateTimeZone("UTC"));
            $p->updatedAt->setTimestamp($json->updatedAt);
        }
        return $p;
    }
}