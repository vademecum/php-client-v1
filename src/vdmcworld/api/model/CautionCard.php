<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class CautionCard implements Model
{
    /**
     * @var string
     */
    protected $caution;

    /**
     * @var string
     */
    protected $geriatrics;

    /**
     * @var string
     */
    protected $pediatrics;

    /**
     * @var string
     */
    protected $derangement;

    /**
     * @var string
     */
    protected $specialCaution;

    /**
     * @var string
     */
    protected $interactionWithLabTests;

    /**
     * @var string
     */
    protected $effectOnDriver;

    /**
     * @var PregnancyCategory[]
     */
    protected $pregnancyCategories;

    /**
     * @var MedicationTimeInfo
     */
    protected $medicationTimeInfo;

    /**
     * @var string
     */
    protected $storageCondition;

    /**
     * @return string
     */
    public function getCaution()
    {
        return $this->caution;
    }

    /**
     * @return string
     */
    public function getGeriatrics()
    {
        return $this->geriatrics;
    }

    /**
     * @return string
     */
    public function getPediatrics()
    {
        return $this->pediatrics;
    }

    /**
     * @return string
     */
    public function getDerangement()
    {
        return $this->derangement;
    }

    /**
     * @return string
     */
    public function getSpecialCaution()
    {
        return $this->specialCaution;
    }

    /**
     * @return string
     */
    public function getInteractionWithLabTests()
    {
        return $this->interactionWithLabTests;
    }

    /**
     * @return string
     */
    public function getEffectOnDriver()
    {
        return $this->effectOnDriver;
    }

    /**
     * @return PregnancyCategory[]
     */
    public function getPregnancyCategories()
    {
        return $this->pregnancyCategories;
    }

    /**
     * @return MedicationTimeInfo
     */
    public function getMedicationTimeInfo()
    {
        return $this->medicationTimeInfo;
    }

    /**
     * @return string
     */
    public function getStorageCondition()
    {
        return $this->storageCondition;
    }

    /**
     * @param $json
     * @return CautionCard
     */
    public static function fromJson($json)
    {
        $instance = new CautionCard();

        if ($json->pregnancyCategories) {
            foreach ($json->pregnancyCategories as $pc) {
                $instance->pregnancyCategories[] = PregnancyCategory::fromJson($pc);
            }
        }

        if ($json->medicationTimeInfo) {
            $instance->medicationTimeInfo = MedicationTimeInfo::fromJson($json->medicationTimeInfo);
        }

        foreach ([
                     "caution", "geriatrics", "pediatrics", "derangement", "specialCaution", "interactionWithLabTests",
                     "effectOnDriver", "storageCondition",
                 ] as $item) {
            $instance->$item = $json->$item;
        }
        return $instance;
    }
}