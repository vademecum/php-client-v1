<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class IngredientInfo implements Model
{
    /**
     * @var int
     */
    protected $substanceId;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var int
     */
    protected $unitId;

    /**
     * @var int
     */
    protected $chemicalformId;

    /**
     * @return int
     */
    public function getSubstanceId()
    {
        return $this->substanceId;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getUnitId()
    {
        return $this->unitId;
    }

    /**
     * @return int
     */
    public function getChemicalformId()
    {
        return $this->chemicalformId;
    }

    /**
     * @param $json
     * @return IngredientInfo
     */
    public static function fromJson($json)
    {
        $i = new IngredientInfo();
        $i->substanceId = $json->substanceId;
        $i->amount = (float)$json->amount;
        $i->unitId = $json->unitId;
        $i->chemicalformId = $json->chemicalformId;
        return $i;
    }
}