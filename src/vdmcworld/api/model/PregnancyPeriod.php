<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PregnancyPeriod implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return PregnancyPeriod
     */
    public static function fromJson($json)
    {
        $pp = new PregnancyPeriod();
        $pp->id = $json->id;
        $pp->name = $json->name;
        return $pp;
    }
}