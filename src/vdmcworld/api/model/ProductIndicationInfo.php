<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductIndicationInfo implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var string
     */
    protected $indication;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getIndication()
    {
        return $this->indication;
    }

    /**
     * @param $json
     * @return ProductIndicationInfo
     */
    public static function fromJson($json)
    {
        $instance = new ProductIndicationInfo();
        $instance->product = Product::fromJson($json->product);
        $instance->indication = $json->indication;
        return $instance;
    }
}