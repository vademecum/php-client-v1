<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ReimbursementReportType implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */

    protected $reportType;

    /**
     * @var string
     */
    protected $note;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @return string
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * @param $json
     * @return ReimbursementReportType
     */
    public static function fromJson($json)
    {
        $rt = new ReimbursementReportType();
        $rt->id = $json->id;
        $rt->note = $json->note;
        $rt->reportType = $json->reportType;
        return $rt;
    }
}