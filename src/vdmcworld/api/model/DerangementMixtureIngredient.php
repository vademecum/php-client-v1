<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementMixtureIngredient implements Model
{
    /**
     * @var DerangementMixtureIngredientSubstance[]
     */
    public $substances = [];

    /**
     * @var DerangementMixtureIngredientSolvent[]
     */
    public $solvents = [];

    public static function fromJson($json)
    {
        $c = new self();
        if (!empty($json->substances)) {
            foreach ($json->substances as $s) {
                $c->substances [] = DerangementMixtureIngredientSubstance::fromJson($s);
            }
        }
        if (!empty($json->solvents)) {
            foreach ($json->solvents as $s) {
                $c->solvents [] = DerangementMixtureIngredientSolvent::fromJson($s);
            }
        }
        return $c;
    }
}