<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceProductInfo implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var boolean
     */
    protected $combined;

    /**
     * @var GenericStatusType
     */
    protected $genericStatusType;

    /**
     * @var Status
     */
    protected $reimbursementStatus;

    /**
     * @var Status
     */
    protected $onMarket;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return boolean
     */
    public function isCombined()
    {
        return $this->combined;
    }

    /**
     * @return GenericStatusType
     */
    public function getGenericStatusType()
    {
        return $this->genericStatusType;
    }

    /**
     * @return Status
     */
    public function getReimbursementStatus()
    {
        return $this->reimbursementStatus;
    }

    /**
     * @return Status
     */
    public function getOnMarket()
    {
        return $this->onMarket;
    }

    /**
     * @param $json
     * @return SubstanceProductInfo
     */
    public static function fromJson($json)
    {
        $instance = new SubstanceProductInfo();
        $instance->product = Product::fromJson($json->product);
        if (isset($json->company) && !empty($json->company)) {
            $instance->company = Company::fromJson($json->company);
        }

        if (isset($json->genericStatusType) && !empty($json->genericStatusType)) {
            $instance->genericStatusType = GenericStatusType::fromJson($json->genericStatusType);
        }

        if (isset($json->reimbursementStatus)) {
            $instance->reimbursementStatus = Status::fromJson($json->reimbursementStatus);

        }

        if (isset($json->onMarket)) {
            $instance->onMarket = Status::fromJson($json->onMarket);

        }

        $instance->combined = $json->combined;
        return $instance;
    }
}