<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class EssentialDrugs implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var DrugType
     */
    protected $drugType;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return DrugType
     */
    public function getDrugType()
    {
        return $this->drugType;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param $json
     * @return EssentialDrugs
     */
    public static function fromJson($json)
    {
        $instance = new EssentialDrugs();

        $instance->product = Product::fromJson($json->product);
        $instance->drugType = DrugType::fromJson($json->drugType);
        if ($json->company) {
            $instance->company = Company::fromJson($json->company);
        }
        return $instance;
    }
}