<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Color implements Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $priority;
    /**
     * @var string
     */
    private $htmlCode;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return string
     */
    public function getHtmlCode()
    {
        return $this->htmlCode;
    }

    /**
     * @param $json
     * @return Model
     */
    public static function fromJson($json)
    {
        $c = new Color();
        $c->id = $json->id;
        $c->name = $json->name;
        $c->priority = $json->priority;
        $c->htmlCode = $json->htmlCode;
        return $c;
    }
}