<?php

namespace vdmcworld\api\model;

class DerangementMixture extends DerangementElement
{
    /**
     * @var DerangementMixtureIngredientSubstance[]
     */
    public $substances = [];

    /**
     * @var DerangementMixtureIngredientSolvent[]
     */
    public $solvents = [];

    public static function fromJson($json)
    {
        $c = parent::fromJson($json);
        $ing = DerangementMixtureIngredient::fromJson($json);
        $c->substances = $ing->substances;
        $c->solvents = $ing->solvents;
        return $c;
    }
}