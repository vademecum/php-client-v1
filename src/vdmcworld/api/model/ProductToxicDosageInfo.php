<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductToxicDosageInfo implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var string
     */
    protected $toxicDosageInfo;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getToxicDosageInfo()
    {
        return $this->toxicDosageInfo;
    }

    /**
     * @param $json
     * @return ProductToxicDosageInfo
     */
    public static function fromJson($json)
    {
        $instance = new ProductToxicDosageInfo();
        $instance->product = Product::fromJson($json->product);
        $instance->toxicDosageInfo = $json->toxicDosage;
        return $instance;
    }
}