<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class SubstanceMixtures implements Model
{
    /* @var Substance */
    protected $substance;

    /* @var DerangementMixture[] */
    protected $mixtures = [];

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return DerangementMixture[]
     */
    public function getMixtures()
    {
        return $this->mixtures;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->substance)) {
            $c->substance = Substance::fromJson($json->substance);
        }

        if (!empty($json->mixtures)) {
            foreach ($json->mixtures as $mx) {
                $c->mixtures [] = DerangementMixture::fromJson($mx);
            }
        }

        return $c;
    }
}
