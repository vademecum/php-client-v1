<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;


class Reimbursement implements Model
{
    /**
     * @var ReimbursementSummary[]
     */
    protected $summaries = [];

    /**
     * @var SubstancePrescriptionRule[]
     */
    protected $substancePrescriptionRules = [];

    /**
     * @var string
     */
    protected $generalInfo;

    /**
     * @var string
     */
    protected $prescriptionRules;

    /**
     * @var object
     */
    protected $sutStatuses = [];

    /**
     * @return ReimbursementSummary[]
     */
    public function getSummaries()
    {
        return $this->summaries;
    }

    /**
     * @return SubstancePrescriptionRule[]
     */
    public function getSubstancePrescriptionRules()
    {
        return $this->substancePrescriptionRules;
    }

    /**
     * @return string
     */
    public function getGeneralInfo()
    {
        return $this->generalInfo;
    }

    /**
     * @return string
     */
    public function getPrescriptionRules()
    {
        return $this->prescriptionRules;
    }


    /**
     * @return object
     */
    public function getSutStatuses()
    {
        return $this->sutStatuses;
    }

    /**
     * @param $json
     * @return Reimbursement
     */
    public static function fromJson($json)
    {
        $r = new Reimbursement();
        if ($json->reimbursementSummaries) {
            foreach ($json->reimbursementSummaries as $rs) {
                $r->summaries[] = ReimbursementSummary::fromJson($rs);
            }
        }

        if ($json->substancePrescriptionRules) {
            foreach ($json->substancePrescriptionRules as $sr) {
                $r->substancePrescriptionRules[] = SubstancePrescriptionRule::fromJson($sr);
            }
        }

        $r->generalInfo = $json->generalInfo;
        $r->prescriptionRules = $json->prescriptionRules;
        if (!empty($json->sutStatuses)) {
            $r->sutStatuses = $json->sutStatuses;
        }
        return $r;
    }
}