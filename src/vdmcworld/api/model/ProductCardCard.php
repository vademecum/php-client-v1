<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductCardCard extends BaseCard
{
    /**
     * @var NfcCode
     */
    protected $nfcCode;

    /**
     * @var PrescriptionType
     */
    protected $prescriptionType;

    /**
     * @var string[]
     */
    protected $equivalentGroups = [];

    /**
     * @var Status
     */
    protected $reimbursementStatus;

    /**
     * @var ProductCardSpecialInformation
     */
    protected $specialInformation;

    /**
     * @var string[]
     */
    protected $sgkEquivalentCodes = [];

    /**
     * @var string[]
     */
    protected $sgkPriceReferenceCodes = [];

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var string
     */
    protected $publicNumber;

    /**
     * @var AtcIndex[]
     */
    protected $atcIndices = [];

    /**
     * @var File[]
     */
    protected $images = [];

    /**
     * @var string
     */
    protected $shelfLife;

    /**
     * @var string
     */
    protected $notice = null;

    /**
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }

    /**
     * @return NfcCode
     */
    public function getNfcCode()
    {
        return $this->nfcCode;
    }

    /**
     * @return PrescriptionType
     */
    public function getPrescriptionType()
    {
        return $this->prescriptionType;
    }

    /**
     * @return \string[]
     */
    public function getEquivalentGroups()
    {
        return $this->equivalentGroups;
    }

    /**
     * @return Status
     */
    public function getReimbursementStatus()
    {
        return $this->reimbursementStatus;
    }

    /**
     * @return ProductCardSpecialInformation
     */
    public function getSpecialInformation()
    {
        return $this->specialInformation;
    }

    /**
     * @return string[]
     */
    public function getSgkEquivalentCodes()
    {
        return $this->sgkEquivalentCodes;
    }

    /**
     * @return string[]
     */
    public function getSgkPriceReferenceCodes()
    {
        return $this->sgkPriceReferenceCodes;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return string
     */
    public function getPublicNumber()
    {
        return $this->publicNumber;
    }

    /**
     * @return AtcIndex[]
     */
    public function getAtcIndices()
    {
        return $this->atcIndices;
    }

    /**
     * @return File[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return string
     */
    public function getShelfLife()
    {
        return $this->shelfLife;
    }

    /**
     * @param $json
     * @return ProductCardCard
     */
    public static function fromJson($json)
    {
        $instance = new ProductCardCard();
        self::parseJson($json, $instance);

        foreach ($json->equivalentGroups as $e) {
            $instance->equivalentGroups[] = $e;
        }

        $instance->reimbursementStatus = Status::fromJson($json->reimbursementStatus);

        $instance->specialInformation = ProductCardSpecialInformation::fromJson($json->specialInformation);

        if ($json->company) {
            $instance->company = Company::fromJson($json->company);
        }

        if ($json->nfcCode) {
            $instance->nfcCode = NfcCode::fromJson($json->nfcCode);
        }

        if ($json->prescriptionType) {
            $instance->prescriptionType = PrescriptionType::fromJson($json->prescriptionType);
        }

        foreach ($json->atcIndex as $atcIndex) {
            $instance->atcIndices[] = AtcIndex::fromJson($atcIndex);
        }

        if ($json->images) {
            foreach ($json->images as $image) {
                $instance->images[] = File::fromJson($image);
            }
        }

        foreach (["sgkEquivalentCodes", "sgkPriceReferenceCodes", "publicNumber", "shelfLife", "notice"] as $k) {
            $instance->{$k} = $json->{$k};
        }

        return $instance;
    }
}