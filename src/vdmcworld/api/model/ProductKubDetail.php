<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductKubDetail implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var KubDetail
     */
    private $kubDetail;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return KubDetail
     */
    public function getKubDetail()
    {
        return $this->kubDetail;
    }

    /**
     * @param $json
     * @return ProductKubDetail
     */
    public static function fromJson($json)
    {
        $instance = new ProductKubDetail();
        $instance->product = Product::fromJson($json->product);
        if ($json->kubDetails) {
            $instance->kubDetail = KubDetail::fromJson($json->kubDetails);
        }
        return $instance;
    }
}