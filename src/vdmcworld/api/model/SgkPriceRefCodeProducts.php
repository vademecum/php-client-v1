<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SgkPriceRefCodeProducts implements Model
{
    /**
     * @var string
     */
    protected $priceRefCode;

    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @return string
     */
    public function getPriceRefCode()
    {
        return $this->priceRefCode;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param $json
     * @return SgkPriceRefCodeProducts
     */
    public static function fromJson($json)
    {
        $instance = new SgkPriceRefCodeProducts();
        $instance->priceRefCode = $json->sgkPriceRefCode;
        if ($json->products) {
            foreach ($json->products as $p) {
                $instance->products[] = CodeProduct::fromJson($p);
            }
        }
        return $instance;
    }
}