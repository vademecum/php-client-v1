<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductInfo implements Model
{

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var ReimbursementPriceInfo
     */
    protected $priceInfo;

    /**
     * @var ProductStatus[]
     */
    protected $productStatuses = [];

    /**
     * @var Status
     */
    protected $reimbursementStatus;

    /**
     * @var Status
     */
    protected $onMarketStatus;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return ReimbursementPriceInfo
     */
    public function getPriceInfo()
    {
        return $this->priceInfo;
    }

    /**
     * @return ProductStatus[]
     */
    public function getProductStatuses()
    {
        return $this->productStatuses;
    }

    /**
     * @return Status
     */
    public function getReimbursementStatus()
    {
        return $this->reimbursementStatus;
    }

    /**
     * @return Status
     */
    public function getOnMarketStatus()
    {
        return $this->onMarketStatus;
    }

    /**
     * @param $json
     * @return ProductInfo
     */
    public static function fromJson($json)
    {
        $instance = new ProductInfo();
        $instance->product = Product::fromJson($json->product);
        if (isset($json->company) && $json->company) {
            $instance->company = Company::fromJson($json->company);
        }
        if ($json->priceInfo) {
            $instance->priceInfo = ReimbursementPriceInfo::fromJson($json->priceInfo);
        }
        if (isset($json->productStatus)) {
            foreach ($json->productStatus as $ps) {
                $instance->productStatuses[] = ProductStatus::fromJson($ps);
            }
        }
        if (isset($json->reimbursementStatus)) {
            $instance->reimbursementStatus = Status::fromJson($json->reimbursementStatus);
        }
        if (isset($json->onMarketStatus)) {
            $instance->onMarketStatus = Status::fromJson($json->onMarketStatus);
        }
        return $instance;
    }
}