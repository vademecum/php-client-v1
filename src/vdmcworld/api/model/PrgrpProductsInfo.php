<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PrgrpProductsInfo implements Model
{
    /**
     * @var PriceReferenceGroup
     */
    protected $priceReferenceGroup;

    /**
     * @var ProductInfo[]
     */
    protected $productsInfo = [];

    /**
     * @return PriceReferenceGroup
     */
    public function getPriceReferenceGroup()
    {
        return $this->priceReferenceGroup;
    }

    /**
     * @return ProductInfo[]
     */
    public function getProductsInfo()
    {
        return $this->productsInfo;
    }

    /**
     * @param $json
     * @return PrgrpProductsInfo
     */
    public static function fromJson($json)
    {
        $instance = new PrgrpProductsInfo();
        $instance->priceReferenceGroup = PriceReferenceGroup::fromJson($json->priceReferenceGroup);
        foreach ($json->productsInfo as $p) {
            $instance->productsInfo[] = ProductInfo::fromJson($p);
        }
        return $instance;
    }
}