<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductSideEffects implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var string
     */
    protected $sideEffects;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getSideEffects()
    {
        return $this->sideEffects;
    }

    public static function fromJson($json)
    {
        $instance = new ProductSideEffects();
        $instance->product = Product::fromJson($json->product);
        $instance->sideEffects = $json->sideEffects;
        return $instance;
    }
}