<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductDopingData implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var DopingData
     */
    protected $dopingData;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return DopingData
     */
    public function getDopingData()
    {
        return $this->dopingData;
    }

    /**
     * @param $json
     * @return Model|ProductDopingData
     */
    public static function fromJson($json)
    {
        $instance = new ProductDopingData();
        $instance->product = Product::fromJson($json->product);
        $instance->dopingData = DopingData::fromJson($json->dopingData);
        return $instance;
    }
}