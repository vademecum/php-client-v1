<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class StatusHistory implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var Status
     */
    protected $reimbursementStatus;

    /**
     * @var Status
     */
    protected $onMarketStatus;

    /**
     * @var Substance[]
     */
    protected $substances;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return Status
     */
    public function getReimbursementStatus()
    {
        return $this->reimbursementStatus;
    }

    /**
     * @return Status
     */
    public function getMarketStatus()
    {
        return $this->onMarketStatus;
    }

    public function getSubstances()
    {
        return $this->substances;
    }

    /**
     * @param $json
     * @return StatusHistory
     */
    public static function fromJson($json)
    {
        $instance = new StatusHistory();
        $instance->date = \DateTime::createFromFormat(
            "Y-m-d H:i:s",
            $json->date . " 00:00:00",
            new \DateTimeZone("UTC")
        );
        $instance->product = Product::fromJson($json->product);

        if (isset($json->company) && !empty($json->company)) {
            $instance->company = Company::fromJson($json->company);
        }
        if (isset($json->reimbursementStatus)) {
            $instance->reimbursementStatus = Status::fromJson($json->reimbursementStatus);
        }
        if (isset($json->onMarketStatus)) {
            $instance->onMarketStatus = Status::fromJson($json->onMarketStatus);
        }

        if (isset($json->substances)) {
            $instance->substances = $json->substances;
        }
        return $instance;
    }
}