<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class AllProductPrice extends ReimbursementPriceInfo
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $barcode;
    
    /**
     * @var PackageAmount
     */
    protected $packageAmount;

    /**
     * @var FuturePrice
     */
    protected $futurePrice;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }


    /**
     * @return PackageAmount
     */
    public function getPackageAmount()
    {
        return $this->packageAmount;
    }

    /**
     * @return FuturePrice
     */
    public function getFuturePrice()
    {
        return $this->futurePrice;
    }

    /**
     * @param AllProductPrice $instance
     * @param $json
     * @return AllProductPrice
     */
    protected static function _parseJson($instance, $json)
    {
        parent::_parseJson($instance, $json);
        $instance->id = $json->id;
        $instance->barcode = $json->barcode;
        if ($json->packageAmount) {
            $instance->packageAmount = PackageAmount::fromJson($json->packageAmount);
        }

        if ($json->futurePrice) {
            $instance->futurePrice = ReimbursementFuturePrice::fromJson($json->futurePrice);
        }

        return $instance;
    }

    /**
     * @param $json
     * @return AllProductPrice
     */
    public static function fromJson($json)
    {
        return AllProductPrice::_parseJson(new AllProductPrice(), $json);
    }
}