<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceSgkCodesSubstance implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $substance_name;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var Unit
     */
    protected $unit;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSubstanceName()
    {
        return $this->substance_name;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }


    protected static function parseJson($json, $instance)
    {
        $instance->id = $json->id;
        $instance->substance_name = $json->substance_name;
        $instance->description = $json->description;
        if (isset($json->unit) && !empty($json->unit)) {
            $instance->unit = $json->unit;
        }
        $instance->amount = $json->amount;

        return $instance;
    }

    /**
     * @param $json
     * @return SubstanceSgkCodesSubstance
     */
    public static function fromJson($json)
    {
        return self::parseJson($json, new SubstanceSgkCodesSubstance());
    }
}