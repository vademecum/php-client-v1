<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceProductsInfo implements Model
{
    /**
     * @var Substance
     */
    protected $substance;

    /**
     * @var SubstanceProductInfo[]
     */
    protected $productsInfo = [];

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return SubstanceProductInfo[]
     */
    public function getProductsInfo()
    {
        return $this->productsInfo;
    }

    /**
     * @param $json
     * @return SubstanceProductsInfo
     */
    public static function fromJson($json)
    {
        $instance = new SubstanceProductsInfo();
        $instance->substance = Substance::fromJson($json->substance);
        if ($json->productsInfo) {
            foreach ($json->productsInfo as $pi) {
                $instance->productsInfo[] = SubstanceProductInfo::fromJson($pi);
            }
        }
        return $instance;
    }
}