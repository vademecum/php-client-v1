<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductKtUsageDetail implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var KtUsageDetail
     */
    private $ktUsageDetail;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return KtUsageDetail
     */
    public function getKtUsageDetail()
    {
        return $this->ktUsageDetail;
    }

    /**
     * @param $json
     * @return ProductKtUsageDetail
     */
    public static function fromJson($json)
    {
        $instance = new self();
        $instance->product = Product::fromJson($json->product);
        if ($json->ktUsageDetails) {
            $instance->ktUsageDetail = KtUsageDetail::fromJson($json->ktUsageDetails);
        }
        return $instance;
    }
}