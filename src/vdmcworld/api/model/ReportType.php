<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ReportType implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return ReportType
     */
    public static function fromJson($json)
    {
        $rt = new ReportType();
        $rt->id = $json->id;
        $rt->name = $json->name;
        return $rt;
    }
}