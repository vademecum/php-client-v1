<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class PhDerangement implements Model
{
    /* @var Substance */
    protected $substance;

    /* @var float */
    protected $minPh = null;

    /* @var float */
    protected $maxPh = null;

    /* @var DerangementConsequence */
    protected $consequence;

    /* @var string */
    protected $references;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return string
     */
    public function getMinPh()
    {
        return $this->minPh;
    }

    /**
     * @return string
     */
    public function getMaxPh()
    {
        return $this->maxPh;
    }

    /**
     * @return DerangementConsequence
     */
    public function getConsequence()
    {
        return $this->consequence;
    }

    /**
     * @return string
     */
    public function getReferences()
    {
        return $this->references;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->substance)) {
            $c->substance = Substance::fromJson($json->substance);
        }

        if (isset($json->minPh)) {
            $c->minPh = $json->minPh;
        }

        if (isset($json->maxPh)) {
            $c->maxPh = $json->maxPh;
        }

        if (isset($json->consequence)) {
            $c->consequence = DerangementConsequence::fromJson($json->consequence);
        }

        if (isset($json->references)) {
            $c->references = $json->references;
        }

        return $c;
    }
}