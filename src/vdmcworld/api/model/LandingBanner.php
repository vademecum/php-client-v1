<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class LandingBanner implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $image_url;

    /**
     * @var string
     */
    protected $target_url;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->image_url;
    }

    /**
     * @param string $image_url
     */
    public function setImageUrl(string $image_url): void
    {
        $this->image_url = $image_url;
    }

    /**
     * @return string
     */
    public function getTargetUrl(): string
    {
        return $this->target_url;
    }

    /**
     * @param string $target_url
     */
    public function setTargetUrl(string $target_url): void
    {
        $this->target_url = $target_url;
    }


    /**
     * @param $json
     * @return LandingBanner
     */
    public static function fromJson($json)
    {
        $instance = new LandingBanner();

        $instance->id = $json->id;
        $instance->image_url = $json->image_url;
        $instance->target_url = $json->target_url;

        return $instance;
    }
}