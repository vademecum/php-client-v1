<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Amount implements Model
{
    /**
     * @var int
     */
    protected $amount;

    /**
     * @var Unit
     */
    protected $unit;

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param $json
     * @return Amount
     */
    public static function fromJson($json)
    {
        $instance = new Amount();
        if ($json->amount) {
            $instance->amount = $json->amount;
        }
        if ($json->unit) {
            $instance->unit = Unit::fromJson($json->unit);
        }
        return $instance;
    }
}