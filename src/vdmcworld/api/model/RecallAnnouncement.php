<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class RecallAnnouncement implements Model
{
    /**
     * @var int
     */
    protected $count;

    /**
     * @var RecallAnnouncementInfo[]
     */
    protected $announcements;


    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return RecallAnnouncementInfo[]
     */
    public function getAnnouncements()
    {
        return $this->announcements;
    }

    /**
     * @param $json
     * @return RecallAnnouncement
     */
    public static function fromJson($json)
    {
        $instance = new RecallAnnouncement();

        $instance->count = $json->count;
        if (!empty($json->announcements) && isset($json->announcements)) {
            foreach ($json->announcements as $announcement) {
                $instance->announcements[] = RecallAnnouncementInfo::fromJson($announcement);
            }
        }

        return $instance;
    }
}