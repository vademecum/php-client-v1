<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementStabilityDuration implements Model
{
    /* @var int */
    protected $amount;

    /* @var Unit[] */
    protected $unit;

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Unit[]
     */
    public function getUnit()
    {
        return $this->unit;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->amount)) {
            $c->amount = $json->amount;
        }

        if (isset($json->unit)) {
            $c->unit = Unit::fromJson($json->unit);
        }

        return $c;
    }

}
