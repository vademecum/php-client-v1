<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductMonographyCard implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var MonographyCard
     */
    protected $card;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return MonographyCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param $json
     * @return ProductMonographyCard
     */
    public static function fromJson($json)
    {
        $instance = new ProductMonographyCard();
        $instance->product = Product::fromJson($json->product);
        $instance->card = MonographyCard::fromJson($json->card);
        return $instance;
    }
}