<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class PriceInfo implements Model
{

    /**
     * @var float
     */
    protected $factory;

    /**
     * @var float
     */
    protected $publicFactory;

    /**
     * @var float
     */
    protected $storage;

    /**
     * @var float
     */
    protected $publicStorage;

    /**
     * @var boolean
     */
    protected $dualPriced;

    /**
     * @var float
     */
    protected $pharmacy;

    /**
     * @var float
     */
    protected $retail;

    /**
     * @var \DateTime
     */
    protected $effectiveDate;

    /**
     * @var float
     */
    protected $taxPercent;

    /**
     * @var float
     */
    protected $discountRate;

    /**
     * @var integer
     */
    protected $discountRateType;

    /**
     * @var float
     */
    protected $publicPrice;

    /**
     * @var float
     */
    protected $publicPriceVatInc;

    /**
     * @var string
     */
    protected $currencyCode;

    /**
     * @var boolean
     */
    protected $storageBased;

    /**
     * @var float
     */
    protected $storageSalesPrice;


    /**
     * @var PublicPaidPriceInfo[]
     */
    protected $publicPaidPrice = null;

    /**
     * @var boolean
     */
    protected $abroadProduct;

    /**
     * @var PublicPaidPriceInfo[]
     */
    protected $abroadPublicPaidPrice;

    /**
     * @var float
     */
    protected $abroadRetail;

    /**
     * @var float
     */
    protected $abroadPublicPrice;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @return float
     */
    protected $alternativeReimbursement;

    /**
     * @return boolean
     */
    public function getFactory()
    {
        return $this->factory;
    }

    /**
     * @return float
     */
    public function getPublicFactory()
    {
        return $this->publicFactory;
    }

    /**
     * @return float
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @return float
     */
    public function getPublicStorage()
    {
        return $this->publicStorage;
    }

    /**
     * @return boolean
     */
    public function getDualPriced()
    {
        return $this->dualPriced;
    }

    /**
     * @return float
     */
    public function getPharmacy()
    {
        return $this->pharmacy;
    }

    /**
     * @return float
     */
    public function getRetail()
    {
        return $this->retail;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }

    /**
     * @return float
     */
    public function getTaxPercent()
    {
        return $this->taxPercent;
    }

    /**
     * @return float
     */
    public function getDiscountRate()
    {
        return $this->discountRate;
    }

    /**
     * @return integer
     */
    public function getDiscountRateType()
    {
        return $this->discountRateType;
    }

    /**
     * @return float
     */
    public function getPublicPrice()
    {
        return $this->publicPrice;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @return boolean
     */
    public function isStorageBased()
    {
        return $this->storageBased;
    }

    /**
     * @return PublicPaidPriceInfo[]
     */
    public function getPublicPaidPrice()
    {
        return $this->publicPaidPrice;
    }

    /**
     * @return float
     */
    public function getPublicPriceVatInc()
    {
        return $this->publicPriceVatInc;
    }

    /**
     * @return float
     */
    public function getStorageSalesPrice()
    {
        return $this->storageSalesPrice;
    }

    /**
     * @return bool
     */
    public function isAbroadProduct()
    {
        return $this->abroadProduct;
    }

    /**
     * @return PublicPaidPriceInfo[]
     */
    public function getAbroadPublicPaidPrice()
    {
        return $this->abroadPublicPaidPrice;
    }

    /**
     * @return float
     */
    public function getAbroadRetail()
    {
        return $this->abroadRetail;
    }

    /**
     * @return float
     */
    public function getAbroadPublicPrice()
    {
        return $this->abroadPublicPrice;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $json
     * @return PriceInfo
     */
    public static function fromJson($json)
    {
        return PriceInfo::_parseJson(new PriceInfo(), $json);
    }

    /**
     * @param PriceInfo $instance
     * @param $json
     * @return PriceInfo
     */
    protected static function _parseJson($instance, $json)
    {
        foreach ([
                     "factory", "storage", "pharmacy", "retail", "taxPercent", "discountRate",
                     "publicPrice", "storageBased", "dualPriced", "currencyCode", "storageSalesPrice",
                     "abroadProduct", "abroadRetail", "abroadPublicPrice", "publicStorage", "publicFactory",
                 ] as $key) {
            $instance->$key = $json->$key;
        }
        $instance->publicPriceVatInc = $json->publicPriceVATInc;
        $instance->discountRateType = $json->discountRateType;

        $instance->updatedAt = new \DateTime("now", new \DateTimeZone("UTC"));
        $instance->updatedAt->setTimestamp($json->updatedAt);
        $instance->alternativeReimbursement = $json->alternativeReimbursement;

        if (isset($json->publicPaidPrice) && $json->publicPaidPrice) {
            $instance->publicPaidPrice = [];
            foreach ($json->publicPaidPrice as $ppp) {
                $instance->publicPaidPrice[] = PublicPaidPriceInfo::fromJson($ppp);
            }
        }

        if (isset($json->abroadPublicPaidPrice) && $json->abroadPublicPaidPrice) {
            $instance->abroadPublicPaidPrice = [];
            foreach ($json->abroadPublicPaidPrice as $ppp) {
                $instance->abroadPublicPaidPrice[] = PublicPaidPriceInfo::fromJson($ppp);
            }
        }

        $instance->effectiveDate = \DateTime::createFromFormat(
            "Y-m-d H:i:s",
            $json->effectiveDate . " 00:00:00",
            new \DateTimeZone("UTC")
        );

        return $instance;
    }
}