<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductPatientCharacteristicInteraction implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var PatientCategory
     */
    private $affectingPatientCategory;

    /**
     * @var Color
     */
    private $color;

    /**
     * @var string
     */
    private $description;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return PatientCategory
     */
    public function getAffectingPatientCategory()
    {
        return $this->affectingPatientCategory;
    }

    /**
     * @return Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return ProductPatientCharacteristicInteraction
     */
    public static function fromJson($json)
    {
        $ppci = new ProductPatientCharacteristicInteraction();
        $ppci->product = Product::fromJson($json->product);
        $ppci->affectingPatientCategory = PatientCategory::fromJson($json->affectingPatientCategory);
        $ppci->color = Color::fromJson($json->color);
        $ppci->description = $json->description;
        return $ppci;
    }
}