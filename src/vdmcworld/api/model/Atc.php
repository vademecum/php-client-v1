<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Atc implements Model
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $description;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return Atc
     */
    public static function fromJson($json)
    {
        $a = new Atc();
        $a->code = $json->code;
        $a->description = $json->description;
        return $a;
    }
}