<?php


namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ReimbursementFuturePrice implements Model
{
    /**
     * @var float
     */
    protected $price;

    /**
     * @var \DateTime
     */
    protected $effectiveDate;

    /**
     * @var string
     */
    protected $currencyCode;

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param $json
     * @return ReimbursementFuturePrice
     */
    public static function fromJson($json)
    {
        $instance = new ReimbursementFuturePrice();
        $instance->price = $json->price;
        $instance->effectiveDate = \DateTime::createFromFormat(
            "Y-m-d H:i:s",
            $json->effectiveDate . " 00:00:00",
            new \DateTimeZone("UTC")
        );
        $instance->currencyCode = $json->currencyCode;
        return $instance;
    }
}
