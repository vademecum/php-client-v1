<?php

namespace vdmcworld\api\model;


class Nutrition extends Substance
{
    /**
     * @param $json
     * @return Nutrition
     */
    public static function fromJson($json)
    {
        return self::parseJson($json, new Nutrition());
    }
}