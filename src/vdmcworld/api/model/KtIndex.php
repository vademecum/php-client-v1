<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class KtIndex implements Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var int
     */
    private $parentId;

    /**
     * @var bool
     */
    private $hasContent;

    public function hasContent()
    {
        return $this->hasContent;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param $json
     * @return KtIndex
     */
    public static function fromJson($json)
    {
        $instance = new KtIndex();
        $instance->id = $json->id;
        $instance->title = $json->title;
        $instance->parentId = $json->parentId;
        $instance->hasContent = $json->hasContent;
        return $instance;
    }
}