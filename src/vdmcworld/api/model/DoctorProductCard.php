<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class DoctorProductCard implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var DoctorCard
     */
    protected $card;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return DoctorCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param $json
     * @return DoctorProductCard
     */
    public static function fromJson($json)
    {
        $dpc = new DoctorProductCard();
        $dpc->product = Product::fromJson($json->product);
        $dpc->card = DoctorCard::fromJson($json->card);
        return $dpc;
    }
}