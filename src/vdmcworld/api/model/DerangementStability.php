<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementStability implements Model
{
    /* @var DerangementStability */
    protected $id;


    /* @var DerangementContainer */
    protected $container;

    /* @var float[] */
    protected $temperature;


    /* @var DerangementStabilityDuration */
    protected $duration;


    /* @var DerangementMixtureStorage */
    protected $storage;


    /* @var bool */
    protected $isCompatible;


    /* @var string */
    protected $description;


    /* @var string */
    protected $references;


    /**
     * @return DerangementStability
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DerangementContainer
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return float[]
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * @return DerangementStabilityDuration
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @return DerangementMixtureStorage
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @return bool
     */
    public function isCompatible()
    {
        return $this->isCompatible;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getReferences()
    {
        return $this->references;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->id)) {
            $c->id = $json->id;
        }

        if (isset($json->container)) {
            $c->container = DerangementContainer::fromJson($json->container);
        }

        if (isset($json->temperature)) {
            $c->temperature = $json->temperature;
        }

        if (isset($json->duration)) {
            $c->duration = DerangementStabilityDuration::fromJson($json->duration);
        }

        if (isset($json->storage)) {
            $c->storage = DerangementMixtureStorage::fromJson($json->storage);
        }

        if (isset($json->isCompatible)) {
            $c->isCompatible = $json->isCompatible;
        }

        if (isset($json->description)) {
            $c->description = $json->description;
        }

        if (isset($json->references)) {
            $c->references = $json->references;
        }

        return $c;
    }
}
