<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductTemperatureDerangement implements Model
{
    /* @var Product */
    protected $product;

    /* @var TemperatureDerangement[] */
    protected $temperatureDerangements = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return TemperatureDerangement[]
     */
    public function getTemperatureDerangements()
    {
        return $this->temperatureDerangements;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->product)) {
            $c->product = Product::fromJson($json->product);
        }

        if (isset($json->temperatureDerangements)) {
            foreach ($json->temperatureDerangements as $cd) {
                $c->temperatureDerangements [] = TemperatureDerangement::fromJson($cd);
            }
        }

        return $c;
    }
}
