<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductWarning implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Color
     */
    private $color;

    /**
     * @var string
     */
    private $description;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return Model
     */
    public static function fromJson($json)
    {
        $pw = new ProductWarning();
        $pw->product = Product::fromJson($json->product);
        $pw->color = Color::fromJson($json->color);
        $pw->description = $json->description;
        return $pw;
    }
}