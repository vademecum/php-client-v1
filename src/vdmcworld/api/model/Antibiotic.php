<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class Antibiotic implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var IcdCompatibility[]
     */
    private $icdCompatibilities;

    /**
     * @return IcdCompatibility[]
     */
    public function getIcdCompatibilities()
    {
        return $this->icdCompatibilities;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param $json
     * @return Model
     */
    public static function fromJson($json)
    {
        $a = new Antibiotic();
        $a->product = Product::fromJson($json->product);
        foreach ($json->icdCompatibility as $item) {
            $a->icdCompatibilities[] = IcdCompatibility::fromJson($item);
        }
        return $a;
    }
}