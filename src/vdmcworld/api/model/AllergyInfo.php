<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class AllergyInfo implements Model
{
    /**
     * @var string
     */
    private $info;
    /**
     * @var string
     */
    private $desensitization;

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @return string
     */
    public function getDesensitization()
    {
        return $this->desensitization;
    }

    /**
     * @param $json
     * @return AllergyInfo
     */
    public static function fromJson($json)
    {
        $a = new AllergyInfo();
        $a->info = $json->info;
        $a->desensitization = $json->desensitization;
        return $a;
    }
}