<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PublicPaidPrice implements Model
{
    /**
     * @var float
     */
    protected $price;

    /**
     * @var float
     */
    protected $priceVatInc;

    /**
     * @var float
     */
    protected $discountedPriceVatInc;

    /**
     * @var string
     */
    protected $groupCode;

    /**
     * @var integer
     */
    protected $groupType;

    /**
     * @var float
     */
    protected $groupLowestUnitPrice;

    /**
     * @var string
     */
    protected $equivalentGroupCode;

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getPriceVatInc()
    {
        return $this->priceVatInc;
    }

    /**
     * @param float $priceVatInc
     */
    public function setPriceVatInc($priceVatInc)
    {
        $this->priceVatInc = $priceVatInc;
    }

    /**
     * @return float
     */
    public function getDiscountedPriceVatInc()
    {
        return $this->discountedPriceVatInc;
    }

    /**
     * @param float $discountedPriceVatInc
     */
    public function setDiscountedPriceVatInc($discountedPriceVatInc)
    {
        $this->discountedPriceVatInc = $discountedPriceVatInc;
    }

    /**
     * @return string
     */
    public function getGroupCode()
    {
        return $this->groupCode;
    }

    /**
     * @param string $groupCode
     */
    public function setGroupCode($groupCode)
    {
        $this->groupCode = $groupCode;
    }

    /**
     * @return int
     */
    public function getGroupType()
    {
        return $this->groupType;
    }

    /**
     * @param int $groupType
     */
    public function setGroupType($groupType)
    {
        $this->groupType = $groupType;
    }

    /**
     * @return float
     */
    public function getGroupLowestUnitPrice()
    {
        return $this->groupLowestUnitPrice;
    }

    /**
     * @param float $groupLowestUnitPrice
     */
    public function setGroupLowestUnitPrice($groupLowestUnitPrice)
    {
        $this->groupLowestUnitPrice = $groupLowestUnitPrice;
    }

    /**
     * @return string
     */
    public function getEquivalentGroupCode()
    {
        return $this->equivalentGroupCode;
    }

    /**
     * @param string $equivalentGroupCode
     */
    public function setEquivalentGroupCode($equivalentGroupCode)
    {
        $this->equivalentGroupCode = $equivalentGroupCode;
    }

    /**
     * @param $json
     * @return TitckPriceInfo
     */
    public static function fromJson($json)
    {
        $instance = new PublicPaidPrice();

        foreach (["price", "priceVatInc", "discountedPriceVatInc", "groupCode", "groupType", "groupLowestUnitPrice", "equivalentGroupCode"] as $key) {
            $instance->$key = $json->$key;
        }

        return $instance;
    }
}