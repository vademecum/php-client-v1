<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Sticker implements Model
{
    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $caution;

    /**
     * @var string
     */
    protected $riskCaution;

    /**
     * @var string
     */
    protected $applicationMethod;

    /**
     * @var string
     */
    protected $storageCondition;

    /**
     * @var string
     */
    protected $medicationTime;

    /**
     * @var string
     */
    protected $effectOnDriver;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getCaution()
    {
        return $this->caution;
    }

    /**
     * @return string
     */
    public function getRiskCaution()
    {
        return $this->riskCaution;
    }

    /**
     * @return string
     */
    public function getApplicationMethod()
    {
        return $this->applicationMethod;
    }

    /**
     * @return string
     */
    public function getStorageCondition()
    {
        return $this->storageCondition;
    }

    /**
     * @return string
     */
    public function getMedicationTime()
    {
        return $this->medicationTime;
    }

    /**
     * @return string
     */
    public function getEffectOnDriver()
    {
        return $this->effectOnDriver;
    }

    /**
     * @param $json
     * @return Sticker
     */
    public static function fromJson($json)
    {
        $instance = new Sticker();

        foreach (["description", "caution", "riskCaution", "applicationMethod", "storageCondition", "medicationTime", "effectOnDriver"] as $k) {
            if (isset($json->$k)) {
                $instance->$k = $json->$k;
            }
        }

        return $instance;
    }
}