<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductNutritionInfo implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var NutritionInfo
     */
    protected $nutritionInfo;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return NutritionInfo
     */
    public function getNutritionInfo()
    {
        return $this->nutritionInfo;
    }


    /**
     * @param $json
     * @return ProductNutritionInfo
     */
    public static function fromJson($json)
    {
        $instance = new ProductNutritionInfo();
        $instance->product = Product::fromJson($json->product);
        if ($json->nutritionInfo) {
            $instance->nutritionInfo = NutritionInfo::fromJson($json->nutritionInfo);
        }

        return $instance;
    }
}