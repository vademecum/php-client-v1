<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ReimbursementSummary implements Model
{
    /**
     * @var ReimbursementCondition
     */
    protected $condition;

    /**
     * @var Status
     */
    protected $status;

    /**
     * @var int
     */
    protected $maximumReportTime;

    /**
     * @var string
     */
    protected $maximumReportTimeText;

    /**
     * @var string
     */
    protected $maximumReportTimeDescription;

    /**
     * @var string
     */
    protected $reportTypeDescription;

    /**
     * @var HealthInstitutionCategory
     */
    protected $healthInstitutionCategories;

    /**
     * @var HealthInstitutionCategory
     */
    protected $healthInstitutionCategoriesNote;

    /**
     * @var string
     */
    protected $specialCases;

    /**
     * @var ReportType
     */
    protected $reportType;

    /**
     * @var Prescriber[]
     */
    protected $prescribers;

    /**
     * @var ReportIssuer[]
     */
    protected $reportIssuers;

    /**
     * @var Prescriber[]
     */
    protected $prescribersNote;

    /**
     * @var ReportIssuer[]
     */
    protected $reportIssuersNote;

    /**
     * @var ReimbursementMaxReportTime[]
     */
    protected $maximumReportTimeArray;

    /**
     * @var ReimbursementReportType[]
     */
    protected $reportTypeArray;

    /**
     * @return ReimbursementCondition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @return Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getMaximumReportTime()
    {
        return $this->maximumReportTime;
    }

    /**
     * @return string
     */
    public function getMaximumReportTimeText()
    {
        return $this->maximumReportTimeText;
    }

    /**
     * @return string
     */
    public function getMaximumReportTimeDescription()
    {
        return $this->maximumReportTimeDescription;
    }

    /**
     * @return string
     */
    public function getReportTypeDescription()
    {
        return $this->reportTypeDescription;
    }

    /**
     * @return HealthInstitutionCategory
     */
    public function getHealthInstitutionCategories()
    {
        return $this->healthInstitutionCategories;
    }

    /**
     * @return HealthInstitutionCategory
     */
    public function getHealthInstitutionCategoriesNote()
    {
        return $this->healthInstitutionCategoriesNote;
    }

    /**
     * @return string
     */
    public function getSpecialCases()
    {
        return $this->specialCases;
    }

    /**
     * @return ReportType
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * @return Prescriber[]
     */
    public function getPrescribers()
    {
        return $this->prescribers;
    }

    /**
     * @return ReportIssuer[]
     */
    public function getReportIssuers()
    {
        return $this->reportIssuers;
    }

    /**
     * @return Prescriber[]
     */
    public function getPrescribersNote()
    {
        return $this->prescribersNote;
    }

    /**
     * @return ReportIssuer[]
     */
    public function getReportIssuersNote()
    {
        return $this->reportIssuersNote;
    }

    /**
     * @return ReimbursementMaxReportTime[]
     */
    public function getMaximumReportTimeArray()
    {
        return $this->maximumReportTimeArray;
    }

    /**
     * @return ReimbursementReportType[]
     */
    public function getReportTypeArray()
    {
        return $this->reportTypeArray;
    }

    /**
     * @param $json
     * @return ReimbursementSummary
     */
    public static function fromJson($json)
    {
        $rs = new ReimbursementSummary();
        $rs->condition = ReimbursementCondition::fromJson($json->reimbursementCondition);
        $rs->status = ReimbursementStatus::fromJson($json->reimbursementStatus);

        foreach ($json->healthInstitutionCategories as $hic) {
            $rs->healthInstitutionCategories[] = HealthInstitutionCategory::fromJson($hic);
        }
        foreach ($json->prescribers as $p) {
            $rs->prescribers[] = Prescriber::fromJson($p);
        }
        foreach ($json->reportIssuers as $ri) {
            $rs->reportIssuers[] = ReportIssuer::fromJson($ri);
        }
        if (!empty($json->healthInstitutionCategoriesNote)) {
            foreach ($json->healthInstitutionCategoriesNote as $hic) {
                $rs->healthInstitutionCategoriesNote[] = HealthInstitutionCategory::fromJson($hic);
            }
        }
        if (!empty($json->prescribersNote)) {
            foreach ($json->prescribersNote as $p) {
                $rs->prescribersNote[] = Prescriber::fromJson($p);
            }
        }
        if (!empty($json->reportIssuersNote)) {
            foreach ($json->reportIssuersNote as $ri) {
                $rs->reportIssuersNote[] = ReportIssuer::fromJson($ri);
            }
        }
        $rs->maximumReportTime = $json->maximumReportTime;
        $rs->maximumReportTimeText = $json->maximumReportTimeText;
        $rs->maximumReportTimeDescription = $json->maximumReportTimeDescription;
        $rs->specialCases = $json->specialCases;
        if ($json->reportType) {
            $rs->reportType = ReportType::fromJson($json->reportType);
            $rs->reportTypeDescription = $json->reportTypeDescription;
        }
        if (!empty($json->reportTypeArray)) {
            foreach ($json->reportTypeArray as $rta) {
                $rs->reportTypeArray[] = ReimbursementReportType::fromJson($rta);
            }
        }
        if (!empty($json->maximumReportTimeArray)) {
            foreach ($json->maximumReportTimeArray as $mrt) {
                $rs->maximumReportTimeArray[] = ReimbursementMaxReportTime::fromJson($mrt);
            }
        }

        return $rs;
    }
}