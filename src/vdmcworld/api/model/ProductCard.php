<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductCard implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductCardCard
     */
    protected $card;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ProductCardCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param $json
     * @return ProductCard
     */
    public static function fromJson($json)
    {
        $pc = new ProductCard();
        $pc->product = Product::fromJson($json->product);
        $pc->card = ProductCardCard::fromJson($json->card);
        return $pc;
    }
}