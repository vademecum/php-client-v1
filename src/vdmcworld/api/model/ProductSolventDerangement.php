<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductSolventDerangement implements Model
{
    /* @var Product */
    protected $product;

    /* @var SolventDerangement[] */
    protected $solventDerangements = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return SolventDerangement[]
     */
    public function getSolventDerangements()
    {
        return $this->solventDerangements;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->product)) {
            $c->product = Product::fromJson($json->product);
        }

        if (isset($json->solventDerangements)) {
            foreach ($json->solventDerangements as $cd) {
                $c->solventDerangements [] = SolventDerangement::fromJson($cd);
            }
        }

        return $c;
    }
}
