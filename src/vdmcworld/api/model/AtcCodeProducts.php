<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class AtcCodeProducts implements Model
{
    /**
     * @var string
     */
    protected $atc;

    /**
     * @var Product[]
     */
    protected $products;

    /**
     * @return string
     */
    public function getAtc()
    {
        return $this->atc;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param $json
     * @return AtcCodeProducts
     */
    public static function fromJson($json)
    {
        $instance = new AtcCodeProducts();
        $instance->atc = $json->atc;
        $instance->products = [];
        foreach ($json->products as $p) {
            $instance->products[] = Product::fromJson($p);
        }
        return $instance;
    }
}
