<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ProductStatus implements Model
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return ProductStatus
     */
    public static function fromJson($json)
    {
        $ps = new ProductStatus();
        $ps->id = $json->id;
        $ps->name = $json->name;
        return $ps;
    }
}