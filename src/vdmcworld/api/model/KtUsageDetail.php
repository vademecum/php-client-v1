<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class KtUsageDetail implements Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $headerId;

    /**
     * @var string
     */
    private $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getHeaderId()
    {
        return $this->headerId;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return KtUsageDetail
     */
    public static function fromJson($json)
    {
        $instance = new self();
        $instance->id = $json->id;
        $instance->headerId = $json->headerId;
        $instance->description = $json->description;
        return $instance;
    }
}