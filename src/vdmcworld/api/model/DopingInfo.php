<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DopingInfo implements Model
{
    /**
     * @var boolean
     */
    protected $prohibited;

    /**
     * @var string
     */
    protected $prohibitionTime;

    /**
     * @var string
     */
    protected $prohibitionCondition;

    /**
     * @var string
     */
    protected $prohibitedGender;

    /**
     * @var string
     */
    protected $prohibitionConditionDescription;

    /**
     * @var string
     */
    protected $specialCases;

    /**
     * @var Sport[]
     */
    protected $prohibitedSports;

    /**
     * @var ApplicationMethod[]
     */
    protected $prohibitedApplicationMethods;

    /**
     * @var string[]
     */
    protected $wadaCode;

    /**
     * @return bool
     */
    public function isProhibited()
    {
        return $this->prohibited;
    }

    /**
     * @return string
     */
    public function getProhibitionTime()
    {
        return $this->prohibitionTime;
    }

    /**
     * @return string
     */
    public function getProhibitionCondition()
    {
        return $this->prohibitionCondition;
    }

    /**
     * @return string
     */
    public function getProhibitedGender()
    {
        return $this->prohibitedGender;
    }

    /**
     * @return string
     */
    public function getProhibitionConditionDescription()
    {
        return $this->prohibitionConditionDescription;
    }

    /**
     * @return string
     */
    public function getSpecialCases()
    {
        return $this->specialCases;
    }

    /**
     * @return Sport[]
     */
    public function getProhibitedSports()
    {
        return $this->prohibitedSports;
    }

    /**
     * @return ApplicationMethod[]
     */
    public function getProhibitedApplicationMethods()
    {
        return $this->prohibitedApplicationMethods;
    }

    /**
     * @return string[]
     */
    public function getWadaCode()
    {
        return $this->wadaCode;
    }

    /**
     * @param $json
     * @return Model|DopingInfo
     */
    public static function fromJson($json)
    {
        $instance = new DopingInfo();
        $instance->prohibited = $json->prohibited;
        $instance->prohibitionTime = $json->prohibitionTime;
        $instance->prohibitionCondition = $json->prohibitionCondition;
        $instance->prohibitedGender = $json->prohibitedGender;
        $instance->prohibitionConditionDescription = $json->prohibitionConditionDescription;
        $instance->specialCases = $json->specialCases;
        if ($json->prohibitedSports) {
            foreach ($json->prohibitedSports as $sport) {
                $instance->prohibitedSports[] = Sport::fromJson($sport);
            }
        }
        if ($json->prohibitedApplicationMethods) {
            foreach ($json->prohibitedApplicationMethods as $applicationMethod) {
                $instance->prohibitedApplicationMethods[] = ApplicationMethod::fromJson($applicationMethod);
            }
        }
        $instance->wadaCode = $json->wadaCode;

        return $instance;
    }
}