<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductCautionCard implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var CautionCard
     */
    protected $card;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return CautionCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param $json
     * @return ProductCautionCard
     */
    public static function fromJson($json)
    {
        $instance = new ProductCautionCard();
        $instance->product = Product::fromJson($json->product);
        $instance->card = CautionCard::fromJson($json->card);
        return $instance;
    }
}