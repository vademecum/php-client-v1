<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class BodyPart implements Model
{
    /**
     * @var string
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return BodyPart
     */
    public static function fromJson($json)
    {
        $b = new BodyPart();
        $b->id = $json->id;
        $b->name = $json->name;
        return $b;
    }
}