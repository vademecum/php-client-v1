<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductDosageInfo implements Model
{
    /**
     * @var string
     */
    protected $product;

    /**
     * @var string
     */
    protected $dosageInfo;

    /**
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getDosageInfo()
    {
        return $this->dosageInfo;
    }

    /**
     * @param $json
     * @return ProductDosageInfo
     */
    public static function fromJson($json)
    {
        $instance = new ProductDosageInfo();
        $instance->product = Product::fromJson($json->product);
        $instance->dosageInfo = $json->dosage;
        return $instance;
    }
}