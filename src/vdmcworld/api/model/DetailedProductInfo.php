<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class DetailedProductInfo implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var Status
     */
    protected $reimbursementStatus;

    /**
     * @var Status
     */
    protected $onMarketStatus;

    /**
     * @var PriceInfo
     */
    private $priceInfo;

    /**
     * @var boolean
     */
    protected $onMarket;


    /**
     * @var Substance[]
     */
    protected $substances;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return Status
     */
    public function getReimbursementStatus()
    {
        return $this->reimbursementStatus;
    }

    /**
     * @return Status
     */
    public function getOnMarket()
    {
        return $this->onMarket;
    }

    /**
     * @return Status
     */
    public function getMarketStatus()
    {
        return $this->onMarketStatus;
    }

    /**
     * @return PriceInfo
     */
    public function getPriceInfo()
    {
        return $this->priceInfo;
    }

    public function getSubstances()
    {
        return $this->substances;
    }

    /**
     * @param $json
     * @return DetailedProductInfo
     */
    public static function fromJson($json)
    {
        $instance = new DetailedProductInfo();

        if (isset($json->product) && !empty($json->product)) {
            $instance->product = Product::fromJson($json->product);
        }
        if (isset($json->company) && !empty($json->company)) {
            $instance->company = Company::fromJson($json->company);
        }
        if (isset($json->reimbursementStatus)) {
            $instance->reimbursementStatus = Status::fromJson($json->reimbursementStatus);
        }
        if (isset($json->onMarketStatus)) {
            $instance->onMarketStatus = Status::fromJson($json->onMarketStatus);
        }

        if (isset($json->priceInfo) && !empty($json->priceInfo)) {
            $instance->priceInfo = PriceInfo::fromJson($json->priceInfo);
        }

        if (isset($json->onMarket)) {
            $instance->onMarket = $json->onMarket;
        }

        if (isset($json->substances)) {
            $instance->substances = $json->substances;
        }

        return $instance;
    }
}