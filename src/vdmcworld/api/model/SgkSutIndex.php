<?php


namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class SgkSutIndex implements Model
{
    /**
     * @var integer
     */
    private $sutId;

    /**
     * @var string
     */
    private $itemDesc;

    /**
     * @var string
     */
    private $itemNo;

    /**
     * @var integer
     */
    private $parent;

    /**
     * @return int
     */
    public function getSutId()
    {
        return $this->sutId;
    }

    /**
     * @return string
     */
    public function getItemDesc()
    {
        return $this->itemDesc;
    }

    /**
     * @return string
     */
    public function getItemNo()
    {
        return $this->itemNo;
    }

    /**
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param $json
     * @return SgkSutIndex
     */
    public static function fromJson($json)
    {
        $instance = new SgkSutIndex();

        if (isset($json->sutId)) {
            $instance->sutId = $json->sutId;
        }
        if (isset($json->itemDesc)) {
            $instance->itemDesc = $json->itemDesc;
        }
        if (isset($json->itemNo)) {
            $instance->itemNo = $json->itemNo;
        }
        if (isset($json->parent)) {
            $instance->parent = $json->parent;
        }

        return $instance;
    }
}
