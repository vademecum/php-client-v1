<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class SubstanceDopingData implements Model
{
    /**
     * @var Substance
     */
    protected $substance;

    /**
     * @var DopingData
     */
    protected $dopingData;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return DopingData
     */
    public function getDopingData()
    {
        return $this->dopingData;
    }

    /**
     * @param $json
     * @return SubstanceDopingData
     */
    public static function fromJson($json)
    {
        $instance = new SubstanceDopingData();
        $instance->substance = Substance::fromJson($json->substance);
        $instance->dopingData = DopingData::fromJson($json->dopingData);
        return $instance;
    }
}