<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstancePharmacologicalClass implements Model
{
    /**
     * @var Substance
     */
    protected $substance;

    /* @var PharmacologicalClass[] */
    protected $pharmacologicalClass = [];

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return PharmacologicalClass[]
     */
    public function getPharmacologicalClass()
    {
        return $this->pharmacologicalClass;
    }

    /**
     * @param $json
     * @return SubstancePharmacologicalClass
     */
    public static function fromJson($json)
    {
        $phr = new SubstancePharmacologicalClass();
        $phr->substance = Substance::fromJson($json->substance);

        if (!empty($json->pharmacologicalClass)) {
            foreach ($json->pharmacologicalClass as $class) {
                $phr->pharmacologicalClass [] = PharmacologicalSubstanceClass::fromJson($class);
            }
        }

        return $phr;
    }
}