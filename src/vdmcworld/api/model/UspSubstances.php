<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class UspSubstances implements Model
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var Substance[]
     */
    protected $substances;

    /**
     * @return string
     */
    public function getUsp()
    {
        return $this->usp;
    }

    /**
     * @return Substance[]
     */
    public function getSubstances()
    {
        return $this->substances;
    }

    /**
     * @param $json
     * @return UspSubstances
     */
    public static function fromJson($json)
    {
        $instance = new UspSubstances();
        $instance->usp = $json->usp;
        $instance->substances = [];
        foreach ($json->substances as $s) {
            $instance->substances[] = Substance::fromJson($s);
        }
        return $instance;
    }
}
