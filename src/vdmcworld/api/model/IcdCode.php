<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class IcdCode implements Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $branchOf;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getBranchOf()
    {
        return $this->branchOf;
    }

    /**
     * @param $json
     * @return IcdCode
     */
    public static function fromJson($json)
    {
        $icdCode = new IcdCode();
        $icdCode->id = $json->id;
        $icdCode->name = $json->name;
        $icdCode->description = $json->description;
        $icdCode->branchOf = $json->branchOf;
        return $icdCode;
    }
}