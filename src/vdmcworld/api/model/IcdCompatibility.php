<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class IcdCompatibility implements Model
{
    /**
     * @var Icd
     */
    private $icd;

    /**
     * @var Status
     */
    private $compatibility;

    /**
     * @return Icd
     */
    public function getIcd()
    {
        return $this->icd;
    }

    /**
     * @return Status
     */
    public function getCompatibility()
    {
        return $this->compatibility;
    }

    /**
     * @param $json
     * @return IcdCompatibility
     */
    public static function fromJson($json)
    {
        $i = new IcdCompatibility();
        $i->icd = Icd::fromJson($json->icd);
        $i->compatibility = Status::fromJson($json->compatibility);
        return $i;
    }
}