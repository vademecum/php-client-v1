<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Answer implements Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $questionId;

    /**
     * @var string
     */
    public $answer;

    /**
     * @var string
     */
    public $parameter;

    /**
     * @var string
     */
    public $warning;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @return string
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * @return string
     */
    public function getWarning()
    {
        return $this->warning;
    }


    /**
     * @param array $json
     * @return array
     */
    public static function fromJson($json)
    {
        $answers = [];
        foreach ($json as $answer) {
            $instance = new Answer();
            $instance->id = $answer->id;
            $instance->questionId = $answer->questionId;
            $instance->answer = $answer->answer;
            $instance->parameter = $answer->parameter;
            $instance->warning = $answer->warning;//
            $answers[] = $instance;
        }
        return $answers;
    }
}