<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductNutritionInteraction implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Nutrition
     */
    private $affectingNutrition;

    /**
     * @var Color
     */
    private $color;

    /**
     * @var string
     */
    private $description;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Nutrition
     */
    public function getAffectingNutrition()
    {
        return $this->affectingNutrition;
    }

    /**
     * @return Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return ProductNutritionInteraction
     */
    public static function fromJson($json)
    {
        $p = new ProductNutritionInteraction();
        $p->product = Product::fromJson($json->product);
        $p->affectingNutrition = Nutrition::fromJson($json->affectingNutrition);
        $p->color = Color::fromJson($json->color);
        $p->description = $json->description;
        return $p;
    }
}
