<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class AromatherapyPurposeCategory implements Model
{
    /**
     * @var AromatherapyCategory
     */
    protected $category;

    /**
     * @var AromatherapyPurpose
     */
    protected $purpose;

    /**
     * @return AromatherapyCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return AromatherapyPurpose
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @param $json
     * @return AromatherapyPurposeCategory
     */
    public static function fromJson($json)
    {
        $aromatherapyPurposeCategory = new AromatherapyPurposeCategory();
        if (!empty($json->category)) {
            $aromatherapyPurposeCategory->category = AromatherapyCategory::fromJson($json->category);
        }
        if (!empty($json->purpose)) {
            $aromatherapyPurposeCategory->purpose = AromatherapyPurpose::fromJson($json->purpose);
        }
        return $aromatherapyPurposeCategory;
    }
}