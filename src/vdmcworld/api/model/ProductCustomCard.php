<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductCustomCard implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var CustomCard
     */
    protected $card;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return CustomCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param $json
     * @return ProductCustomCard
     */
    public static function fromJson($json)
    {
        $instance = new ProductCustomCard();
        $instance->product = Product::fromJson($json->product);
        $instance->card = CustomCard::fromJson($json->card);
        return $instance;
    }
}