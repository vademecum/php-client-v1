<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SgkSutDetail implements Model
{
    /**
     * @var integer
     */
    private $sutId;

    /**
     * @var string
     */
    private $itemNo;

    /**
     * @var string
     */
    private $itemDesc;

    /**
     * @var string
     */
    private $itemSummaryDesc;

    /**
     * @var \DateTime
     */
    private $changeDate;

    /**
     * @var \DateTime
     */
    private $effectiveDate;

    /**
     * @var string
     */
    private $changeNo;

    /**
     * @var string
     */
    private $changeSummary;

    /**
     * @var string
     */
    private $warning;

    /**
     * @var SgkSutDetail[]
     */
    private $content;

    /**
     * @var SgkSutDetail[]
     */
    private $history;

    /**
     * @var \DateTime
     */
    private $latestEffectDate;

    /**
     * @return int
     */
    public function getSutId()
    {
        return $this->sutId;
    }

    /**
     * @return string
     */
    public function getItemNo()
    {
        return $this->itemNo;
    }

    /**
     * @return string
     */
    public function getItemDesc()
    {
        return $this->itemDesc;
    }

    /**
     * @return string
     */
    public function getItemSummaryDesc()
    {
        return $this->itemSummaryDesc;
    }

    /**
     * @return \DateTime
     */
    public function getChangeDate()
    {
        return $this->changeDate;
    }

    /**
     * @return \DateTime
     */
    public function getEffectiveDate()
    {
        return $this->effectiveDate;
    }

    /**
     * @return string
     */
    public function getChangeNo()
    {
        return $this->changeNo;
    }

    /**
     * @return string
     */
    public function getChangeSummary()
    {
        return $this->changeSummary;
    }

    /**
     * @return string
     */
    public function getWarning()
    {
        return $this->warning;
    }

    /**
     * @return SgkSutDetail[]
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return SgkSutDetail[]
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @return \DateTime
     */
    public function getLatestEffectDate()
    {
        return $this->latestEffectDate;
    }

    /**
     * @param \StdClass $json
     * @return SgkSutDetail
     */
    public static function fromJson($json)
    {
        $instance = new SgkSutDetail();
        foreach (["sutId", "itemNo", "itemDesc", "itemSummaryDesc", "changeNo", "changeSummary","warning"] as $f) {
            $instance->$f = $json->$f;
        }
        foreach (["changeDate", "effectiveDate","latestEffectDate"] as $f) {
            $instance->$f = \DateTime::createFromFormat("d.m.Y", $json->$f);
        }
        $instance->content = [];
        if (isset($json->content) && !empty($json->content)) {
            foreach ($json->content as $c) {
                $instance->content[] = SgkSutDetail::fromJson($c);
            }
        }
        $instance->history = [];
        if (isset($json->history) && !empty($json->history)) {
            foreach ($json->history as $h) {
                $instance->history[] = SgkSutDetail::fromJson($h);
            }
        }
        return $instance;
    }
}
