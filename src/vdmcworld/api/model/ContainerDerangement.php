<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class ContainerDerangement implements Model
{
    /* @var Substance */
    protected $substance;

    /* @var DerangementContainer */
    protected $container;

    /* @var DerangementConsequence */
    protected $consequence;

    /* @var string */
    protected $references;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return DerangementContainer
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return DerangementConsequence
     */
    public function getConsequence()
    {
        return $this->consequence;
    }

    /**
     * @return string
     */
    public function getReferences()
    {
        return $this->references;
    }

    public static function fromJson($json)
    {
        $c = new ContainerDerangement();

        if (isset($json->substance)) {
            $c->substance = Substance::fromJson($json->substance);
        }

        if (isset($json->container)) {
            $c->container = DerangementContainer::fromJson($json->container);
        }

        if (isset($json->consequence)) {
            $c->consequence = DerangementConsequence::fromJson($json->consequence);
        }

        if (isset($json->references)) {
            $c->references = $json->references;
        }

        return $c;
    }
}