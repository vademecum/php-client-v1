<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceCard implements Model
{
    /**
     * @var Substance
     */
    protected $substance;

    /**
     * @var SubstanceCardCard
     */
    protected $card;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return SubstanceCardCard
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param $json
     * @return SubstanceCard
     */
    public static function fromJson($json)
    {
        $instance = new SubstanceCard();
        $instance->substance = Substance::fromJson($json->substance);
        $instance->card = SubstanceCardCard::fromJson($json->card);
        return $instance;
    }
}