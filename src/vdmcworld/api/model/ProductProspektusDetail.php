<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductProspektusDetail implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var ProspektusDetail
     */
    private $prospektusDetail;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ProspektusDetail
     */
    public function getProspektusDetail()
    {
        return $this->prospektusDetail;
    }

    /**
     * @param $json
     * @return ProductProspektusDetail
     */
    public static function fromJson($json)
    {
        $instance = new self();
        $instance->product = Product::fromJson($json->product);
        if (isset($json->prospektusDetails) && !empty($json->prospektusDetails)) {
            $instance->prospektusDetail = ProspektusDetail::fromJson($json->prospektusDetails);
        }
        return $instance;
    }
}