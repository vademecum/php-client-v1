<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductMaximumDosage implements Model
{
    /**
     * @var Product
     */
    private $product;
    /**
     * @var MaximumDosage
     */
    private $maximumDosage;
    /**
     * @var bool
     */
    private $status;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return MaximumDosage
     */
    public function getMaximumDosage()
    {
        return $this->maximumDosage;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $json
     * @return ProductMaximumDosage
     */
    public static function fromJson($json)
    {
        $md = new ProductMaximumDosage();
        $md->product = Product::fromJson($json->product);
        $md->maximumDosage = MaximumDosage::fromJson($json->maximumDosage);
        if (isset($json->status)) {
            $md->status = Status::fromJson($json->status);
        }

        return $md;
    }
}