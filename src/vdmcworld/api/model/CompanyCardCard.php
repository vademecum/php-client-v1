<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class CompanyCardCard implements Model
{
    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @var ProductInfo[]
     */
    protected $advancedProductsInfo = [];

    /**
     * @var CompanyProductSubstance[]
     */
    protected $productsSubstances = [];

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return ProductInfo[]
     */
    public function getAdvancedProductsInfo()
    {
        return $this->advancedProductsInfo;
    }

    /**
     * @return CompanyProductSubstance[]
     */
    public function getProductsSubstances()
    {
        return $this->productsSubstances;
    }

    /**
     * @param $json
     * @return CompanyCardCard
     */
    public static function fromJson($json)
    {
        $instance = new CompanyCardCard();
        if ($json->products) {
            foreach ($json->products as $p) {
                $instance->products[] = Product::fromJson($p);
            }
        }
        if ($json->advancedProductsInfo) {
            foreach ($json->advancedProductsInfo as $p) {
                $instance->advancedProductsInfo[] = ProductInfo::fromJson($p);
            }
        }
        if ($json->productsSubstances) {
            foreach ($json->productsSubstances as $ps) {
                $instance->productsSubstances[] = CompanyProductSubstance::fromJson($ps);
            }
        }

        return $instance;
    }
}