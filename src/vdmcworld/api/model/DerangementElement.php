<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

abstract class DerangementElement implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return static
     */
    public static function fromJson($json)
    {
        $c = new static();
        foreach (["id", "name"] as $f) {
            $c->$f = $json->$f;
        }
        return $c;
    }
}