<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstancePrescriptionRule implements Model
{
    /**
     * @var Substance
     */
    protected $substance;

    /**
     * @var PrescriptionRule
     */
    protected $prescriptionRule;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return PrescriptionRule
     */
    public function getPrescriptionRule()
    {
        return $this->prescriptionRule;
    }

    /**
     * @param $json
     * @return SubstancePrescriptionRule
     */
    public static function fromJson($json)
    {
        $spr = new SubstancePrescriptionRule();
        $spr->substance = Substance::fromJson($json->substance);
        $spr->prescriptionRule = PrescriptionRule::fromJson($json->prescriptionRule);
        return $spr;
    }
}