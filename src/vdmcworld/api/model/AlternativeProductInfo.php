<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class AlternativeProductInfo implements Model
{

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ProductInfo[]
     */
    protected $alternativeProductInfos;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return ProductInfo[]
     */
    public function getAlternativeProductInfos()
    {
        return $this->alternativeProductInfos;
    }

    /**
     * @param $json
     * @return AlternativeProductInfo
     */
    public static function fromJson($json)
    {
        $instance = new AlternativeProductInfo();
        $instance->product = Product::fromJson($json->product);
        $instance->alternativeProductInfos = [];
        if ($json->alternativeProductsInfo) {
            foreach ($json->alternativeProductsInfo as $api) {
                $instance->alternativeProductInfos[] = ProductInfo::fromJson($api);
            }
        }
        return $instance;
    }
}