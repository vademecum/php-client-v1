<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Company implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $officialName;

    /**
     * @var string
     */
    protected $website;

    /**
     * @var string
     */
    protected $fax;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $district;

    /**
     * @var array
     */
    protected $types;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getOfficialName()
    {
        return $this->officialName;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param $json
     * @return Company
     */
    public static function fromJson($json)
    {
        $c = new Company();
        foreach (["id", "name", "officialName", "website", "fax", "phone", "address", "email", "city", "district", "types"] as $f) {
            $c->$f = $json->$f;
        }
        return $c;
    }
}