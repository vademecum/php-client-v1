<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PatientCategory implements Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return PatientCategory
     */
    public static function fromJson($json)
    {
        $p = new PatientCategory();
        $p->id = $json->id;
        $p->name = $json->name;
        return $p;
    }
}