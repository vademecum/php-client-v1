<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class DopingInteraction implements Model
{
    /**
     * @var DopingNutritionInteractionWarning[]
     */
    protected $productWarnings = [];

    /**
     * @var DopingNuritionInteraction[]
     */
    protected $productNutritionInteractions = [];

    /**
     * @var Product
     */
    protected $product;

    /**
     * @return DopingNutritionInteractionWarning[]
     */
    public function getProductWarnings()
    {
        return $this->productWarnings;
    }

    /**
     * @return DopingNuritionInteraction[]
     */
    public function getProductNutritionInteractions()
    {
        return $this->productNutritionInteractions;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param $json
     * @return DopingInteraction
     */
    public static function fromJson($json)
    {
        $instance = new DopingInteraction();
        if ($json->productWarning) {
            foreach ($json->productWarning as $pw) {
                $instance->productWarnings[] = DopingNutritionInteractionWarning::fromJson($pw);
            }
        }
        if ($json->productNutritionInteraction) {
            foreach ($json->productNutritionInteraction as $item) {
                $instance->productNutritionInteractions[] = DopingNuritionInteraction::fromJson($item);
            }
        }

        $instance->product = Product::fromJson($json->product);

        return $instance;

    }
}