<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ForeignDrug implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $companyName;

    /**
     * @var string
     */
    protected $pharmaceuticalForm;

    /**
     * @var string
     */
    protected $country;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getPharmaceuticalForm()
    {
        return $this->pharmaceuticalForm;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param $json
     * @return ForeignDrug
     */
    public static function fromJson($json)
    {
        $instance = new ForeignDrug();

        foreach (["id", "name", "companyName", "pharmaceuticalForm", "country"] as $k) {
            if ($json->$k) {
                $instance->$k = $json->$k;
            }
        }

        return $instance;
    }
}