<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class EqgrpProductsInfo implements Model
{
    /**
     * @var EquivalentGroup
     */
    protected $equivalentGroup;

    /**
     * @var ProductInfo[]
     */
    protected $productsInfo = [];

    /**
     * @return EquivalentGroup
     */
    public function getEquivalentGroup()
    {
        return $this->equivalentGroup;
    }

    /**
     * @return ProductInfo[]
     */
    public function getProductsInfo()
    {
        return $this->productsInfo;
    }

    /**
     * @param $json
     * @return EqgrpProductsInfo
     */
    public static function fromJson($json)
    {
        $instance = new EqgrpProductsInfo();
        $instance->equivalentGroup = EquivalentGroup::fromJson($json->equivalentGroup);
        foreach ($json->productsInfo as $p) {
            $instance->productsInfo[] = ProductInfo::fromJson($p);
        }
        return $instance;
    }
}