<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class KatarProduct implements Model
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $tradeName;
    /**
     * @var string
     */
    protected $serialNo;

    /**
     * @var string
     */
    protected $NDC10;

    /**
     * @var string
     */
    protected $NDC11;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTradeName()
    {
        return $this->tradeName;
    }

    /**
     * @return string
     */
    public function getSerialNo()
    {
        return $this->serialNo;
    }

    /**
     * @return string
     */
    public function getNDC10()
    {
        return $this->NDC10;
    }

    /**
     * @return string
     */
    public function getNDC11()
    {
        return $this->NDC11;
    }


    /**
     * @param $json
     * @return KatarProduct
     */
    public static function fromJson($json)
    {
        $p = new KatarProduct();
        $p->id = $json->id;
        $p->tradeName = $json->tradeName;
        $p->serialNo = $json->serialNo;
        $p->NDC10 = $json->NDC10;
        $p->NDC11 = $json->NDC11;
        return $p;
    }
}