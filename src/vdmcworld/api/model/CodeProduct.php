<?php


namespace vdmcworld\api\model;


class CodeProduct extends Product
{
    /**
     * @var Company
     */
    protected $company;

    /**
     * @var PriceInfo
     */
    protected $priceInfo;

    /**
     * @var ProductStatus[]
     */
    protected $productStatuses = [];

    /**
     * @var Status
     */
    protected $reimbursementStatus;

    /**
     * @var Status
     */
    protected $onMarketStatus;

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return PriceInfo
     */
    public function getPriceInfo()
    {
        return $this->priceInfo;
    }

    /**
     * @return ProductStatus[]
     */
    public function getProductStatuses()
    {
        return $this->productStatuses;
    }

    /**
     * @param $json
     * @return CodeProduct
     */
    public static function fromJson($json)
    {
        $p = new CodeProduct();
        if (isset($json->product)) {
            $p->id = $json->product->id;
            $p->name = $json->product->name;
            $p->barcode = $json->product->barcode;
            if (isset($json->product->titckName)) {
                $p->titckName = $json->product->titckName;
            }
        } else {
            $p->id = $json->id;
            $p->name = $json->name;
            $p->barcode = $json->barcode;
            if (isset($json->titckName)) {
                $p->titckName = $json->titckName;
            }
        }
        if (isset($json->company)) {
            $p->company = Company::fromJson($json->company);
        }
        if (isset($json->priceInfo)) {
            $p->priceInfo = ReimbursementPriceInfo::fromJson($json->priceInfo);
        }
        if (isset($json->productStatus)) {
            foreach ($json->productStatus as $ps) {
                $p->productStatuses[] = ProductStatus::fromJson($ps);
            }
        }
        if (isset($json->reimbursementStatus)) {
            $p->reimbursementStatus = Status::fromJson($json->reimbursementStatus);
        }
        if (isset($json->onMarketStatus)) {
            $p->onMarketStatus = Status::fromJson($json->onMarketStatus);
        }
        return $p;
    }
}