<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductIcdGroup implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var IcdGroup[]
     */
    protected $icdGroups;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return IcdGroup[]
     */
    public function getIcdGroups()
    {
        return $this->icdGroups;
    }

    /**
     * @param $json
     * @return ProductIcdGroup
     */
    public static function fromJson($json)
    {
        $instance = new ProductIcdGroup();
        $instance->product = Product::fromJson($json->product);

        $instance->icdGroups = [];
        if ($json->icdGroups) {
            foreach ($json->icdGroups as $i) {
                $instance->icdGroups[] = IcdGroup::fromJson($i);
            }
        }

        return $instance;
    }
}