<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class EquivalentProduct implements Model
{

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Product[]
     */
    protected $equivalentProducts = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Product[]
     */
    public function getEquivalentProducts()
    {
        return $this->equivalentProducts;
    }

    /**
     * @param $json
     * @return EquivalentProduct
     */
    public static function fromJson($json)
    {
        $instance = new EquivalentProduct();
        $instance->product = Product::fromJson($json->product);
        if ($json->equivalentProducts) {
            foreach ($json->equivalentProducts as $p) {
                $instance->equivalentProducts[] = Product::fromJson($p);
            }
        }
        return $instance;
    }
}