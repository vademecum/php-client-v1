<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PriceReferenceGroup implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $price_reference_code;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->price_reference_code;
    }

    /**
     * @param $json
     * @return PriceReferenceGroup
     */
    public static function fromJson($json)
    {
        $instance = new PriceReferenceGroup();
        $instance->id = $json->id;
        $instance->price_reference_code = $json->price_reference_code;
        return $instance;
    }
}