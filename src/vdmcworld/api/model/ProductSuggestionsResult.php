<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductSuggestionsResult implements Model
{
    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @var Product[]
     */
    protected $suggestions = [];

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return Product[]
     */
    public function getSuggestions()
    {
        return $this->suggestions;
    }

    /**
     * @param $json
     * @return Model
     */
    public static function fromJson($json)
    {
        $instance = new ProductSuggestionsResult();
        $instance->products = [];
        if ($json->products) {
            foreach ($json->products as $p) {
                $instance->products[] = Product::fromJson($p);
            }
        }
        $instance->suggestions = [];
        if ($json->suggestions) {
            foreach ($json->suggestions as $p) {
                $instance->suggestions[] = Product::fromJson($p);
            }
        }
        return $instance;
    }
}