<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class TemperatureDerangement implements Model
{
    /* @var Substance */
    protected $substance;

    /* @var string */
    protected $temperature;

    /* @var DerangementConsequence */
    protected $consequence;

    /* @var string */
    protected $references;

    /**
     * @return Substance
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * @return string
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * @return DerangementConsequence
     */
    public function getConsequence()
    {
        return $this->consequence;
    }

    /**
     * @return string
     */
    public function getReferences()
    {
        return $this->references;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->substance)) {
            $c->substance = Substance::fromJson($json->substance);
        }

        if (isset($json->temperature)) {
            $c->temperature = $json->temperature;
        }

        if (isset($json->consequence)) {
            $c->consequence = DerangementConsequence::fromJson($json->consequence);
        }

        if (isset($json->references)) {
            $c->references = $json->references;
        }

        return $c;
    }
}