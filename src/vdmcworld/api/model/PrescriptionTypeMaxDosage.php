<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PrescriptionTypeMaxDosage implements Model
{
    /**
     * @var PrescriptionType
     */
    protected $prescriptionType;

    /**
     * @var Amount
     */
    protected $maxPackageAmount;

    /**
     * @var Amount
     */
    protected $maxAmount;

    /**
     * @var string
     */
    protected $info;

    /**
     * @return PrescriptionType
     */
    public function getPrescriptionType()
    {
        return $this->prescriptionType;
    }

    /**
     * @return Amount
     */
    public function getMaxPackageAmount()
    {
        return $this->maxPackageAmount;
    }

    /**
     * @return Amount
     */
    public function getMaxAmount()
    {
        return $this->maxAmount;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param $json
     * @return PrescriptionTypeMaxDosage
     */
    public static function fromJson($json)
    {
        $instance = new PrescriptionTypeMaxDosage();
        if ($json->prescriptionType) {
            $instance->prescriptionType = PrescriptionType::fromJson($json->prescriptionType);
        }

        if ($json->maxPackageAmount) {
            $instance->maxPackageAmount = Amount::fromJson($json->maxPackageAmount);
        }

        if ($json->maxAmount) {
            $instance->maxAmount = Amount::fromJson($json->maxAmount);
        }

        $instance->info = $json->info;
        return $instance;
    }
}