<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

/**
 * Class CustomCard
 * @package vdmcworld\api\model
 */
class CustomCard implements Model
{

    /**
     * @param $json
     * @return CustomCard
     */
    public static function fromJson($json)
    {
        $instance = new CustomCard();

        foreach ($json as $k => $v) {
            $instance->$k = $v;
        }
        return $instance;
    }
}