<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class File implements Model
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param $json
     * @return File
     */
    public static function fromJson($json)
    {
        $instance = new File();
        $instance->url = $json->url;
        if (isset($json->date) && !empty($json->date)) {
            $instance->date = \DateTime::createFromFormat(
                "Y-m-d H:i:s",
                $json->date . " 00:00:00",
                new \DateTimeZone("UTC")
            );
        }

        return $instance;
    }
}