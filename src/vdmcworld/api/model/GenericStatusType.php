<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class GenericStatusType implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return GenericStatusType
     */
    public static function fromJson($json)
    {
        $g = new GenericStatusType();
        $g->id = $json->id;
        $g->name = $json->name;
        return $g;
    }
}