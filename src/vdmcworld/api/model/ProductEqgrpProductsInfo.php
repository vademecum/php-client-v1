<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductEqgrpProductsInfo implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var EqgrpProductsInfo[]
     */
    protected $equivalentProductsInfo = [];

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return EqgrpProductsInfo[]
     */
    public function getEquivalentProductsInfo()
    {
        return $this->equivalentProductsInfo;
    }

    /**
     * @param $json
     * @return ProductEqgrpProductsInfo
     */
    public static function fromJson($json)
    {
        $instance = new ProductEqgrpProductsInfo();
        $instance->product = Product::fromJson($json->product);
        if ($json->equivalentGroupProductsInfo) {
            foreach ($json->equivalentGroupProductsInfo as $p) {
                $instance->equivalentProductsInfo[] = EqgrpProductsInfo::fromJson($p);
            }
        }

        return $instance;
    }
}