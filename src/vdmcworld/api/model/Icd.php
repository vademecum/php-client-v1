<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Icd implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $branchOf;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getBranchOf()
    {
        return $this->branchOf;
    }

    /**
     * @return Ek4d
     */
    public function getEk4d()
    {
        return $this->ek4d;
    }

    /**
     * @param $json
     * @return Icd
     */
    public static function fromJson($json)
    {
        $icd = new Icd();
        $icd->id = $json->id;
        $icd->name = $json->name;
        $icd->description = $json->description;
        $icd->branchOf = $json->branchOf;
        $icd->ek4d = isset($json->ek4d) ? $json->ek4d : null;
        return $icd;
    }
}