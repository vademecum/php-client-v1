<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class Symptom implements Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return Symptom
     */
    public static function fromJson($json)
    {
        $s = new Symptom();
        $s->id = $json->id;
        $s->name = $json->name;
        return $s;
    }
}
