<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class MedicationTime implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $shortName;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param $json
     * @return MedicationTime
     */
    public static function fromJson($json)
    {
        $instance = new MedicationTime();
        foreach (["id", "name", "shortName"] as $key) {
            $instance->{$key} = $json->{$key};
        }
        return $instance;
    }
}