<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ApplicationMethod implements Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return ApplicationMethod
     */
    public static function fromJson($json)
    {
        $am = new ApplicationMethod();
        $am->id = $json->id;
        $am->name = $json->name;
        return $am;
    }
}