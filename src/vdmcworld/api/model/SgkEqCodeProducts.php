<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SgkEqCodeProducts implements Model
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param $json
     * @return SgkEqCodeProducts
     */
    public static function fromJson($json)
    {
        $instance = new SgkEqCodeProducts();
        $instance->code = $json->sgkEqCode;
        if ($json->products) {
            foreach ($json->products as $p) {
                $instance->products[] = CodeProduct::fromJson($p);
            }
        }
        return $instance;
    }
}