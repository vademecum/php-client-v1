<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class PrescriptionRule implements Model
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var string
     */
    protected $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $json
     * @return PrescriptionRule
     */
    public static function fromJson($json)
    {
        $pr = new PrescriptionRule();
        $pr->id = $json->id;
        $pr->name = $json->name;
        $pr->description = $json->description;
        return $pr;
    }
}