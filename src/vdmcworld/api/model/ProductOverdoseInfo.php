<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductOverdoseInfo implements Model
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var string
     */
    protected $overdoseInfo;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getOverdoseInfo()
    {
        return $this->overdoseInfo;
    }

    /**
     * @param $json
     * @return ProductOverdoseInfo
     */
    public static function fromJson($json)
    {
        $instance = new ProductOverdoseInfo();
        $instance->product = Product::fromJson($json->product);
        $instance->overdoseInfo = $json->overdose;
        return $instance;
    }
}