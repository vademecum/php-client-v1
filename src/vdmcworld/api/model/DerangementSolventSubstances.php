<?php

namespace vdmcworld\api\model;

use vdmcworld\api\Model;

class DerangementSolventSubstances implements Model
{
    /* @var DerangementSolvent */
    protected $solvent;

    /* @var Substance[] */
    protected $substances = [];

    /**
     * @return DerangementSolvent
     */
    public function getSolvent()
    {
        return $this->solvent;
    }

    /**
     * @return Substance[]
     */
    public function getSubstances()
    {
        return $this->substances;
    }

    public static function fromJson($json)
    {
        $c = new self();

        if (isset($json->solvent)) {
            $c->solvent = DerangementSolvent::fromJson($json->solvent);
        }

        if (isset($json->substances)) {
            foreach ($json->substances as $cd) {
                $c->substances [] = Substance::fromJson($cd);
            }
        }

        return $c;
    }
}
