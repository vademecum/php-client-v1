<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class ProductInteraction implements Model
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var Product
     */
    private $affectingProduct;

    /**
     * @var Color
     */
    private $color;

    /**
     * @var string
     */
    private $description;

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Product
     */
    public function getAffectingProduct()
    {
        return $this->affectingProduct;
    }

    /**
     * @return Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $json
     * @return Model
     */
    public static function fromJson($json)
    {
        $r = new ProductInteraction();
        $r->product = Product::fromJson($json->product);
        $r->affectingProduct = Product::fromJson($json->affectingProduct);
        $r->color = Color::fromJson($json->color);
        $r->description = $json->description;
        return $r;
    }
}