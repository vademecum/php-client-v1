<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SubstanceCardCard implements Model
{
    /**
     * @var string
     */
    protected $info;

    /**
     * @var string[]
     */
    protected $alternativeNames = [];

    /**
     * @var Atc[]
     */
    protected $atcIndices = [];

    /**
     * @var PharmacologicalClass[]
     */
    protected $pharmacologicalClass = [];

    /**
     * @var SpecialInformation
     */
    protected $specialInformation;

    /**
     * @var SubstanceProducts[]
     */
    protected $substanceProducts = [];

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @return \string[]
     */
    public function getAlternativeNames()
    {
        return $this->alternativeNames;
    }

    /**
     * @return Atc[]
     */
    public function getAtcIndices()
    {
        return $this->atcIndices;
    }

    /**
     * @return PharmacologicalClass[]
     */
    public function getPharmacologicalClass()
    {
        return $this->pharmacologicalClass;
    }

    /**
     * @return SpecialInformation
     */
    public function getSpecialInformation()
    {
        return $this->specialInformation;
    }

    /**
     * @return SubstanceProducts[]
     */
    public function getSubstanceProducts()
    {
        return $this->substanceProducts;
    }

    /**
     * @param $json
     * @return SubstanceCardCard
     */
    public static function fromJson($json)
    {
        $instance = new SubstanceCardCard();
        $instance->info = $json->info;
        $instance->alternativeNames = $json->alternativeNames;
        if ($json->atcIndices) {
            foreach ($json->atcIndices as $a) {
                $instance->atcIndices[] = Atc::fromJson($a);
            }
        }
        if ($json->pharmacologicalClass) {
            foreach ($json->pharmacologicalClass as $p) {
                $instance->pharmacologicalClass[] = PharmacologicalClass::fromJson($p);
            }
        }
        if (isset($json->substanceProducts)) {
            foreach ($json->substanceProducts as $sp){
                $instance->substanceProducts[] = SubstanceProducts::fromJson($sp);
            }
        }
        $instance->specialInformation = SpecialInformation::fromJson($json->specialInformation);
        return $instance;
    }
}