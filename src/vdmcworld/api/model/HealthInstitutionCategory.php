<?php

namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class HealthInstitutionCategory implements Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $note;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param $json
     * @return HealthInstitutionCategory
     */
    public static function fromJson($json)
    {
        $hic = new HealthInstitutionCategory();
        $hic->id = $json->id;
        $hic->name = $json->name;
        if (!empty($json->note)) {
            $hic->note = $json->note;
        }
        return $hic;
    }
}