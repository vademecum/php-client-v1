<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class Document implements Model
{
    /**
     * @var File
     */
    protected $kt;

    /**
     * @var File
     */
    protected $kub;

    /**
     * @var File
     */
    protected $prospectus;

    /**
     * @var File
     */
    protected $doctorLetter;

    /**
     * @var File[]
     */
    protected $doctorLetters;

    /**
     * @return File
     */
    public function getKt()
    {
        return $this->kt;
    }

    /**
     * @return File
     */
    public function getKub()
    {
        return $this->kub;
    }

    /**
     * @return File
     */
    public function getProspectus()
    {
        return $this->prospectus;
    }

    /**
     * @return File
     */
    public function getDoctorLetter()
    {
        return $this->doctorLetter;
    }

    /**
     * @return File[]
     */
    public function getDoctorLetters()
    {
        return $this->doctorLetters;
    }

    /**
     * @param $json
     * @return Document
     */
    public static function fromJson($json)
    {
        $instance = new Document();
        if ($json->KT) {
            $instance->kt = File::fromJson($json->KT);
        }
        if ($json->KUB) {
            $instance->kub = File::fromJson($json->KUB);
        }
        if ($json->prospectus) {
            $instance->prospectus = File::fromJson($json->prospectus);
        }
        if ($json->doctorLetter) {
            $instance->doctorLetter = File::fromJson($json->doctorLetter);
        }
        if ($json->doctorLetters) {
            foreach ($json->doctorLetters as $item) {
                $instance->doctorLetters[] = File::fromJson($item);
            }
        }

        return $instance;
    }
}