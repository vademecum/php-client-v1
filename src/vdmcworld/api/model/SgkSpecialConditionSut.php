<?php


namespace vdmcworld\api\model;


use vdmcworld\api\Model;

class SgkSpecialConditionSut implements Model
{
    /**
     * @var ReimbursementCondition
     */
    private $specialCondition;

    /**
     * @var SgkSutDetail[]
     */
    private $sut = [];

    /**
     * @return SgkSutDetail[]
     */
    public function getSut()
    {
        return $this->sut;
    }

    /**
     * @return ReimbursementCondition
     */
    public function getSpecialCondition()
    {
        return $this->specialCondition;
    }

    /**
     * @param \StdClass $json
     * @return SgkSpecialConditionSut
     */
    public static function fromJson($json)
    {
        $instance = new SgkSpecialConditionSut();

        $instance->specialCondition = ReimbursementCondition::fromJson($json->specialCondition);

        foreach ($json->sut as $sut) {
            $instance->sut [] = SgkSutDetail::fromJson($sut);
        }

        return $instance;
    }
}
