# Api Doc
https://vapi.vademecumonline.com.tr/doc/

https://vapi.vademecumonline.com.tr/doc/tr

# Using the Library


When the  dependency is set. using composer update or install will download the required libraries to your system. You can start using it by using something similar:


```
#!php
<?php
require_once "vendor/autoload.php";

$api = new \vdmcworld\Api("username", "password");

$api->setSandbox();

$api->auth();

var_dump($api->getToken());
```

To make basic requests and get data you can use the following:

```
#!php
<?php
// Create the $api object as above...
$request = new \vdmcworld\api\request\lists\ProductListRequest();
$response = $request->fetchData($api);
foreach ($response as $p) {
    var_dump($p->getName());
}
```

For an advanced usage to get the raw result with Http Response you can use the following code:

```
#!php
<?php
// Create the $api object as above...
$request = new \vdmcworld\api\request\lists\ProductListRequest();
$response = $api->send($request);
var_dump($response->getResponse());
```


